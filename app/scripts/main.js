$(document)
  .ready(function () {

    // Selector tipo de dato y visualización
    var liToSelect = 1;
    $('ul.dropdown-menu li[role=\'tipologia\']:eq(' + (liToSelect - 1) + ')').addClass('active');
    $('ul.dropdown-menu li[role=\'visualizacion\']:eq(' + (liToSelect - 1) + ')').addClass('active');

    $('ul.dropdown-menu li[role=\'tipologia\']').on('click', function () {
      $('ul.dropdown-menu li[role=\'tipologia\']').removeClass('active');
      $(this).addClass('active');
    });

    $('ul.dropdown-menu li[role=\'visualizacion\']').on('click', function () {
      $('ul.dropdown-menu li[role=\'visualizacion\']').removeClass('active');
      $(this).addClass('active');
    });

    // Activamos vistas

    $('ul.dropdown-menu li a').click(function (e) {
      e.preventDefault();
      var tabIndex = $('ul.dropdown-menu li a').index(this);
      // $(this).parent().siblings().removeClass("active");
      // $(this).parent().addClass("active");
      $('.tab-pane:eq( ' + tabIndex + ' )')
        .siblings()
        .removeClass('in active');
      $('.tab-pane:eq( ' + tabIndex + ' )').addClass('in active');
    });

    // Ejemplo Chars.js

    // Ajuste tamaño truco
    $(window).on('resize', function () {
      $('.chart-item').css('width', $('.tirio-dashboard-panel').width() - 30);
      $('.chart-item').css('height', $('.tirio-dashboard-panel').height() - (30 + 60));
    });
    window.dispatchEvent(new Event('resize'));


    $.getJSON('https://www.highcharts.com/samples/data/jsonp.php?filename=aapl-c.json&callback=' +
        '?',
    function (data) {

      // Create the chart
      var chart = Highcharts.stockChart('chart-container1', {

        chart: {
          // height: 400
        },

        title: {
          text: ''
        },

        subtitle: {
          text: ''
        },

        rangeSelector: {
          selected: 1
        },

        series: [
          {
            name: 'AAPL Stock Price',
            data: data,
            type: 'area',
            threshold: null,
            tooltip: {
              valueDecimals: 2
            }
          }
        ],

        // responsive: {     rules: [{         condition: {             maxWidth: 500
        //   },         chartOptions: {             chart: {                 height: 300
        //             },             subtitle: {                 text: null     },
        //        navigator: {                 enabled: false             }        }
        // }] }
      });

    });

    Highcharts.theme = {
      colors: [
        '#2b908f',
        '#90ee7e',
        '#f45b5b',
        '#7798BF',
        '#aaeeee',
        '#ff0066',
        '#eeaaee',
        '#55BF3B',
        '#DF5353',
        '#7798BF',
        '#aaeeee'
      ],
      chart: {
        backgroundColor: 'rgba(255, 255, 255, 0.0)',
        style: {
          fontFamily: '\'Myriad Pro Regular\', sans-serif'
        },
        plotBorderColor: '#606063'
      },
      title: {
        style: {
          color: '#E0E0E3',
          textTransform: 'uppercase',
          fontSize: '20px'
        }
      },
      subtitle: {
        style: {
          color: '#E0E0E3',
          textTransform: 'uppercase'
        }
      },
      xAxis: {
        gridLineColor: '#707073',
        labels: {
          style: {
            color: '#E0E0E3'
          }
        },
        lineColor: '#707073',
        minorGridLineColor: '#505053',
        tickColor: '#707073',
        title: {
          style: {
            color: '#A0A0A3'

          }
        }
      },
      yAxis: {
        gridLineColor: '#707073',
        labels: {
          style: {
            color: '#E0E0E3'
          }
        },
        lineColor: '#707073',
        minorGridLineColor: '#505053',
        tickColor: '#707073',
        tickWidth: 1,
        title: {
          style: {
            color: '#A0A0A3'
          }
        }
      },
      tooltip: {
        backgroundColor: 'rgba(0, 0, 0, 0.85)',
        style: {
          color: '#F0F0F0'
        }
      },
      plotOptions: {
        series: {
          dataLabels: {
            color: '#B0B0B3'
          },
          marker: {
            lineColor: '#333'
          }
        },
        boxplot: {
          fillColor: '#505053'
        },
        candlestick: {
          lineColor: 'white'
        },
        errorbar: {
          color: 'white'
        }
      },
      legend: {
        itemStyle: {
          color: '#E0E0E3'
        },
        itemHoverStyle: {
          color: '#FFF'
        },
        itemHiddenStyle: {
          color: '#606063'
        }
      },
      credits: {
        style: {
          color: '#666'
        }
      },
      labels: {
        style: {
          color: '#707073'
        }
      },

      drilldown: {
        activeAxisLabelStyle: {
          color: '#F0F0F3'
        },
        activeDataLabelStyle: {
          color: '#F0F0F3'
        }
      },

      navigation: {
        buttonOptions: {
          symbolStroke: '#DDDDDD',
          theme: {
            fill: '#505053'
          }
        }
      },

      // scroll charts
      rangeSelector: {
        buttonTheme: {
          fill: '#505053',
          stroke: '#000000',
          style: {
            color: '#CCC'
          },
          states: {
            hover: {
              fill: '#707073',
              stroke: '#000000',
              style: {
                color: 'white'
              }
            },
            select: {
              fill: '#000003',
              stroke: '#000000',
              style: {
                color: 'white'
              }
            }
          }
        },
        inputBoxBorderColor: '#505053',
        inputStyle: {
          backgroundColor: '#333',
          color: 'silver'
        },
        labelStyle: {
          color: 'silver'
        }
      },

      navigator: {
        enabled: false
      },

      scrollbar: {
        barBackgroundColor: '#808083',
        barBorderColor: '#808083',
        buttonArrowColor: '#CCC',
        buttonBackgroundColor: '#606063',
        buttonBorderColor: '#606063',
        rifleColor: '#FFF',
        trackBackgroundColor: '#404043',
        trackBorderColor: '#404043'
      },

      // special colors for some of the
      legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
      background2: '#505053',
      dataLabelsColor: '#B0B0B3',
      textColor: '#C0C0C0',
      contrastTextColor: '#F0F0F3',
      maskColor: 'rgba(255,255,255,0.3)'
    };
    // Apply the theme
    Highcharts.setOptions(Highcharts.theme);

  });
  
// Plugin select con auto width

(function ($, window) {
  var arrowWidth = 30;

  $.fn.resizeselect = function (settings) {
    return this.each(function () {

      $(this)
        .change(function () {
          var $this = $(this);

          // create test element
          var text = $this
            .find('option:selected')
            .text();

          var $test = $('<span>')
            .html(text)
            .css({
              'font-size': $this.css('font-size'), // ensures same size text
              'visibility': 'hidden' // prevents FOUC
            });

          // add to body, get width, and get out
          $test.appendTo($this.parent());
          var width = $test.width();
          $test.remove();

          // set select width
          $this.width((width + arrowWidth) - 14);

          // run on start
        })
        .change();

    });
  };

  // run by default
  $('select.resizeselect').resizeselect();

})(jQuery, window);
