/*!
 * jQuery JavaScript Library v3.2.1
 * https://jquery.com/
 *
 * Includes Sizzle.js
 * https://sizzlejs.com/
 *
 * Copyright JS Foundation and other contributors
 * Released under the MIT license
 * https://jquery.org/license
 *
 * Date: 2017-03-20T18:59Z
 */
( function( global, factory ) {

	"use strict";

	if ( typeof module === "object" && typeof module.exports === "object" ) {

		// For CommonJS and CommonJS-like environments where a proper `window`
		// is present, execute the factory and get jQuery.
		// For environments that do not have a `window` with a `document`
		// (such as Node.js), expose a factory as module.exports.
		// This accentuates the need for the creation of a real `window`.
		// e.g. var jQuery = require("jquery")(window);
		// See ticket #14549 for more info.
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "jQuery requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		factory( global );
	}

// Pass this if window is not defined yet
} )( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {

// Edge <= 12 - 13+, Firefox <=18 - 45+, IE 10 - 11, Safari 5.1 - 9+, iOS 6 - 9.1
// throw exceptions when non-strict code (e.g., ASP.NET 4.5) accesses strict mode
// arguments.callee.caller (trac-13335). But as of jQuery 3.0 (2016), strict mode should be common
// enough that all such attempts are guarded in a try block.
"use strict";

var arr = [];

var document = window.document;

var getProto = Object.getPrototypeOf;

var slice = arr.slice;

var concat = arr.concat;

var push = arr.push;

var indexOf = arr.indexOf;

var class2type = {};

var toString = class2type.toString;

var hasOwn = class2type.hasOwnProperty;

var fnToString = hasOwn.toString;

var ObjectFunctionString = fnToString.call( Object );

var support = {};



	function DOMEval( code, doc ) {
		doc = doc || document;

		var script = doc.createElement( "script" );

		script.text = code;
		doc.head.appendChild( script ).parentNode.removeChild( script );
	}
/* global Symbol */
// Defining this global in .eslintrc.json would create a danger of using the global
// unguarded in another place, it seems safer to define global only for this module



var
	version = "3.2.1",

	// Define a local copy of jQuery
	jQuery = function( selector, context ) {

		// The jQuery object is actually just the init constructor 'enhanced'
		// Need init if jQuery is called (just allow error to be thrown if not included)
		return new jQuery.fn.init( selector, context );
	},

	// Support: Android <=4.0 only
	// Make sure we trim BOM and NBSP
	rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,

	// Matches dashed string for camelizing
	rmsPrefix = /^-ms-/,
	rdashAlpha = /-([a-z])/g,

	// Used by jQuery.camelCase as callback to replace()
	fcamelCase = function( all, letter ) {
		return letter.toUpperCase();
	};

jQuery.fn = jQuery.prototype = {

	// The current version of jQuery being used
	jquery: version,

	constructor: jQuery,

	// The default length of a jQuery object is 0
	length: 0,

	toArray: function() {
		return slice.call( this );
	},

	// Get the Nth element in the matched element set OR
	// Get the whole matched element set as a clean array
	get: function( num ) {

		// Return all the elements in a clean array
		if ( num == null ) {
			return slice.call( this );
		}

		// Return just the one element from the set
		return num < 0 ? this[ num + this.length ] : this[ num ];
	},

	// Take an array of elements and push it onto the stack
	// (returning the new matched element set)
	pushStack: function( elems ) {

		// Build a new jQuery matched element set
		var ret = jQuery.merge( this.constructor(), elems );

		// Add the old object onto the stack (as a reference)
		ret.prevObject = this;

		// Return the newly-formed element set
		return ret;
	},

	// Execute a callback for every element in the matched set.
	each: function( callback ) {
		return jQuery.each( this, callback );
	},

	map: function( callback ) {
		return this.pushStack( jQuery.map( this, function( elem, i ) {
			return callback.call( elem, i, elem );
		} ) );
	},

	slice: function() {
		return this.pushStack( slice.apply( this, arguments ) );
	},

	first: function() {
		return this.eq( 0 );
	},

	last: function() {
		return this.eq( -1 );
	},

	eq: function( i ) {
		var len = this.length,
			j = +i + ( i < 0 ? len : 0 );
		return this.pushStack( j >= 0 && j < len ? [ this[ j ] ] : [] );
	},

	end: function() {
		return this.prevObject || this.constructor();
	},

	// For internal use only.
	// Behaves like an Array's method, not like a jQuery method.
	push: push,
	sort: arr.sort,
	splice: arr.splice
};

jQuery.extend = jQuery.fn.extend = function() {
	var options, name, src, copy, copyIsArray, clone,
		target = arguments[ 0 ] || {},
		i = 1,
		length = arguments.length,
		deep = false;

	// Handle a deep copy situation
	if ( typeof target === "boolean" ) {
		deep = target;

		// Skip the boolean and the target
		target = arguments[ i ] || {};
		i++;
	}

	// Handle case when target is a string or something (possible in deep copy)
	if ( typeof target !== "object" && !jQuery.isFunction( target ) ) {
		target = {};
	}

	// Extend jQuery itself if only one argument is passed
	if ( i === length ) {
		target = this;
		i--;
	}

	for ( ; i < length; i++ ) {

		// Only deal with non-null/undefined values
		if ( ( options = arguments[ i ] ) != null ) {

			// Extend the base object
			for ( name in options ) {
				src = target[ name ];
				copy = options[ name ];

				// Prevent never-ending loop
				if ( target === copy ) {
					continue;
				}

				// Recurse if we're merging plain objects or arrays
				if ( deep && copy && ( jQuery.isPlainObject( copy ) ||
					( copyIsArray = Array.isArray( copy ) ) ) ) {

					if ( copyIsArray ) {
						copyIsArray = false;
						clone = src && Array.isArray( src ) ? src : [];

					} else {
						clone = src && jQuery.isPlainObject( src ) ? src : {};
					}

					// Never move original objects, clone them
					target[ name ] = jQuery.extend( deep, clone, copy );

				// Don't bring in undefined values
				} else if ( copy !== undefined ) {
					target[ name ] = copy;
				}
			}
		}
	}

	// Return the modified object
	return target;
};

jQuery.extend( {

	// Unique for each copy of jQuery on the page
	expando: "jQuery" + ( version + Math.random() ).replace( /\D/g, "" ),

	// Assume jQuery is ready without the ready module
	isReady: true,

	error: function( msg ) {
		throw new Error( msg );
	},

	noop: function() {},

	isFunction: function( obj ) {
		return jQuery.type( obj ) === "function";
	},

	isWindow: function( obj ) {
		return obj != null && obj === obj.window;
	},

	isNumeric: function( obj ) {

		// As of jQuery 3.0, isNumeric is limited to
		// strings and numbers (primitives or objects)
		// that can be coerced to finite numbers (gh-2662)
		var type = jQuery.type( obj );
		return ( type === "number" || type === "string" ) &&

			// parseFloat NaNs numeric-cast false positives ("")
			// ...but misinterprets leading-number strings, particularly hex literals ("0x...")
			// subtraction forces infinities to NaN
			!isNaN( obj - parseFloat( obj ) );
	},

	isPlainObject: function( obj ) {
		var proto, Ctor;

		// Detect obvious negatives
		// Use toString instead of jQuery.type to catch host objects
		if ( !obj || toString.call( obj ) !== "[object Object]" ) {
			return false;
		}

		proto = getProto( obj );

		// Objects with no prototype (e.g., `Object.create( null )`) are plain
		if ( !proto ) {
			return true;
		}

		// Objects with prototype are plain iff they were constructed by a global Object function
		Ctor = hasOwn.call( proto, "constructor" ) && proto.constructor;
		return typeof Ctor === "function" && fnToString.call( Ctor ) === ObjectFunctionString;
	},

	isEmptyObject: function( obj ) {

		/* eslint-disable no-unused-vars */
		// See https://github.com/eslint/eslint/issues/6125
		var name;

		for ( name in obj ) {
			return false;
		}
		return true;
	},

	type: function( obj ) {
		if ( obj == null ) {
			return obj + "";
		}

		// Support: Android <=2.3 only (functionish RegExp)
		return typeof obj === "object" || typeof obj === "function" ?
			class2type[ toString.call( obj ) ] || "object" :
			typeof obj;
	},

	// Evaluates a script in a global context
	globalEval: function( code ) {
		DOMEval( code );
	},

	// Convert dashed to camelCase; used by the css and data modules
	// Support: IE <=9 - 11, Edge 12 - 13
	// Microsoft forgot to hump their vendor prefix (#9572)
	camelCase: function( string ) {
		return string.replace( rmsPrefix, "ms-" ).replace( rdashAlpha, fcamelCase );
	},

	each: function( obj, callback ) {
		var length, i = 0;

		if ( isArrayLike( obj ) ) {
			length = obj.length;
			for ( ; i < length; i++ ) {
				if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
					break;
				}
			}
		} else {
			for ( i in obj ) {
				if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
					break;
				}
			}
		}

		return obj;
	},

	// Support: Android <=4.0 only
	trim: function( text ) {
		return text == null ?
			"" :
			( text + "" ).replace( rtrim, "" );
	},

	// results is for internal usage only
	makeArray: function( arr, results ) {
		var ret = results || [];

		if ( arr != null ) {
			if ( isArrayLike( Object( arr ) ) ) {
				jQuery.merge( ret,
					typeof arr === "string" ?
					[ arr ] : arr
				);
			} else {
				push.call( ret, arr );
			}
		}

		return ret;
	},

	inArray: function( elem, arr, i ) {
		return arr == null ? -1 : indexOf.call( arr, elem, i );
	},

	// Support: Android <=4.0 only, PhantomJS 1 only
	// push.apply(_, arraylike) throws on ancient WebKit
	merge: function( first, second ) {
		var len = +second.length,
			j = 0,
			i = first.length;

		for ( ; j < len; j++ ) {
			first[ i++ ] = second[ j ];
		}

		first.length = i;

		return first;
	},

	grep: function( elems, callback, invert ) {
		var callbackInverse,
			matches = [],
			i = 0,
			length = elems.length,
			callbackExpect = !invert;

		// Go through the array, only saving the items
		// that pass the validator function
		for ( ; i < length; i++ ) {
			callbackInverse = !callback( elems[ i ], i );
			if ( callbackInverse !== callbackExpect ) {
				matches.push( elems[ i ] );
			}
		}

		return matches;
	},

	// arg is for internal usage only
	map: function( elems, callback, arg ) {
		var length, value,
			i = 0,
			ret = [];

		// Go through the array, translating each of the items to their new values
		if ( isArrayLike( elems ) ) {
			length = elems.length;
			for ( ; i < length; i++ ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}

		// Go through every key on the object,
		} else {
			for ( i in elems ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}
		}

		// Flatten any nested arrays
		return concat.apply( [], ret );
	},

	// A global GUID counter for objects
	guid: 1,

	// Bind a function to a context, optionally partially applying any
	// arguments.
	proxy: function( fn, context ) {
		var tmp, args, proxy;

		if ( typeof context === "string" ) {
			tmp = fn[ context ];
			context = fn;
			fn = tmp;
		}

		// Quick check to determine if target is callable, in the spec
		// this throws a TypeError, but we will just return undefined.
		if ( !jQuery.isFunction( fn ) ) {
			return undefined;
		}

		// Simulated bind
		args = slice.call( arguments, 2 );
		proxy = function() {
			return fn.apply( context || this, args.concat( slice.call( arguments ) ) );
		};

		// Set the guid of unique handler to the same of original handler, so it can be removed
		proxy.guid = fn.guid = fn.guid || jQuery.guid++;

		return proxy;
	},

	now: Date.now,

	// jQuery.support is not used in Core but other projects attach their
	// properties to it so it needs to exist.
	support: support
} );

if ( typeof Symbol === "function" ) {
	jQuery.fn[ Symbol.iterator ] = arr[ Symbol.iterator ];
}

// Populate the class2type map
jQuery.each( "Boolean Number String Function Array Date RegExp Object Error Symbol".split( " " ),
function( i, name ) {
	class2type[ "[object " + name + "]" ] = name.toLowerCase();
} );

function isArrayLike( obj ) {

	// Support: real iOS 8.2 only (not reproducible in simulator)
	// `in` check used to prevent JIT error (gh-2145)
	// hasOwn isn't used here due to false negatives
	// regarding Nodelist length in IE
	var length = !!obj && "length" in obj && obj.length,
		type = jQuery.type( obj );

	if ( type === "function" || jQuery.isWindow( obj ) ) {
		return false;
	}

	return type === "array" || length === 0 ||
		typeof length === "number" && length > 0 && ( length - 1 ) in obj;
}
var Sizzle =
/*!
 * Sizzle CSS Selector Engine v2.3.3
 * https://sizzlejs.com/
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2016-08-08
 */
(function( window ) {

var i,
	support,
	Expr,
	getText,
	isXML,
	tokenize,
	compile,
	select,
	outermostContext,
	sortInput,
	hasDuplicate,

	// Local document vars
	setDocument,
	document,
	docElem,
	documentIsHTML,
	rbuggyQSA,
	rbuggyMatches,
	matches,
	contains,

	// Instance-specific data
	expando = "sizzle" + 1 * new Date(),
	preferredDoc = window.document,
	dirruns = 0,
	done = 0,
	classCache = createCache(),
	tokenCache = createCache(),
	compilerCache = createCache(),
	sortOrder = function( a, b ) {
		if ( a === b ) {
			hasDuplicate = true;
		}
		return 0;
	},

	// Instance methods
	hasOwn = ({}).hasOwnProperty,
	arr = [],
	pop = arr.pop,
	push_native = arr.push,
	push = arr.push,
	slice = arr.slice,
	// Use a stripped-down indexOf as it's faster than native
	// https://jsperf.com/thor-indexof-vs-for/5
	indexOf = function( list, elem ) {
		var i = 0,
			len = list.length;
		for ( ; i < len; i++ ) {
			if ( list[i] === elem ) {
				return i;
			}
		}
		return -1;
	},

	booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",

	// Regular expressions

	// http://www.w3.org/TR/css3-selectors/#whitespace
	whitespace = "[\\x20\\t\\r\\n\\f]",

	// http://www.w3.org/TR/CSS21/syndata.html#value-def-identifier
	identifier = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",

	// Attribute selectors: http://www.w3.org/TR/selectors/#attribute-selectors
	attributes = "\\[" + whitespace + "*(" + identifier + ")(?:" + whitespace +
		// Operator (capture 2)
		"*([*^$|!~]?=)" + whitespace +
		// "Attribute values must be CSS identifiers [capture 5] or strings [capture 3 or capture 4]"
		"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + identifier + "))|)" + whitespace +
		"*\\]",

	pseudos = ":(" + identifier + ")(?:\\((" +
		// To reduce the number of selectors needing tokenize in the preFilter, prefer arguments:
		// 1. quoted (capture 3; capture 4 or capture 5)
		"('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" +
		// 2. simple (capture 6)
		"((?:\\\\.|[^\\\\()[\\]]|" + attributes + ")*)|" +
		// 3. anything else (capture 2)
		".*" +
		")\\)|)",

	// Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter
	rwhitespace = new RegExp( whitespace + "+", "g" ),
	rtrim = new RegExp( "^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g" ),

	rcomma = new RegExp( "^" + whitespace + "*," + whitespace + "*" ),
	rcombinators = new RegExp( "^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace + "*" ),

	rattributeQuotes = new RegExp( "=" + whitespace + "*([^\\]'\"]*?)" + whitespace + "*\\]", "g" ),

	rpseudo = new RegExp( pseudos ),
	ridentifier = new RegExp( "^" + identifier + "$" ),

	matchExpr = {
		"ID": new RegExp( "^#(" + identifier + ")" ),
		"CLASS": new RegExp( "^\\.(" + identifier + ")" ),
		"TAG": new RegExp( "^(" + identifier + "|[*])" ),
		"ATTR": new RegExp( "^" + attributes ),
		"PSEUDO": new RegExp( "^" + pseudos ),
		"CHILD": new RegExp( "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace +
			"*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace +
			"*(\\d+)|))" + whitespace + "*\\)|)", "i" ),
		"bool": new RegExp( "^(?:" + booleans + ")$", "i" ),
		// For use in libraries implementing .is()
		// We use this for POS matching in `select`
		"needsContext": new RegExp( "^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
			whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i" )
	},

	rinputs = /^(?:input|select|textarea|button)$/i,
	rheader = /^h\d$/i,

	rnative = /^[^{]+\{\s*\[native \w/,

	// Easily-parseable/retrievable ID or TAG or CLASS selectors
	rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,

	rsibling = /[+~]/,

	// CSS escapes
	// http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
	runescape = new RegExp( "\\\\([\\da-f]{1,6}" + whitespace + "?|(" + whitespace + ")|.)", "ig" ),
	funescape = function( _, escaped, escapedWhitespace ) {
		var high = "0x" + escaped - 0x10000;
		// NaN means non-codepoint
		// Support: Firefox<24
		// Workaround erroneous numeric interpretation of +"0x"
		return high !== high || escapedWhitespace ?
			escaped :
			high < 0 ?
				// BMP codepoint
				String.fromCharCode( high + 0x10000 ) :
				// Supplemental Plane codepoint (surrogate pair)
				String.fromCharCode( high >> 10 | 0xD800, high & 0x3FF | 0xDC00 );
	},

	// CSS string/identifier serialization
	// https://drafts.csswg.org/cssom/#common-serializing-idioms
	rcssescape = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
	fcssescape = function( ch, asCodePoint ) {
		if ( asCodePoint ) {

			// U+0000 NULL becomes U+FFFD REPLACEMENT CHARACTER
			if ( ch === "\0" ) {
				return "\uFFFD";
			}

			// Control characters and (dependent upon position) numbers get escaped as code points
			return ch.slice( 0, -1 ) + "\\" + ch.charCodeAt( ch.length - 1 ).toString( 16 ) + " ";
		}

		// Other potentially-special ASCII characters get backslash-escaped
		return "\\" + ch;
	},

	// Used for iframes
	// See setDocument()
	// Removing the function wrapper causes a "Permission Denied"
	// error in IE
	unloadHandler = function() {
		setDocument();
	},

	disabledAncestor = addCombinator(
		function( elem ) {
			return elem.disabled === true && ("form" in elem || "label" in elem);
		},
		{ dir: "parentNode", next: "legend" }
	);

// Optimize for push.apply( _, NodeList )
try {
	push.apply(
		(arr = slice.call( preferredDoc.childNodes )),
		preferredDoc.childNodes
	);
	// Support: Android<4.0
	// Detect silently failing push.apply
	arr[ preferredDoc.childNodes.length ].nodeType;
} catch ( e ) {
	push = { apply: arr.length ?

		// Leverage slice if possible
		function( target, els ) {
			push_native.apply( target, slice.call(els) );
		} :

		// Support: IE<9
		// Otherwise append directly
		function( target, els ) {
			var j = target.length,
				i = 0;
			// Can't trust NodeList.length
			while ( (target[j++] = els[i++]) ) {}
			target.length = j - 1;
		}
	};
}

function Sizzle( selector, context, results, seed ) {
	var m, i, elem, nid, match, groups, newSelector,
		newContext = context && context.ownerDocument,

		// nodeType defaults to 9, since context defaults to document
		nodeType = context ? context.nodeType : 9;

	results = results || [];

	// Return early from calls with invalid selector or context
	if ( typeof selector !== "string" || !selector ||
		nodeType !== 1 && nodeType !== 9 && nodeType !== 11 ) {

		return results;
	}

	// Try to shortcut find operations (as opposed to filters) in HTML documents
	if ( !seed ) {

		if ( ( context ? context.ownerDocument || context : preferredDoc ) !== document ) {
			setDocument( context );
		}
		context = context || document;

		if ( documentIsHTML ) {

			// If the selector is sufficiently simple, try using a "get*By*" DOM method
			// (excepting DocumentFragment context, where the methods don't exist)
			if ( nodeType !== 11 && (match = rquickExpr.exec( selector )) ) {

				// ID selector
				if ( (m = match[1]) ) {

					// Document context
					if ( nodeType === 9 ) {
						if ( (elem = context.getElementById( m )) ) {

							// Support: IE, Opera, Webkit
							// TODO: identify versions
							// getElementById can match elements by name instead of ID
							if ( elem.id === m ) {
								results.push( elem );
								return results;
							}
						} else {
							return results;
						}

					// Element context
					} else {

						// Support: IE, Opera, Webkit
						// TODO: identify versions
						// getElementById can match elements by name instead of ID
						if ( newContext && (elem = newContext.getElementById( m )) &&
							contains( context, elem ) &&
							elem.id === m ) {

							results.push( elem );
							return results;
						}
					}

				// Type selector
				} else if ( match[2] ) {
					push.apply( results, context.getElementsByTagName( selector ) );
					return results;

				// Class selector
				} else if ( (m = match[3]) && support.getElementsByClassName &&
					context.getElementsByClassName ) {

					push.apply( results, context.getElementsByClassName( m ) );
					return results;
				}
			}

			// Take advantage of querySelectorAll
			if ( support.qsa &&
				!compilerCache[ selector + " " ] &&
				(!rbuggyQSA || !rbuggyQSA.test( selector )) ) {

				if ( nodeType !== 1 ) {
					newContext = context;
					newSelector = selector;

				// qSA looks outside Element context, which is not what we want
				// Thanks to Andrew Dupont for this workaround technique
				// Support: IE <=8
				// Exclude object elements
				} else if ( context.nodeName.toLowerCase() !== "object" ) {

					// Capture the context ID, setting it first if necessary
					if ( (nid = context.getAttribute( "id" )) ) {
						nid = nid.replace( rcssescape, fcssescape );
					} else {
						context.setAttribute( "id", (nid = expando) );
					}

					// Prefix every selector in the list
					groups = tokenize( selector );
					i = groups.length;
					while ( i-- ) {
						groups[i] = "#" + nid + " " + toSelector( groups[i] );
					}
					newSelector = groups.join( "," );

					// Expand context for sibling selectors
					newContext = rsibling.test( selector ) && testContext( context.parentNode ) ||
						context;
				}

				if ( newSelector ) {
					try {
						push.apply( results,
							newContext.querySelectorAll( newSelector )
						);
						return results;
					} catch ( qsaError ) {
					} finally {
						if ( nid === expando ) {
							context.removeAttribute( "id" );
						}
					}
				}
			}
		}
	}

	// All others
	return select( selector.replace( rtrim, "$1" ), context, results, seed );
}

/**
 * Create key-value caches of limited size
 * @returns {function(string, object)} Returns the Object data after storing it on itself with
 *	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
 *	deleting the oldest entry
 */
function createCache() {
	var keys = [];

	function cache( key, value ) {
		// Use (key + " ") to avoid collision with native prototype properties (see Issue #157)
		if ( keys.push( key + " " ) > Expr.cacheLength ) {
			// Only keep the most recent entries
			delete cache[ keys.shift() ];
		}
		return (cache[ key + " " ] = value);
	}
	return cache;
}

/**
 * Mark a function for special use by Sizzle
 * @param {Function} fn The function to mark
 */
function markFunction( fn ) {
	fn[ expando ] = true;
	return fn;
}

/**
 * Support testing using an element
 * @param {Function} fn Passed the created element and returns a boolean result
 */
function assert( fn ) {
	var el = document.createElement("fieldset");

	try {
		return !!fn( el );
	} catch (e) {
		return false;
	} finally {
		// Remove from its parent by default
		if ( el.parentNode ) {
			el.parentNode.removeChild( el );
		}
		// release memory in IE
		el = null;
	}
}

/**
 * Adds the same handler for all of the specified attrs
 * @param {String} attrs Pipe-separated list of attributes
 * @param {Function} handler The method that will be applied
 */
function addHandle( attrs, handler ) {
	var arr = attrs.split("|"),
		i = arr.length;

	while ( i-- ) {
		Expr.attrHandle[ arr[i] ] = handler;
	}
}

/**
 * Checks document order of two siblings
 * @param {Element} a
 * @param {Element} b
 * @returns {Number} Returns less than 0 if a precedes b, greater than 0 if a follows b
 */
function siblingCheck( a, b ) {
	var cur = b && a,
		diff = cur && a.nodeType === 1 && b.nodeType === 1 &&
			a.sourceIndex - b.sourceIndex;

	// Use IE sourceIndex if available on both nodes
	if ( diff ) {
		return diff;
	}

	// Check if b follows a
	if ( cur ) {
		while ( (cur = cur.nextSibling) ) {
			if ( cur === b ) {
				return -1;
			}
		}
	}

	return a ? 1 : -1;
}

/**
 * Returns a function to use in pseudos for input types
 * @param {String} type
 */
function createInputPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return name === "input" && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for buttons
 * @param {String} type
 */
function createButtonPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return (name === "input" || name === "button") && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for :enabled/:disabled
 * @param {Boolean} disabled true for :disabled; false for :enabled
 */
function createDisabledPseudo( disabled ) {

	// Known :disabled false positives: fieldset[disabled] > legend:nth-of-type(n+2) :can-disable
	return function( elem ) {

		// Only certain elements can match :enabled or :disabled
		// https://html.spec.whatwg.org/multipage/scripting.html#selector-enabled
		// https://html.spec.whatwg.org/multipage/scripting.html#selector-disabled
		if ( "form" in elem ) {

			// Check for inherited disabledness on relevant non-disabled elements:
			// * listed form-associated elements in a disabled fieldset
			//   https://html.spec.whatwg.org/multipage/forms.html#category-listed
			//   https://html.spec.whatwg.org/multipage/forms.html#concept-fe-disabled
			// * option elements in a disabled optgroup
			//   https://html.spec.whatwg.org/multipage/forms.html#concept-option-disabled
			// All such elements have a "form" property.
			if ( elem.parentNode && elem.disabled === false ) {

				// Option elements defer to a parent optgroup if present
				if ( "label" in elem ) {
					if ( "label" in elem.parentNode ) {
						return elem.parentNode.disabled === disabled;
					} else {
						return elem.disabled === disabled;
					}
				}

				// Support: IE 6 - 11
				// Use the isDisabled shortcut property to check for disabled fieldset ancestors
				return elem.isDisabled === disabled ||

					// Where there is no isDisabled, check manually
					/* jshint -W018 */
					elem.isDisabled !== !disabled &&
						disabledAncestor( elem ) === disabled;
			}

			return elem.disabled === disabled;

		// Try to winnow out elements that can't be disabled before trusting the disabled property.
		// Some victims get caught in our net (label, legend, menu, track), but it shouldn't
		// even exist on them, let alone have a boolean value.
		} else if ( "label" in elem ) {
			return elem.disabled === disabled;
		}

		// Remaining elements are neither :enabled nor :disabled
		return false;
	};
}

/**
 * Returns a function to use in pseudos for positionals
 * @param {Function} fn
 */
function createPositionalPseudo( fn ) {
	return markFunction(function( argument ) {
		argument = +argument;
		return markFunction(function( seed, matches ) {
			var j,
				matchIndexes = fn( [], seed.length, argument ),
				i = matchIndexes.length;

			// Match elements found at the specified indexes
			while ( i-- ) {
				if ( seed[ (j = matchIndexes[i]) ] ) {
					seed[j] = !(matches[j] = seed[j]);
				}
			}
		});
	});
}

/**
 * Checks a node for validity as a Sizzle context
 * @param {Element|Object=} context
 * @returns {Element|Object|Boolean} The input node if acceptable, otherwise a falsy value
 */
function testContext( context ) {
	return context && typeof context.getElementsByTagName !== "undefined" && context;
}

// Expose support vars for convenience
support = Sizzle.support = {};

/**
 * Detects XML nodes
 * @param {Element|Object} elem An element or a document
 * @returns {Boolean} True iff elem is a non-HTML XML node
 */
isXML = Sizzle.isXML = function( elem ) {
	// documentElement is verified for cases where it doesn't yet exist
	// (such as loading iframes in IE - #4833)
	var documentElement = elem && (elem.ownerDocument || elem).documentElement;
	return documentElement ? documentElement.nodeName !== "HTML" : false;
};

/**
 * Sets document-related variables once based on the current document
 * @param {Element|Object} [doc] An element or document object to use to set the document
 * @returns {Object} Returns the current document
 */
setDocument = Sizzle.setDocument = function( node ) {
	var hasCompare, subWindow,
		doc = node ? node.ownerDocument || node : preferredDoc;

	// Return early if doc is invalid or already selected
	if ( doc === document || doc.nodeType !== 9 || !doc.documentElement ) {
		return document;
	}

	// Update global variables
	document = doc;
	docElem = document.documentElement;
	documentIsHTML = !isXML( document );

	// Support: IE 9-11, Edge
	// Accessing iframe documents after unload throws "permission denied" errors (jQuery #13936)
	if ( preferredDoc !== document &&
		(subWindow = document.defaultView) && subWindow.top !== subWindow ) {

		// Support: IE 11, Edge
		if ( subWindow.addEventListener ) {
			subWindow.addEventListener( "unload", unloadHandler, false );

		// Support: IE 9 - 10 only
		} else if ( subWindow.attachEvent ) {
			subWindow.attachEvent( "onunload", unloadHandler );
		}
	}

	/* Attributes
	---------------------------------------------------------------------- */

	// Support: IE<8
	// Verify that getAttribute really returns attributes and not properties
	// (excepting IE8 booleans)
	support.attributes = assert(function( el ) {
		el.className = "i";
		return !el.getAttribute("className");
	});

	/* getElement(s)By*
	---------------------------------------------------------------------- */

	// Check if getElementsByTagName("*") returns only elements
	support.getElementsByTagName = assert(function( el ) {
		el.appendChild( document.createComment("") );
		return !el.getElementsByTagName("*").length;
	});

	// Support: IE<9
	support.getElementsByClassName = rnative.test( document.getElementsByClassName );

	// Support: IE<10
	// Check if getElementById returns elements by name
	// The broken getElementById methods don't pick up programmatically-set names,
	// so use a roundabout getElementsByName test
	support.getById = assert(function( el ) {
		docElem.appendChild( el ).id = expando;
		return !document.getElementsByName || !document.getElementsByName( expando ).length;
	});

	// ID filter and find
	if ( support.getById ) {
		Expr.filter["ID"] = function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				return elem.getAttribute("id") === attrId;
			};
		};
		Expr.find["ID"] = function( id, context ) {
			if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
				var elem = context.getElementById( id );
				return elem ? [ elem ] : [];
			}
		};
	} else {
		Expr.filter["ID"] =  function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				var node = typeof elem.getAttributeNode !== "undefined" &&
					elem.getAttributeNode("id");
				return node && node.value === attrId;
			};
		};

		// Support: IE 6 - 7 only
		// getElementById is not reliable as a find shortcut
		Expr.find["ID"] = function( id, context ) {
			if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
				var node, i, elems,
					elem = context.getElementById( id );

				if ( elem ) {

					// Verify the id attribute
					node = elem.getAttributeNode("id");
					if ( node && node.value === id ) {
						return [ elem ];
					}

					// Fall back on getElementsByName
					elems = context.getElementsByName( id );
					i = 0;
					while ( (elem = elems[i++]) ) {
						node = elem.getAttributeNode("id");
						if ( node && node.value === id ) {
							return [ elem ];
						}
					}
				}

				return [];
			}
		};
	}

	// Tag
	Expr.find["TAG"] = support.getElementsByTagName ?
		function( tag, context ) {
			if ( typeof context.getElementsByTagName !== "undefined" ) {
				return context.getElementsByTagName( tag );

			// DocumentFragment nodes don't have gEBTN
			} else if ( support.qsa ) {
				return context.querySelectorAll( tag );
			}
		} :

		function( tag, context ) {
			var elem,
				tmp = [],
				i = 0,
				// By happy coincidence, a (broken) gEBTN appears on DocumentFragment nodes too
				results = context.getElementsByTagName( tag );

			// Filter out possible comments
			if ( tag === "*" ) {
				while ( (elem = results[i++]) ) {
					if ( elem.nodeType === 1 ) {
						tmp.push( elem );
					}
				}

				return tmp;
			}
			return results;
		};

	// Class
	Expr.find["CLASS"] = support.getElementsByClassName && function( className, context ) {
		if ( typeof context.getElementsByClassName !== "undefined" && documentIsHTML ) {
			return context.getElementsByClassName( className );
		}
	};

	/* QSA/matchesSelector
	---------------------------------------------------------------------- */

	// QSA and matchesSelector support

	// matchesSelector(:active) reports false when true (IE9/Opera 11.5)
	rbuggyMatches = [];

	// qSa(:focus) reports false when true (Chrome 21)
	// We allow this because of a bug in IE8/9 that throws an error
	// whenever `document.activeElement` is accessed on an iframe
	// So, we allow :focus to pass through QSA all the time to avoid the IE error
	// See https://bugs.jquery.com/ticket/13378
	rbuggyQSA = [];

	if ( (support.qsa = rnative.test( document.querySelectorAll )) ) {
		// Build QSA regex
		// Regex strategy adopted from Diego Perini
		assert(function( el ) {
			// Select is set to empty string on purpose
			// This is to test IE's treatment of not explicitly
			// setting a boolean content attribute,
			// since its presence should be enough
			// https://bugs.jquery.com/ticket/12359
			docElem.appendChild( el ).innerHTML = "<a id='" + expando + "'></a>" +
				"<select id='" + expando + "-\r\\' msallowcapture=''>" +
				"<option selected=''></option></select>";

			// Support: IE8, Opera 11-12.16
			// Nothing should be selected when empty strings follow ^= or $= or *=
			// The test attribute must be unknown in Opera but "safe" for WinRT
			// https://msdn.microsoft.com/en-us/library/ie/hh465388.aspx#attribute_section
			if ( el.querySelectorAll("[msallowcapture^='']").length ) {
				rbuggyQSA.push( "[*^$]=" + whitespace + "*(?:''|\"\")" );
			}

			// Support: IE8
			// Boolean attributes and "value" are not treated correctly
			if ( !el.querySelectorAll("[selected]").length ) {
				rbuggyQSA.push( "\\[" + whitespace + "*(?:value|" + booleans + ")" );
			}

			// Support: Chrome<29, Android<4.4, Safari<7.0+, iOS<7.0+, PhantomJS<1.9.8+
			if ( !el.querySelectorAll( "[id~=" + expando + "-]" ).length ) {
				rbuggyQSA.push("~=");
			}

			// Webkit/Opera - :checked should return selected option elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			// IE8 throws error here and will not see later tests
			if ( !el.querySelectorAll(":checked").length ) {
				rbuggyQSA.push(":checked");
			}

			// Support: Safari 8+, iOS 8+
			// https://bugs.webkit.org/show_bug.cgi?id=136851
			// In-page `selector#id sibling-combinator selector` fails
			if ( !el.querySelectorAll( "a#" + expando + "+*" ).length ) {
				rbuggyQSA.push(".#.+[+~]");
			}
		});

		assert(function( el ) {
			el.innerHTML = "<a href='' disabled='disabled'></a>" +
				"<select disabled='disabled'><option/></select>";

			// Support: Windows 8 Native Apps
			// The type and name attributes are restricted during .innerHTML assignment
			var input = document.createElement("input");
			input.setAttribute( "type", "hidden" );
			el.appendChild( input ).setAttribute( "name", "D" );

			// Support: IE8
			// Enforce case-sensitivity of name attribute
			if ( el.querySelectorAll("[name=d]").length ) {
				rbuggyQSA.push( "name" + whitespace + "*[*^$|!~]?=" );
			}

			// FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)
			// IE8 throws error here and will not see later tests
			if ( el.querySelectorAll(":enabled").length !== 2 ) {
				rbuggyQSA.push( ":enabled", ":disabled" );
			}

			// Support: IE9-11+
			// IE's :disabled selector does not pick up the children of disabled fieldsets
			docElem.appendChild( el ).disabled = true;
			if ( el.querySelectorAll(":disabled").length !== 2 ) {
				rbuggyQSA.push( ":enabled", ":disabled" );
			}

			// Opera 10-11 does not throw on post-comma invalid pseudos
			el.querySelectorAll("*,:x");
			rbuggyQSA.push(",.*:");
		});
	}

	if ( (support.matchesSelector = rnative.test( (matches = docElem.matches ||
		docElem.webkitMatchesSelector ||
		docElem.mozMatchesSelector ||
		docElem.oMatchesSelector ||
		docElem.msMatchesSelector) )) ) {

		assert(function( el ) {
			// Check to see if it's possible to do matchesSelector
			// on a disconnected node (IE 9)
			support.disconnectedMatch = matches.call( el, "*" );

			// This should fail with an exception
			// Gecko does not error, returns false instead
			matches.call( el, "[s!='']:x" );
			rbuggyMatches.push( "!=", pseudos );
		});
	}

	rbuggyQSA = rbuggyQSA.length && new RegExp( rbuggyQSA.join("|") );
	rbuggyMatches = rbuggyMatches.length && new RegExp( rbuggyMatches.join("|") );

	/* Contains
	---------------------------------------------------------------------- */
	hasCompare = rnative.test( docElem.compareDocumentPosition );

	// Element contains another
	// Purposefully self-exclusive
	// As in, an element does not contain itself
	contains = hasCompare || rnative.test( docElem.contains ) ?
		function( a, b ) {
			var adown = a.nodeType === 9 ? a.documentElement : a,
				bup = b && b.parentNode;
			return a === bup || !!( bup && bup.nodeType === 1 && (
				adown.contains ?
					adown.contains( bup ) :
					a.compareDocumentPosition && a.compareDocumentPosition( bup ) & 16
			));
		} :
		function( a, b ) {
			if ( b ) {
				while ( (b = b.parentNode) ) {
					if ( b === a ) {
						return true;
					}
				}
			}
			return false;
		};

	/* Sorting
	---------------------------------------------------------------------- */

	// Document order sorting
	sortOrder = hasCompare ?
	function( a, b ) {

		// Flag for duplicate removal
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		// Sort on method existence if only one input has compareDocumentPosition
		var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
		if ( compare ) {
			return compare;
		}

		// Calculate position if both inputs belong to the same document
		compare = ( a.ownerDocument || a ) === ( b.ownerDocument || b ) ?
			a.compareDocumentPosition( b ) :

			// Otherwise we know they are disconnected
			1;

		// Disconnected nodes
		if ( compare & 1 ||
			(!support.sortDetached && b.compareDocumentPosition( a ) === compare) ) {

			// Choose the first element that is related to our preferred document
			if ( a === document || a.ownerDocument === preferredDoc && contains(preferredDoc, a) ) {
				return -1;
			}
			if ( b === document || b.ownerDocument === preferredDoc && contains(preferredDoc, b) ) {
				return 1;
			}

			// Maintain original order
			return sortInput ?
				( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
				0;
		}

		return compare & 4 ? -1 : 1;
	} :
	function( a, b ) {
		// Exit early if the nodes are identical
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		var cur,
			i = 0,
			aup = a.parentNode,
			bup = b.parentNode,
			ap = [ a ],
			bp = [ b ];

		// Parentless nodes are either documents or disconnected
		if ( !aup || !bup ) {
			return a === document ? -1 :
				b === document ? 1 :
				aup ? -1 :
				bup ? 1 :
				sortInput ?
				( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
				0;

		// If the nodes are siblings, we can do a quick check
		} else if ( aup === bup ) {
			return siblingCheck( a, b );
		}

		// Otherwise we need full lists of their ancestors for comparison
		cur = a;
		while ( (cur = cur.parentNode) ) {
			ap.unshift( cur );
		}
		cur = b;
		while ( (cur = cur.parentNode) ) {
			bp.unshift( cur );
		}

		// Walk down the tree looking for a discrepancy
		while ( ap[i] === bp[i] ) {
			i++;
		}

		return i ?
			// Do a sibling check if the nodes have a common ancestor
			siblingCheck( ap[i], bp[i] ) :

			// Otherwise nodes in our document sort first
			ap[i] === preferredDoc ? -1 :
			bp[i] === preferredDoc ? 1 :
			0;
	};

	return document;
};

Sizzle.matches = function( expr, elements ) {
	return Sizzle( expr, null, null, elements );
};

Sizzle.matchesSelector = function( elem, expr ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	// Make sure that attribute selectors are quoted
	expr = expr.replace( rattributeQuotes, "='$1']" );

	if ( support.matchesSelector && documentIsHTML &&
		!compilerCache[ expr + " " ] &&
		( !rbuggyMatches || !rbuggyMatches.test( expr ) ) &&
		( !rbuggyQSA     || !rbuggyQSA.test( expr ) ) ) {

		try {
			var ret = matches.call( elem, expr );

			// IE 9's matchesSelector returns false on disconnected nodes
			if ( ret || support.disconnectedMatch ||
					// As well, disconnected nodes are said to be in a document
					// fragment in IE 9
					elem.document && elem.document.nodeType !== 11 ) {
				return ret;
			}
		} catch (e) {}
	}

	return Sizzle( expr, document, null, [ elem ] ).length > 0;
};

Sizzle.contains = function( context, elem ) {
	// Set document vars if needed
	if ( ( context.ownerDocument || context ) !== document ) {
		setDocument( context );
	}
	return contains( context, elem );
};

Sizzle.attr = function( elem, name ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	var fn = Expr.attrHandle[ name.toLowerCase() ],
		// Don't get fooled by Object.prototype properties (jQuery #13807)
		val = fn && hasOwn.call( Expr.attrHandle, name.toLowerCase() ) ?
			fn( elem, name, !documentIsHTML ) :
			undefined;

	return val !== undefined ?
		val :
		support.attributes || !documentIsHTML ?
			elem.getAttribute( name ) :
			(val = elem.getAttributeNode(name)) && val.specified ?
				val.value :
				null;
};

Sizzle.escape = function( sel ) {
	return (sel + "").replace( rcssescape, fcssescape );
};

Sizzle.error = function( msg ) {
	throw new Error( "Syntax error, unrecognized expression: " + msg );
};

/**
 * Document sorting and removing duplicates
 * @param {ArrayLike} results
 */
Sizzle.uniqueSort = function( results ) {
	var elem,
		duplicates = [],
		j = 0,
		i = 0;

	// Unless we *know* we can detect duplicates, assume their presence
	hasDuplicate = !support.detectDuplicates;
	sortInput = !support.sortStable && results.slice( 0 );
	results.sort( sortOrder );

	if ( hasDuplicate ) {
		while ( (elem = results[i++]) ) {
			if ( elem === results[ i ] ) {
				j = duplicates.push( i );
			}
		}
		while ( j-- ) {
			results.splice( duplicates[ j ], 1 );
		}
	}

	// Clear input after sorting to release objects
	// See https://github.com/jquery/sizzle/pull/225
	sortInput = null;

	return results;
};

/**
 * Utility function for retrieving the text value of an array of DOM nodes
 * @param {Array|Element} elem
 */
getText = Sizzle.getText = function( elem ) {
	var node,
		ret = "",
		i = 0,
		nodeType = elem.nodeType;

	if ( !nodeType ) {
		// If no nodeType, this is expected to be an array
		while ( (node = elem[i++]) ) {
			// Do not traverse comment nodes
			ret += getText( node );
		}
	} else if ( nodeType === 1 || nodeType === 9 || nodeType === 11 ) {
		// Use textContent for elements
		// innerText usage removed for consistency of new lines (jQuery #11153)
		if ( typeof elem.textContent === "string" ) {
			return elem.textContent;
		} else {
			// Traverse its children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				ret += getText( elem );
			}
		}
	} else if ( nodeType === 3 || nodeType === 4 ) {
		return elem.nodeValue;
	}
	// Do not include comment or processing instruction nodes

	return ret;
};

Expr = Sizzle.selectors = {

	// Can be adjusted by the user
	cacheLength: 50,

	createPseudo: markFunction,

	match: matchExpr,

	attrHandle: {},

	find: {},

	relative: {
		">": { dir: "parentNode", first: true },
		" ": { dir: "parentNode" },
		"+": { dir: "previousSibling", first: true },
		"~": { dir: "previousSibling" }
	},

	preFilter: {
		"ATTR": function( match ) {
			match[1] = match[1].replace( runescape, funescape );

			// Move the given value to match[3] whether quoted or unquoted
			match[3] = ( match[3] || match[4] || match[5] || "" ).replace( runescape, funescape );

			if ( match[2] === "~=" ) {
				match[3] = " " + match[3] + " ";
			}

			return match.slice( 0, 4 );
		},

		"CHILD": function( match ) {
			/* matches from matchExpr["CHILD"]
				1 type (only|nth|...)
				2 what (child|of-type)
				3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)
				4 xn-component of xn+y argument ([+-]?\d*n|)
				5 sign of xn-component
				6 x of xn-component
				7 sign of y-component
				8 y of y-component
			*/
			match[1] = match[1].toLowerCase();

			if ( match[1].slice( 0, 3 ) === "nth" ) {
				// nth-* requires argument
				if ( !match[3] ) {
					Sizzle.error( match[0] );
				}

				// numeric x and y parameters for Expr.filter.CHILD
				// remember that false/true cast respectively to 0/1
				match[4] = +( match[4] ? match[5] + (match[6] || 1) : 2 * ( match[3] === "even" || match[3] === "odd" ) );
				match[5] = +( ( match[7] + match[8] ) || match[3] === "odd" );

			// other types prohibit arguments
			} else if ( match[3] ) {
				Sizzle.error( match[0] );
			}

			return match;
		},

		"PSEUDO": function( match ) {
			var excess,
				unquoted = !match[6] && match[2];

			if ( matchExpr["CHILD"].test( match[0] ) ) {
				return null;
			}

			// Accept quoted arguments as-is
			if ( match[3] ) {
				match[2] = match[4] || match[5] || "";

			// Strip excess characters from unquoted arguments
			} else if ( unquoted && rpseudo.test( unquoted ) &&
				// Get excess from tokenize (recursively)
				(excess = tokenize( unquoted, true )) &&
				// advance to the next closing parenthesis
				(excess = unquoted.indexOf( ")", unquoted.length - excess ) - unquoted.length) ) {

				// excess is a negative index
				match[0] = match[0].slice( 0, excess );
				match[2] = unquoted.slice( 0, excess );
			}

			// Return only captures needed by the pseudo filter method (type and argument)
			return match.slice( 0, 3 );
		}
	},

	filter: {

		"TAG": function( nodeNameSelector ) {
			var nodeName = nodeNameSelector.replace( runescape, funescape ).toLowerCase();
			return nodeNameSelector === "*" ?
				function() { return true; } :
				function( elem ) {
					return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
				};
		},

		"CLASS": function( className ) {
			var pattern = classCache[ className + " " ];

			return pattern ||
				(pattern = new RegExp( "(^|" + whitespace + ")" + className + "(" + whitespace + "|$)" )) &&
				classCache( className, function( elem ) {
					return pattern.test( typeof elem.className === "string" && elem.className || typeof elem.getAttribute !== "undefined" && elem.getAttribute("class") || "" );
				});
		},

		"ATTR": function( name, operator, check ) {
			return function( elem ) {
				var result = Sizzle.attr( elem, name );

				if ( result == null ) {
					return operator === "!=";
				}
				if ( !operator ) {
					return true;
				}

				result += "";

				return operator === "=" ? result === check :
					operator === "!=" ? result !== check :
					operator === "^=" ? check && result.indexOf( check ) === 0 :
					operator === "*=" ? check && result.indexOf( check ) > -1 :
					operator === "$=" ? check && result.slice( -check.length ) === check :
					operator === "~=" ? ( " " + result.replace( rwhitespace, " " ) + " " ).indexOf( check ) > -1 :
					operator === "|=" ? result === check || result.slice( 0, check.length + 1 ) === check + "-" :
					false;
			};
		},

		"CHILD": function( type, what, argument, first, last ) {
			var simple = type.slice( 0, 3 ) !== "nth",
				forward = type.slice( -4 ) !== "last",
				ofType = what === "of-type";

			return first === 1 && last === 0 ?

				// Shortcut for :nth-*(n)
				function( elem ) {
					return !!elem.parentNode;
				} :

				function( elem, context, xml ) {
					var cache, uniqueCache, outerCache, node, nodeIndex, start,
						dir = simple !== forward ? "nextSibling" : "previousSibling",
						parent = elem.parentNode,
						name = ofType && elem.nodeName.toLowerCase(),
						useCache = !xml && !ofType,
						diff = false;

					if ( parent ) {

						// :(first|last|only)-(child|of-type)
						if ( simple ) {
							while ( dir ) {
								node = elem;
								while ( (node = node[ dir ]) ) {
									if ( ofType ?
										node.nodeName.toLowerCase() === name :
										node.nodeType === 1 ) {

										return false;
									}
								}
								// Reverse direction for :only-* (if we haven't yet done so)
								start = dir = type === "only" && !start && "nextSibling";
							}
							return true;
						}

						start = [ forward ? parent.firstChild : parent.lastChild ];

						// non-xml :nth-child(...) stores cache data on `parent`
						if ( forward && useCache ) {

							// Seek `elem` from a previously-cached index

							// ...in a gzip-friendly way
							node = parent;
							outerCache = node[ expando ] || (node[ expando ] = {});

							// Support: IE <9 only
							// Defend against cloned attroperties (jQuery gh-1709)
							uniqueCache = outerCache[ node.uniqueID ] ||
								(outerCache[ node.uniqueID ] = {});

							cache = uniqueCache[ type ] || [];
							nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
							diff = nodeIndex && cache[ 2 ];
							node = nodeIndex && parent.childNodes[ nodeIndex ];

							while ( (node = ++nodeIndex && node && node[ dir ] ||

								// Fallback to seeking `elem` from the start
								(diff = nodeIndex = 0) || start.pop()) ) {

								// When found, cache indexes on `parent` and break
								if ( node.nodeType === 1 && ++diff && node === elem ) {
									uniqueCache[ type ] = [ dirruns, nodeIndex, diff ];
									break;
								}
							}

						} else {
							// Use previously-cached element index if available
							if ( useCache ) {
								// ...in a gzip-friendly way
								node = elem;
								outerCache = node[ expando ] || (node[ expando ] = {});

								// Support: IE <9 only
								// Defend against cloned attroperties (jQuery gh-1709)
								uniqueCache = outerCache[ node.uniqueID ] ||
									(outerCache[ node.uniqueID ] = {});

								cache = uniqueCache[ type ] || [];
								nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
								diff = nodeIndex;
							}

							// xml :nth-child(...)
							// or :nth-last-child(...) or :nth(-last)?-of-type(...)
							if ( diff === false ) {
								// Use the same loop as above to seek `elem` from the start
								while ( (node = ++nodeIndex && node && node[ dir ] ||
									(diff = nodeIndex = 0) || start.pop()) ) {

									if ( ( ofType ?
										node.nodeName.toLowerCase() === name :
										node.nodeType === 1 ) &&
										++diff ) {

										// Cache the index of each encountered element
										if ( useCache ) {
											outerCache = node[ expando ] || (node[ expando ] = {});

											// Support: IE <9 only
											// Defend against cloned attroperties (jQuery gh-1709)
											uniqueCache = outerCache[ node.uniqueID ] ||
												(outerCache[ node.uniqueID ] = {});

											uniqueCache[ type ] = [ dirruns, diff ];
										}

										if ( node === elem ) {
											break;
										}
									}
								}
							}
						}

						// Incorporate the offset, then check against cycle size
						diff -= last;
						return diff === first || ( diff % first === 0 && diff / first >= 0 );
					}
				};
		},

		"PSEUDO": function( pseudo, argument ) {
			// pseudo-class names are case-insensitive
			// http://www.w3.org/TR/selectors/#pseudo-classes
			// Prioritize by case sensitivity in case custom pseudos are added with uppercase letters
			// Remember that setFilters inherits from pseudos
			var args,
				fn = Expr.pseudos[ pseudo ] || Expr.setFilters[ pseudo.toLowerCase() ] ||
					Sizzle.error( "unsupported pseudo: " + pseudo );

			// The user may use createPseudo to indicate that
			// arguments are needed to create the filter function
			// just as Sizzle does
			if ( fn[ expando ] ) {
				return fn( argument );
			}

			// But maintain support for old signatures
			if ( fn.length > 1 ) {
				args = [ pseudo, pseudo, "", argument ];
				return Expr.setFilters.hasOwnProperty( pseudo.toLowerCase() ) ?
					markFunction(function( seed, matches ) {
						var idx,
							matched = fn( seed, argument ),
							i = matched.length;
						while ( i-- ) {
							idx = indexOf( seed, matched[i] );
							seed[ idx ] = !( matches[ idx ] = matched[i] );
						}
					}) :
					function( elem ) {
						return fn( elem, 0, args );
					};
			}

			return fn;
		}
	},

	pseudos: {
		// Potentially complex pseudos
		"not": markFunction(function( selector ) {
			// Trim the selector passed to compile
			// to avoid treating leading and trailing
			// spaces as combinators
			var input = [],
				results = [],
				matcher = compile( selector.replace( rtrim, "$1" ) );

			return matcher[ expando ] ?
				markFunction(function( seed, matches, context, xml ) {
					var elem,
						unmatched = matcher( seed, null, xml, [] ),
						i = seed.length;

					// Match elements unmatched by `matcher`
					while ( i-- ) {
						if ( (elem = unmatched[i]) ) {
							seed[i] = !(matches[i] = elem);
						}
					}
				}) :
				function( elem, context, xml ) {
					input[0] = elem;
					matcher( input, null, xml, results );
					// Don't keep the element (issue #299)
					input[0] = null;
					return !results.pop();
				};
		}),

		"has": markFunction(function( selector ) {
			return function( elem ) {
				return Sizzle( selector, elem ).length > 0;
			};
		}),

		"contains": markFunction(function( text ) {
			text = text.replace( runescape, funescape );
			return function( elem ) {
				return ( elem.textContent || elem.innerText || getText( elem ) ).indexOf( text ) > -1;
			};
		}),

		// "Whether an element is represented by a :lang() selector
		// is based solely on the element's language value
		// being equal to the identifier C,
		// or beginning with the identifier C immediately followed by "-".
		// The matching of C against the element's language value is performed case-insensitively.
		// The identifier C does not have to be a valid language name."
		// http://www.w3.org/TR/selectors/#lang-pseudo
		"lang": markFunction( function( lang ) {
			// lang value must be a valid identifier
			if ( !ridentifier.test(lang || "") ) {
				Sizzle.error( "unsupported lang: " + lang );
			}
			lang = lang.replace( runescape, funescape ).toLowerCase();
			return function( elem ) {
				var elemLang;
				do {
					if ( (elemLang = documentIsHTML ?
						elem.lang :
						elem.getAttribute("xml:lang") || elem.getAttribute("lang")) ) {

						elemLang = elemLang.toLowerCase();
						return elemLang === lang || elemLang.indexOf( lang + "-" ) === 0;
					}
				} while ( (elem = elem.parentNode) && elem.nodeType === 1 );
				return false;
			};
		}),

		// Miscellaneous
		"target": function( elem ) {
			var hash = window.location && window.location.hash;
			return hash && hash.slice( 1 ) === elem.id;
		},

		"root": function( elem ) {
			return elem === docElem;
		},

		"focus": function( elem ) {
			return elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
		},

		// Boolean properties
		"enabled": createDisabledPseudo( false ),
		"disabled": createDisabledPseudo( true ),

		"checked": function( elem ) {
			// In CSS3, :checked should return both checked and selected elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			var nodeName = elem.nodeName.toLowerCase();
			return (nodeName === "input" && !!elem.checked) || (nodeName === "option" && !!elem.selected);
		},

		"selected": function( elem ) {
			// Accessing this property makes selected-by-default
			// options in Safari work properly
			if ( elem.parentNode ) {
				elem.parentNode.selectedIndex;
			}

			return elem.selected === true;
		},

		// Contents
		"empty": function( elem ) {
			// http://www.w3.org/TR/selectors/#empty-pseudo
			// :empty is negated by element (1) or content nodes (text: 3; cdata: 4; entity ref: 5),
			//   but not by others (comment: 8; processing instruction: 7; etc.)
			// nodeType < 6 works because attributes (2) do not appear as children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				if ( elem.nodeType < 6 ) {
					return false;
				}
			}
			return true;
		},

		"parent": function( elem ) {
			return !Expr.pseudos["empty"]( elem );
		},

		// Element/input types
		"header": function( elem ) {
			return rheader.test( elem.nodeName );
		},

		"input": function( elem ) {
			return rinputs.test( elem.nodeName );
		},

		"button": function( elem ) {
			var name = elem.nodeName.toLowerCase();
			return name === "input" && elem.type === "button" || name === "button";
		},

		"text": function( elem ) {
			var attr;
			return elem.nodeName.toLowerCase() === "input" &&
				elem.type === "text" &&

				// Support: IE<8
				// New HTML5 attribute values (e.g., "search") appear with elem.type === "text"
				( (attr = elem.getAttribute("type")) == null || attr.toLowerCase() === "text" );
		},

		// Position-in-collection
		"first": createPositionalPseudo(function() {
			return [ 0 ];
		}),

		"last": createPositionalPseudo(function( matchIndexes, length ) {
			return [ length - 1 ];
		}),

		"eq": createPositionalPseudo(function( matchIndexes, length, argument ) {
			return [ argument < 0 ? argument + length : argument ];
		}),

		"even": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 0;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"odd": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 1;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"lt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; --i >= 0; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"gt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; ++i < length; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		})
	}
};

Expr.pseudos["nth"] = Expr.pseudos["eq"];

// Add button/input type pseudos
for ( i in { radio: true, checkbox: true, file: true, password: true, image: true } ) {
	Expr.pseudos[ i ] = createInputPseudo( i );
}
for ( i in { submit: true, reset: true } ) {
	Expr.pseudos[ i ] = createButtonPseudo( i );
}

// Easy API for creating new setFilters
function setFilters() {}
setFilters.prototype = Expr.filters = Expr.pseudos;
Expr.setFilters = new setFilters();

tokenize = Sizzle.tokenize = function( selector, parseOnly ) {
	var matched, match, tokens, type,
		soFar, groups, preFilters,
		cached = tokenCache[ selector + " " ];

	if ( cached ) {
		return parseOnly ? 0 : cached.slice( 0 );
	}

	soFar = selector;
	groups = [];
	preFilters = Expr.preFilter;

	while ( soFar ) {

		// Comma and first run
		if ( !matched || (match = rcomma.exec( soFar )) ) {
			if ( match ) {
				// Don't consume trailing commas as valid
				soFar = soFar.slice( match[0].length ) || soFar;
			}
			groups.push( (tokens = []) );
		}

		matched = false;

		// Combinators
		if ( (match = rcombinators.exec( soFar )) ) {
			matched = match.shift();
			tokens.push({
				value: matched,
				// Cast descendant combinators to space
				type: match[0].replace( rtrim, " " )
			});
			soFar = soFar.slice( matched.length );
		}

		// Filters
		for ( type in Expr.filter ) {
			if ( (match = matchExpr[ type ].exec( soFar )) && (!preFilters[ type ] ||
				(match = preFilters[ type ]( match ))) ) {
				matched = match.shift();
				tokens.push({
					value: matched,
					type: type,
					matches: match
				});
				soFar = soFar.slice( matched.length );
			}
		}

		if ( !matched ) {
			break;
		}
	}

	// Return the length of the invalid excess
	// if we're just parsing
	// Otherwise, throw an error or return tokens
	return parseOnly ?
		soFar.length :
		soFar ?
			Sizzle.error( selector ) :
			// Cache the tokens
			tokenCache( selector, groups ).slice( 0 );
};

function toSelector( tokens ) {
	var i = 0,
		len = tokens.length,
		selector = "";
	for ( ; i < len; i++ ) {
		selector += tokens[i].value;
	}
	return selector;
}

function addCombinator( matcher, combinator, base ) {
	var dir = combinator.dir,
		skip = combinator.next,
		key = skip || dir,
		checkNonElements = base && key === "parentNode",
		doneName = done++;

	return combinator.first ?
		// Check against closest ancestor/preceding element
		function( elem, context, xml ) {
			while ( (elem = elem[ dir ]) ) {
				if ( elem.nodeType === 1 || checkNonElements ) {
					return matcher( elem, context, xml );
				}
			}
			return false;
		} :

		// Check against all ancestor/preceding elements
		function( elem, context, xml ) {
			var oldCache, uniqueCache, outerCache,
				newCache = [ dirruns, doneName ];

			// We can't set arbitrary data on XML nodes, so they don't benefit from combinator caching
			if ( xml ) {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						if ( matcher( elem, context, xml ) ) {
							return true;
						}
					}
				}
			} else {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						outerCache = elem[ expando ] || (elem[ expando ] = {});

						// Support: IE <9 only
						// Defend against cloned attroperties (jQuery gh-1709)
						uniqueCache = outerCache[ elem.uniqueID ] || (outerCache[ elem.uniqueID ] = {});

						if ( skip && skip === elem.nodeName.toLowerCase() ) {
							elem = elem[ dir ] || elem;
						} else if ( (oldCache = uniqueCache[ key ]) &&
							oldCache[ 0 ] === dirruns && oldCache[ 1 ] === doneName ) {

							// Assign to newCache so results back-propagate to previous elements
							return (newCache[ 2 ] = oldCache[ 2 ]);
						} else {
							// Reuse newcache so results back-propagate to previous elements
							uniqueCache[ key ] = newCache;

							// A match means we're done; a fail means we have to keep checking
							if ( (newCache[ 2 ] = matcher( elem, context, xml )) ) {
								return true;
							}
						}
					}
				}
			}
			return false;
		};
}

function elementMatcher( matchers ) {
	return matchers.length > 1 ?
		function( elem, context, xml ) {
			var i = matchers.length;
			while ( i-- ) {
				if ( !matchers[i]( elem, context, xml ) ) {
					return false;
				}
			}
			return true;
		} :
		matchers[0];
}

function multipleContexts( selector, contexts, results ) {
	var i = 0,
		len = contexts.length;
	for ( ; i < len; i++ ) {
		Sizzle( selector, contexts[i], results );
	}
	return results;
}

function condense( unmatched, map, filter, context, xml ) {
	var elem,
		newUnmatched = [],
		i = 0,
		len = unmatched.length,
		mapped = map != null;

	for ( ; i < len; i++ ) {
		if ( (elem = unmatched[i]) ) {
			if ( !filter || filter( elem, context, xml ) ) {
				newUnmatched.push( elem );
				if ( mapped ) {
					map.push( i );
				}
			}
		}
	}

	return newUnmatched;
}

function setMatcher( preFilter, selector, matcher, postFilter, postFinder, postSelector ) {
	if ( postFilter && !postFilter[ expando ] ) {
		postFilter = setMatcher( postFilter );
	}
	if ( postFinder && !postFinder[ expando ] ) {
		postFinder = setMatcher( postFinder, postSelector );
	}
	return markFunction(function( seed, results, context, xml ) {
		var temp, i, elem,
			preMap = [],
			postMap = [],
			preexisting = results.length,

			// Get initial elements from seed or context
			elems = seed || multipleContexts( selector || "*", context.nodeType ? [ context ] : context, [] ),

			// Prefilter to get matcher input, preserving a map for seed-results synchronization
			matcherIn = preFilter && ( seed || !selector ) ?
				condense( elems, preMap, preFilter, context, xml ) :
				elems,

			matcherOut = matcher ?
				// If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,
				postFinder || ( seed ? preFilter : preexisting || postFilter ) ?

					// ...intermediate processing is necessary
					[] :

					// ...otherwise use results directly
					results :
				matcherIn;

		// Find primary matches
		if ( matcher ) {
			matcher( matcherIn, matcherOut, context, xml );
		}

		// Apply postFilter
		if ( postFilter ) {
			temp = condense( matcherOut, postMap );
			postFilter( temp, [], context, xml );

			// Un-match failing elements by moving them back to matcherIn
			i = temp.length;
			while ( i-- ) {
				if ( (elem = temp[i]) ) {
					matcherOut[ postMap[i] ] = !(matcherIn[ postMap[i] ] = elem);
				}
			}
		}

		if ( seed ) {
			if ( postFinder || preFilter ) {
				if ( postFinder ) {
					// Get the final matcherOut by condensing this intermediate into postFinder contexts
					temp = [];
					i = matcherOut.length;
					while ( i-- ) {
						if ( (elem = matcherOut[i]) ) {
							// Restore matcherIn since elem is not yet a final match
							temp.push( (matcherIn[i] = elem) );
						}
					}
					postFinder( null, (matcherOut = []), temp, xml );
				}

				// Move matched elements from seed to results to keep them synchronized
				i = matcherOut.length;
				while ( i-- ) {
					if ( (elem = matcherOut[i]) &&
						(temp = postFinder ? indexOf( seed, elem ) : preMap[i]) > -1 ) {

						seed[temp] = !(results[temp] = elem);
					}
				}
			}

		// Add elements to results, through postFinder if defined
		} else {
			matcherOut = condense(
				matcherOut === results ?
					matcherOut.splice( preexisting, matcherOut.length ) :
					matcherOut
			);
			if ( postFinder ) {
				postFinder( null, results, matcherOut, xml );
			} else {
				push.apply( results, matcherOut );
			}
		}
	});
}

function matcherFromTokens( tokens ) {
	var checkContext, matcher, j,
		len = tokens.length,
		leadingRelative = Expr.relative[ tokens[0].type ],
		implicitRelative = leadingRelative || Expr.relative[" "],
		i = leadingRelative ? 1 : 0,

		// The foundational matcher ensures that elements are reachable from top-level context(s)
		matchContext = addCombinator( function( elem ) {
			return elem === checkContext;
		}, implicitRelative, true ),
		matchAnyContext = addCombinator( function( elem ) {
			return indexOf( checkContext, elem ) > -1;
		}, implicitRelative, true ),
		matchers = [ function( elem, context, xml ) {
			var ret = ( !leadingRelative && ( xml || context !== outermostContext ) ) || (
				(checkContext = context).nodeType ?
					matchContext( elem, context, xml ) :
					matchAnyContext( elem, context, xml ) );
			// Avoid hanging onto element (issue #299)
			checkContext = null;
			return ret;
		} ];

	for ( ; i < len; i++ ) {
		if ( (matcher = Expr.relative[ tokens[i].type ]) ) {
			matchers = [ addCombinator(elementMatcher( matchers ), matcher) ];
		} else {
			matcher = Expr.filter[ tokens[i].type ].apply( null, tokens[i].matches );

			// Return special upon seeing a positional matcher
			if ( matcher[ expando ] ) {
				// Find the next relative operator (if any) for proper handling
				j = ++i;
				for ( ; j < len; j++ ) {
					if ( Expr.relative[ tokens[j].type ] ) {
						break;
					}
				}
				return setMatcher(
					i > 1 && elementMatcher( matchers ),
					i > 1 && toSelector(
						// If the preceding token was a descendant combinator, insert an implicit any-element `*`
						tokens.slice( 0, i - 1 ).concat({ value: tokens[ i - 2 ].type === " " ? "*" : "" })
					).replace( rtrim, "$1" ),
					matcher,
					i < j && matcherFromTokens( tokens.slice( i, j ) ),
					j < len && matcherFromTokens( (tokens = tokens.slice( j )) ),
					j < len && toSelector( tokens )
				);
			}
			matchers.push( matcher );
		}
	}

	return elementMatcher( matchers );
}

function matcherFromGroupMatchers( elementMatchers, setMatchers ) {
	var bySet = setMatchers.length > 0,
		byElement = elementMatchers.length > 0,
		superMatcher = function( seed, context, xml, results, outermost ) {
			var elem, j, matcher,
				matchedCount = 0,
				i = "0",
				unmatched = seed && [],
				setMatched = [],
				contextBackup = outermostContext,
				// We must always have either seed elements or outermost context
				elems = seed || byElement && Expr.find["TAG"]( "*", outermost ),
				// Use integer dirruns iff this is the outermost matcher
				dirrunsUnique = (dirruns += contextBackup == null ? 1 : Math.random() || 0.1),
				len = elems.length;

			if ( outermost ) {
				outermostContext = context === document || context || outermost;
			}

			// Add elements passing elementMatchers directly to results
			// Support: IE<9, Safari
			// Tolerate NodeList properties (IE: "length"; Safari: <number>) matching elements by id
			for ( ; i !== len && (elem = elems[i]) != null; i++ ) {
				if ( byElement && elem ) {
					j = 0;
					if ( !context && elem.ownerDocument !== document ) {
						setDocument( elem );
						xml = !documentIsHTML;
					}
					while ( (matcher = elementMatchers[j++]) ) {
						if ( matcher( elem, context || document, xml) ) {
							results.push( elem );
							break;
						}
					}
					if ( outermost ) {
						dirruns = dirrunsUnique;
					}
				}

				// Track unmatched elements for set filters
				if ( bySet ) {
					// They will have gone through all possible matchers
					if ( (elem = !matcher && elem) ) {
						matchedCount--;
					}

					// Lengthen the array for every element, matched or not
					if ( seed ) {
						unmatched.push( elem );
					}
				}
			}

			// `i` is now the count of elements visited above, and adding it to `matchedCount`
			// makes the latter nonnegative.
			matchedCount += i;

			// Apply set filters to unmatched elements
			// NOTE: This can be skipped if there are no unmatched elements (i.e., `matchedCount`
			// equals `i`), unless we didn't visit _any_ elements in the above loop because we have
			// no element matchers and no seed.
			// Incrementing an initially-string "0" `i` allows `i` to remain a string only in that
			// case, which will result in a "00" `matchedCount` that differs from `i` but is also
			// numerically zero.
			if ( bySet && i !== matchedCount ) {
				j = 0;
				while ( (matcher = setMatchers[j++]) ) {
					matcher( unmatched, setMatched, context, xml );
				}

				if ( seed ) {
					// Reintegrate element matches to eliminate the need for sorting
					if ( matchedCount > 0 ) {
						while ( i-- ) {
							if ( !(unmatched[i] || setMatched[i]) ) {
								setMatched[i] = pop.call( results );
							}
						}
					}

					// Discard index placeholder values to get only actual matches
					setMatched = condense( setMatched );
				}

				// Add matches to results
				push.apply( results, setMatched );

				// Seedless set matches succeeding multiple successful matchers stipulate sorting
				if ( outermost && !seed && setMatched.length > 0 &&
					( matchedCount + setMatchers.length ) > 1 ) {

					Sizzle.uniqueSort( results );
				}
			}

			// Override manipulation of globals by nested matchers
			if ( outermost ) {
				dirruns = dirrunsUnique;
				outermostContext = contextBackup;
			}

			return unmatched;
		};

	return bySet ?
		markFunction( superMatcher ) :
		superMatcher;
}

compile = Sizzle.compile = function( selector, match /* Internal Use Only */ ) {
	var i,
		setMatchers = [],
		elementMatchers = [],
		cached = compilerCache[ selector + " " ];

	if ( !cached ) {
		// Generate a function of recursive functions that can be used to check each element
		if ( !match ) {
			match = tokenize( selector );
		}
		i = match.length;
		while ( i-- ) {
			cached = matcherFromTokens( match[i] );
			if ( cached[ expando ] ) {
				setMatchers.push( cached );
			} else {
				elementMatchers.push( cached );
			}
		}

		// Cache the compiled function
		cached = compilerCache( selector, matcherFromGroupMatchers( elementMatchers, setMatchers ) );

		// Save selector and tokenization
		cached.selector = selector;
	}
	return cached;
};

/**
 * A low-level selection function that works with Sizzle's compiled
 *  selector functions
 * @param {String|Function} selector A selector or a pre-compiled
 *  selector function built with Sizzle.compile
 * @param {Element} context
 * @param {Array} [results]
 * @param {Array} [seed] A set of elements to match against
 */
select = Sizzle.select = function( selector, context, results, seed ) {
	var i, tokens, token, type, find,
		compiled = typeof selector === "function" && selector,
		match = !seed && tokenize( (selector = compiled.selector || selector) );

	results = results || [];

	// Try to minimize operations if there is only one selector in the list and no seed
	// (the latter of which guarantees us context)
	if ( match.length === 1 ) {

		// Reduce context if the leading compound selector is an ID
		tokens = match[0] = match[0].slice( 0 );
		if ( tokens.length > 2 && (token = tokens[0]).type === "ID" &&
				context.nodeType === 9 && documentIsHTML && Expr.relative[ tokens[1].type ] ) {

			context = ( Expr.find["ID"]( token.matches[0].replace(runescape, funescape), context ) || [] )[0];
			if ( !context ) {
				return results;

			// Precompiled matchers will still verify ancestry, so step up a level
			} else if ( compiled ) {
				context = context.parentNode;
			}

			selector = selector.slice( tokens.shift().value.length );
		}

		// Fetch a seed set for right-to-left matching
		i = matchExpr["needsContext"].test( selector ) ? 0 : tokens.length;
		while ( i-- ) {
			token = tokens[i];

			// Abort if we hit a combinator
			if ( Expr.relative[ (type = token.type) ] ) {
				break;
			}
			if ( (find = Expr.find[ type ]) ) {
				// Search, expanding context for leading sibling combinators
				if ( (seed = find(
					token.matches[0].replace( runescape, funescape ),
					rsibling.test( tokens[0].type ) && testContext( context.parentNode ) || context
				)) ) {

					// If seed is empty or no tokens remain, we can return early
					tokens.splice( i, 1 );
					selector = seed.length && toSelector( tokens );
					if ( !selector ) {
						push.apply( results, seed );
						return results;
					}

					break;
				}
			}
		}
	}

	// Compile and execute a filtering function if one is not provided
	// Provide `match` to avoid retokenization if we modified the selector above
	( compiled || compile( selector, match ) )(
		seed,
		context,
		!documentIsHTML,
		results,
		!context || rsibling.test( selector ) && testContext( context.parentNode ) || context
	);
	return results;
};

// One-time assignments

// Sort stability
support.sortStable = expando.split("").sort( sortOrder ).join("") === expando;

// Support: Chrome 14-35+
// Always assume duplicates if they aren't passed to the comparison function
support.detectDuplicates = !!hasDuplicate;

// Initialize against the default document
setDocument();

// Support: Webkit<537.32 - Safari 6.0.3/Chrome 25 (fixed in Chrome 27)
// Detached nodes confoundingly follow *each other*
support.sortDetached = assert(function( el ) {
	// Should return 1, but returns 4 (following)
	return el.compareDocumentPosition( document.createElement("fieldset") ) & 1;
});

// Support: IE<8
// Prevent attribute/property "interpolation"
// https://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
if ( !assert(function( el ) {
	el.innerHTML = "<a href='#'></a>";
	return el.firstChild.getAttribute("href") === "#" ;
}) ) {
	addHandle( "type|href|height|width", function( elem, name, isXML ) {
		if ( !isXML ) {
			return elem.getAttribute( name, name.toLowerCase() === "type" ? 1 : 2 );
		}
	});
}

// Support: IE<9
// Use defaultValue in place of getAttribute("value")
if ( !support.attributes || !assert(function( el ) {
	el.innerHTML = "<input/>";
	el.firstChild.setAttribute( "value", "" );
	return el.firstChild.getAttribute( "value" ) === "";
}) ) {
	addHandle( "value", function( elem, name, isXML ) {
		if ( !isXML && elem.nodeName.toLowerCase() === "input" ) {
			return elem.defaultValue;
		}
	});
}

// Support: IE<9
// Use getAttributeNode to fetch booleans when getAttribute lies
if ( !assert(function( el ) {
	return el.getAttribute("disabled") == null;
}) ) {
	addHandle( booleans, function( elem, name, isXML ) {
		var val;
		if ( !isXML ) {
			return elem[ name ] === true ? name.toLowerCase() :
					(val = elem.getAttributeNode( name )) && val.specified ?
					val.value :
				null;
		}
	});
}

return Sizzle;

})( window );



jQuery.find = Sizzle;
jQuery.expr = Sizzle.selectors;

// Deprecated
jQuery.expr[ ":" ] = jQuery.expr.pseudos;
jQuery.uniqueSort = jQuery.unique = Sizzle.uniqueSort;
jQuery.text = Sizzle.getText;
jQuery.isXMLDoc = Sizzle.isXML;
jQuery.contains = Sizzle.contains;
jQuery.escapeSelector = Sizzle.escape;




var dir = function( elem, dir, until ) {
	var matched = [],
		truncate = until !== undefined;

	while ( ( elem = elem[ dir ] ) && elem.nodeType !== 9 ) {
		if ( elem.nodeType === 1 ) {
			if ( truncate && jQuery( elem ).is( until ) ) {
				break;
			}
			matched.push( elem );
		}
	}
	return matched;
};


var siblings = function( n, elem ) {
	var matched = [];

	for ( ; n; n = n.nextSibling ) {
		if ( n.nodeType === 1 && n !== elem ) {
			matched.push( n );
		}
	}

	return matched;
};


var rneedsContext = jQuery.expr.match.needsContext;



function nodeName( elem, name ) {

  return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();

};
var rsingleTag = ( /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i );



var risSimple = /^.[^:#\[\.,]*$/;

// Implement the identical functionality for filter and not
function winnow( elements, qualifier, not ) {
	if ( jQuery.isFunction( qualifier ) ) {
		return jQuery.grep( elements, function( elem, i ) {
			return !!qualifier.call( elem, i, elem ) !== not;
		} );
	}

	// Single element
	if ( qualifier.nodeType ) {
		return jQuery.grep( elements, function( elem ) {
			return ( elem === qualifier ) !== not;
		} );
	}

	// Arraylike of elements (jQuery, arguments, Array)
	if ( typeof qualifier !== "string" ) {
		return jQuery.grep( elements, function( elem ) {
			return ( indexOf.call( qualifier, elem ) > -1 ) !== not;
		} );
	}

	// Simple selector that can be filtered directly, removing non-Elements
	if ( risSimple.test( qualifier ) ) {
		return jQuery.filter( qualifier, elements, not );
	}

	// Complex selector, compare the two sets, removing non-Elements
	qualifier = jQuery.filter( qualifier, elements );
	return jQuery.grep( elements, function( elem ) {
		return ( indexOf.call( qualifier, elem ) > -1 ) !== not && elem.nodeType === 1;
	} );
}

jQuery.filter = function( expr, elems, not ) {
	var elem = elems[ 0 ];

	if ( not ) {
		expr = ":not(" + expr + ")";
	}

	if ( elems.length === 1 && elem.nodeType === 1 ) {
		return jQuery.find.matchesSelector( elem, expr ) ? [ elem ] : [];
	}

	return jQuery.find.matches( expr, jQuery.grep( elems, function( elem ) {
		return elem.nodeType === 1;
	} ) );
};

jQuery.fn.extend( {
	find: function( selector ) {
		var i, ret,
			len = this.length,
			self = this;

		if ( typeof selector !== "string" ) {
			return this.pushStack( jQuery( selector ).filter( function() {
				for ( i = 0; i < len; i++ ) {
					if ( jQuery.contains( self[ i ], this ) ) {
						return true;
					}
				}
			} ) );
		}

		ret = this.pushStack( [] );

		for ( i = 0; i < len; i++ ) {
			jQuery.find( selector, self[ i ], ret );
		}

		return len > 1 ? jQuery.uniqueSort( ret ) : ret;
	},
	filter: function( selector ) {
		return this.pushStack( winnow( this, selector || [], false ) );
	},
	not: function( selector ) {
		return this.pushStack( winnow( this, selector || [], true ) );
	},
	is: function( selector ) {
		return !!winnow(
			this,

			// If this is a positional/relative selector, check membership in the returned set
			// so $("p:first").is("p:last") won't return true for a doc with two "p".
			typeof selector === "string" && rneedsContext.test( selector ) ?
				jQuery( selector ) :
				selector || [],
			false
		).length;
	}
} );


// Initialize a jQuery object


// A central reference to the root jQuery(document)
var rootjQuery,

	// A simple way to check for HTML strings
	// Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
	// Strict HTML recognition (#11290: must start with <)
	// Shortcut simple #id case for speed
	rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,

	init = jQuery.fn.init = function( selector, context, root ) {
		var match, elem;

		// HANDLE: $(""), $(null), $(undefined), $(false)
		if ( !selector ) {
			return this;
		}

		// Method init() accepts an alternate rootjQuery
		// so migrate can support jQuery.sub (gh-2101)
		root = root || rootjQuery;

		// Handle HTML strings
		if ( typeof selector === "string" ) {
			if ( selector[ 0 ] === "<" &&
				selector[ selector.length - 1 ] === ">" &&
				selector.length >= 3 ) {

				// Assume that strings that start and end with <> are HTML and skip the regex check
				match = [ null, selector, null ];

			} else {
				match = rquickExpr.exec( selector );
			}

			// Match html or make sure no context is specified for #id
			if ( match && ( match[ 1 ] || !context ) ) {

				// HANDLE: $(html) -> $(array)
				if ( match[ 1 ] ) {
					context = context instanceof jQuery ? context[ 0 ] : context;

					// Option to run scripts is true for back-compat
					// Intentionally let the error be thrown if parseHTML is not present
					jQuery.merge( this, jQuery.parseHTML(
						match[ 1 ],
						context && context.nodeType ? context.ownerDocument || context : document,
						true
					) );

					// HANDLE: $(html, props)
					if ( rsingleTag.test( match[ 1 ] ) && jQuery.isPlainObject( context ) ) {
						for ( match in context ) {

							// Properties of context are called as methods if possible
							if ( jQuery.isFunction( this[ match ] ) ) {
								this[ match ]( context[ match ] );

							// ...and otherwise set as attributes
							} else {
								this.attr( match, context[ match ] );
							}
						}
					}

					return this;

				// HANDLE: $(#id)
				} else {
					elem = document.getElementById( match[ 2 ] );

					if ( elem ) {

						// Inject the element directly into the jQuery object
						this[ 0 ] = elem;
						this.length = 1;
					}
					return this;
				}

			// HANDLE: $(expr, $(...))
			} else if ( !context || context.jquery ) {
				return ( context || root ).find( selector );

			// HANDLE: $(expr, context)
			// (which is just equivalent to: $(context).find(expr)
			} else {
				return this.constructor( context ).find( selector );
			}

		// HANDLE: $(DOMElement)
		} else if ( selector.nodeType ) {
			this[ 0 ] = selector;
			this.length = 1;
			return this;

		// HANDLE: $(function)
		// Shortcut for document ready
		} else if ( jQuery.isFunction( selector ) ) {
			return root.ready !== undefined ?
				root.ready( selector ) :

				// Execute immediately if ready is not present
				selector( jQuery );
		}

		return jQuery.makeArray( selector, this );
	};

// Give the init function the jQuery prototype for later instantiation
init.prototype = jQuery.fn;

// Initialize central reference
rootjQuery = jQuery( document );


var rparentsprev = /^(?:parents|prev(?:Until|All))/,

	// Methods guaranteed to produce a unique set when starting from a unique set
	guaranteedUnique = {
		children: true,
		contents: true,
		next: true,
		prev: true
	};

jQuery.fn.extend( {
	has: function( target ) {
		var targets = jQuery( target, this ),
			l = targets.length;

		return this.filter( function() {
			var i = 0;
			for ( ; i < l; i++ ) {
				if ( jQuery.contains( this, targets[ i ] ) ) {
					return true;
				}
			}
		} );
	},

	closest: function( selectors, context ) {
		var cur,
			i = 0,
			l = this.length,
			matched = [],
			targets = typeof selectors !== "string" && jQuery( selectors );

		// Positional selectors never match, since there's no _selection_ context
		if ( !rneedsContext.test( selectors ) ) {
			for ( ; i < l; i++ ) {
				for ( cur = this[ i ]; cur && cur !== context; cur = cur.parentNode ) {

					// Always skip document fragments
					if ( cur.nodeType < 11 && ( targets ?
						targets.index( cur ) > -1 :

						// Don't pass non-elements to Sizzle
						cur.nodeType === 1 &&
							jQuery.find.matchesSelector( cur, selectors ) ) ) {

						matched.push( cur );
						break;
					}
				}
			}
		}

		return this.pushStack( matched.length > 1 ? jQuery.uniqueSort( matched ) : matched );
	},

	// Determine the position of an element within the set
	index: function( elem ) {

		// No argument, return index in parent
		if ( !elem ) {
			return ( this[ 0 ] && this[ 0 ].parentNode ) ? this.first().prevAll().length : -1;
		}

		// Index in selector
		if ( typeof elem === "string" ) {
			return indexOf.call( jQuery( elem ), this[ 0 ] );
		}

		// Locate the position of the desired element
		return indexOf.call( this,

			// If it receives a jQuery object, the first element is used
			elem.jquery ? elem[ 0 ] : elem
		);
	},

	add: function( selector, context ) {
		return this.pushStack(
			jQuery.uniqueSort(
				jQuery.merge( this.get(), jQuery( selector, context ) )
			)
		);
	},

	addBack: function( selector ) {
		return this.add( selector == null ?
			this.prevObject : this.prevObject.filter( selector )
		);
	}
} );

function sibling( cur, dir ) {
	while ( ( cur = cur[ dir ] ) && cur.nodeType !== 1 ) {}
	return cur;
}

jQuery.each( {
	parent: function( elem ) {
		var parent = elem.parentNode;
		return parent && parent.nodeType !== 11 ? parent : null;
	},
	parents: function( elem ) {
		return dir( elem, "parentNode" );
	},
	parentsUntil: function( elem, i, until ) {
		return dir( elem, "parentNode", until );
	},
	next: function( elem ) {
		return sibling( elem, "nextSibling" );
	},
	prev: function( elem ) {
		return sibling( elem, "previousSibling" );
	},
	nextAll: function( elem ) {
		return dir( elem, "nextSibling" );
	},
	prevAll: function( elem ) {
		return dir( elem, "previousSibling" );
	},
	nextUntil: function( elem, i, until ) {
		return dir( elem, "nextSibling", until );
	},
	prevUntil: function( elem, i, until ) {
		return dir( elem, "previousSibling", until );
	},
	siblings: function( elem ) {
		return siblings( ( elem.parentNode || {} ).firstChild, elem );
	},
	children: function( elem ) {
		return siblings( elem.firstChild );
	},
	contents: function( elem ) {
        if ( nodeName( elem, "iframe" ) ) {
            return elem.contentDocument;
        }

        // Support: IE 9 - 11 only, iOS 7 only, Android Browser <=4.3 only
        // Treat the template element as a regular one in browsers that
        // don't support it.
        if ( nodeName( elem, "template" ) ) {
            elem = elem.content || elem;
        }

        return jQuery.merge( [], elem.childNodes );
	}
}, function( name, fn ) {
	jQuery.fn[ name ] = function( until, selector ) {
		var matched = jQuery.map( this, fn, until );

		if ( name.slice( -5 ) !== "Until" ) {
			selector = until;
		}

		if ( selector && typeof selector === "string" ) {
			matched = jQuery.filter( selector, matched );
		}

		if ( this.length > 1 ) {

			// Remove duplicates
			if ( !guaranteedUnique[ name ] ) {
				jQuery.uniqueSort( matched );
			}

			// Reverse order for parents* and prev-derivatives
			if ( rparentsprev.test( name ) ) {
				matched.reverse();
			}
		}

		return this.pushStack( matched );
	};
} );
var rnothtmlwhite = ( /[^\x20\t\r\n\f]+/g );



// Convert String-formatted options into Object-formatted ones
function createOptions( options ) {
	var object = {};
	jQuery.each( options.match( rnothtmlwhite ) || [], function( _, flag ) {
		object[ flag ] = true;
	} );
	return object;
}

/*
 * Create a callback list using the following parameters:
 *
 *	options: an optional list of space-separated options that will change how
 *			the callback list behaves or a more traditional option object
 *
 * By default a callback list will act like an event callback list and can be
 * "fired" multiple times.
 *
 * Possible options:
 *
 *	once:			will ensure the callback list can only be fired once (like a Deferred)
 *
 *	memory:			will keep track of previous values and will call any callback added
 *					after the list has been fired right away with the latest "memorized"
 *					values (like a Deferred)
 *
 *	unique:			will ensure a callback can only be added once (no duplicate in the list)
 *
 *	stopOnFalse:	interrupt callings when a callback returns false
 *
 */
jQuery.Callbacks = function( options ) {

	// Convert options from String-formatted to Object-formatted if needed
	// (we check in cache first)
	options = typeof options === "string" ?
		createOptions( options ) :
		jQuery.extend( {}, options );

	var // Flag to know if list is currently firing
		firing,

		// Last fire value for non-forgettable lists
		memory,

		// Flag to know if list was already fired
		fired,

		// Flag to prevent firing
		locked,

		// Actual callback list
		list = [],

		// Queue of execution data for repeatable lists
		queue = [],

		// Index of currently firing callback (modified by add/remove as needed)
		firingIndex = -1,

		// Fire callbacks
		fire = function() {

			// Enforce single-firing
			locked = locked || options.once;

			// Execute callbacks for all pending executions,
			// respecting firingIndex overrides and runtime changes
			fired = firing = true;
			for ( ; queue.length; firingIndex = -1 ) {
				memory = queue.shift();
				while ( ++firingIndex < list.length ) {

					// Run callback and check for early termination
					if ( list[ firingIndex ].apply( memory[ 0 ], memory[ 1 ] ) === false &&
						options.stopOnFalse ) {

						// Jump to end and forget the data so .add doesn't re-fire
						firingIndex = list.length;
						memory = false;
					}
				}
			}

			// Forget the data if we're done with it
			if ( !options.memory ) {
				memory = false;
			}

			firing = false;

			// Clean up if we're done firing for good
			if ( locked ) {

				// Keep an empty list if we have data for future add calls
				if ( memory ) {
					list = [];

				// Otherwise, this object is spent
				} else {
					list = "";
				}
			}
		},

		// Actual Callbacks object
		self = {

			// Add a callback or a collection of callbacks to the list
			add: function() {
				if ( list ) {

					// If we have memory from a past run, we should fire after adding
					if ( memory && !firing ) {
						firingIndex = list.length - 1;
						queue.push( memory );
					}

					( function add( args ) {
						jQuery.each( args, function( _, arg ) {
							if ( jQuery.isFunction( arg ) ) {
								if ( !options.unique || !self.has( arg ) ) {
									list.push( arg );
								}
							} else if ( arg && arg.length && jQuery.type( arg ) !== "string" ) {

								// Inspect recursively
								add( arg );
							}
						} );
					} )( arguments );

					if ( memory && !firing ) {
						fire();
					}
				}
				return this;
			},

			// Remove a callback from the list
			remove: function() {
				jQuery.each( arguments, function( _, arg ) {
					var index;
					while ( ( index = jQuery.inArray( arg, list, index ) ) > -1 ) {
						list.splice( index, 1 );

						// Handle firing indexes
						if ( index <= firingIndex ) {
							firingIndex--;
						}
					}
				} );
				return this;
			},

			// Check if a given callback is in the list.
			// If no argument is given, return whether or not list has callbacks attached.
			has: function( fn ) {
				return fn ?
					jQuery.inArray( fn, list ) > -1 :
					list.length > 0;
			},

			// Remove all callbacks from the list
			empty: function() {
				if ( list ) {
					list = [];
				}
				return this;
			},

			// Disable .fire and .add
			// Abort any current/pending executions
			// Clear all callbacks and values
			disable: function() {
				locked = queue = [];
				list = memory = "";
				return this;
			},
			disabled: function() {
				return !list;
			},

			// Disable .fire
			// Also disable .add unless we have memory (since it would have no effect)
			// Abort any pending executions
			lock: function() {
				locked = queue = [];
				if ( !memory && !firing ) {
					list = memory = "";
				}
				return this;
			},
			locked: function() {
				return !!locked;
			},

			// Call all callbacks with the given context and arguments
			fireWith: function( context, args ) {
				if ( !locked ) {
					args = args || [];
					args = [ context, args.slice ? args.slice() : args ];
					queue.push( args );
					if ( !firing ) {
						fire();
					}
				}
				return this;
			},

			// Call all the callbacks with the given arguments
			fire: function() {
				self.fireWith( this, arguments );
				return this;
			},

			// To know if the callbacks have already been called at least once
			fired: function() {
				return !!fired;
			}
		};

	return self;
};


function Identity( v ) {
	return v;
}
function Thrower( ex ) {
	throw ex;
}

function adoptValue( value, resolve, reject, noValue ) {
	var method;

	try {

		// Check for promise aspect first to privilege synchronous behavior
		if ( value && jQuery.isFunction( ( method = value.promise ) ) ) {
			method.call( value ).done( resolve ).fail( reject );

		// Other thenables
		} else if ( value && jQuery.isFunction( ( method = value.then ) ) ) {
			method.call( value, resolve, reject );

		// Other non-thenables
		} else {

			// Control `resolve` arguments by letting Array#slice cast boolean `noValue` to integer:
			// * false: [ value ].slice( 0 ) => resolve( value )
			// * true: [ value ].slice( 1 ) => resolve()
			resolve.apply( undefined, [ value ].slice( noValue ) );
		}

	// For Promises/A+, convert exceptions into rejections
	// Since jQuery.when doesn't unwrap thenables, we can skip the extra checks appearing in
	// Deferred#then to conditionally suppress rejection.
	} catch ( value ) {

		// Support: Android 4.0 only
		// Strict mode functions invoked without .call/.apply get global-object context
		reject.apply( undefined, [ value ] );
	}
}

jQuery.extend( {

	Deferred: function( func ) {
		var tuples = [

				// action, add listener, callbacks,
				// ... .then handlers, argument index, [final state]
				[ "notify", "progress", jQuery.Callbacks( "memory" ),
					jQuery.Callbacks( "memory" ), 2 ],
				[ "resolve", "done", jQuery.Callbacks( "once memory" ),
					jQuery.Callbacks( "once memory" ), 0, "resolved" ],
				[ "reject", "fail", jQuery.Callbacks( "once memory" ),
					jQuery.Callbacks( "once memory" ), 1, "rejected" ]
			],
			state = "pending",
			promise = {
				state: function() {
					return state;
				},
				always: function() {
					deferred.done( arguments ).fail( arguments );
					return this;
				},
				"catch": function( fn ) {
					return promise.then( null, fn );
				},

				// Keep pipe for back-compat
				pipe: function( /* fnDone, fnFail, fnProgress */ ) {
					var fns = arguments;

					return jQuery.Deferred( function( newDefer ) {
						jQuery.each( tuples, function( i, tuple ) {

							// Map tuples (progress, done, fail) to arguments (done, fail, progress)
							var fn = jQuery.isFunction( fns[ tuple[ 4 ] ] ) && fns[ tuple[ 4 ] ];

							// deferred.progress(function() { bind to newDefer or newDefer.notify })
							// deferred.done(function() { bind to newDefer or newDefer.resolve })
							// deferred.fail(function() { bind to newDefer or newDefer.reject })
							deferred[ tuple[ 1 ] ]( function() {
								var returned = fn && fn.apply( this, arguments );
								if ( returned && jQuery.isFunction( returned.promise ) ) {
									returned.promise()
										.progress( newDefer.notify )
										.done( newDefer.resolve )
										.fail( newDefer.reject );
								} else {
									newDefer[ tuple[ 0 ] + "With" ](
										this,
										fn ? [ returned ] : arguments
									);
								}
							} );
						} );
						fns = null;
					} ).promise();
				},
				then: function( onFulfilled, onRejected, onProgress ) {
					var maxDepth = 0;
					function resolve( depth, deferred, handler, special ) {
						return function() {
							var that = this,
								args = arguments,
								mightThrow = function() {
									var returned, then;

									// Support: Promises/A+ section 2.3.3.3.3
									// https://promisesaplus.com/#point-59
									// Ignore double-resolution attempts
									if ( depth < maxDepth ) {
										return;
									}

									returned = handler.apply( that, args );

									// Support: Promises/A+ section 2.3.1
									// https://promisesaplus.com/#point-48
									if ( returned === deferred.promise() ) {
										throw new TypeError( "Thenable self-resolution" );
									}

									// Support: Promises/A+ sections 2.3.3.1, 3.5
									// https://promisesaplus.com/#point-54
									// https://promisesaplus.com/#point-75
									// Retrieve `then` only once
									then = returned &&

										// Support: Promises/A+ section 2.3.4
										// https://promisesaplus.com/#point-64
										// Only check objects and functions for thenability
										( typeof returned === "object" ||
											typeof returned === "function" ) &&
										returned.then;

									// Handle a returned thenable
									if ( jQuery.isFunction( then ) ) {

										// Special processors (notify) just wait for resolution
										if ( special ) {
											then.call(
												returned,
												resolve( maxDepth, deferred, Identity, special ),
												resolve( maxDepth, deferred, Thrower, special )
											);

										// Normal processors (resolve) also hook into progress
										} else {

											// ...and disregard older resolution values
											maxDepth++;

											then.call(
												returned,
												resolve( maxDepth, deferred, Identity, special ),
												resolve( maxDepth, deferred, Thrower, special ),
												resolve( maxDepth, deferred, Identity,
													deferred.notifyWith )
											);
										}

									// Handle all other returned values
									} else {

										// Only substitute handlers pass on context
										// and multiple values (non-spec behavior)
										if ( handler !== Identity ) {
											that = undefined;
											args = [ returned ];
										}

										// Process the value(s)
										// Default process is resolve
										( special || deferred.resolveWith )( that, args );
									}
								},

								// Only normal processors (resolve) catch and reject exceptions
								process = special ?
									mightThrow :
									function() {
										try {
											mightThrow();
										} catch ( e ) {

											if ( jQuery.Deferred.exceptionHook ) {
												jQuery.Deferred.exceptionHook( e,
													process.stackTrace );
											}

											// Support: Promises/A+ section 2.3.3.3.4.1
											// https://promisesaplus.com/#point-61
											// Ignore post-resolution exceptions
											if ( depth + 1 >= maxDepth ) {

												// Only substitute handlers pass on context
												// and multiple values (non-spec behavior)
												if ( handler !== Thrower ) {
													that = undefined;
													args = [ e ];
												}

												deferred.rejectWith( that, args );
											}
										}
									};

							// Support: Promises/A+ section 2.3.3.3.1
							// https://promisesaplus.com/#point-57
							// Re-resolve promises immediately to dodge false rejection from
							// subsequent errors
							if ( depth ) {
								process();
							} else {

								// Call an optional hook to record the stack, in case of exception
								// since it's otherwise lost when execution goes async
								if ( jQuery.Deferred.getStackHook ) {
									process.stackTrace = jQuery.Deferred.getStackHook();
								}
								window.setTimeout( process );
							}
						};
					}

					return jQuery.Deferred( function( newDefer ) {

						// progress_handlers.add( ... )
						tuples[ 0 ][ 3 ].add(
							resolve(
								0,
								newDefer,
								jQuery.isFunction( onProgress ) ?
									onProgress :
									Identity,
								newDefer.notifyWith
							)
						);

						// fulfilled_handlers.add( ... )
						tuples[ 1 ][ 3 ].add(
							resolve(
								0,
								newDefer,
								jQuery.isFunction( onFulfilled ) ?
									onFulfilled :
									Identity
							)
						);

						// rejected_handlers.add( ... )
						tuples[ 2 ][ 3 ].add(
							resolve(
								0,
								newDefer,
								jQuery.isFunction( onRejected ) ?
									onRejected :
									Thrower
							)
						);
					} ).promise();
				},

				// Get a promise for this deferred
				// If obj is provided, the promise aspect is added to the object
				promise: function( obj ) {
					return obj != null ? jQuery.extend( obj, promise ) : promise;
				}
			},
			deferred = {};

		// Add list-specific methods
		jQuery.each( tuples, function( i, tuple ) {
			var list = tuple[ 2 ],
				stateString = tuple[ 5 ];

			// promise.progress = list.add
			// promise.done = list.add
			// promise.fail = list.add
			promise[ tuple[ 1 ] ] = list.add;

			// Handle state
			if ( stateString ) {
				list.add(
					function() {

						// state = "resolved" (i.e., fulfilled)
						// state = "rejected"
						state = stateString;
					},

					// rejected_callbacks.disable
					// fulfilled_callbacks.disable
					tuples[ 3 - i ][ 2 ].disable,

					// progress_callbacks.lock
					tuples[ 0 ][ 2 ].lock
				);
			}

			// progress_handlers.fire
			// fulfilled_handlers.fire
			// rejected_handlers.fire
			list.add( tuple[ 3 ].fire );

			// deferred.notify = function() { deferred.notifyWith(...) }
			// deferred.resolve = function() { deferred.resolveWith(...) }
			// deferred.reject = function() { deferred.rejectWith(...) }
			deferred[ tuple[ 0 ] ] = function() {
				deferred[ tuple[ 0 ] + "With" ]( this === deferred ? undefined : this, arguments );
				return this;
			};

			// deferred.notifyWith = list.fireWith
			// deferred.resolveWith = list.fireWith
			// deferred.rejectWith = list.fireWith
			deferred[ tuple[ 0 ] + "With" ] = list.fireWith;
		} );

		// Make the deferred a promise
		promise.promise( deferred );

		// Call given func if any
		if ( func ) {
			func.call( deferred, deferred );
		}

		// All done!
		return deferred;
	},

	// Deferred helper
	when: function( singleValue ) {
		var

			// count of uncompleted subordinates
			remaining = arguments.length,

			// count of unprocessed arguments
			i = remaining,

			// subordinate fulfillment data
			resolveContexts = Array( i ),
			resolveValues = slice.call( arguments ),

			// the master Deferred
			master = jQuery.Deferred(),

			// subordinate callback factory
			updateFunc = function( i ) {
				return function( value ) {
					resolveContexts[ i ] = this;
					resolveValues[ i ] = arguments.length > 1 ? slice.call( arguments ) : value;
					if ( !( --remaining ) ) {
						master.resolveWith( resolveContexts, resolveValues );
					}
				};
			};

		// Single- and empty arguments are adopted like Promise.resolve
		if ( remaining <= 1 ) {
			adoptValue( singleValue, master.done( updateFunc( i ) ).resolve, master.reject,
				!remaining );

			// Use .then() to unwrap secondary thenables (cf. gh-3000)
			if ( master.state() === "pending" ||
				jQuery.isFunction( resolveValues[ i ] && resolveValues[ i ].then ) ) {

				return master.then();
			}
		}

		// Multiple arguments are aggregated like Promise.all array elements
		while ( i-- ) {
			adoptValue( resolveValues[ i ], updateFunc( i ), master.reject );
		}

		return master.promise();
	}
} );


// These usually indicate a programmer mistake during development,
// warn about them ASAP rather than swallowing them by default.
var rerrorNames = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;

jQuery.Deferred.exceptionHook = function( error, stack ) {

	// Support: IE 8 - 9 only
	// Console exists when dev tools are open, which can happen at any time
	if ( window.console && window.console.warn && error && rerrorNames.test( error.name ) ) {
		window.console.warn( "jQuery.Deferred exception: " + error.message, error.stack, stack );
	}
};




jQuery.readyException = function( error ) {
	window.setTimeout( function() {
		throw error;
	} );
};




// The deferred used on DOM ready
var readyList = jQuery.Deferred();

jQuery.fn.ready = function( fn ) {

	readyList
		.then( fn )

		// Wrap jQuery.readyException in a function so that the lookup
		// happens at the time of error handling instead of callback
		// registration.
		.catch( function( error ) {
			jQuery.readyException( error );
		} );

	return this;
};

jQuery.extend( {

	// Is the DOM ready to be used? Set to true once it occurs.
	isReady: false,

	// A counter to track how many items to wait for before
	// the ready event fires. See #6781
	readyWait: 1,

	// Handle when the DOM is ready
	ready: function( wait ) {

		// Abort if there are pending holds or we're already ready
		if ( wait === true ? --jQuery.readyWait : jQuery.isReady ) {
			return;
		}

		// Remember that the DOM is ready
		jQuery.isReady = true;

		// If a normal DOM Ready event fired, decrement, and wait if need be
		if ( wait !== true && --jQuery.readyWait > 0 ) {
			return;
		}

		// If there are functions bound, to execute
		readyList.resolveWith( document, [ jQuery ] );
	}
} );

jQuery.ready.then = readyList.then;

// The ready event handler and self cleanup method
function completed() {
	document.removeEventListener( "DOMContentLoaded", completed );
	window.removeEventListener( "load", completed );
	jQuery.ready();
}

// Catch cases where $(document).ready() is called
// after the browser event has already occurred.
// Support: IE <=9 - 10 only
// Older IE sometimes signals "interactive" too soon
if ( document.readyState === "complete" ||
	( document.readyState !== "loading" && !document.documentElement.doScroll ) ) {

	// Handle it asynchronously to allow scripts the opportunity to delay ready
	window.setTimeout( jQuery.ready );

} else {

	// Use the handy event callback
	document.addEventListener( "DOMContentLoaded", completed );

	// A fallback to window.onload, that will always work
	window.addEventListener( "load", completed );
}




// Multifunctional method to get and set values of a collection
// The value/s can optionally be executed if it's a function
var access = function( elems, fn, key, value, chainable, emptyGet, raw ) {
	var i = 0,
		len = elems.length,
		bulk = key == null;

	// Sets many values
	if ( jQuery.type( key ) === "object" ) {
		chainable = true;
		for ( i in key ) {
			access( elems, fn, i, key[ i ], true, emptyGet, raw );
		}

	// Sets one value
	} else if ( value !== undefined ) {
		chainable = true;

		if ( !jQuery.isFunction( value ) ) {
			raw = true;
		}

		if ( bulk ) {

			// Bulk operations run against the entire set
			if ( raw ) {
				fn.call( elems, value );
				fn = null;

			// ...except when executing function values
			} else {
				bulk = fn;
				fn = function( elem, key, value ) {
					return bulk.call( jQuery( elem ), value );
				};
			}
		}

		if ( fn ) {
			for ( ; i < len; i++ ) {
				fn(
					elems[ i ], key, raw ?
					value :
					value.call( elems[ i ], i, fn( elems[ i ], key ) )
				);
			}
		}
	}

	if ( chainable ) {
		return elems;
	}

	// Gets
	if ( bulk ) {
		return fn.call( elems );
	}

	return len ? fn( elems[ 0 ], key ) : emptyGet;
};
var acceptData = function( owner ) {

	// Accepts only:
	//  - Node
	//    - Node.ELEMENT_NODE
	//    - Node.DOCUMENT_NODE
	//  - Object
	//    - Any
	return owner.nodeType === 1 || owner.nodeType === 9 || !( +owner.nodeType );
};




function Data() {
	this.expando = jQuery.expando + Data.uid++;
}

Data.uid = 1;

Data.prototype = {

	cache: function( owner ) {

		// Check if the owner object already has a cache
		var value = owner[ this.expando ];

		// If not, create one
		if ( !value ) {
			value = {};

			// We can accept data for non-element nodes in modern browsers,
			// but we should not, see #8335.
			// Always return an empty object.
			if ( acceptData( owner ) ) {

				// If it is a node unlikely to be stringify-ed or looped over
				// use plain assignment
				if ( owner.nodeType ) {
					owner[ this.expando ] = value;

				// Otherwise secure it in a non-enumerable property
				// configurable must be true to allow the property to be
				// deleted when data is removed
				} else {
					Object.defineProperty( owner, this.expando, {
						value: value,
						configurable: true
					} );
				}
			}
		}

		return value;
	},
	set: function( owner, data, value ) {
		var prop,
			cache = this.cache( owner );

		// Handle: [ owner, key, value ] args
		// Always use camelCase key (gh-2257)
		if ( typeof data === "string" ) {
			cache[ jQuery.camelCase( data ) ] = value;

		// Handle: [ owner, { properties } ] args
		} else {

			// Copy the properties one-by-one to the cache object
			for ( prop in data ) {
				cache[ jQuery.camelCase( prop ) ] = data[ prop ];
			}
		}
		return cache;
	},
	get: function( owner, key ) {
		return key === undefined ?
			this.cache( owner ) :

			// Always use camelCase key (gh-2257)
			owner[ this.expando ] && owner[ this.expando ][ jQuery.camelCase( key ) ];
	},
	access: function( owner, key, value ) {

		// In cases where either:
		//
		//   1. No key was specified
		//   2. A string key was specified, but no value provided
		//
		// Take the "read" path and allow the get method to determine
		// which value to return, respectively either:
		//
		//   1. The entire cache object
		//   2. The data stored at the key
		//
		if ( key === undefined ||
				( ( key && typeof key === "string" ) && value === undefined ) ) {

			return this.get( owner, key );
		}

		// When the key is not a string, or both a key and value
		// are specified, set or extend (existing objects) with either:
		//
		//   1. An object of properties
		//   2. A key and value
		//
		this.set( owner, key, value );

		// Since the "set" path can have two possible entry points
		// return the expected data based on which path was taken[*]
		return value !== undefined ? value : key;
	},
	remove: function( owner, key ) {
		var i,
			cache = owner[ this.expando ];

		if ( cache === undefined ) {
			return;
		}

		if ( key !== undefined ) {

			// Support array or space separated string of keys
			if ( Array.isArray( key ) ) {

				// If key is an array of keys...
				// We always set camelCase keys, so remove that.
				key = key.map( jQuery.camelCase );
			} else {
				key = jQuery.camelCase( key );

				// If a key with the spaces exists, use it.
				// Otherwise, create an array by matching non-whitespace
				key = key in cache ?
					[ key ] :
					( key.match( rnothtmlwhite ) || [] );
			}

			i = key.length;

			while ( i-- ) {
				delete cache[ key[ i ] ];
			}
		}

		// Remove the expando if there's no more data
		if ( key === undefined || jQuery.isEmptyObject( cache ) ) {

			// Support: Chrome <=35 - 45
			// Webkit & Blink performance suffers when deleting properties
			// from DOM nodes, so set to undefined instead
			// https://bugs.chromium.org/p/chromium/issues/detail?id=378607 (bug restricted)
			if ( owner.nodeType ) {
				owner[ this.expando ] = undefined;
			} else {
				delete owner[ this.expando ];
			}
		}
	},
	hasData: function( owner ) {
		var cache = owner[ this.expando ];
		return cache !== undefined && !jQuery.isEmptyObject( cache );
	}
};
var dataPriv = new Data();

var dataUser = new Data();



//	Implementation Summary
//
//	1. Enforce API surface and semantic compatibility with 1.9.x branch
//	2. Improve the module's maintainability by reducing the storage
//		paths to a single mechanism.
//	3. Use the same single mechanism to support "private" and "user" data.
//	4. _Never_ expose "private" data to user code (TODO: Drop _data, _removeData)
//	5. Avoid exposing implementation details on user objects (eg. expando properties)
//	6. Provide a clear path for implementation upgrade to WeakMap in 2014

var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
	rmultiDash = /[A-Z]/g;

function getData( data ) {
	if ( data === "true" ) {
		return true;
	}

	if ( data === "false" ) {
		return false;
	}

	if ( data === "null" ) {
		return null;
	}

	// Only convert to a number if it doesn't change the string
	if ( data === +data + "" ) {
		return +data;
	}

	if ( rbrace.test( data ) ) {
		return JSON.parse( data );
	}

	return data;
}

function dataAttr( elem, key, data ) {
	var name;

	// If nothing was found internally, try to fetch any
	// data from the HTML5 data-* attribute
	if ( data === undefined && elem.nodeType === 1 ) {
		name = "data-" + key.replace( rmultiDash, "-$&" ).toLowerCase();
		data = elem.getAttribute( name );

		if ( typeof data === "string" ) {
			try {
				data = getData( data );
			} catch ( e ) {}

			// Make sure we set the data so it isn't changed later
			dataUser.set( elem, key, data );
		} else {
			data = undefined;
		}
	}
	return data;
}

jQuery.extend( {
	hasData: function( elem ) {
		return dataUser.hasData( elem ) || dataPriv.hasData( elem );
	},

	data: function( elem, name, data ) {
		return dataUser.access( elem, name, data );
	},

	removeData: function( elem, name ) {
		dataUser.remove( elem, name );
	},

	// TODO: Now that all calls to _data and _removeData have been replaced
	// with direct calls to dataPriv methods, these can be deprecated.
	_data: function( elem, name, data ) {
		return dataPriv.access( elem, name, data );
	},

	_removeData: function( elem, name ) {
		dataPriv.remove( elem, name );
	}
} );

jQuery.fn.extend( {
	data: function( key, value ) {
		var i, name, data,
			elem = this[ 0 ],
			attrs = elem && elem.attributes;

		// Gets all values
		if ( key === undefined ) {
			if ( this.length ) {
				data = dataUser.get( elem );

				if ( elem.nodeType === 1 && !dataPriv.get( elem, "hasDataAttrs" ) ) {
					i = attrs.length;
					while ( i-- ) {

						// Support: IE 11 only
						// The attrs elements can be null (#14894)
						if ( attrs[ i ] ) {
							name = attrs[ i ].name;
							if ( name.indexOf( "data-" ) === 0 ) {
								name = jQuery.camelCase( name.slice( 5 ) );
								dataAttr( elem, name, data[ name ] );
							}
						}
					}
					dataPriv.set( elem, "hasDataAttrs", true );
				}
			}

			return data;
		}

		// Sets multiple values
		if ( typeof key === "object" ) {
			return this.each( function() {
				dataUser.set( this, key );
			} );
		}

		return access( this, function( value ) {
			var data;

			// The calling jQuery object (element matches) is not empty
			// (and therefore has an element appears at this[ 0 ]) and the
			// `value` parameter was not undefined. An empty jQuery object
			// will result in `undefined` for elem = this[ 0 ] which will
			// throw an exception if an attempt to read a data cache is made.
			if ( elem && value === undefined ) {

				// Attempt to get data from the cache
				// The key will always be camelCased in Data
				data = dataUser.get( elem, key );
				if ( data !== undefined ) {
					return data;
				}

				// Attempt to "discover" the data in
				// HTML5 custom data-* attrs
				data = dataAttr( elem, key );
				if ( data !== undefined ) {
					return data;
				}

				// We tried really hard, but the data doesn't exist.
				return;
			}

			// Set the data...
			this.each( function() {

				// We always store the camelCased key
				dataUser.set( this, key, value );
			} );
		}, null, value, arguments.length > 1, null, true );
	},

	removeData: function( key ) {
		return this.each( function() {
			dataUser.remove( this, key );
		} );
	}
} );


jQuery.extend( {
	queue: function( elem, type, data ) {
		var queue;

		if ( elem ) {
			type = ( type || "fx" ) + "queue";
			queue = dataPriv.get( elem, type );

			// Speed up dequeue by getting out quickly if this is just a lookup
			if ( data ) {
				if ( !queue || Array.isArray( data ) ) {
					queue = dataPriv.access( elem, type, jQuery.makeArray( data ) );
				} else {
					queue.push( data );
				}
			}
			return queue || [];
		}
	},

	dequeue: function( elem, type ) {
		type = type || "fx";

		var queue = jQuery.queue( elem, type ),
			startLength = queue.length,
			fn = queue.shift(),
			hooks = jQuery._queueHooks( elem, type ),
			next = function() {
				jQuery.dequeue( elem, type );
			};

		// If the fx queue is dequeued, always remove the progress sentinel
		if ( fn === "inprogress" ) {
			fn = queue.shift();
			startLength--;
		}

		if ( fn ) {

			// Add a progress sentinel to prevent the fx queue from being
			// automatically dequeued
			if ( type === "fx" ) {
				queue.unshift( "inprogress" );
			}

			// Clear up the last queue stop function
			delete hooks.stop;
			fn.call( elem, next, hooks );
		}

		if ( !startLength && hooks ) {
			hooks.empty.fire();
		}
	},

	// Not public - generate a queueHooks object, or return the current one
	_queueHooks: function( elem, type ) {
		var key = type + "queueHooks";
		return dataPriv.get( elem, key ) || dataPriv.access( elem, key, {
			empty: jQuery.Callbacks( "once memory" ).add( function() {
				dataPriv.remove( elem, [ type + "queue", key ] );
			} )
		} );
	}
} );

jQuery.fn.extend( {
	queue: function( type, data ) {
		var setter = 2;

		if ( typeof type !== "string" ) {
			data = type;
			type = "fx";
			setter--;
		}

		if ( arguments.length < setter ) {
			return jQuery.queue( this[ 0 ], type );
		}

		return data === undefined ?
			this :
			this.each( function() {
				var queue = jQuery.queue( this, type, data );

				// Ensure a hooks for this queue
				jQuery._queueHooks( this, type );

				if ( type === "fx" && queue[ 0 ] !== "inprogress" ) {
					jQuery.dequeue( this, type );
				}
			} );
	},
	dequeue: function( type ) {
		return this.each( function() {
			jQuery.dequeue( this, type );
		} );
	},
	clearQueue: function( type ) {
		return this.queue( type || "fx", [] );
	},

	// Get a promise resolved when queues of a certain type
	// are emptied (fx is the type by default)
	promise: function( type, obj ) {
		var tmp,
			count = 1,
			defer = jQuery.Deferred(),
			elements = this,
			i = this.length,
			resolve = function() {
				if ( !( --count ) ) {
					defer.resolveWith( elements, [ elements ] );
				}
			};

		if ( typeof type !== "string" ) {
			obj = type;
			type = undefined;
		}
		type = type || "fx";

		while ( i-- ) {
			tmp = dataPriv.get( elements[ i ], type + "queueHooks" );
			if ( tmp && tmp.empty ) {
				count++;
				tmp.empty.add( resolve );
			}
		}
		resolve();
		return defer.promise( obj );
	}
} );
var pnum = ( /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/ ).source;

var rcssNum = new RegExp( "^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i" );


var cssExpand = [ "Top", "Right", "Bottom", "Left" ];

var isHiddenWithinTree = function( elem, el ) {

		// isHiddenWithinTree might be called from jQuery#filter function;
		// in that case, element will be second argument
		elem = el || elem;

		// Inline style trumps all
		return elem.style.display === "none" ||
			elem.style.display === "" &&

			// Otherwise, check computed style
			// Support: Firefox <=43 - 45
			// Disconnected elements can have computed display: none, so first confirm that elem is
			// in the document.
			jQuery.contains( elem.ownerDocument, elem ) &&

			jQuery.css( elem, "display" ) === "none";
	};

var swap = function( elem, options, callback, args ) {
	var ret, name,
		old = {};

	// Remember the old values, and insert the new ones
	for ( name in options ) {
		old[ name ] = elem.style[ name ];
		elem.style[ name ] = options[ name ];
	}

	ret = callback.apply( elem, args || [] );

	// Revert the old values
	for ( name in options ) {
		elem.style[ name ] = old[ name ];
	}

	return ret;
};




function adjustCSS( elem, prop, valueParts, tween ) {
	var adjusted,
		scale = 1,
		maxIterations = 20,
		currentValue = tween ?
			function() {
				return tween.cur();
			} :
			function() {
				return jQuery.css( elem, prop, "" );
			},
		initial = currentValue(),
		unit = valueParts && valueParts[ 3 ] || ( jQuery.cssNumber[ prop ] ? "" : "px" ),

		// Starting value computation is required for potential unit mismatches
		initialInUnit = ( jQuery.cssNumber[ prop ] || unit !== "px" && +initial ) &&
			rcssNum.exec( jQuery.css( elem, prop ) );

	if ( initialInUnit && initialInUnit[ 3 ] !== unit ) {

		// Trust units reported by jQuery.css
		unit = unit || initialInUnit[ 3 ];

		// Make sure we update the tween properties later on
		valueParts = valueParts || [];

		// Iteratively approximate from a nonzero starting point
		initialInUnit = +initial || 1;

		do {

			// If previous iteration zeroed out, double until we get *something*.
			// Use string for doubling so we don't accidentally see scale as unchanged below
			scale = scale || ".5";

			// Adjust and apply
			initialInUnit = initialInUnit / scale;
			jQuery.style( elem, prop, initialInUnit + unit );

		// Update scale, tolerating zero or NaN from tween.cur()
		// Break the loop if scale is unchanged or perfect, or if we've just had enough.
		} while (
			scale !== ( scale = currentValue() / initial ) && scale !== 1 && --maxIterations
		);
	}

	if ( valueParts ) {
		initialInUnit = +initialInUnit || +initial || 0;

		// Apply relative offset (+=/-=) if specified
		adjusted = valueParts[ 1 ] ?
			initialInUnit + ( valueParts[ 1 ] + 1 ) * valueParts[ 2 ] :
			+valueParts[ 2 ];
		if ( tween ) {
			tween.unit = unit;
			tween.start = initialInUnit;
			tween.end = adjusted;
		}
	}
	return adjusted;
}


var defaultDisplayMap = {};

function getDefaultDisplay( elem ) {
	var temp,
		doc = elem.ownerDocument,
		nodeName = elem.nodeName,
		display = defaultDisplayMap[ nodeName ];

	if ( display ) {
		return display;
	}

	temp = doc.body.appendChild( doc.createElement( nodeName ) );
	display = jQuery.css( temp, "display" );

	temp.parentNode.removeChild( temp );

	if ( display === "none" ) {
		display = "block";
	}
	defaultDisplayMap[ nodeName ] = display;

	return display;
}

function showHide( elements, show ) {
	var display, elem,
		values = [],
		index = 0,
		length = elements.length;

	// Determine new display value for elements that need to change
	for ( ; index < length; index++ ) {
		elem = elements[ index ];
		if ( !elem.style ) {
			continue;
		}

		display = elem.style.display;
		if ( show ) {

			// Since we force visibility upon cascade-hidden elements, an immediate (and slow)
			// check is required in this first loop unless we have a nonempty display value (either
			// inline or about-to-be-restored)
			if ( display === "none" ) {
				values[ index ] = dataPriv.get( elem, "display" ) || null;
				if ( !values[ index ] ) {
					elem.style.display = "";
				}
			}
			if ( elem.style.display === "" && isHiddenWithinTree( elem ) ) {
				values[ index ] = getDefaultDisplay( elem );
			}
		} else {
			if ( display !== "none" ) {
				values[ index ] = "none";

				// Remember what we're overwriting
				dataPriv.set( elem, "display", display );
			}
		}
	}

	// Set the display of the elements in a second loop to avoid constant reflow
	for ( index = 0; index < length; index++ ) {
		if ( values[ index ] != null ) {
			elements[ index ].style.display = values[ index ];
		}
	}

	return elements;
}

jQuery.fn.extend( {
	show: function() {
		return showHide( this, true );
	},
	hide: function() {
		return showHide( this );
	},
	toggle: function( state ) {
		if ( typeof state === "boolean" ) {
			return state ? this.show() : this.hide();
		}

		return this.each( function() {
			if ( isHiddenWithinTree( this ) ) {
				jQuery( this ).show();
			} else {
				jQuery( this ).hide();
			}
		} );
	}
} );
var rcheckableType = ( /^(?:checkbox|radio)$/i );

var rtagName = ( /<([a-z][^\/\0>\x20\t\r\n\f]+)/i );

var rscriptType = ( /^$|\/(?:java|ecma)script/i );



// We have to close these tags to support XHTML (#13200)
var wrapMap = {

	// Support: IE <=9 only
	option: [ 1, "<select multiple='multiple'>", "</select>" ],

	// XHTML parsers do not magically insert elements in the
	// same way that tag soup parsers do. So we cannot shorten
	// this by omitting <tbody> or other required elements.
	thead: [ 1, "<table>", "</table>" ],
	col: [ 2, "<table><colgroup>", "</colgroup></table>" ],
	tr: [ 2, "<table><tbody>", "</tbody></table>" ],
	td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],

	_default: [ 0, "", "" ]
};

// Support: IE <=9 only
wrapMap.optgroup = wrapMap.option;

wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
wrapMap.th = wrapMap.td;


function getAll( context, tag ) {

	// Support: IE <=9 - 11 only
	// Use typeof to avoid zero-argument method invocation on host objects (#15151)
	var ret;

	if ( typeof context.getElementsByTagName !== "undefined" ) {
		ret = context.getElementsByTagName( tag || "*" );

	} else if ( typeof context.querySelectorAll !== "undefined" ) {
		ret = context.querySelectorAll( tag || "*" );

	} else {
		ret = [];
	}

	if ( tag === undefined || tag && nodeName( context, tag ) ) {
		return jQuery.merge( [ context ], ret );
	}

	return ret;
}


// Mark scripts as having already been evaluated
function setGlobalEval( elems, refElements ) {
	var i = 0,
		l = elems.length;

	for ( ; i < l; i++ ) {
		dataPriv.set(
			elems[ i ],
			"globalEval",
			!refElements || dataPriv.get( refElements[ i ], "globalEval" )
		);
	}
}


var rhtml = /<|&#?\w+;/;

function buildFragment( elems, context, scripts, selection, ignored ) {
	var elem, tmp, tag, wrap, contains, j,
		fragment = context.createDocumentFragment(),
		nodes = [],
		i = 0,
		l = elems.length;

	for ( ; i < l; i++ ) {
		elem = elems[ i ];

		if ( elem || elem === 0 ) {

			// Add nodes directly
			if ( jQuery.type( elem ) === "object" ) {

				// Support: Android <=4.0 only, PhantomJS 1 only
				// push.apply(_, arraylike) throws on ancient WebKit
				jQuery.merge( nodes, elem.nodeType ? [ elem ] : elem );

			// Convert non-html into a text node
			} else if ( !rhtml.test( elem ) ) {
				nodes.push( context.createTextNode( elem ) );

			// Convert html into DOM nodes
			} else {
				tmp = tmp || fragment.appendChild( context.createElement( "div" ) );

				// Deserialize a standard representation
				tag = ( rtagName.exec( elem ) || [ "", "" ] )[ 1 ].toLowerCase();
				wrap = wrapMap[ tag ] || wrapMap._default;
				tmp.innerHTML = wrap[ 1 ] + jQuery.htmlPrefilter( elem ) + wrap[ 2 ];

				// Descend through wrappers to the right content
				j = wrap[ 0 ];
				while ( j-- ) {
					tmp = tmp.lastChild;
				}

				// Support: Android <=4.0 only, PhantomJS 1 only
				// push.apply(_, arraylike) throws on ancient WebKit
				jQuery.merge( nodes, tmp.childNodes );

				// Remember the top-level container
				tmp = fragment.firstChild;

				// Ensure the created nodes are orphaned (#12392)
				tmp.textContent = "";
			}
		}
	}

	// Remove wrapper from fragment
	fragment.textContent = "";

	i = 0;
	while ( ( elem = nodes[ i++ ] ) ) {

		// Skip elements already in the context collection (trac-4087)
		if ( selection && jQuery.inArray( elem, selection ) > -1 ) {
			if ( ignored ) {
				ignored.push( elem );
			}
			continue;
		}

		contains = jQuery.contains( elem.ownerDocument, elem );

		// Append to fragment
		tmp = getAll( fragment.appendChild( elem ), "script" );

		// Preserve script evaluation history
		if ( contains ) {
			setGlobalEval( tmp );
		}

		// Capture executables
		if ( scripts ) {
			j = 0;
			while ( ( elem = tmp[ j++ ] ) ) {
				if ( rscriptType.test( elem.type || "" ) ) {
					scripts.push( elem );
				}
			}
		}
	}

	return fragment;
}


( function() {
	var fragment = document.createDocumentFragment(),
		div = fragment.appendChild( document.createElement( "div" ) ),
		input = document.createElement( "input" );

	// Support: Android 4.0 - 4.3 only
	// Check state lost if the name is set (#11217)
	// Support: Windows Web Apps (WWA)
	// `name` and `type` must use .setAttribute for WWA (#14901)
	input.setAttribute( "type", "radio" );
	input.setAttribute( "checked", "checked" );
	input.setAttribute( "name", "t" );

	div.appendChild( input );

	// Support: Android <=4.1 only
	// Older WebKit doesn't clone checked state correctly in fragments
	support.checkClone = div.cloneNode( true ).cloneNode( true ).lastChild.checked;

	// Support: IE <=11 only
	// Make sure textarea (and checkbox) defaultValue is properly cloned
	div.innerHTML = "<textarea>x</textarea>";
	support.noCloneChecked = !!div.cloneNode( true ).lastChild.defaultValue;
} )();
var documentElement = document.documentElement;



var
	rkeyEvent = /^key/,
	rmouseEvent = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
	rtypenamespace = /^([^.]*)(?:\.(.+)|)/;

function returnTrue() {
	return true;
}

function returnFalse() {
	return false;
}

// Support: IE <=9 only
// See #13393 for more info
function safeActiveElement() {
	try {
		return document.activeElement;
	} catch ( err ) { }
}

function on( elem, types, selector, data, fn, one ) {
	var origFn, type;

	// Types can be a map of types/handlers
	if ( typeof types === "object" ) {

		// ( types-Object, selector, data )
		if ( typeof selector !== "string" ) {

			// ( types-Object, data )
			data = data || selector;
			selector = undefined;
		}
		for ( type in types ) {
			on( elem, type, selector, data, types[ type ], one );
		}
		return elem;
	}

	if ( data == null && fn == null ) {

		// ( types, fn )
		fn = selector;
		data = selector = undefined;
	} else if ( fn == null ) {
		if ( typeof selector === "string" ) {

			// ( types, selector, fn )
			fn = data;
			data = undefined;
		} else {

			// ( types, data, fn )
			fn = data;
			data = selector;
			selector = undefined;
		}
	}
	if ( fn === false ) {
		fn = returnFalse;
	} else if ( !fn ) {
		return elem;
	}

	if ( one === 1 ) {
		origFn = fn;
		fn = function( event ) {

			// Can use an empty set, since event contains the info
			jQuery().off( event );
			return origFn.apply( this, arguments );
		};

		// Use same guid so caller can remove using origFn
		fn.guid = origFn.guid || ( origFn.guid = jQuery.guid++ );
	}
	return elem.each( function() {
		jQuery.event.add( this, types, fn, data, selector );
	} );
}

/*
 * Helper functions for managing events -- not part of the public interface.
 * Props to Dean Edwards' addEvent library for many of the ideas.
 */
jQuery.event = {

	global: {},

	add: function( elem, types, handler, data, selector ) {

		var handleObjIn, eventHandle, tmp,
			events, t, handleObj,
			special, handlers, type, namespaces, origType,
			elemData = dataPriv.get( elem );

		// Don't attach events to noData or text/comment nodes (but allow plain objects)
		if ( !elemData ) {
			return;
		}

		// Caller can pass in an object of custom data in lieu of the handler
		if ( handler.handler ) {
			handleObjIn = handler;
			handler = handleObjIn.handler;
			selector = handleObjIn.selector;
		}

		// Ensure that invalid selectors throw exceptions at attach time
		// Evaluate against documentElement in case elem is a non-element node (e.g., document)
		if ( selector ) {
			jQuery.find.matchesSelector( documentElement, selector );
		}

		// Make sure that the handler has a unique ID, used to find/remove it later
		if ( !handler.guid ) {
			handler.guid = jQuery.guid++;
		}

		// Init the element's event structure and main handler, if this is the first
		if ( !( events = elemData.events ) ) {
			events = elemData.events = {};
		}
		if ( !( eventHandle = elemData.handle ) ) {
			eventHandle = elemData.handle = function( e ) {

				// Discard the second event of a jQuery.event.trigger() and
				// when an event is called after a page has unloaded
				return typeof jQuery !== "undefined" && jQuery.event.triggered !== e.type ?
					jQuery.event.dispatch.apply( elem, arguments ) : undefined;
			};
		}

		// Handle multiple events separated by a space
		types = ( types || "" ).match( rnothtmlwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[ t ] ) || [];
			type = origType = tmp[ 1 ];
			namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();

			// There *must* be a type, no attaching namespace-only handlers
			if ( !type ) {
				continue;
			}

			// If event changes its type, use the special event handlers for the changed type
			special = jQuery.event.special[ type ] || {};

			// If selector defined, determine special event api type, otherwise given type
			type = ( selector ? special.delegateType : special.bindType ) || type;

			// Update special based on newly reset type
			special = jQuery.event.special[ type ] || {};

			// handleObj is passed to all event handlers
			handleObj = jQuery.extend( {
				type: type,
				origType: origType,
				data: data,
				handler: handler,
				guid: handler.guid,
				selector: selector,
				needsContext: selector && jQuery.expr.match.needsContext.test( selector ),
				namespace: namespaces.join( "." )
			}, handleObjIn );

			// Init the event handler queue if we're the first
			if ( !( handlers = events[ type ] ) ) {
				handlers = events[ type ] = [];
				handlers.delegateCount = 0;

				// Only use addEventListener if the special events handler returns false
				if ( !special.setup ||
					special.setup.call( elem, data, namespaces, eventHandle ) === false ) {

					if ( elem.addEventListener ) {
						elem.addEventListener( type, eventHandle );
					}
				}
			}

			if ( special.add ) {
				special.add.call( elem, handleObj );

				if ( !handleObj.handler.guid ) {
					handleObj.handler.guid = handler.guid;
				}
			}

			// Add to the element's handler list, delegates in front
			if ( selector ) {
				handlers.splice( handlers.delegateCount++, 0, handleObj );
			} else {
				handlers.push( handleObj );
			}

			// Keep track of which events have ever been used, for event optimization
			jQuery.event.global[ type ] = true;
		}

	},

	// Detach an event or set of events from an element
	remove: function( elem, types, handler, selector, mappedTypes ) {

		var j, origCount, tmp,
			events, t, handleObj,
			special, handlers, type, namespaces, origType,
			elemData = dataPriv.hasData( elem ) && dataPriv.get( elem );

		if ( !elemData || !( events = elemData.events ) ) {
			return;
		}

		// Once for each type.namespace in types; type may be omitted
		types = ( types || "" ).match( rnothtmlwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[ t ] ) || [];
			type = origType = tmp[ 1 ];
			namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();

			// Unbind all events (on this namespace, if provided) for the element
			if ( !type ) {
				for ( type in events ) {
					jQuery.event.remove( elem, type + types[ t ], handler, selector, true );
				}
				continue;
			}

			special = jQuery.event.special[ type ] || {};
			type = ( selector ? special.delegateType : special.bindType ) || type;
			handlers = events[ type ] || [];
			tmp = tmp[ 2 ] &&
				new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" );

			// Remove matching events
			origCount = j = handlers.length;
			while ( j-- ) {
				handleObj = handlers[ j ];

				if ( ( mappedTypes || origType === handleObj.origType ) &&
					( !handler || handler.guid === handleObj.guid ) &&
					( !tmp || tmp.test( handleObj.namespace ) ) &&
					( !selector || selector === handleObj.selector ||
						selector === "**" && handleObj.selector ) ) {
					handlers.splice( j, 1 );

					if ( handleObj.selector ) {
						handlers.delegateCount--;
					}
					if ( special.remove ) {
						special.remove.call( elem, handleObj );
					}
				}
			}

			// Remove generic event handler if we removed something and no more handlers exist
			// (avoids potential for endless recursion during removal of special event handlers)
			if ( origCount && !handlers.length ) {
				if ( !special.teardown ||
					special.teardown.call( elem, namespaces, elemData.handle ) === false ) {

					jQuery.removeEvent( elem, type, elemData.handle );
				}

				delete events[ type ];
			}
		}

		// Remove data and the expando if it's no longer used
		if ( jQuery.isEmptyObject( events ) ) {
			dataPriv.remove( elem, "handle events" );
		}
	},

	dispatch: function( nativeEvent ) {

		// Make a writable jQuery.Event from the native event object
		var event = jQuery.event.fix( nativeEvent );

		var i, j, ret, matched, handleObj, handlerQueue,
			args = new Array( arguments.length ),
			handlers = ( dataPriv.get( this, "events" ) || {} )[ event.type ] || [],
			special = jQuery.event.special[ event.type ] || {};

		// Use the fix-ed jQuery.Event rather than the (read-only) native event
		args[ 0 ] = event;

		for ( i = 1; i < arguments.length; i++ ) {
			args[ i ] = arguments[ i ];
		}

		event.delegateTarget = this;

		// Call the preDispatch hook for the mapped type, and let it bail if desired
		if ( special.preDispatch && special.preDispatch.call( this, event ) === false ) {
			return;
		}

		// Determine handlers
		handlerQueue = jQuery.event.handlers.call( this, event, handlers );

		// Run delegates first; they may want to stop propagation beneath us
		i = 0;
		while ( ( matched = handlerQueue[ i++ ] ) && !event.isPropagationStopped() ) {
			event.currentTarget = matched.elem;

			j = 0;
			while ( ( handleObj = matched.handlers[ j++ ] ) &&
				!event.isImmediatePropagationStopped() ) {

				// Triggered event must either 1) have no namespace, or 2) have namespace(s)
				// a subset or equal to those in the bound event (both can have no namespace).
				if ( !event.rnamespace || event.rnamespace.test( handleObj.namespace ) ) {

					event.handleObj = handleObj;
					event.data = handleObj.data;

					ret = ( ( jQuery.event.special[ handleObj.origType ] || {} ).handle ||
						handleObj.handler ).apply( matched.elem, args );

					if ( ret !== undefined ) {
						if ( ( event.result = ret ) === false ) {
							event.preventDefault();
							event.stopPropagation();
						}
					}
				}
			}
		}

		// Call the postDispatch hook for the mapped type
		if ( special.postDispatch ) {
			special.postDispatch.call( this, event );
		}

		return event.result;
	},

	handlers: function( event, handlers ) {
		var i, handleObj, sel, matchedHandlers, matchedSelectors,
			handlerQueue = [],
			delegateCount = handlers.delegateCount,
			cur = event.target;

		// Find delegate handlers
		if ( delegateCount &&

			// Support: IE <=9
			// Black-hole SVG <use> instance trees (trac-13180)
			cur.nodeType &&

			// Support: Firefox <=42
			// Suppress spec-violating clicks indicating a non-primary pointer button (trac-3861)
			// https://www.w3.org/TR/DOM-Level-3-Events/#event-type-click
			// Support: IE 11 only
			// ...but not arrow key "clicks" of radio inputs, which can have `button` -1 (gh-2343)
			!( event.type === "click" && event.button >= 1 ) ) {

			for ( ; cur !== this; cur = cur.parentNode || this ) {

				// Don't check non-elements (#13208)
				// Don't process clicks on disabled elements (#6911, #8165, #11382, #11764)
				if ( cur.nodeType === 1 && !( event.type === "click" && cur.disabled === true ) ) {
					matchedHandlers = [];
					matchedSelectors = {};
					for ( i = 0; i < delegateCount; i++ ) {
						handleObj = handlers[ i ];

						// Don't conflict with Object.prototype properties (#13203)
						sel = handleObj.selector + " ";

						if ( matchedSelectors[ sel ] === undefined ) {
							matchedSelectors[ sel ] = handleObj.needsContext ?
								jQuery( sel, this ).index( cur ) > -1 :
								jQuery.find( sel, this, null, [ cur ] ).length;
						}
						if ( matchedSelectors[ sel ] ) {
							matchedHandlers.push( handleObj );
						}
					}
					if ( matchedHandlers.length ) {
						handlerQueue.push( { elem: cur, handlers: matchedHandlers } );
					}
				}
			}
		}

		// Add the remaining (directly-bound) handlers
		cur = this;
		if ( delegateCount < handlers.length ) {
			handlerQueue.push( { elem: cur, handlers: handlers.slice( delegateCount ) } );
		}

		return handlerQueue;
	},

	addProp: function( name, hook ) {
		Object.defineProperty( jQuery.Event.prototype, name, {
			enumerable: true,
			configurable: true,

			get: jQuery.isFunction( hook ) ?
				function() {
					if ( this.originalEvent ) {
							return hook( this.originalEvent );
					}
				} :
				function() {
					if ( this.originalEvent ) {
							return this.originalEvent[ name ];
					}
				},

			set: function( value ) {
				Object.defineProperty( this, name, {
					enumerable: true,
					configurable: true,
					writable: true,
					value: value
				} );
			}
		} );
	},

	fix: function( originalEvent ) {
		return originalEvent[ jQuery.expando ] ?
			originalEvent :
			new jQuery.Event( originalEvent );
	},

	special: {
		load: {

			// Prevent triggered image.load events from bubbling to window.load
			noBubble: true
		},
		focus: {

			// Fire native event if possible so blur/focus sequence is correct
			trigger: function() {
				if ( this !== safeActiveElement() && this.focus ) {
					this.focus();
					return false;
				}
			},
			delegateType: "focusin"
		},
		blur: {
			trigger: function() {
				if ( this === safeActiveElement() && this.blur ) {
					this.blur();
					return false;
				}
			},
			delegateType: "focusout"
		},
		click: {

			// For checkbox, fire native event so checked state will be right
			trigger: function() {
				if ( this.type === "checkbox" && this.click && nodeName( this, "input" ) ) {
					this.click();
					return false;
				}
			},

			// For cross-browser consistency, don't fire native .click() on links
			_default: function( event ) {
				return nodeName( event.target, "a" );
			}
		},

		beforeunload: {
			postDispatch: function( event ) {

				// Support: Firefox 20+
				// Firefox doesn't alert if the returnValue field is not set.
				if ( event.result !== undefined && event.originalEvent ) {
					event.originalEvent.returnValue = event.result;
				}
			}
		}
	}
};

jQuery.removeEvent = function( elem, type, handle ) {

	// This "if" is needed for plain objects
	if ( elem.removeEventListener ) {
		elem.removeEventListener( type, handle );
	}
};

jQuery.Event = function( src, props ) {

	// Allow instantiation without the 'new' keyword
	if ( !( this instanceof jQuery.Event ) ) {
		return new jQuery.Event( src, props );
	}

	// Event object
	if ( src && src.type ) {
		this.originalEvent = src;
		this.type = src.type;

		// Events bubbling up the document may have been marked as prevented
		// by a handler lower down the tree; reflect the correct value.
		this.isDefaultPrevented = src.defaultPrevented ||
				src.defaultPrevented === undefined &&

				// Support: Android <=2.3 only
				src.returnValue === false ?
			returnTrue :
			returnFalse;

		// Create target properties
		// Support: Safari <=6 - 7 only
		// Target should not be a text node (#504, #13143)
		this.target = ( src.target && src.target.nodeType === 3 ) ?
			src.target.parentNode :
			src.target;

		this.currentTarget = src.currentTarget;
		this.relatedTarget = src.relatedTarget;

	// Event type
	} else {
		this.type = src;
	}

	// Put explicitly provided properties onto the event object
	if ( props ) {
		jQuery.extend( this, props );
	}

	// Create a timestamp if incoming event doesn't have one
	this.timeStamp = src && src.timeStamp || jQuery.now();

	// Mark it as fixed
	this[ jQuery.expando ] = true;
};

// jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
// https://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
jQuery.Event.prototype = {
	constructor: jQuery.Event,
	isDefaultPrevented: returnFalse,
	isPropagationStopped: returnFalse,
	isImmediatePropagationStopped: returnFalse,
	isSimulated: false,

	preventDefault: function() {
		var e = this.originalEvent;

		this.isDefaultPrevented = returnTrue;

		if ( e && !this.isSimulated ) {
			e.preventDefault();
		}
	},
	stopPropagation: function() {
		var e = this.originalEvent;

		this.isPropagationStopped = returnTrue;

		if ( e && !this.isSimulated ) {
			e.stopPropagation();
		}
	},
	stopImmediatePropagation: function() {
		var e = this.originalEvent;

		this.isImmediatePropagationStopped = returnTrue;

		if ( e && !this.isSimulated ) {
			e.stopImmediatePropagation();
		}

		this.stopPropagation();
	}
};

// Includes all common event props including KeyEvent and MouseEvent specific props
jQuery.each( {
	altKey: true,
	bubbles: true,
	cancelable: true,
	changedTouches: true,
	ctrlKey: true,
	detail: true,
	eventPhase: true,
	metaKey: true,
	pageX: true,
	pageY: true,
	shiftKey: true,
	view: true,
	"char": true,
	charCode: true,
	key: true,
	keyCode: true,
	button: true,
	buttons: true,
	clientX: true,
	clientY: true,
	offsetX: true,
	offsetY: true,
	pointerId: true,
	pointerType: true,
	screenX: true,
	screenY: true,
	targetTouches: true,
	toElement: true,
	touches: true,

	which: function( event ) {
		var button = event.button;

		// Add which for key events
		if ( event.which == null && rkeyEvent.test( event.type ) ) {
			return event.charCode != null ? event.charCode : event.keyCode;
		}

		// Add which for click: 1 === left; 2 === middle; 3 === right
		if ( !event.which && button !== undefined && rmouseEvent.test( event.type ) ) {
			if ( button & 1 ) {
				return 1;
			}

			if ( button & 2 ) {
				return 3;
			}

			if ( button & 4 ) {
				return 2;
			}

			return 0;
		}

		return event.which;
	}
}, jQuery.event.addProp );

// Create mouseenter/leave events using mouseover/out and event-time checks
// so that event delegation works in jQuery.
// Do the same for pointerenter/pointerleave and pointerover/pointerout
//
// Support: Safari 7 only
// Safari sends mouseenter too often; see:
// https://bugs.chromium.org/p/chromium/issues/detail?id=470258
// for the description of the bug (it existed in older Chrome versions as well).
jQuery.each( {
	mouseenter: "mouseover",
	mouseleave: "mouseout",
	pointerenter: "pointerover",
	pointerleave: "pointerout"
}, function( orig, fix ) {
	jQuery.event.special[ orig ] = {
		delegateType: fix,
		bindType: fix,

		handle: function( event ) {
			var ret,
				target = this,
				related = event.relatedTarget,
				handleObj = event.handleObj;

			// For mouseenter/leave call the handler if related is outside the target.
			// NB: No relatedTarget if the mouse left/entered the browser window
			if ( !related || ( related !== target && !jQuery.contains( target, related ) ) ) {
				event.type = handleObj.origType;
				ret = handleObj.handler.apply( this, arguments );
				event.type = fix;
			}
			return ret;
		}
	};
} );

jQuery.fn.extend( {

	on: function( types, selector, data, fn ) {
		return on( this, types, selector, data, fn );
	},
	one: function( types, selector, data, fn ) {
		return on( this, types, selector, data, fn, 1 );
	},
	off: function( types, selector, fn ) {
		var handleObj, type;
		if ( types && types.preventDefault && types.handleObj ) {

			// ( event )  dispatched jQuery.Event
			handleObj = types.handleObj;
			jQuery( types.delegateTarget ).off(
				handleObj.namespace ?
					handleObj.origType + "." + handleObj.namespace :
					handleObj.origType,
				handleObj.selector,
				handleObj.handler
			);
			return this;
		}
		if ( typeof types === "object" ) {

			// ( types-object [, selector] )
			for ( type in types ) {
				this.off( type, selector, types[ type ] );
			}
			return this;
		}
		if ( selector === false || typeof selector === "function" ) {

			// ( types [, fn] )
			fn = selector;
			selector = undefined;
		}
		if ( fn === false ) {
			fn = returnFalse;
		}
		return this.each( function() {
			jQuery.event.remove( this, types, fn, selector );
		} );
	}
} );


var

	/* eslint-disable max-len */

	// See https://github.com/eslint/eslint/issues/3229
	rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,

	/* eslint-enable */

	// Support: IE <=10 - 11, Edge 12 - 13
	// In IE/Edge using regex groups here causes severe slowdowns.
	// See https://connect.microsoft.com/IE/feedback/details/1736512/
	rnoInnerhtml = /<script|<style|<link/i,

	// checked="checked" or checked
	rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
	rscriptTypeMasked = /^true\/(.*)/,
	rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

// Prefer a tbody over its parent table for containing new rows
function manipulationTarget( elem, content ) {
	if ( nodeName( elem, "table" ) &&
		nodeName( content.nodeType !== 11 ? content : content.firstChild, "tr" ) ) {

		return jQuery( ">tbody", elem )[ 0 ] || elem;
	}

	return elem;
}

// Replace/restore the type attribute of script elements for safe DOM manipulation
function disableScript( elem ) {
	elem.type = ( elem.getAttribute( "type" ) !== null ) + "/" + elem.type;
	return elem;
}
function restoreScript( elem ) {
	var match = rscriptTypeMasked.exec( elem.type );

	if ( match ) {
		elem.type = match[ 1 ];
	} else {
		elem.removeAttribute( "type" );
	}

	return elem;
}

function cloneCopyEvent( src, dest ) {
	var i, l, type, pdataOld, pdataCur, udataOld, udataCur, events;

	if ( dest.nodeType !== 1 ) {
		return;
	}

	// 1. Copy private data: events, handlers, etc.
	if ( dataPriv.hasData( src ) ) {
		pdataOld = dataPriv.access( src );
		pdataCur = dataPriv.set( dest, pdataOld );
		events = pdataOld.events;

		if ( events ) {
			delete pdataCur.handle;
			pdataCur.events = {};

			for ( type in events ) {
				for ( i = 0, l = events[ type ].length; i < l; i++ ) {
					jQuery.event.add( dest, type, events[ type ][ i ] );
				}
			}
		}
	}

	// 2. Copy user data
	if ( dataUser.hasData( src ) ) {
		udataOld = dataUser.access( src );
		udataCur = jQuery.extend( {}, udataOld );

		dataUser.set( dest, udataCur );
	}
}

// Fix IE bugs, see support tests
function fixInput( src, dest ) {
	var nodeName = dest.nodeName.toLowerCase();

	// Fails to persist the checked state of a cloned checkbox or radio button.
	if ( nodeName === "input" && rcheckableType.test( src.type ) ) {
		dest.checked = src.checked;

	// Fails to return the selected option to the default selected state when cloning options
	} else if ( nodeName === "input" || nodeName === "textarea" ) {
		dest.defaultValue = src.defaultValue;
	}
}

function domManip( collection, args, callback, ignored ) {

	// Flatten any nested arrays
	args = concat.apply( [], args );

	var fragment, first, scripts, hasScripts, node, doc,
		i = 0,
		l = collection.length,
		iNoClone = l - 1,
		value = args[ 0 ],
		isFunction = jQuery.isFunction( value );

	// We can't cloneNode fragments that contain checked, in WebKit
	if ( isFunction ||
			( l > 1 && typeof value === "string" &&
				!support.checkClone && rchecked.test( value ) ) ) {
		return collection.each( function( index ) {
			var self = collection.eq( index );
			if ( isFunction ) {
				args[ 0 ] = value.call( this, index, self.html() );
			}
			domManip( self, args, callback, ignored );
		} );
	}

	if ( l ) {
		fragment = buildFragment( args, collection[ 0 ].ownerDocument, false, collection, ignored );
		first = fragment.firstChild;

		if ( fragment.childNodes.length === 1 ) {
			fragment = first;
		}

		// Require either new content or an interest in ignored elements to invoke the callback
		if ( first || ignored ) {
			scripts = jQuery.map( getAll( fragment, "script" ), disableScript );
			hasScripts = scripts.length;

			// Use the original fragment for the last item
			// instead of the first because it can end up
			// being emptied incorrectly in certain situations (#8070).
			for ( ; i < l; i++ ) {
				node = fragment;

				if ( i !== iNoClone ) {
					node = jQuery.clone( node, true, true );

					// Keep references to cloned scripts for later restoration
					if ( hasScripts ) {

						// Support: Android <=4.0 only, PhantomJS 1 only
						// push.apply(_, arraylike) throws on ancient WebKit
						jQuery.merge( scripts, getAll( node, "script" ) );
					}
				}

				callback.call( collection[ i ], node, i );
			}

			if ( hasScripts ) {
				doc = scripts[ scripts.length - 1 ].ownerDocument;

				// Reenable scripts
				jQuery.map( scripts, restoreScript );

				// Evaluate executable scripts on first document insertion
				for ( i = 0; i < hasScripts; i++ ) {
					node = scripts[ i ];
					if ( rscriptType.test( node.type || "" ) &&
						!dataPriv.access( node, "globalEval" ) &&
						jQuery.contains( doc, node ) ) {

						if ( node.src ) {

							// Optional AJAX dependency, but won't run scripts if not present
							if ( jQuery._evalUrl ) {
								jQuery._evalUrl( node.src );
							}
						} else {
							DOMEval( node.textContent.replace( rcleanScript, "" ), doc );
						}
					}
				}
			}
		}
	}

	return collection;
}

function remove( elem, selector, keepData ) {
	var node,
		nodes = selector ? jQuery.filter( selector, elem ) : elem,
		i = 0;

	for ( ; ( node = nodes[ i ] ) != null; i++ ) {
		if ( !keepData && node.nodeType === 1 ) {
			jQuery.cleanData( getAll( node ) );
		}

		if ( node.parentNode ) {
			if ( keepData && jQuery.contains( node.ownerDocument, node ) ) {
				setGlobalEval( getAll( node, "script" ) );
			}
			node.parentNode.removeChild( node );
		}
	}

	return elem;
}

jQuery.extend( {
	htmlPrefilter: function( html ) {
		return html.replace( rxhtmlTag, "<$1></$2>" );
	},

	clone: function( elem, dataAndEvents, deepDataAndEvents ) {
		var i, l, srcElements, destElements,
			clone = elem.cloneNode( true ),
			inPage = jQuery.contains( elem.ownerDocument, elem );

		// Fix IE cloning issues
		if ( !support.noCloneChecked && ( elem.nodeType === 1 || elem.nodeType === 11 ) &&
				!jQuery.isXMLDoc( elem ) ) {

			// We eschew Sizzle here for performance reasons: https://jsperf.com/getall-vs-sizzle/2
			destElements = getAll( clone );
			srcElements = getAll( elem );

			for ( i = 0, l = srcElements.length; i < l; i++ ) {
				fixInput( srcElements[ i ], destElements[ i ] );
			}
		}

		// Copy the events from the original to the clone
		if ( dataAndEvents ) {
			if ( deepDataAndEvents ) {
				srcElements = srcElements || getAll( elem );
				destElements = destElements || getAll( clone );

				for ( i = 0, l = srcElements.length; i < l; i++ ) {
					cloneCopyEvent( srcElements[ i ], destElements[ i ] );
				}
			} else {
				cloneCopyEvent( elem, clone );
			}
		}

		// Preserve script evaluation history
		destElements = getAll( clone, "script" );
		if ( destElements.length > 0 ) {
			setGlobalEval( destElements, !inPage && getAll( elem, "script" ) );
		}

		// Return the cloned set
		return clone;
	},

	cleanData: function( elems ) {
		var data, elem, type,
			special = jQuery.event.special,
			i = 0;

		for ( ; ( elem = elems[ i ] ) !== undefined; i++ ) {
			if ( acceptData( elem ) ) {
				if ( ( data = elem[ dataPriv.expando ] ) ) {
					if ( data.events ) {
						for ( type in data.events ) {
							if ( special[ type ] ) {
								jQuery.event.remove( elem, type );

							// This is a shortcut to avoid jQuery.event.remove's overhead
							} else {
								jQuery.removeEvent( elem, type, data.handle );
							}
						}
					}

					// Support: Chrome <=35 - 45+
					// Assign undefined instead of using delete, see Data#remove
					elem[ dataPriv.expando ] = undefined;
				}
				if ( elem[ dataUser.expando ] ) {

					// Support: Chrome <=35 - 45+
					// Assign undefined instead of using delete, see Data#remove
					elem[ dataUser.expando ] = undefined;
				}
			}
		}
	}
} );

jQuery.fn.extend( {
	detach: function( selector ) {
		return remove( this, selector, true );
	},

	remove: function( selector ) {
		return remove( this, selector );
	},

	text: function( value ) {
		return access( this, function( value ) {
			return value === undefined ?
				jQuery.text( this ) :
				this.empty().each( function() {
					if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
						this.textContent = value;
					}
				} );
		}, null, value, arguments.length );
	},

	append: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.appendChild( elem );
			}
		} );
	},

	prepend: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.insertBefore( elem, target.firstChild );
			}
		} );
	},

	before: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this );
			}
		} );
	},

	after: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this.nextSibling );
			}
		} );
	},

	empty: function() {
		var elem,
			i = 0;

		for ( ; ( elem = this[ i ] ) != null; i++ ) {
			if ( elem.nodeType === 1 ) {

				// Prevent memory leaks
				jQuery.cleanData( getAll( elem, false ) );

				// Remove any remaining nodes
				elem.textContent = "";
			}
		}

		return this;
	},

	clone: function( dataAndEvents, deepDataAndEvents ) {
		dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
		deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;

		return this.map( function() {
			return jQuery.clone( this, dataAndEvents, deepDataAndEvents );
		} );
	},

	html: function( value ) {
		return access( this, function( value ) {
			var elem = this[ 0 ] || {},
				i = 0,
				l = this.length;

			if ( value === undefined && elem.nodeType === 1 ) {
				return elem.innerHTML;
			}

			// See if we can take a shortcut and just use innerHTML
			if ( typeof value === "string" && !rnoInnerhtml.test( value ) &&
				!wrapMap[ ( rtagName.exec( value ) || [ "", "" ] )[ 1 ].toLowerCase() ] ) {

				value = jQuery.htmlPrefilter( value );

				try {
					for ( ; i < l; i++ ) {
						elem = this[ i ] || {};

						// Remove element nodes and prevent memory leaks
						if ( elem.nodeType === 1 ) {
							jQuery.cleanData( getAll( elem, false ) );
							elem.innerHTML = value;
						}
					}

					elem = 0;

				// If using innerHTML throws an exception, use the fallback method
				} catch ( e ) {}
			}

			if ( elem ) {
				this.empty().append( value );
			}
		}, null, value, arguments.length );
	},

	replaceWith: function() {
		var ignored = [];

		// Make the changes, replacing each non-ignored context element with the new content
		return domManip( this, arguments, function( elem ) {
			var parent = this.parentNode;

			if ( jQuery.inArray( this, ignored ) < 0 ) {
				jQuery.cleanData( getAll( this ) );
				if ( parent ) {
					parent.replaceChild( elem, this );
				}
			}

		// Force callback invocation
		}, ignored );
	}
} );

jQuery.each( {
	appendTo: "append",
	prependTo: "prepend",
	insertBefore: "before",
	insertAfter: "after",
	replaceAll: "replaceWith"
}, function( name, original ) {
	jQuery.fn[ name ] = function( selector ) {
		var elems,
			ret = [],
			insert = jQuery( selector ),
			last = insert.length - 1,
			i = 0;

		for ( ; i <= last; i++ ) {
			elems = i === last ? this : this.clone( true );
			jQuery( insert[ i ] )[ original ]( elems );

			// Support: Android <=4.0 only, PhantomJS 1 only
			// .get() because push.apply(_, arraylike) throws on ancient WebKit
			push.apply( ret, elems.get() );
		}

		return this.pushStack( ret );
	};
} );
var rmargin = ( /^margin/ );

var rnumnonpx = new RegExp( "^(" + pnum + ")(?!px)[a-z%]+$", "i" );

var getStyles = function( elem ) {

		// Support: IE <=11 only, Firefox <=30 (#15098, #14150)
		// IE throws on elements created in popups
		// FF meanwhile throws on frame elements through "defaultView.getComputedStyle"
		var view = elem.ownerDocument.defaultView;

		if ( !view || !view.opener ) {
			view = window;
		}

		return view.getComputedStyle( elem );
	};



( function() {

	// Executing both pixelPosition & boxSizingReliable tests require only one layout
	// so they're executed at the same time to save the second computation.
	function computeStyleTests() {

		// This is a singleton, we need to execute it only once
		if ( !div ) {
			return;
		}

		div.style.cssText =
			"box-sizing:border-box;" +
			"position:relative;display:block;" +
			"margin:auto;border:1px;padding:1px;" +
			"top:1%;width:50%";
		div.innerHTML = "";
		documentElement.appendChild( container );

		var divStyle = window.getComputedStyle( div );
		pixelPositionVal = divStyle.top !== "1%";

		// Support: Android 4.0 - 4.3 only, Firefox <=3 - 44
		reliableMarginLeftVal = divStyle.marginLeft === "2px";
		boxSizingReliableVal = divStyle.width === "4px";

		// Support: Android 4.0 - 4.3 only
		// Some styles come back with percentage values, even though they shouldn't
		div.style.marginRight = "50%";
		pixelMarginRightVal = divStyle.marginRight === "4px";

		documentElement.removeChild( container );

		// Nullify the div so it wouldn't be stored in the memory and
		// it will also be a sign that checks already performed
		div = null;
	}

	var pixelPositionVal, boxSizingReliableVal, pixelMarginRightVal, reliableMarginLeftVal,
		container = document.createElement( "div" ),
		div = document.createElement( "div" );

	// Finish early in limited (non-browser) environments
	if ( !div.style ) {
		return;
	}

	// Support: IE <=9 - 11 only
	// Style of cloned element affects source element cloned (#8908)
	div.style.backgroundClip = "content-box";
	div.cloneNode( true ).style.backgroundClip = "";
	support.clearCloneStyle = div.style.backgroundClip === "content-box";

	container.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;" +
		"padding:0;margin-top:1px;position:absolute";
	container.appendChild( div );

	jQuery.extend( support, {
		pixelPosition: function() {
			computeStyleTests();
			return pixelPositionVal;
		},
		boxSizingReliable: function() {
			computeStyleTests();
			return boxSizingReliableVal;
		},
		pixelMarginRight: function() {
			computeStyleTests();
			return pixelMarginRightVal;
		},
		reliableMarginLeft: function() {
			computeStyleTests();
			return reliableMarginLeftVal;
		}
	} );
} )();


function curCSS( elem, name, computed ) {
	var width, minWidth, maxWidth, ret,

		// Support: Firefox 51+
		// Retrieving style before computed somehow
		// fixes an issue with getting wrong values
		// on detached elements
		style = elem.style;

	computed = computed || getStyles( elem );

	// getPropertyValue is needed for:
	//   .css('filter') (IE 9 only, #12537)
	//   .css('--customProperty) (#3144)
	if ( computed ) {
		ret = computed.getPropertyValue( name ) || computed[ name ];

		if ( ret === "" && !jQuery.contains( elem.ownerDocument, elem ) ) {
			ret = jQuery.style( elem, name );
		}

		// A tribute to the "awesome hack by Dean Edwards"
		// Android Browser returns percentage for some values,
		// but width seems to be reliably pixels.
		// This is against the CSSOM draft spec:
		// https://drafts.csswg.org/cssom/#resolved-values
		if ( !support.pixelMarginRight() && rnumnonpx.test( ret ) && rmargin.test( name ) ) {

			// Remember the original values
			width = style.width;
			minWidth = style.minWidth;
			maxWidth = style.maxWidth;

			// Put in the new values to get a computed value out
			style.minWidth = style.maxWidth = style.width = ret;
			ret = computed.width;

			// Revert the changed values
			style.width = width;
			style.minWidth = minWidth;
			style.maxWidth = maxWidth;
		}
	}

	return ret !== undefined ?

		// Support: IE <=9 - 11 only
		// IE returns zIndex value as an integer.
		ret + "" :
		ret;
}


function addGetHookIf( conditionFn, hookFn ) {

	// Define the hook, we'll check on the first run if it's really needed.
	return {
		get: function() {
			if ( conditionFn() ) {

				// Hook not needed (or it's not possible to use it due
				// to missing dependency), remove it.
				delete this.get;
				return;
			}

			// Hook needed; redefine it so that the support test is not executed again.
			return ( this.get = hookFn ).apply( this, arguments );
		}
	};
}


var

	// Swappable if display is none or starts with table
	// except "table", "table-cell", or "table-caption"
	// See here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
	rdisplayswap = /^(none|table(?!-c[ea]).+)/,
	rcustomProp = /^--/,
	cssShow = { position: "absolute", visibility: "hidden", display: "block" },
	cssNormalTransform = {
		letterSpacing: "0",
		fontWeight: "400"
	},

	cssPrefixes = [ "Webkit", "Moz", "ms" ],
	emptyStyle = document.createElement( "div" ).style;

// Return a css property mapped to a potentially vendor prefixed property
function vendorPropName( name ) {

	// Shortcut for names that are not vendor prefixed
	if ( name in emptyStyle ) {
		return name;
	}

	// Check for vendor prefixed names
	var capName = name[ 0 ].toUpperCase() + name.slice( 1 ),
		i = cssPrefixes.length;

	while ( i-- ) {
		name = cssPrefixes[ i ] + capName;
		if ( name in emptyStyle ) {
			return name;
		}
	}
}

// Return a property mapped along what jQuery.cssProps suggests or to
// a vendor prefixed property.
function finalPropName( name ) {
	var ret = jQuery.cssProps[ name ];
	if ( !ret ) {
		ret = jQuery.cssProps[ name ] = vendorPropName( name ) || name;
	}
	return ret;
}

function setPositiveNumber( elem, value, subtract ) {

	// Any relative (+/-) values have already been
	// normalized at this point
	var matches = rcssNum.exec( value );
	return matches ?

		// Guard against undefined "subtract", e.g., when used as in cssHooks
		Math.max( 0, matches[ 2 ] - ( subtract || 0 ) ) + ( matches[ 3 ] || "px" ) :
		value;
}

function augmentWidthOrHeight( elem, name, extra, isBorderBox, styles ) {
	var i,
		val = 0;

	// If we already have the right measurement, avoid augmentation
	if ( extra === ( isBorderBox ? "border" : "content" ) ) {
		i = 4;

	// Otherwise initialize for horizontal or vertical properties
	} else {
		i = name === "width" ? 1 : 0;
	}

	for ( ; i < 4; i += 2 ) {

		// Both box models exclude margin, so add it if we want it
		if ( extra === "margin" ) {
			val += jQuery.css( elem, extra + cssExpand[ i ], true, styles );
		}

		if ( isBorderBox ) {

			// border-box includes padding, so remove it if we want content
			if ( extra === "content" ) {
				val -= jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );
			}

			// At this point, extra isn't border nor margin, so remove border
			if ( extra !== "margin" ) {
				val -= jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		} else {

			// At this point, extra isn't content, so add padding
			val += jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );

			// At this point, extra isn't content nor padding, so add border
			if ( extra !== "padding" ) {
				val += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		}
	}

	return val;
}

function getWidthOrHeight( elem, name, extra ) {

	// Start with computed style
	var valueIsBorderBox,
		styles = getStyles( elem ),
		val = curCSS( elem, name, styles ),
		isBorderBox = jQuery.css( elem, "boxSizing", false, styles ) === "border-box";

	// Computed unit is not pixels. Stop here and return.
	if ( rnumnonpx.test( val ) ) {
		return val;
	}

	// Check for style in case a browser which returns unreliable values
	// for getComputedStyle silently falls back to the reliable elem.style
	valueIsBorderBox = isBorderBox &&
		( support.boxSizingReliable() || val === elem.style[ name ] );

	// Fall back to offsetWidth/Height when value is "auto"
	// This happens for inline elements with no explicit setting (gh-3571)
	if ( val === "auto" ) {
		val = elem[ "offset" + name[ 0 ].toUpperCase() + name.slice( 1 ) ];
	}

	// Normalize "", auto, and prepare for extra
	val = parseFloat( val ) || 0;

	// Use the active box-sizing model to add/subtract irrelevant styles
	return ( val +
		augmentWidthOrHeight(
			elem,
			name,
			extra || ( isBorderBox ? "border" : "content" ),
			valueIsBorderBox,
			styles
		)
	) + "px";
}

jQuery.extend( {

	// Add in style property hooks for overriding the default
	// behavior of getting and setting a style property
	cssHooks: {
		opacity: {
			get: function( elem, computed ) {
				if ( computed ) {

					// We should always get a number back from opacity
					var ret = curCSS( elem, "opacity" );
					return ret === "" ? "1" : ret;
				}
			}
		}
	},

	// Don't automatically add "px" to these possibly-unitless properties
	cssNumber: {
		"animationIterationCount": true,
		"columnCount": true,
		"fillOpacity": true,
		"flexGrow": true,
		"flexShrink": true,
		"fontWeight": true,
		"lineHeight": true,
		"opacity": true,
		"order": true,
		"orphans": true,
		"widows": true,
		"zIndex": true,
		"zoom": true
	},

	// Add in properties whose names you wish to fix before
	// setting or getting the value
	cssProps: {
		"float": "cssFloat"
	},

	// Get and set the style property on a DOM Node
	style: function( elem, name, value, extra ) {

		// Don't set styles on text and comment nodes
		if ( !elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style ) {
			return;
		}

		// Make sure that we're working with the right name
		var ret, type, hooks,
			origName = jQuery.camelCase( name ),
			isCustomProp = rcustomProp.test( name ),
			style = elem.style;

		// Make sure that we're working with the right name. We don't
		// want to query the value if it is a CSS custom property
		// since they are user-defined.
		if ( !isCustomProp ) {
			name = finalPropName( origName );
		}

		// Gets hook for the prefixed version, then unprefixed version
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// Check if we're setting a value
		if ( value !== undefined ) {
			type = typeof value;

			// Convert "+=" or "-=" to relative numbers (#7345)
			if ( type === "string" && ( ret = rcssNum.exec( value ) ) && ret[ 1 ] ) {
				value = adjustCSS( elem, name, ret );

				// Fixes bug #9237
				type = "number";
			}

			// Make sure that null and NaN values aren't set (#7116)
			if ( value == null || value !== value ) {
				return;
			}

			// If a number was passed in, add the unit (except for certain CSS properties)
			if ( type === "number" ) {
				value += ret && ret[ 3 ] || ( jQuery.cssNumber[ origName ] ? "" : "px" );
			}

			// background-* props affect original clone's values
			if ( !support.clearCloneStyle && value === "" && name.indexOf( "background" ) === 0 ) {
				style[ name ] = "inherit";
			}

			// If a hook was provided, use that value, otherwise just set the specified value
			if ( !hooks || !( "set" in hooks ) ||
				( value = hooks.set( elem, value, extra ) ) !== undefined ) {

				if ( isCustomProp ) {
					style.setProperty( name, value );
				} else {
					style[ name ] = value;
				}
			}

		} else {

			// If a hook was provided get the non-computed value from there
			if ( hooks && "get" in hooks &&
				( ret = hooks.get( elem, false, extra ) ) !== undefined ) {

				return ret;
			}

			// Otherwise just get the value from the style object
			return style[ name ];
		}
	},

	css: function( elem, name, extra, styles ) {
		var val, num, hooks,
			origName = jQuery.camelCase( name ),
			isCustomProp = rcustomProp.test( name );

		// Make sure that we're working with the right name. We don't
		// want to modify the value if it is a CSS custom property
		// since they are user-defined.
		if ( !isCustomProp ) {
			name = finalPropName( origName );
		}

		// Try prefixed name followed by the unprefixed name
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// If a hook was provided get the computed value from there
		if ( hooks && "get" in hooks ) {
			val = hooks.get( elem, true, extra );
		}

		// Otherwise, if a way to get the computed value exists, use that
		if ( val === undefined ) {
			val = curCSS( elem, name, styles );
		}

		// Convert "normal" to computed value
		if ( val === "normal" && name in cssNormalTransform ) {
			val = cssNormalTransform[ name ];
		}

		// Make numeric if forced or a qualifier was provided and val looks numeric
		if ( extra === "" || extra ) {
			num = parseFloat( val );
			return extra === true || isFinite( num ) ? num || 0 : val;
		}

		return val;
	}
} );

jQuery.each( [ "height", "width" ], function( i, name ) {
	jQuery.cssHooks[ name ] = {
		get: function( elem, computed, extra ) {
			if ( computed ) {

				// Certain elements can have dimension info if we invisibly show them
				// but it must have a current display style that would benefit
				return rdisplayswap.test( jQuery.css( elem, "display" ) ) &&

					// Support: Safari 8+
					// Table columns in Safari have non-zero offsetWidth & zero
					// getBoundingClientRect().width unless display is changed.
					// Support: IE <=11 only
					// Running getBoundingClientRect on a disconnected node
					// in IE throws an error.
					( !elem.getClientRects().length || !elem.getBoundingClientRect().width ) ?
						swap( elem, cssShow, function() {
							return getWidthOrHeight( elem, name, extra );
						} ) :
						getWidthOrHeight( elem, name, extra );
			}
		},

		set: function( elem, value, extra ) {
			var matches,
				styles = extra && getStyles( elem ),
				subtract = extra && augmentWidthOrHeight(
					elem,
					name,
					extra,
					jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
					styles
				);

			// Convert to pixels if value adjustment is needed
			if ( subtract && ( matches = rcssNum.exec( value ) ) &&
				( matches[ 3 ] || "px" ) !== "px" ) {

				elem.style[ name ] = value;
				value = jQuery.css( elem, name );
			}

			return setPositiveNumber( elem, value, subtract );
		}
	};
} );

jQuery.cssHooks.marginLeft = addGetHookIf( support.reliableMarginLeft,
	function( elem, computed ) {
		if ( computed ) {
			return ( parseFloat( curCSS( elem, "marginLeft" ) ) ||
				elem.getBoundingClientRect().left -
					swap( elem, { marginLeft: 0 }, function() {
						return elem.getBoundingClientRect().left;
					} )
				) + "px";
		}
	}
);

// These hooks are used by animate to expand properties
jQuery.each( {
	margin: "",
	padding: "",
	border: "Width"
}, function( prefix, suffix ) {
	jQuery.cssHooks[ prefix + suffix ] = {
		expand: function( value ) {
			var i = 0,
				expanded = {},

				// Assumes a single number if not a string
				parts = typeof value === "string" ? value.split( " " ) : [ value ];

			for ( ; i < 4; i++ ) {
				expanded[ prefix + cssExpand[ i ] + suffix ] =
					parts[ i ] || parts[ i - 2 ] || parts[ 0 ];
			}

			return expanded;
		}
	};

	if ( !rmargin.test( prefix ) ) {
		jQuery.cssHooks[ prefix + suffix ].set = setPositiveNumber;
	}
} );

jQuery.fn.extend( {
	css: function( name, value ) {
		return access( this, function( elem, name, value ) {
			var styles, len,
				map = {},
				i = 0;

			if ( Array.isArray( name ) ) {
				styles = getStyles( elem );
				len = name.length;

				for ( ; i < len; i++ ) {
					map[ name[ i ] ] = jQuery.css( elem, name[ i ], false, styles );
				}

				return map;
			}

			return value !== undefined ?
				jQuery.style( elem, name, value ) :
				jQuery.css( elem, name );
		}, name, value, arguments.length > 1 );
	}
} );


function Tween( elem, options, prop, end, easing ) {
	return new Tween.prototype.init( elem, options, prop, end, easing );
}
jQuery.Tween = Tween;

Tween.prototype = {
	constructor: Tween,
	init: function( elem, options, prop, end, easing, unit ) {
		this.elem = elem;
		this.prop = prop;
		this.easing = easing || jQuery.easing._default;
		this.options = options;
		this.start = this.now = this.cur();
		this.end = end;
		this.unit = unit || ( jQuery.cssNumber[ prop ] ? "" : "px" );
	},
	cur: function() {
		var hooks = Tween.propHooks[ this.prop ];

		return hooks && hooks.get ?
			hooks.get( this ) :
			Tween.propHooks._default.get( this );
	},
	run: function( percent ) {
		var eased,
			hooks = Tween.propHooks[ this.prop ];

		if ( this.options.duration ) {
			this.pos = eased = jQuery.easing[ this.easing ](
				percent, this.options.duration * percent, 0, 1, this.options.duration
			);
		} else {
			this.pos = eased = percent;
		}
		this.now = ( this.end - this.start ) * eased + this.start;

		if ( this.options.step ) {
			this.options.step.call( this.elem, this.now, this );
		}

		if ( hooks && hooks.set ) {
			hooks.set( this );
		} else {
			Tween.propHooks._default.set( this );
		}
		return this;
	}
};

Tween.prototype.init.prototype = Tween.prototype;

Tween.propHooks = {
	_default: {
		get: function( tween ) {
			var result;

			// Use a property on the element directly when it is not a DOM element,
			// or when there is no matching style property that exists.
			if ( tween.elem.nodeType !== 1 ||
				tween.elem[ tween.prop ] != null && tween.elem.style[ tween.prop ] == null ) {
				return tween.elem[ tween.prop ];
			}

			// Passing an empty string as a 3rd parameter to .css will automatically
			// attempt a parseFloat and fallback to a string if the parse fails.
			// Simple values such as "10px" are parsed to Float;
			// complex values such as "rotate(1rad)" are returned as-is.
			result = jQuery.css( tween.elem, tween.prop, "" );

			// Empty strings, null, undefined and "auto" are converted to 0.
			return !result || result === "auto" ? 0 : result;
		},
		set: function( tween ) {

			// Use step hook for back compat.
			// Use cssHook if its there.
			// Use .style if available and use plain properties where available.
			if ( jQuery.fx.step[ tween.prop ] ) {
				jQuery.fx.step[ tween.prop ]( tween );
			} else if ( tween.elem.nodeType === 1 &&
				( tween.elem.style[ jQuery.cssProps[ tween.prop ] ] != null ||
					jQuery.cssHooks[ tween.prop ] ) ) {
				jQuery.style( tween.elem, tween.prop, tween.now + tween.unit );
			} else {
				tween.elem[ tween.prop ] = tween.now;
			}
		}
	}
};

// Support: IE <=9 only
// Panic based approach to setting things on disconnected nodes
Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
	set: function( tween ) {
		if ( tween.elem.nodeType && tween.elem.parentNode ) {
			tween.elem[ tween.prop ] = tween.now;
		}
	}
};

jQuery.easing = {
	linear: function( p ) {
		return p;
	},
	swing: function( p ) {
		return 0.5 - Math.cos( p * Math.PI ) / 2;
	},
	_default: "swing"
};

jQuery.fx = Tween.prototype.init;

// Back compat <1.8 extension point
jQuery.fx.step = {};




var
	fxNow, inProgress,
	rfxtypes = /^(?:toggle|show|hide)$/,
	rrun = /queueHooks$/;

function schedule() {
	if ( inProgress ) {
		if ( document.hidden === false && window.requestAnimationFrame ) {
			window.requestAnimationFrame( schedule );
		} else {
			window.setTimeout( schedule, jQuery.fx.interval );
		}

		jQuery.fx.tick();
	}
}

// Animations created synchronously will run synchronously
function createFxNow() {
	window.setTimeout( function() {
		fxNow = undefined;
	} );
	return ( fxNow = jQuery.now() );
}

// Generate parameters to create a standard animation
function genFx( type, includeWidth ) {
	var which,
		i = 0,
		attrs = { height: type };

	// If we include width, step value is 1 to do all cssExpand values,
	// otherwise step value is 2 to skip over Left and Right
	includeWidth = includeWidth ? 1 : 0;
	for ( ; i < 4; i += 2 - includeWidth ) {
		which = cssExpand[ i ];
		attrs[ "margin" + which ] = attrs[ "padding" + which ] = type;
	}

	if ( includeWidth ) {
		attrs.opacity = attrs.width = type;
	}

	return attrs;
}

function createTween( value, prop, animation ) {
	var tween,
		collection = ( Animation.tweeners[ prop ] || [] ).concat( Animation.tweeners[ "*" ] ),
		index = 0,
		length = collection.length;
	for ( ; index < length; index++ ) {
		if ( ( tween = collection[ index ].call( animation, prop, value ) ) ) {

			// We're done with this property
			return tween;
		}
	}
}

function defaultPrefilter( elem, props, opts ) {
	var prop, value, toggle, hooks, oldfire, propTween, restoreDisplay, display,
		isBox = "width" in props || "height" in props,
		anim = this,
		orig = {},
		style = elem.style,
		hidden = elem.nodeType && isHiddenWithinTree( elem ),
		dataShow = dataPriv.get( elem, "fxshow" );

	// Queue-skipping animations hijack the fx hooks
	if ( !opts.queue ) {
		hooks = jQuery._queueHooks( elem, "fx" );
		if ( hooks.unqueued == null ) {
			hooks.unqueued = 0;
			oldfire = hooks.empty.fire;
			hooks.empty.fire = function() {
				if ( !hooks.unqueued ) {
					oldfire();
				}
			};
		}
		hooks.unqueued++;

		anim.always( function() {

			// Ensure the complete handler is called before this completes
			anim.always( function() {
				hooks.unqueued--;
				if ( !jQuery.queue( elem, "fx" ).length ) {
					hooks.empty.fire();
				}
			} );
		} );
	}

	// Detect show/hide animations
	for ( prop in props ) {
		value = props[ prop ];
		if ( rfxtypes.test( value ) ) {
			delete props[ prop ];
			toggle = toggle || value === "toggle";
			if ( value === ( hidden ? "hide" : "show" ) ) {

				// Pretend to be hidden if this is a "show" and
				// there is still data from a stopped show/hide
				if ( value === "show" && dataShow && dataShow[ prop ] !== undefined ) {
					hidden = true;

				// Ignore all other no-op show/hide data
				} else {
					continue;
				}
			}
			orig[ prop ] = dataShow && dataShow[ prop ] || jQuery.style( elem, prop );
		}
	}

	// Bail out if this is a no-op like .hide().hide()
	propTween = !jQuery.isEmptyObject( props );
	if ( !propTween && jQuery.isEmptyObject( orig ) ) {
		return;
	}

	// Restrict "overflow" and "display" styles during box animations
	if ( isBox && elem.nodeType === 1 ) {

		// Support: IE <=9 - 11, Edge 12 - 13
		// Record all 3 overflow attributes because IE does not infer the shorthand
		// from identically-valued overflowX and overflowY
		opts.overflow = [ style.overflow, style.overflowX, style.overflowY ];

		// Identify a display type, preferring old show/hide data over the CSS cascade
		restoreDisplay = dataShow && dataShow.display;
		if ( restoreDisplay == null ) {
			restoreDisplay = dataPriv.get( elem, "display" );
		}
		display = jQuery.css( elem, "display" );
		if ( display === "none" ) {
			if ( restoreDisplay ) {
				display = restoreDisplay;
			} else {

				// Get nonempty value(s) by temporarily forcing visibility
				showHide( [ elem ], true );
				restoreDisplay = elem.style.display || restoreDisplay;
				display = jQuery.css( elem, "display" );
				showHide( [ elem ] );
			}
		}

		// Animate inline elements as inline-block
		if ( display === "inline" || display === "inline-block" && restoreDisplay != null ) {
			if ( jQuery.css( elem, "float" ) === "none" ) {

				// Restore the original display value at the end of pure show/hide animations
				if ( !propTween ) {
					anim.done( function() {
						style.display = restoreDisplay;
					} );
					if ( restoreDisplay == null ) {
						display = style.display;
						restoreDisplay = display === "none" ? "" : display;
					}
				}
				style.display = "inline-block";
			}
		}
	}

	if ( opts.overflow ) {
		style.overflow = "hidden";
		anim.always( function() {
			style.overflow = opts.overflow[ 0 ];
			style.overflowX = opts.overflow[ 1 ];
			style.overflowY = opts.overflow[ 2 ];
		} );
	}

	// Implement show/hide animations
	propTween = false;
	for ( prop in orig ) {

		// General show/hide setup for this element animation
		if ( !propTween ) {
			if ( dataShow ) {
				if ( "hidden" in dataShow ) {
					hidden = dataShow.hidden;
				}
			} else {
				dataShow = dataPriv.access( elem, "fxshow", { display: restoreDisplay } );
			}

			// Store hidden/visible for toggle so `.stop().toggle()` "reverses"
			if ( toggle ) {
				dataShow.hidden = !hidden;
			}

			// Show elements before animating them
			if ( hidden ) {
				showHide( [ elem ], true );
			}

			/* eslint-disable no-loop-func */

			anim.done( function() {

			/* eslint-enable no-loop-func */

				// The final step of a "hide" animation is actually hiding the element
				if ( !hidden ) {
					showHide( [ elem ] );
				}
				dataPriv.remove( elem, "fxshow" );
				for ( prop in orig ) {
					jQuery.style( elem, prop, orig[ prop ] );
				}
			} );
		}

		// Per-property setup
		propTween = createTween( hidden ? dataShow[ prop ] : 0, prop, anim );
		if ( !( prop in dataShow ) ) {
			dataShow[ prop ] = propTween.start;
			if ( hidden ) {
				propTween.end = propTween.start;
				propTween.start = 0;
			}
		}
	}
}

function propFilter( props, specialEasing ) {
	var index, name, easing, value, hooks;

	// camelCase, specialEasing and expand cssHook pass
	for ( index in props ) {
		name = jQuery.camelCase( index );
		easing = specialEasing[ name ];
		value = props[ index ];
		if ( Array.isArray( value ) ) {
			easing = value[ 1 ];
			value = props[ index ] = value[ 0 ];
		}

		if ( index !== name ) {
			props[ name ] = value;
			delete props[ index ];
		}

		hooks = jQuery.cssHooks[ name ];
		if ( hooks && "expand" in hooks ) {
			value = hooks.expand( value );
			delete props[ name ];

			// Not quite $.extend, this won't overwrite existing keys.
			// Reusing 'index' because we have the correct "name"
			for ( index in value ) {
				if ( !( index in props ) ) {
					props[ index ] = value[ index ];
					specialEasing[ index ] = easing;
				}
			}
		} else {
			specialEasing[ name ] = easing;
		}
	}
}

function Animation( elem, properties, options ) {
	var result,
		stopped,
		index = 0,
		length = Animation.prefilters.length,
		deferred = jQuery.Deferred().always( function() {

			// Don't match elem in the :animated selector
			delete tick.elem;
		} ),
		tick = function() {
			if ( stopped ) {
				return false;
			}
			var currentTime = fxNow || createFxNow(),
				remaining = Math.max( 0, animation.startTime + animation.duration - currentTime ),

				// Support: Android 2.3 only
				// Archaic crash bug won't allow us to use `1 - ( 0.5 || 0 )` (#12497)
				temp = remaining / animation.duration || 0,
				percent = 1 - temp,
				index = 0,
				length = animation.tweens.length;

			for ( ; index < length; index++ ) {
				animation.tweens[ index ].run( percent );
			}

			deferred.notifyWith( elem, [ animation, percent, remaining ] );

			// If there's more to do, yield
			if ( percent < 1 && length ) {
				return remaining;
			}

			// If this was an empty animation, synthesize a final progress notification
			if ( !length ) {
				deferred.notifyWith( elem, [ animation, 1, 0 ] );
			}

			// Resolve the animation and report its conclusion
			deferred.resolveWith( elem, [ animation ] );
			return false;
		},
		animation = deferred.promise( {
			elem: elem,
			props: jQuery.extend( {}, properties ),
			opts: jQuery.extend( true, {
				specialEasing: {},
				easing: jQuery.easing._default
			}, options ),
			originalProperties: properties,
			originalOptions: options,
			startTime: fxNow || createFxNow(),
			duration: options.duration,
			tweens: [],
			createTween: function( prop, end ) {
				var tween = jQuery.Tween( elem, animation.opts, prop, end,
						animation.opts.specialEasing[ prop ] || animation.opts.easing );
				animation.tweens.push( tween );
				return tween;
			},
			stop: function( gotoEnd ) {
				var index = 0,

					// If we are going to the end, we want to run all the tweens
					// otherwise we skip this part
					length = gotoEnd ? animation.tweens.length : 0;
				if ( stopped ) {
					return this;
				}
				stopped = true;
				for ( ; index < length; index++ ) {
					animation.tweens[ index ].run( 1 );
				}

				// Resolve when we played the last frame; otherwise, reject
				if ( gotoEnd ) {
					deferred.notifyWith( elem, [ animation, 1, 0 ] );
					deferred.resolveWith( elem, [ animation, gotoEnd ] );
				} else {
					deferred.rejectWith( elem, [ animation, gotoEnd ] );
				}
				return this;
			}
		} ),
		props = animation.props;

	propFilter( props, animation.opts.specialEasing );

	for ( ; index < length; index++ ) {
		result = Animation.prefilters[ index ].call( animation, elem, props, animation.opts );
		if ( result ) {
			if ( jQuery.isFunction( result.stop ) ) {
				jQuery._queueHooks( animation.elem, animation.opts.queue ).stop =
					jQuery.proxy( result.stop, result );
			}
			return result;
		}
	}

	jQuery.map( props, createTween, animation );

	if ( jQuery.isFunction( animation.opts.start ) ) {
		animation.opts.start.call( elem, animation );
	}

	// Attach callbacks from options
	animation
		.progress( animation.opts.progress )
		.done( animation.opts.done, animation.opts.complete )
		.fail( animation.opts.fail )
		.always( animation.opts.always );

	jQuery.fx.timer(
		jQuery.extend( tick, {
			elem: elem,
			anim: animation,
			queue: animation.opts.queue
		} )
	);

	return animation;
}

jQuery.Animation = jQuery.extend( Animation, {

	tweeners: {
		"*": [ function( prop, value ) {
			var tween = this.createTween( prop, value );
			adjustCSS( tween.elem, prop, rcssNum.exec( value ), tween );
			return tween;
		} ]
	},

	tweener: function( props, callback ) {
		if ( jQuery.isFunction( props ) ) {
			callback = props;
			props = [ "*" ];
		} else {
			props = props.match( rnothtmlwhite );
		}

		var prop,
			index = 0,
			length = props.length;

		for ( ; index < length; index++ ) {
			prop = props[ index ];
			Animation.tweeners[ prop ] = Animation.tweeners[ prop ] || [];
			Animation.tweeners[ prop ].unshift( callback );
		}
	},

	prefilters: [ defaultPrefilter ],

	prefilter: function( callback, prepend ) {
		if ( prepend ) {
			Animation.prefilters.unshift( callback );
		} else {
			Animation.prefilters.push( callback );
		}
	}
} );

jQuery.speed = function( speed, easing, fn ) {
	var opt = speed && typeof speed === "object" ? jQuery.extend( {}, speed ) : {
		complete: fn || !fn && easing ||
			jQuery.isFunction( speed ) && speed,
		duration: speed,
		easing: fn && easing || easing && !jQuery.isFunction( easing ) && easing
	};

	// Go to the end state if fx are off
	if ( jQuery.fx.off ) {
		opt.duration = 0;

	} else {
		if ( typeof opt.duration !== "number" ) {
			if ( opt.duration in jQuery.fx.speeds ) {
				opt.duration = jQuery.fx.speeds[ opt.duration ];

			} else {
				opt.duration = jQuery.fx.speeds._default;
			}
		}
	}

	// Normalize opt.queue - true/undefined/null -> "fx"
	if ( opt.queue == null || opt.queue === true ) {
		opt.queue = "fx";
	}

	// Queueing
	opt.old = opt.complete;

	opt.complete = function() {
		if ( jQuery.isFunction( opt.old ) ) {
			opt.old.call( this );
		}

		if ( opt.queue ) {
			jQuery.dequeue( this, opt.queue );
		}
	};

	return opt;
};

jQuery.fn.extend( {
	fadeTo: function( speed, to, easing, callback ) {

		// Show any hidden elements after setting opacity to 0
		return this.filter( isHiddenWithinTree ).css( "opacity", 0 ).show()

			// Animate to the value specified
			.end().animate( { opacity: to }, speed, easing, callback );
	},
	animate: function( prop, speed, easing, callback ) {
		var empty = jQuery.isEmptyObject( prop ),
			optall = jQuery.speed( speed, easing, callback ),
			doAnimation = function() {

				// Operate on a copy of prop so per-property easing won't be lost
				var anim = Animation( this, jQuery.extend( {}, prop ), optall );

				// Empty animations, or finishing resolves immediately
				if ( empty || dataPriv.get( this, "finish" ) ) {
					anim.stop( true );
				}
			};
			doAnimation.finish = doAnimation;

		return empty || optall.queue === false ?
			this.each( doAnimation ) :
			this.queue( optall.queue, doAnimation );
	},
	stop: function( type, clearQueue, gotoEnd ) {
		var stopQueue = function( hooks ) {
			var stop = hooks.stop;
			delete hooks.stop;
			stop( gotoEnd );
		};

		if ( typeof type !== "string" ) {
			gotoEnd = clearQueue;
			clearQueue = type;
			type = undefined;
		}
		if ( clearQueue && type !== false ) {
			this.queue( type || "fx", [] );
		}

		return this.each( function() {
			var dequeue = true,
				index = type != null && type + "queueHooks",
				timers = jQuery.timers,
				data = dataPriv.get( this );

			if ( index ) {
				if ( data[ index ] && data[ index ].stop ) {
					stopQueue( data[ index ] );
				}
			} else {
				for ( index in data ) {
					if ( data[ index ] && data[ index ].stop && rrun.test( index ) ) {
						stopQueue( data[ index ] );
					}
				}
			}

			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this &&
					( type == null || timers[ index ].queue === type ) ) {

					timers[ index ].anim.stop( gotoEnd );
					dequeue = false;
					timers.splice( index, 1 );
				}
			}

			// Start the next in the queue if the last step wasn't forced.
			// Timers currently will call their complete callbacks, which
			// will dequeue but only if they were gotoEnd.
			if ( dequeue || !gotoEnd ) {
				jQuery.dequeue( this, type );
			}
		} );
	},
	finish: function( type ) {
		if ( type !== false ) {
			type = type || "fx";
		}
		return this.each( function() {
			var index,
				data = dataPriv.get( this ),
				queue = data[ type + "queue" ],
				hooks = data[ type + "queueHooks" ],
				timers = jQuery.timers,
				length = queue ? queue.length : 0;

			// Enable finishing flag on private data
			data.finish = true;

			// Empty the queue first
			jQuery.queue( this, type, [] );

			if ( hooks && hooks.stop ) {
				hooks.stop.call( this, true );
			}

			// Look for any active animations, and finish them
			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this && timers[ index ].queue === type ) {
					timers[ index ].anim.stop( true );
					timers.splice( index, 1 );
				}
			}

			// Look for any animations in the old queue and finish them
			for ( index = 0; index < length; index++ ) {
				if ( queue[ index ] && queue[ index ].finish ) {
					queue[ index ].finish.call( this );
				}
			}

			// Turn off finishing flag
			delete data.finish;
		} );
	}
} );

jQuery.each( [ "toggle", "show", "hide" ], function( i, name ) {
	var cssFn = jQuery.fn[ name ];
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return speed == null || typeof speed === "boolean" ?
			cssFn.apply( this, arguments ) :
			this.animate( genFx( name, true ), speed, easing, callback );
	};
} );

// Generate shortcuts for custom animations
jQuery.each( {
	slideDown: genFx( "show" ),
	slideUp: genFx( "hide" ),
	slideToggle: genFx( "toggle" ),
	fadeIn: { opacity: "show" },
	fadeOut: { opacity: "hide" },
	fadeToggle: { opacity: "toggle" }
}, function( name, props ) {
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return this.animate( props, speed, easing, callback );
	};
} );

jQuery.timers = [];
jQuery.fx.tick = function() {
	var timer,
		i = 0,
		timers = jQuery.timers;

	fxNow = jQuery.now();

	for ( ; i < timers.length; i++ ) {
		timer = timers[ i ];

		// Run the timer and safely remove it when done (allowing for external removal)
		if ( !timer() && timers[ i ] === timer ) {
			timers.splice( i--, 1 );
		}
	}

	if ( !timers.length ) {
		jQuery.fx.stop();
	}
	fxNow = undefined;
};

jQuery.fx.timer = function( timer ) {
	jQuery.timers.push( timer );
	jQuery.fx.start();
};

jQuery.fx.interval = 13;
jQuery.fx.start = function() {
	if ( inProgress ) {
		return;
	}

	inProgress = true;
	schedule();
};

jQuery.fx.stop = function() {
	inProgress = null;
};

jQuery.fx.speeds = {
	slow: 600,
	fast: 200,

	// Default speed
	_default: 400
};


// Based off of the plugin by Clint Helfers, with permission.
// https://web.archive.org/web/20100324014747/http://blindsignals.com/index.php/2009/07/jquery-delay/
jQuery.fn.delay = function( time, type ) {
	time = jQuery.fx ? jQuery.fx.speeds[ time ] || time : time;
	type = type || "fx";

	return this.queue( type, function( next, hooks ) {
		var timeout = window.setTimeout( next, time );
		hooks.stop = function() {
			window.clearTimeout( timeout );
		};
	} );
};


( function() {
	var input = document.createElement( "input" ),
		select = document.createElement( "select" ),
		opt = select.appendChild( document.createElement( "option" ) );

	input.type = "checkbox";

	// Support: Android <=4.3 only
	// Default value for a checkbox should be "on"
	support.checkOn = input.value !== "";

	// Support: IE <=11 only
	// Must access selectedIndex to make default options select
	support.optSelected = opt.selected;

	// Support: IE <=11 only
	// An input loses its value after becoming a radio
	input = document.createElement( "input" );
	input.value = "t";
	input.type = "radio";
	support.radioValue = input.value === "t";
} )();


var boolHook,
	attrHandle = jQuery.expr.attrHandle;

jQuery.fn.extend( {
	attr: function( name, value ) {
		return access( this, jQuery.attr, name, value, arguments.length > 1 );
	},

	removeAttr: function( name ) {
		return this.each( function() {
			jQuery.removeAttr( this, name );
		} );
	}
} );

jQuery.extend( {
	attr: function( elem, name, value ) {
		var ret, hooks,
			nType = elem.nodeType;

		// Don't get/set attributes on text, comment and attribute nodes
		if ( nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		// Fallback to prop when attributes are not supported
		if ( typeof elem.getAttribute === "undefined" ) {
			return jQuery.prop( elem, name, value );
		}

		// Attribute hooks are determined by the lowercase version
		// Grab necessary hook if one is defined
		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {
			hooks = jQuery.attrHooks[ name.toLowerCase() ] ||
				( jQuery.expr.match.bool.test( name ) ? boolHook : undefined );
		}

		if ( value !== undefined ) {
			if ( value === null ) {
				jQuery.removeAttr( elem, name );
				return;
			}

			if ( hooks && "set" in hooks &&
				( ret = hooks.set( elem, value, name ) ) !== undefined ) {
				return ret;
			}

			elem.setAttribute( name, value + "" );
			return value;
		}

		if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
			return ret;
		}

		ret = jQuery.find.attr( elem, name );

		// Non-existent attributes return null, we normalize to undefined
		return ret == null ? undefined : ret;
	},

	attrHooks: {
		type: {
			set: function( elem, value ) {
				if ( !support.radioValue && value === "radio" &&
					nodeName( elem, "input" ) ) {
					var val = elem.value;
					elem.setAttribute( "type", value );
					if ( val ) {
						elem.value = val;
					}
					return value;
				}
			}
		}
	},

	removeAttr: function( elem, value ) {
		var name,
			i = 0,

			// Attribute names can contain non-HTML whitespace characters
			// https://html.spec.whatwg.org/multipage/syntax.html#attributes-2
			attrNames = value && value.match( rnothtmlwhite );

		if ( attrNames && elem.nodeType === 1 ) {
			while ( ( name = attrNames[ i++ ] ) ) {
				elem.removeAttribute( name );
			}
		}
	}
} );

// Hooks for boolean attributes
boolHook = {
	set: function( elem, value, name ) {
		if ( value === false ) {

			// Remove boolean attributes when set to false
			jQuery.removeAttr( elem, name );
		} else {
			elem.setAttribute( name, name );
		}
		return name;
	}
};

jQuery.each( jQuery.expr.match.bool.source.match( /\w+/g ), function( i, name ) {
	var getter = attrHandle[ name ] || jQuery.find.attr;

	attrHandle[ name ] = function( elem, name, isXML ) {
		var ret, handle,
			lowercaseName = name.toLowerCase();

		if ( !isXML ) {

			// Avoid an infinite loop by temporarily removing this function from the getter
			handle = attrHandle[ lowercaseName ];
			attrHandle[ lowercaseName ] = ret;
			ret = getter( elem, name, isXML ) != null ?
				lowercaseName :
				null;
			attrHandle[ lowercaseName ] = handle;
		}
		return ret;
	};
} );




var rfocusable = /^(?:input|select|textarea|button)$/i,
	rclickable = /^(?:a|area)$/i;

jQuery.fn.extend( {
	prop: function( name, value ) {
		return access( this, jQuery.prop, name, value, arguments.length > 1 );
	},

	removeProp: function( name ) {
		return this.each( function() {
			delete this[ jQuery.propFix[ name ] || name ];
		} );
	}
} );

jQuery.extend( {
	prop: function( elem, name, value ) {
		var ret, hooks,
			nType = elem.nodeType;

		// Don't get/set properties on text, comment and attribute nodes
		if ( nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {

			// Fix name and attach hooks
			name = jQuery.propFix[ name ] || name;
			hooks = jQuery.propHooks[ name ];
		}

		if ( value !== undefined ) {
			if ( hooks && "set" in hooks &&
				( ret = hooks.set( elem, value, name ) ) !== undefined ) {
				return ret;
			}

			return ( elem[ name ] = value );
		}

		if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
			return ret;
		}

		return elem[ name ];
	},

	propHooks: {
		tabIndex: {
			get: function( elem ) {

				// Support: IE <=9 - 11 only
				// elem.tabIndex doesn't always return the
				// correct value when it hasn't been explicitly set
				// https://web.archive.org/web/20141116233347/http://fluidproject.org/blog/2008/01/09/getting-setting-and-removing-tabindex-values-with-javascript/
				// Use proper attribute retrieval(#12072)
				var tabindex = jQuery.find.attr( elem, "tabindex" );

				if ( tabindex ) {
					return parseInt( tabindex, 10 );
				}

				if (
					rfocusable.test( elem.nodeName ) ||
					rclickable.test( elem.nodeName ) &&
					elem.href
				) {
					return 0;
				}

				return -1;
			}
		}
	},

	propFix: {
		"for": "htmlFor",
		"class": "className"
	}
} );

// Support: IE <=11 only
// Accessing the selectedIndex property
// forces the browser to respect setting selected
// on the option
// The getter ensures a default option is selected
// when in an optgroup
// eslint rule "no-unused-expressions" is disabled for this code
// since it considers such accessions noop
if ( !support.optSelected ) {
	jQuery.propHooks.selected = {
		get: function( elem ) {

			/* eslint no-unused-expressions: "off" */

			var parent = elem.parentNode;
			if ( parent && parent.parentNode ) {
				parent.parentNode.selectedIndex;
			}
			return null;
		},
		set: function( elem ) {

			/* eslint no-unused-expressions: "off" */

			var parent = elem.parentNode;
			if ( parent ) {
				parent.selectedIndex;

				if ( parent.parentNode ) {
					parent.parentNode.selectedIndex;
				}
			}
		}
	};
}

jQuery.each( [
	"tabIndex",
	"readOnly",
	"maxLength",
	"cellSpacing",
	"cellPadding",
	"rowSpan",
	"colSpan",
	"useMap",
	"frameBorder",
	"contentEditable"
], function() {
	jQuery.propFix[ this.toLowerCase() ] = this;
} );




	// Strip and collapse whitespace according to HTML spec
	// https://html.spec.whatwg.org/multipage/infrastructure.html#strip-and-collapse-whitespace
	function stripAndCollapse( value ) {
		var tokens = value.match( rnothtmlwhite ) || [];
		return tokens.join( " " );
	}


function getClass( elem ) {
	return elem.getAttribute && elem.getAttribute( "class" ) || "";
}

jQuery.fn.extend( {
	addClass: function( value ) {
		var classes, elem, cur, curValue, clazz, j, finalValue,
			i = 0;

		if ( jQuery.isFunction( value ) ) {
			return this.each( function( j ) {
				jQuery( this ).addClass( value.call( this, j, getClass( this ) ) );
			} );
		}

		if ( typeof value === "string" && value ) {
			classes = value.match( rnothtmlwhite ) || [];

			while ( ( elem = this[ i++ ] ) ) {
				curValue = getClass( elem );
				cur = elem.nodeType === 1 && ( " " + stripAndCollapse( curValue ) + " " );

				if ( cur ) {
					j = 0;
					while ( ( clazz = classes[ j++ ] ) ) {
						if ( cur.indexOf( " " + clazz + " " ) < 0 ) {
							cur += clazz + " ";
						}
					}

					// Only assign if different to avoid unneeded rendering.
					finalValue = stripAndCollapse( cur );
					if ( curValue !== finalValue ) {
						elem.setAttribute( "class", finalValue );
					}
				}
			}
		}

		return this;
	},

	removeClass: function( value ) {
		var classes, elem, cur, curValue, clazz, j, finalValue,
			i = 0;

		if ( jQuery.isFunction( value ) ) {
			return this.each( function( j ) {
				jQuery( this ).removeClass( value.call( this, j, getClass( this ) ) );
			} );
		}

		if ( !arguments.length ) {
			return this.attr( "class", "" );
		}

		if ( typeof value === "string" && value ) {
			classes = value.match( rnothtmlwhite ) || [];

			while ( ( elem = this[ i++ ] ) ) {
				curValue = getClass( elem );

				// This expression is here for better compressibility (see addClass)
				cur = elem.nodeType === 1 && ( " " + stripAndCollapse( curValue ) + " " );

				if ( cur ) {
					j = 0;
					while ( ( clazz = classes[ j++ ] ) ) {

						// Remove *all* instances
						while ( cur.indexOf( " " + clazz + " " ) > -1 ) {
							cur = cur.replace( " " + clazz + " ", " " );
						}
					}

					// Only assign if different to avoid unneeded rendering.
					finalValue = stripAndCollapse( cur );
					if ( curValue !== finalValue ) {
						elem.setAttribute( "class", finalValue );
					}
				}
			}
		}

		return this;
	},

	toggleClass: function( value, stateVal ) {
		var type = typeof value;

		if ( typeof stateVal === "boolean" && type === "string" ) {
			return stateVal ? this.addClass( value ) : this.removeClass( value );
		}

		if ( jQuery.isFunction( value ) ) {
			return this.each( function( i ) {
				jQuery( this ).toggleClass(
					value.call( this, i, getClass( this ), stateVal ),
					stateVal
				);
			} );
		}

		return this.each( function() {
			var className, i, self, classNames;

			if ( type === "string" ) {

				// Toggle individual class names
				i = 0;
				self = jQuery( this );
				classNames = value.match( rnothtmlwhite ) || [];

				while ( ( className = classNames[ i++ ] ) ) {

					// Check each className given, space separated list
					if ( self.hasClass( className ) ) {
						self.removeClass( className );
					} else {
						self.addClass( className );
					}
				}

			// Toggle whole class name
			} else if ( value === undefined || type === "boolean" ) {
				className = getClass( this );
				if ( className ) {

					// Store className if set
					dataPriv.set( this, "__className__", className );
				}

				// If the element has a class name or if we're passed `false`,
				// then remove the whole classname (if there was one, the above saved it).
				// Otherwise bring back whatever was previously saved (if anything),
				// falling back to the empty string if nothing was stored.
				if ( this.setAttribute ) {
					this.setAttribute( "class",
						className || value === false ?
						"" :
						dataPriv.get( this, "__className__" ) || ""
					);
				}
			}
		} );
	},

	hasClass: function( selector ) {
		var className, elem,
			i = 0;

		className = " " + selector + " ";
		while ( ( elem = this[ i++ ] ) ) {
			if ( elem.nodeType === 1 &&
				( " " + stripAndCollapse( getClass( elem ) ) + " " ).indexOf( className ) > -1 ) {
					return true;
			}
		}

		return false;
	}
} );




var rreturn = /\r/g;

jQuery.fn.extend( {
	val: function( value ) {
		var hooks, ret, isFunction,
			elem = this[ 0 ];

		if ( !arguments.length ) {
			if ( elem ) {
				hooks = jQuery.valHooks[ elem.type ] ||
					jQuery.valHooks[ elem.nodeName.toLowerCase() ];

				if ( hooks &&
					"get" in hooks &&
					( ret = hooks.get( elem, "value" ) ) !== undefined
				) {
					return ret;
				}

				ret = elem.value;

				// Handle most common string cases
				if ( typeof ret === "string" ) {
					return ret.replace( rreturn, "" );
				}

				// Handle cases where value is null/undef or number
				return ret == null ? "" : ret;
			}

			return;
		}

		isFunction = jQuery.isFunction( value );

		return this.each( function( i ) {
			var val;

			if ( this.nodeType !== 1 ) {
				return;
			}

			if ( isFunction ) {
				val = value.call( this, i, jQuery( this ).val() );
			} else {
				val = value;
			}

			// Treat null/undefined as ""; convert numbers to string
			if ( val == null ) {
				val = "";

			} else if ( typeof val === "number" ) {
				val += "";

			} else if ( Array.isArray( val ) ) {
				val = jQuery.map( val, function( value ) {
					return value == null ? "" : value + "";
				} );
			}

			hooks = jQuery.valHooks[ this.type ] || jQuery.valHooks[ this.nodeName.toLowerCase() ];

			// If set returns undefined, fall back to normal setting
			if ( !hooks || !( "set" in hooks ) || hooks.set( this, val, "value" ) === undefined ) {
				this.value = val;
			}
		} );
	}
} );

jQuery.extend( {
	valHooks: {
		option: {
			get: function( elem ) {

				var val = jQuery.find.attr( elem, "value" );
				return val != null ?
					val :

					// Support: IE <=10 - 11 only
					// option.text throws exceptions (#14686, #14858)
					// Strip and collapse whitespace
					// https://html.spec.whatwg.org/#strip-and-collapse-whitespace
					stripAndCollapse( jQuery.text( elem ) );
			}
		},
		select: {
			get: function( elem ) {
				var value, option, i,
					options = elem.options,
					index = elem.selectedIndex,
					one = elem.type === "select-one",
					values = one ? null : [],
					max = one ? index + 1 : options.length;

				if ( index < 0 ) {
					i = max;

				} else {
					i = one ? index : 0;
				}

				// Loop through all the selected options
				for ( ; i < max; i++ ) {
					option = options[ i ];

					// Support: IE <=9 only
					// IE8-9 doesn't update selected after form reset (#2551)
					if ( ( option.selected || i === index ) &&

							// Don't return options that are disabled or in a disabled optgroup
							!option.disabled &&
							( !option.parentNode.disabled ||
								!nodeName( option.parentNode, "optgroup" ) ) ) {

						// Get the specific value for the option
						value = jQuery( option ).val();

						// We don't need an array for one selects
						if ( one ) {
							return value;
						}

						// Multi-Selects return an array
						values.push( value );
					}
				}

				return values;
			},

			set: function( elem, value ) {
				var optionSet, option,
					options = elem.options,
					values = jQuery.makeArray( value ),
					i = options.length;

				while ( i-- ) {
					option = options[ i ];

					/* eslint-disable no-cond-assign */

					if ( option.selected =
						jQuery.inArray( jQuery.valHooks.option.get( option ), values ) > -1
					) {
						optionSet = true;
					}

					/* eslint-enable no-cond-assign */
				}

				// Force browsers to behave consistently when non-matching value is set
				if ( !optionSet ) {
					elem.selectedIndex = -1;
				}
				return values;
			}
		}
	}
} );

// Radios and checkboxes getter/setter
jQuery.each( [ "radio", "checkbox" ], function() {
	jQuery.valHooks[ this ] = {
		set: function( elem, value ) {
			if ( Array.isArray( value ) ) {
				return ( elem.checked = jQuery.inArray( jQuery( elem ).val(), value ) > -1 );
			}
		}
	};
	if ( !support.checkOn ) {
		jQuery.valHooks[ this ].get = function( elem ) {
			return elem.getAttribute( "value" ) === null ? "on" : elem.value;
		};
	}
} );




// Return jQuery for attributes-only inclusion


var rfocusMorph = /^(?:focusinfocus|focusoutblur)$/;

jQuery.extend( jQuery.event, {

	trigger: function( event, data, elem, onlyHandlers ) {

		var i, cur, tmp, bubbleType, ontype, handle, special,
			eventPath = [ elem || document ],
			type = hasOwn.call( event, "type" ) ? event.type : event,
			namespaces = hasOwn.call( event, "namespace" ) ? event.namespace.split( "." ) : [];

		cur = tmp = elem = elem || document;

		// Don't do events on text and comment nodes
		if ( elem.nodeType === 3 || elem.nodeType === 8 ) {
			return;
		}

		// focus/blur morphs to focusin/out; ensure we're not firing them right now
		if ( rfocusMorph.test( type + jQuery.event.triggered ) ) {
			return;
		}

		if ( type.indexOf( "." ) > -1 ) {

			// Namespaced trigger; create a regexp to match event type in handle()
			namespaces = type.split( "." );
			type = namespaces.shift();
			namespaces.sort();
		}
		ontype = type.indexOf( ":" ) < 0 && "on" + type;

		// Caller can pass in a jQuery.Event object, Object, or just an event type string
		event = event[ jQuery.expando ] ?
			event :
			new jQuery.Event( type, typeof event === "object" && event );

		// Trigger bitmask: & 1 for native handlers; & 2 for jQuery (always true)
		event.isTrigger = onlyHandlers ? 2 : 3;
		event.namespace = namespaces.join( "." );
		event.rnamespace = event.namespace ?
			new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" ) :
			null;

		// Clean up the event in case it is being reused
		event.result = undefined;
		if ( !event.target ) {
			event.target = elem;
		}

		// Clone any incoming data and prepend the event, creating the handler arg list
		data = data == null ?
			[ event ] :
			jQuery.makeArray( data, [ event ] );

		// Allow special events to draw outside the lines
		special = jQuery.event.special[ type ] || {};
		if ( !onlyHandlers && special.trigger && special.trigger.apply( elem, data ) === false ) {
			return;
		}

		// Determine event propagation path in advance, per W3C events spec (#9951)
		// Bubble up to document, then to window; watch for a global ownerDocument var (#9724)
		if ( !onlyHandlers && !special.noBubble && !jQuery.isWindow( elem ) ) {

			bubbleType = special.delegateType || type;
			if ( !rfocusMorph.test( bubbleType + type ) ) {
				cur = cur.parentNode;
			}
			for ( ; cur; cur = cur.parentNode ) {
				eventPath.push( cur );
				tmp = cur;
			}

			// Only add window if we got to document (e.g., not plain obj or detached DOM)
			if ( tmp === ( elem.ownerDocument || document ) ) {
				eventPath.push( tmp.defaultView || tmp.parentWindow || window );
			}
		}

		// Fire handlers on the event path
		i = 0;
		while ( ( cur = eventPath[ i++ ] ) && !event.isPropagationStopped() ) {

			event.type = i > 1 ?
				bubbleType :
				special.bindType || type;

			// jQuery handler
			handle = ( dataPriv.get( cur, "events" ) || {} )[ event.type ] &&
				dataPriv.get( cur, "handle" );
			if ( handle ) {
				handle.apply( cur, data );
			}

			// Native handler
			handle = ontype && cur[ ontype ];
			if ( handle && handle.apply && acceptData( cur ) ) {
				event.result = handle.apply( cur, data );
				if ( event.result === false ) {
					event.preventDefault();
				}
			}
		}
		event.type = type;

		// If nobody prevented the default action, do it now
		if ( !onlyHandlers && !event.isDefaultPrevented() ) {

			if ( ( !special._default ||
				special._default.apply( eventPath.pop(), data ) === false ) &&
				acceptData( elem ) ) {

				// Call a native DOM method on the target with the same name as the event.
				// Don't do default actions on window, that's where global variables be (#6170)
				if ( ontype && jQuery.isFunction( elem[ type ] ) && !jQuery.isWindow( elem ) ) {

					// Don't re-trigger an onFOO event when we call its FOO() method
					tmp = elem[ ontype ];

					if ( tmp ) {
						elem[ ontype ] = null;
					}

					// Prevent re-triggering of the same event, since we already bubbled it above
					jQuery.event.triggered = type;
					elem[ type ]();
					jQuery.event.triggered = undefined;

					if ( tmp ) {
						elem[ ontype ] = tmp;
					}
				}
			}
		}

		return event.result;
	},

	// Piggyback on a donor event to simulate a different one
	// Used only for `focus(in | out)` events
	simulate: function( type, elem, event ) {
		var e = jQuery.extend(
			new jQuery.Event(),
			event,
			{
				type: type,
				isSimulated: true
			}
		);

		jQuery.event.trigger( e, null, elem );
	}

} );

jQuery.fn.extend( {

	trigger: function( type, data ) {
		return this.each( function() {
			jQuery.event.trigger( type, data, this );
		} );
	},
	triggerHandler: function( type, data ) {
		var elem = this[ 0 ];
		if ( elem ) {
			return jQuery.event.trigger( type, data, elem, true );
		}
	}
} );


jQuery.each( ( "blur focus focusin focusout resize scroll click dblclick " +
	"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
	"change select submit keydown keypress keyup contextmenu" ).split( " " ),
	function( i, name ) {

	// Handle event binding
	jQuery.fn[ name ] = function( data, fn ) {
		return arguments.length > 0 ?
			this.on( name, null, data, fn ) :
			this.trigger( name );
	};
} );

jQuery.fn.extend( {
	hover: function( fnOver, fnOut ) {
		return this.mouseenter( fnOver ).mouseleave( fnOut || fnOver );
	}
} );




support.focusin = "onfocusin" in window;


// Support: Firefox <=44
// Firefox doesn't have focus(in | out) events
// Related ticket - https://bugzilla.mozilla.org/show_bug.cgi?id=687787
//
// Support: Chrome <=48 - 49, Safari <=9.0 - 9.1
// focus(in | out) events fire after focus & blur events,
// which is spec violation - http://www.w3.org/TR/DOM-Level-3-Events/#events-focusevent-event-order
// Related ticket - https://bugs.chromium.org/p/chromium/issues/detail?id=449857
if ( !support.focusin ) {
	jQuery.each( { focus: "focusin", blur: "focusout" }, function( orig, fix ) {

		// Attach a single capturing handler on the document while someone wants focusin/focusout
		var handler = function( event ) {
			jQuery.event.simulate( fix, event.target, jQuery.event.fix( event ) );
		};

		jQuery.event.special[ fix ] = {
			setup: function() {
				var doc = this.ownerDocument || this,
					attaches = dataPriv.access( doc, fix );

				if ( !attaches ) {
					doc.addEventListener( orig, handler, true );
				}
				dataPriv.access( doc, fix, ( attaches || 0 ) + 1 );
			},
			teardown: function() {
				var doc = this.ownerDocument || this,
					attaches = dataPriv.access( doc, fix ) - 1;

				if ( !attaches ) {
					doc.removeEventListener( orig, handler, true );
					dataPriv.remove( doc, fix );

				} else {
					dataPriv.access( doc, fix, attaches );
				}
			}
		};
	} );
}
var location = window.location;

var nonce = jQuery.now();

var rquery = ( /\?/ );



// Cross-browser xml parsing
jQuery.parseXML = function( data ) {
	var xml;
	if ( !data || typeof data !== "string" ) {
		return null;
	}

	// Support: IE 9 - 11 only
	// IE throws on parseFromString with invalid input.
	try {
		xml = ( new window.DOMParser() ).parseFromString( data, "text/xml" );
	} catch ( e ) {
		xml = undefined;
	}

	if ( !xml || xml.getElementsByTagName( "parsererror" ).length ) {
		jQuery.error( "Invalid XML: " + data );
	}
	return xml;
};


var
	rbracket = /\[\]$/,
	rCRLF = /\r?\n/g,
	rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
	rsubmittable = /^(?:input|select|textarea|keygen)/i;

function buildParams( prefix, obj, traditional, add ) {
	var name;

	if ( Array.isArray( obj ) ) {

		// Serialize array item.
		jQuery.each( obj, function( i, v ) {
			if ( traditional || rbracket.test( prefix ) ) {

				// Treat each array item as a scalar.
				add( prefix, v );

			} else {

				// Item is non-scalar (array or object), encode its numeric index.
				buildParams(
					prefix + "[" + ( typeof v === "object" && v != null ? i : "" ) + "]",
					v,
					traditional,
					add
				);
			}
		} );

	} else if ( !traditional && jQuery.type( obj ) === "object" ) {

		// Serialize object item.
		for ( name in obj ) {
			buildParams( prefix + "[" + name + "]", obj[ name ], traditional, add );
		}

	} else {

		// Serialize scalar item.
		add( prefix, obj );
	}
}

// Serialize an array of form elements or a set of
// key/values into a query string
jQuery.param = function( a, traditional ) {
	var prefix,
		s = [],
		add = function( key, valueOrFunction ) {

			// If value is a function, invoke it and use its return value
			var value = jQuery.isFunction( valueOrFunction ) ?
				valueOrFunction() :
				valueOrFunction;

			s[ s.length ] = encodeURIComponent( key ) + "=" +
				encodeURIComponent( value == null ? "" : value );
		};

	// If an array was passed in, assume that it is an array of form elements.
	if ( Array.isArray( a ) || ( a.jquery && !jQuery.isPlainObject( a ) ) ) {

		// Serialize the form elements
		jQuery.each( a, function() {
			add( this.name, this.value );
		} );

	} else {

		// If traditional, encode the "old" way (the way 1.3.2 or older
		// did it), otherwise encode params recursively.
		for ( prefix in a ) {
			buildParams( prefix, a[ prefix ], traditional, add );
		}
	}

	// Return the resulting serialization
	return s.join( "&" );
};

jQuery.fn.extend( {
	serialize: function() {
		return jQuery.param( this.serializeArray() );
	},
	serializeArray: function() {
		return this.map( function() {

			// Can add propHook for "elements" to filter or add form elements
			var elements = jQuery.prop( this, "elements" );
			return elements ? jQuery.makeArray( elements ) : this;
		} )
		.filter( function() {
			var type = this.type;

			// Use .is( ":disabled" ) so that fieldset[disabled] works
			return this.name && !jQuery( this ).is( ":disabled" ) &&
				rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
				( this.checked || !rcheckableType.test( type ) );
		} )
		.map( function( i, elem ) {
			var val = jQuery( this ).val();

			if ( val == null ) {
				return null;
			}

			if ( Array.isArray( val ) ) {
				return jQuery.map( val, function( val ) {
					return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
				} );
			}

			return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
		} ).get();
	}
} );


var
	r20 = /%20/g,
	rhash = /#.*$/,
	rantiCache = /([?&])_=[^&]*/,
	rheaders = /^(.*?):[ \t]*([^\r\n]*)$/mg,

	// #7653, #8125, #8152: local protocol detection
	rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
	rnoContent = /^(?:GET|HEAD)$/,
	rprotocol = /^\/\//,

	/* Prefilters
	 * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
	 * 2) These are called:
	 *    - BEFORE asking for a transport
	 *    - AFTER param serialization (s.data is a string if s.processData is true)
	 * 3) key is the dataType
	 * 4) the catchall symbol "*" can be used
	 * 5) execution will start with transport dataType and THEN continue down to "*" if needed
	 */
	prefilters = {},

	/* Transports bindings
	 * 1) key is the dataType
	 * 2) the catchall symbol "*" can be used
	 * 3) selection will start with transport dataType and THEN go to "*" if needed
	 */
	transports = {},

	// Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
	allTypes = "*/".concat( "*" ),

	// Anchor tag for parsing the document origin
	originAnchor = document.createElement( "a" );
	originAnchor.href = location.href;

// Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport
function addToPrefiltersOrTransports( structure ) {

	// dataTypeExpression is optional and defaults to "*"
	return function( dataTypeExpression, func ) {

		if ( typeof dataTypeExpression !== "string" ) {
			func = dataTypeExpression;
			dataTypeExpression = "*";
		}

		var dataType,
			i = 0,
			dataTypes = dataTypeExpression.toLowerCase().match( rnothtmlwhite ) || [];

		if ( jQuery.isFunction( func ) ) {

			// For each dataType in the dataTypeExpression
			while ( ( dataType = dataTypes[ i++ ] ) ) {

				// Prepend if requested
				if ( dataType[ 0 ] === "+" ) {
					dataType = dataType.slice( 1 ) || "*";
					( structure[ dataType ] = structure[ dataType ] || [] ).unshift( func );

				// Otherwise append
				} else {
					( structure[ dataType ] = structure[ dataType ] || [] ).push( func );
				}
			}
		}
	};
}

// Base inspection function for prefilters and transports
function inspectPrefiltersOrTransports( structure, options, originalOptions, jqXHR ) {

	var inspected = {},
		seekingTransport = ( structure === transports );

	function inspect( dataType ) {
		var selected;
		inspected[ dataType ] = true;
		jQuery.each( structure[ dataType ] || [], function( _, prefilterOrFactory ) {
			var dataTypeOrTransport = prefilterOrFactory( options, originalOptions, jqXHR );
			if ( typeof dataTypeOrTransport === "string" &&
				!seekingTransport && !inspected[ dataTypeOrTransport ] ) {

				options.dataTypes.unshift( dataTypeOrTransport );
				inspect( dataTypeOrTransport );
				return false;
			} else if ( seekingTransport ) {
				return !( selected = dataTypeOrTransport );
			}
		} );
		return selected;
	}

	return inspect( options.dataTypes[ 0 ] ) || !inspected[ "*" ] && inspect( "*" );
}

// A special extend for ajax options
// that takes "flat" options (not to be deep extended)
// Fixes #9887
function ajaxExtend( target, src ) {
	var key, deep,
		flatOptions = jQuery.ajaxSettings.flatOptions || {};

	for ( key in src ) {
		if ( src[ key ] !== undefined ) {
			( flatOptions[ key ] ? target : ( deep || ( deep = {} ) ) )[ key ] = src[ key ];
		}
	}
	if ( deep ) {
		jQuery.extend( true, target, deep );
	}

	return target;
}

/* Handles responses to an ajax request:
 * - finds the right dataType (mediates between content-type and expected dataType)
 * - returns the corresponding response
 */
function ajaxHandleResponses( s, jqXHR, responses ) {

	var ct, type, finalDataType, firstDataType,
		contents = s.contents,
		dataTypes = s.dataTypes;

	// Remove auto dataType and get content-type in the process
	while ( dataTypes[ 0 ] === "*" ) {
		dataTypes.shift();
		if ( ct === undefined ) {
			ct = s.mimeType || jqXHR.getResponseHeader( "Content-Type" );
		}
	}

	// Check if we're dealing with a known content-type
	if ( ct ) {
		for ( type in contents ) {
			if ( contents[ type ] && contents[ type ].test( ct ) ) {
				dataTypes.unshift( type );
				break;
			}
		}
	}

	// Check to see if we have a response for the expected dataType
	if ( dataTypes[ 0 ] in responses ) {
		finalDataType = dataTypes[ 0 ];
	} else {

		// Try convertible dataTypes
		for ( type in responses ) {
			if ( !dataTypes[ 0 ] || s.converters[ type + " " + dataTypes[ 0 ] ] ) {
				finalDataType = type;
				break;
			}
			if ( !firstDataType ) {
				firstDataType = type;
			}
		}

		// Or just use first one
		finalDataType = finalDataType || firstDataType;
	}

	// If we found a dataType
	// We add the dataType to the list if needed
	// and return the corresponding response
	if ( finalDataType ) {
		if ( finalDataType !== dataTypes[ 0 ] ) {
			dataTypes.unshift( finalDataType );
		}
		return responses[ finalDataType ];
	}
}

/* Chain conversions given the request and the original response
 * Also sets the responseXXX fields on the jqXHR instance
 */
function ajaxConvert( s, response, jqXHR, isSuccess ) {
	var conv2, current, conv, tmp, prev,
		converters = {},

		// Work with a copy of dataTypes in case we need to modify it for conversion
		dataTypes = s.dataTypes.slice();

	// Create converters map with lowercased keys
	if ( dataTypes[ 1 ] ) {
		for ( conv in s.converters ) {
			converters[ conv.toLowerCase() ] = s.converters[ conv ];
		}
	}

	current = dataTypes.shift();

	// Convert to each sequential dataType
	while ( current ) {

		if ( s.responseFields[ current ] ) {
			jqXHR[ s.responseFields[ current ] ] = response;
		}

		// Apply the dataFilter if provided
		if ( !prev && isSuccess && s.dataFilter ) {
			response = s.dataFilter( response, s.dataType );
		}

		prev = current;
		current = dataTypes.shift();

		if ( current ) {

			// There's only work to do if current dataType is non-auto
			if ( current === "*" ) {

				current = prev;

			// Convert response if prev dataType is non-auto and differs from current
			} else if ( prev !== "*" && prev !== current ) {

				// Seek a direct converter
				conv = converters[ prev + " " + current ] || converters[ "* " + current ];

				// If none found, seek a pair
				if ( !conv ) {
					for ( conv2 in converters ) {

						// If conv2 outputs current
						tmp = conv2.split( " " );
						if ( tmp[ 1 ] === current ) {

							// If prev can be converted to accepted input
							conv = converters[ prev + " " + tmp[ 0 ] ] ||
								converters[ "* " + tmp[ 0 ] ];
							if ( conv ) {

								// Condense equivalence converters
								if ( conv === true ) {
									conv = converters[ conv2 ];

								// Otherwise, insert the intermediate dataType
								} else if ( converters[ conv2 ] !== true ) {
									current = tmp[ 0 ];
									dataTypes.unshift( tmp[ 1 ] );
								}
								break;
							}
						}
					}
				}

				// Apply converter (if not an equivalence)
				if ( conv !== true ) {

					// Unless errors are allowed to bubble, catch and return them
					if ( conv && s.throws ) {
						response = conv( response );
					} else {
						try {
							response = conv( response );
						} catch ( e ) {
							return {
								state: "parsererror",
								error: conv ? e : "No conversion from " + prev + " to " + current
							};
						}
					}
				}
			}
		}
	}

	return { state: "success", data: response };
}

jQuery.extend( {

	// Counter for holding the number of active queries
	active: 0,

	// Last-Modified header cache for next request
	lastModified: {},
	etag: {},

	ajaxSettings: {
		url: location.href,
		type: "GET",
		isLocal: rlocalProtocol.test( location.protocol ),
		global: true,
		processData: true,
		async: true,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",

		/*
		timeout: 0,
		data: null,
		dataType: null,
		username: null,
		password: null,
		cache: null,
		throws: false,
		traditional: false,
		headers: {},
		*/

		accepts: {
			"*": allTypes,
			text: "text/plain",
			html: "text/html",
			xml: "application/xml, text/xml",
			json: "application/json, text/javascript"
		},

		contents: {
			xml: /\bxml\b/,
			html: /\bhtml/,
			json: /\bjson\b/
		},

		responseFields: {
			xml: "responseXML",
			text: "responseText",
			json: "responseJSON"
		},

		// Data converters
		// Keys separate source (or catchall "*") and destination types with a single space
		converters: {

			// Convert anything to text
			"* text": String,

			// Text to html (true = no transformation)
			"text html": true,

			// Evaluate text as a json expression
			"text json": JSON.parse,

			// Parse text as xml
			"text xml": jQuery.parseXML
		},

		// For options that shouldn't be deep extended:
		// you can add your own custom options here if
		// and when you create one that shouldn't be
		// deep extended (see ajaxExtend)
		flatOptions: {
			url: true,
			context: true
		}
	},

	// Creates a full fledged settings object into target
	// with both ajaxSettings and settings fields.
	// If target is omitted, writes into ajaxSettings.
	ajaxSetup: function( target, settings ) {
		return settings ?

			// Building a settings object
			ajaxExtend( ajaxExtend( target, jQuery.ajaxSettings ), settings ) :

			// Extending ajaxSettings
			ajaxExtend( jQuery.ajaxSettings, target );
	},

	ajaxPrefilter: addToPrefiltersOrTransports( prefilters ),
	ajaxTransport: addToPrefiltersOrTransports( transports ),

	// Main method
	ajax: function( url, options ) {

		// If url is an object, simulate pre-1.5 signature
		if ( typeof url === "object" ) {
			options = url;
			url = undefined;
		}

		// Force options to be an object
		options = options || {};

		var transport,

			// URL without anti-cache param
			cacheURL,

			// Response headers
			responseHeadersString,
			responseHeaders,

			// timeout handle
			timeoutTimer,

			// Url cleanup var
			urlAnchor,

			// Request state (becomes false upon send and true upon completion)
			completed,

			// To know if global events are to be dispatched
			fireGlobals,

			// Loop variable
			i,

			// uncached part of the url
			uncached,

			// Create the final options object
			s = jQuery.ajaxSetup( {}, options ),

			// Callbacks context
			callbackContext = s.context || s,

			// Context for global events is callbackContext if it is a DOM node or jQuery collection
			globalEventContext = s.context &&
				( callbackContext.nodeType || callbackContext.jquery ) ?
					jQuery( callbackContext ) :
					jQuery.event,

			// Deferreds
			deferred = jQuery.Deferred(),
			completeDeferred = jQuery.Callbacks( "once memory" ),

			// Status-dependent callbacks
			statusCode = s.statusCode || {},

			// Headers (they are sent all at once)
			requestHeaders = {},
			requestHeadersNames = {},

			// Default abort message
			strAbort = "canceled",

			// Fake xhr
			jqXHR = {
				readyState: 0,

				// Builds headers hashtable if needed
				getResponseHeader: function( key ) {
					var match;
					if ( completed ) {
						if ( !responseHeaders ) {
							responseHeaders = {};
							while ( ( match = rheaders.exec( responseHeadersString ) ) ) {
								responseHeaders[ match[ 1 ].toLowerCase() ] = match[ 2 ];
							}
						}
						match = responseHeaders[ key.toLowerCase() ];
					}
					return match == null ? null : match;
				},

				// Raw string
				getAllResponseHeaders: function() {
					return completed ? responseHeadersString : null;
				},

				// Caches the header
				setRequestHeader: function( name, value ) {
					if ( completed == null ) {
						name = requestHeadersNames[ name.toLowerCase() ] =
							requestHeadersNames[ name.toLowerCase() ] || name;
						requestHeaders[ name ] = value;
					}
					return this;
				},

				// Overrides response content-type header
				overrideMimeType: function( type ) {
					if ( completed == null ) {
						s.mimeType = type;
					}
					return this;
				},

				// Status-dependent callbacks
				statusCode: function( map ) {
					var code;
					if ( map ) {
						if ( completed ) {

							// Execute the appropriate callbacks
							jqXHR.always( map[ jqXHR.status ] );
						} else {

							// Lazy-add the new callbacks in a way that preserves old ones
							for ( code in map ) {
								statusCode[ code ] = [ statusCode[ code ], map[ code ] ];
							}
						}
					}
					return this;
				},

				// Cancel the request
				abort: function( statusText ) {
					var finalText = statusText || strAbort;
					if ( transport ) {
						transport.abort( finalText );
					}
					done( 0, finalText );
					return this;
				}
			};

		// Attach deferreds
		deferred.promise( jqXHR );

		// Add protocol if not provided (prefilters might expect it)
		// Handle falsy url in the settings object (#10093: consistency with old signature)
		// We also use the url parameter if available
		s.url = ( ( url || s.url || location.href ) + "" )
			.replace( rprotocol, location.protocol + "//" );

		// Alias method option to type as per ticket #12004
		s.type = options.method || options.type || s.method || s.type;

		// Extract dataTypes list
		s.dataTypes = ( s.dataType || "*" ).toLowerCase().match( rnothtmlwhite ) || [ "" ];

		// A cross-domain request is in order when the origin doesn't match the current origin.
		if ( s.crossDomain == null ) {
			urlAnchor = document.createElement( "a" );

			// Support: IE <=8 - 11, Edge 12 - 13
			// IE throws exception on accessing the href property if url is malformed,
			// e.g. http://example.com:80x/
			try {
				urlAnchor.href = s.url;

				// Support: IE <=8 - 11 only
				// Anchor's host property isn't correctly set when s.url is relative
				urlAnchor.href = urlAnchor.href;
				s.crossDomain = originAnchor.protocol + "//" + originAnchor.host !==
					urlAnchor.protocol + "//" + urlAnchor.host;
			} catch ( e ) {

				// If there is an error parsing the URL, assume it is crossDomain,
				// it can be rejected by the transport if it is invalid
				s.crossDomain = true;
			}
		}

		// Convert data if not already a string
		if ( s.data && s.processData && typeof s.data !== "string" ) {
			s.data = jQuery.param( s.data, s.traditional );
		}

		// Apply prefilters
		inspectPrefiltersOrTransports( prefilters, s, options, jqXHR );

		// If request was aborted inside a prefilter, stop there
		if ( completed ) {
			return jqXHR;
		}

		// We can fire global events as of now if asked to
		// Don't fire events if jQuery.event is undefined in an AMD-usage scenario (#15118)
		fireGlobals = jQuery.event && s.global;

		// Watch for a new set of requests
		if ( fireGlobals && jQuery.active++ === 0 ) {
			jQuery.event.trigger( "ajaxStart" );
		}

		// Uppercase the type
		s.type = s.type.toUpperCase();

		// Determine if request has content
		s.hasContent = !rnoContent.test( s.type );

		// Save the URL in case we're toying with the If-Modified-Since
		// and/or If-None-Match header later on
		// Remove hash to simplify url manipulation
		cacheURL = s.url.replace( rhash, "" );

		// More options handling for requests with no content
		if ( !s.hasContent ) {

			// Remember the hash so we can put it back
			uncached = s.url.slice( cacheURL.length );

			// If data is available, append data to url
			if ( s.data ) {
				cacheURL += ( rquery.test( cacheURL ) ? "&" : "?" ) + s.data;

				// #9682: remove data so that it's not used in an eventual retry
				delete s.data;
			}

			// Add or update anti-cache param if needed
			if ( s.cache === false ) {
				cacheURL = cacheURL.replace( rantiCache, "$1" );
				uncached = ( rquery.test( cacheURL ) ? "&" : "?" ) + "_=" + ( nonce++ ) + uncached;
			}

			// Put hash and anti-cache on the URL that will be requested (gh-1732)
			s.url = cacheURL + uncached;

		// Change '%20' to '+' if this is encoded form body content (gh-2658)
		} else if ( s.data && s.processData &&
			( s.contentType || "" ).indexOf( "application/x-www-form-urlencoded" ) === 0 ) {
			s.data = s.data.replace( r20, "+" );
		}

		// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
		if ( s.ifModified ) {
			if ( jQuery.lastModified[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-Modified-Since", jQuery.lastModified[ cacheURL ] );
			}
			if ( jQuery.etag[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-None-Match", jQuery.etag[ cacheURL ] );
			}
		}

		// Set the correct header, if data is being sent
		if ( s.data && s.hasContent && s.contentType !== false || options.contentType ) {
			jqXHR.setRequestHeader( "Content-Type", s.contentType );
		}

		// Set the Accepts header for the server, depending on the dataType
		jqXHR.setRequestHeader(
			"Accept",
			s.dataTypes[ 0 ] && s.accepts[ s.dataTypes[ 0 ] ] ?
				s.accepts[ s.dataTypes[ 0 ] ] +
					( s.dataTypes[ 0 ] !== "*" ? ", " + allTypes + "; q=0.01" : "" ) :
				s.accepts[ "*" ]
		);

		// Check for headers option
		for ( i in s.headers ) {
			jqXHR.setRequestHeader( i, s.headers[ i ] );
		}

		// Allow custom headers/mimetypes and early abort
		if ( s.beforeSend &&
			( s.beforeSend.call( callbackContext, jqXHR, s ) === false || completed ) ) {

			// Abort if not done already and return
			return jqXHR.abort();
		}

		// Aborting is no longer a cancellation
		strAbort = "abort";

		// Install callbacks on deferreds
		completeDeferred.add( s.complete );
		jqXHR.done( s.success );
		jqXHR.fail( s.error );

		// Get transport
		transport = inspectPrefiltersOrTransports( transports, s, options, jqXHR );

		// If no transport, we auto-abort
		if ( !transport ) {
			done( -1, "No Transport" );
		} else {
			jqXHR.readyState = 1;

			// Send global event
			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxSend", [ jqXHR, s ] );
			}

			// If request was aborted inside ajaxSend, stop there
			if ( completed ) {
				return jqXHR;
			}

			// Timeout
			if ( s.async && s.timeout > 0 ) {
				timeoutTimer = window.setTimeout( function() {
					jqXHR.abort( "timeout" );
				}, s.timeout );
			}

			try {
				completed = false;
				transport.send( requestHeaders, done );
			} catch ( e ) {

				// Rethrow post-completion exceptions
				if ( completed ) {
					throw e;
				}

				// Propagate others as results
				done( -1, e );
			}
		}

		// Callback for when everything is done
		function done( status, nativeStatusText, responses, headers ) {
			var isSuccess, success, error, response, modified,
				statusText = nativeStatusText;

			// Ignore repeat invocations
			if ( completed ) {
				return;
			}

			completed = true;

			// Clear timeout if it exists
			if ( timeoutTimer ) {
				window.clearTimeout( timeoutTimer );
			}

			// Dereference transport for early garbage collection
			// (no matter how long the jqXHR object will be used)
			transport = undefined;

			// Cache response headers
			responseHeadersString = headers || "";

			// Set readyState
			jqXHR.readyState = status > 0 ? 4 : 0;

			// Determine if successful
			isSuccess = status >= 200 && status < 300 || status === 304;

			// Get response data
			if ( responses ) {
				response = ajaxHandleResponses( s, jqXHR, responses );
			}

			// Convert no matter what (that way responseXXX fields are always set)
			response = ajaxConvert( s, response, jqXHR, isSuccess );

			// If successful, handle type chaining
			if ( isSuccess ) {

				// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
				if ( s.ifModified ) {
					modified = jqXHR.getResponseHeader( "Last-Modified" );
					if ( modified ) {
						jQuery.lastModified[ cacheURL ] = modified;
					}
					modified = jqXHR.getResponseHeader( "etag" );
					if ( modified ) {
						jQuery.etag[ cacheURL ] = modified;
					}
				}

				// if no content
				if ( status === 204 || s.type === "HEAD" ) {
					statusText = "nocontent";

				// if not modified
				} else if ( status === 304 ) {
					statusText = "notmodified";

				// If we have data, let's convert it
				} else {
					statusText = response.state;
					success = response.data;
					error = response.error;
					isSuccess = !error;
				}
			} else {

				// Extract error from statusText and normalize for non-aborts
				error = statusText;
				if ( status || !statusText ) {
					statusText = "error";
					if ( status < 0 ) {
						status = 0;
					}
				}
			}

			// Set data for the fake xhr object
			jqXHR.status = status;
			jqXHR.statusText = ( nativeStatusText || statusText ) + "";

			// Success/Error
			if ( isSuccess ) {
				deferred.resolveWith( callbackContext, [ success, statusText, jqXHR ] );
			} else {
				deferred.rejectWith( callbackContext, [ jqXHR, statusText, error ] );
			}

			// Status-dependent callbacks
			jqXHR.statusCode( statusCode );
			statusCode = undefined;

			if ( fireGlobals ) {
				globalEventContext.trigger( isSuccess ? "ajaxSuccess" : "ajaxError",
					[ jqXHR, s, isSuccess ? success : error ] );
			}

			// Complete
			completeDeferred.fireWith( callbackContext, [ jqXHR, statusText ] );

			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxComplete", [ jqXHR, s ] );

				// Handle the global AJAX counter
				if ( !( --jQuery.active ) ) {
					jQuery.event.trigger( "ajaxStop" );
				}
			}
		}

		return jqXHR;
	},

	getJSON: function( url, data, callback ) {
		return jQuery.get( url, data, callback, "json" );
	},

	getScript: function( url, callback ) {
		return jQuery.get( url, undefined, callback, "script" );
	}
} );

jQuery.each( [ "get", "post" ], function( i, method ) {
	jQuery[ method ] = function( url, data, callback, type ) {

		// Shift arguments if data argument was omitted
		if ( jQuery.isFunction( data ) ) {
			type = type || callback;
			callback = data;
			data = undefined;
		}

		// The url can be an options object (which then must have .url)
		return jQuery.ajax( jQuery.extend( {
			url: url,
			type: method,
			dataType: type,
			data: data,
			success: callback
		}, jQuery.isPlainObject( url ) && url ) );
	};
} );


jQuery._evalUrl = function( url ) {
	return jQuery.ajax( {
		url: url,

		// Make this explicit, since user can override this through ajaxSetup (#11264)
		type: "GET",
		dataType: "script",
		cache: true,
		async: false,
		global: false,
		"throws": true
	} );
};


jQuery.fn.extend( {
	wrapAll: function( html ) {
		var wrap;

		if ( this[ 0 ] ) {
			if ( jQuery.isFunction( html ) ) {
				html = html.call( this[ 0 ] );
			}

			// The elements to wrap the target around
			wrap = jQuery( html, this[ 0 ].ownerDocument ).eq( 0 ).clone( true );

			if ( this[ 0 ].parentNode ) {
				wrap.insertBefore( this[ 0 ] );
			}

			wrap.map( function() {
				var elem = this;

				while ( elem.firstElementChild ) {
					elem = elem.firstElementChild;
				}

				return elem;
			} ).append( this );
		}

		return this;
	},

	wrapInner: function( html ) {
		if ( jQuery.isFunction( html ) ) {
			return this.each( function( i ) {
				jQuery( this ).wrapInner( html.call( this, i ) );
			} );
		}

		return this.each( function() {
			var self = jQuery( this ),
				contents = self.contents();

			if ( contents.length ) {
				contents.wrapAll( html );

			} else {
				self.append( html );
			}
		} );
	},

	wrap: function( html ) {
		var isFunction = jQuery.isFunction( html );

		return this.each( function( i ) {
			jQuery( this ).wrapAll( isFunction ? html.call( this, i ) : html );
		} );
	},

	unwrap: function( selector ) {
		this.parent( selector ).not( "body" ).each( function() {
			jQuery( this ).replaceWith( this.childNodes );
		} );
		return this;
	}
} );


jQuery.expr.pseudos.hidden = function( elem ) {
	return !jQuery.expr.pseudos.visible( elem );
};
jQuery.expr.pseudos.visible = function( elem ) {
	return !!( elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length );
};




jQuery.ajaxSettings.xhr = function() {
	try {
		return new window.XMLHttpRequest();
	} catch ( e ) {}
};

var xhrSuccessStatus = {

		// File protocol always yields status code 0, assume 200
		0: 200,

		// Support: IE <=9 only
		// #1450: sometimes IE returns 1223 when it should be 204
		1223: 204
	},
	xhrSupported = jQuery.ajaxSettings.xhr();

support.cors = !!xhrSupported && ( "withCredentials" in xhrSupported );
support.ajax = xhrSupported = !!xhrSupported;

jQuery.ajaxTransport( function( options ) {
	var callback, errorCallback;

	// Cross domain only allowed if supported through XMLHttpRequest
	if ( support.cors || xhrSupported && !options.crossDomain ) {
		return {
			send: function( headers, complete ) {
				var i,
					xhr = options.xhr();

				xhr.open(
					options.type,
					options.url,
					options.async,
					options.username,
					options.password
				);

				// Apply custom fields if provided
				if ( options.xhrFields ) {
					for ( i in options.xhrFields ) {
						xhr[ i ] = options.xhrFields[ i ];
					}
				}

				// Override mime type if needed
				if ( options.mimeType && xhr.overrideMimeType ) {
					xhr.overrideMimeType( options.mimeType );
				}

				// X-Requested-With header
				// For cross-domain requests, seeing as conditions for a preflight are
				// akin to a jigsaw puzzle, we simply never set it to be sure.
				// (it can always be set on a per-request basis or even using ajaxSetup)
				// For same-domain requests, won't change header if already provided.
				if ( !options.crossDomain && !headers[ "X-Requested-With" ] ) {
					headers[ "X-Requested-With" ] = "XMLHttpRequest";
				}

				// Set headers
				for ( i in headers ) {
					xhr.setRequestHeader( i, headers[ i ] );
				}

				// Callback
				callback = function( type ) {
					return function() {
						if ( callback ) {
							callback = errorCallback = xhr.onload =
								xhr.onerror = xhr.onabort = xhr.onreadystatechange = null;

							if ( type === "abort" ) {
								xhr.abort();
							} else if ( type === "error" ) {

								// Support: IE <=9 only
								// On a manual native abort, IE9 throws
								// errors on any property access that is not readyState
								if ( typeof xhr.status !== "number" ) {
									complete( 0, "error" );
								} else {
									complete(

										// File: protocol always yields status 0; see #8605, #14207
										xhr.status,
										xhr.statusText
									);
								}
							} else {
								complete(
									xhrSuccessStatus[ xhr.status ] || xhr.status,
									xhr.statusText,

									// Support: IE <=9 only
									// IE9 has no XHR2 but throws on binary (trac-11426)
									// For XHR2 non-text, let the caller handle it (gh-2498)
									( xhr.responseType || "text" ) !== "text"  ||
									typeof xhr.responseText !== "string" ?
										{ binary: xhr.response } :
										{ text: xhr.responseText },
									xhr.getAllResponseHeaders()
								);
							}
						}
					};
				};

				// Listen to events
				xhr.onload = callback();
				errorCallback = xhr.onerror = callback( "error" );

				// Support: IE 9 only
				// Use onreadystatechange to replace onabort
				// to handle uncaught aborts
				if ( xhr.onabort !== undefined ) {
					xhr.onabort = errorCallback;
				} else {
					xhr.onreadystatechange = function() {

						// Check readyState before timeout as it changes
						if ( xhr.readyState === 4 ) {

							// Allow onerror to be called first,
							// but that will not handle a native abort
							// Also, save errorCallback to a variable
							// as xhr.onerror cannot be accessed
							window.setTimeout( function() {
								if ( callback ) {
									errorCallback();
								}
							} );
						}
					};
				}

				// Create the abort callback
				callback = callback( "abort" );

				try {

					// Do send the request (this may raise an exception)
					xhr.send( options.hasContent && options.data || null );
				} catch ( e ) {

					// #14683: Only rethrow if this hasn't been notified as an error yet
					if ( callback ) {
						throw e;
					}
				}
			},

			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
} );




// Prevent auto-execution of scripts when no explicit dataType was provided (See gh-2432)
jQuery.ajaxPrefilter( function( s ) {
	if ( s.crossDomain ) {
		s.contents.script = false;
	}
} );

// Install script dataType
jQuery.ajaxSetup( {
	accepts: {
		script: "text/javascript, application/javascript, " +
			"application/ecmascript, application/x-ecmascript"
	},
	contents: {
		script: /\b(?:java|ecma)script\b/
	},
	converters: {
		"text script": function( text ) {
			jQuery.globalEval( text );
			return text;
		}
	}
} );

// Handle cache's special case and crossDomain
jQuery.ajaxPrefilter( "script", function( s ) {
	if ( s.cache === undefined ) {
		s.cache = false;
	}
	if ( s.crossDomain ) {
		s.type = "GET";
	}
} );

// Bind script tag hack transport
jQuery.ajaxTransport( "script", function( s ) {

	// This transport only deals with cross domain requests
	if ( s.crossDomain ) {
		var script, callback;
		return {
			send: function( _, complete ) {
				script = jQuery( "<script>" ).prop( {
					charset: s.scriptCharset,
					src: s.url
				} ).on(
					"load error",
					callback = function( evt ) {
						script.remove();
						callback = null;
						if ( evt ) {
							complete( evt.type === "error" ? 404 : 200, evt.type );
						}
					}
				);

				// Use native DOM manipulation to avoid our domManip AJAX trickery
				document.head.appendChild( script[ 0 ] );
			},
			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
} );




var oldCallbacks = [],
	rjsonp = /(=)\?(?=&|$)|\?\?/;

// Default jsonp settings
jQuery.ajaxSetup( {
	jsonp: "callback",
	jsonpCallback: function() {
		var callback = oldCallbacks.pop() || ( jQuery.expando + "_" + ( nonce++ ) );
		this[ callback ] = true;
		return callback;
	}
} );

// Detect, normalize options and install callbacks for jsonp requests
jQuery.ajaxPrefilter( "json jsonp", function( s, originalSettings, jqXHR ) {

	var callbackName, overwritten, responseContainer,
		jsonProp = s.jsonp !== false && ( rjsonp.test( s.url ) ?
			"url" :
			typeof s.data === "string" &&
				( s.contentType || "" )
					.indexOf( "application/x-www-form-urlencoded" ) === 0 &&
				rjsonp.test( s.data ) && "data"
		);

	// Handle iff the expected data type is "jsonp" or we have a parameter to set
	if ( jsonProp || s.dataTypes[ 0 ] === "jsonp" ) {

		// Get callback name, remembering preexisting value associated with it
		callbackName = s.jsonpCallback = jQuery.isFunction( s.jsonpCallback ) ?
			s.jsonpCallback() :
			s.jsonpCallback;

		// Insert callback into url or form data
		if ( jsonProp ) {
			s[ jsonProp ] = s[ jsonProp ].replace( rjsonp, "$1" + callbackName );
		} else if ( s.jsonp !== false ) {
			s.url += ( rquery.test( s.url ) ? "&" : "?" ) + s.jsonp + "=" + callbackName;
		}

		// Use data converter to retrieve json after script execution
		s.converters[ "script json" ] = function() {
			if ( !responseContainer ) {
				jQuery.error( callbackName + " was not called" );
			}
			return responseContainer[ 0 ];
		};

		// Force json dataType
		s.dataTypes[ 0 ] = "json";

		// Install callback
		overwritten = window[ callbackName ];
		window[ callbackName ] = function() {
			responseContainer = arguments;
		};

		// Clean-up function (fires after converters)
		jqXHR.always( function() {

			// If previous value didn't exist - remove it
			if ( overwritten === undefined ) {
				jQuery( window ).removeProp( callbackName );

			// Otherwise restore preexisting value
			} else {
				window[ callbackName ] = overwritten;
			}

			// Save back as free
			if ( s[ callbackName ] ) {

				// Make sure that re-using the options doesn't screw things around
				s.jsonpCallback = originalSettings.jsonpCallback;

				// Save the callback name for future use
				oldCallbacks.push( callbackName );
			}

			// Call if it was a function and we have a response
			if ( responseContainer && jQuery.isFunction( overwritten ) ) {
				overwritten( responseContainer[ 0 ] );
			}

			responseContainer = overwritten = undefined;
		} );

		// Delegate to script
		return "script";
	}
} );




// Support: Safari 8 only
// In Safari 8 documents created via document.implementation.createHTMLDocument
// collapse sibling forms: the second one becomes a child of the first one.
// Because of that, this security measure has to be disabled in Safari 8.
// https://bugs.webkit.org/show_bug.cgi?id=137337
support.createHTMLDocument = ( function() {
	var body = document.implementation.createHTMLDocument( "" ).body;
	body.innerHTML = "<form></form><form></form>";
	return body.childNodes.length === 2;
} )();


// Argument "data" should be string of html
// context (optional): If specified, the fragment will be created in this context,
// defaults to document
// keepScripts (optional): If true, will include scripts passed in the html string
jQuery.parseHTML = function( data, context, keepScripts ) {
	if ( typeof data !== "string" ) {
		return [];
	}
	if ( typeof context === "boolean" ) {
		keepScripts = context;
		context = false;
	}

	var base, parsed, scripts;

	if ( !context ) {

		// Stop scripts or inline event handlers from being executed immediately
		// by using document.implementation
		if ( support.createHTMLDocument ) {
			context = document.implementation.createHTMLDocument( "" );

			// Set the base href for the created document
			// so any parsed elements with URLs
			// are based on the document's URL (gh-2965)
			base = context.createElement( "base" );
			base.href = document.location.href;
			context.head.appendChild( base );
		} else {
			context = document;
		}
	}

	parsed = rsingleTag.exec( data );
	scripts = !keepScripts && [];

	// Single tag
	if ( parsed ) {
		return [ context.createElement( parsed[ 1 ] ) ];
	}

	parsed = buildFragment( [ data ], context, scripts );

	if ( scripts && scripts.length ) {
		jQuery( scripts ).remove();
	}

	return jQuery.merge( [], parsed.childNodes );
};


/**
 * Load a url into a page
 */
jQuery.fn.load = function( url, params, callback ) {
	var selector, type, response,
		self = this,
		off = url.indexOf( " " );

	if ( off > -1 ) {
		selector = stripAndCollapse( url.slice( off ) );
		url = url.slice( 0, off );
	}

	// If it's a function
	if ( jQuery.isFunction( params ) ) {

		// We assume that it's the callback
		callback = params;
		params = undefined;

	// Otherwise, build a param string
	} else if ( params && typeof params === "object" ) {
		type = "POST";
	}

	// If we have elements to modify, make the request
	if ( self.length > 0 ) {
		jQuery.ajax( {
			url: url,

			// If "type" variable is undefined, then "GET" method will be used.
			// Make value of this field explicit since
			// user can override it through ajaxSetup method
			type: type || "GET",
			dataType: "html",
			data: params
		} ).done( function( responseText ) {

			// Save response for use in complete callback
			response = arguments;

			self.html( selector ?

				// If a selector was specified, locate the right elements in a dummy div
				// Exclude scripts to avoid IE 'Permission Denied' errors
				jQuery( "<div>" ).append( jQuery.parseHTML( responseText ) ).find( selector ) :

				// Otherwise use the full result
				responseText );

		// If the request succeeds, this function gets "data", "status", "jqXHR"
		// but they are ignored because response was set above.
		// If it fails, this function gets "jqXHR", "status", "error"
		} ).always( callback && function( jqXHR, status ) {
			self.each( function() {
				callback.apply( this, response || [ jqXHR.responseText, status, jqXHR ] );
			} );
		} );
	}

	return this;
};




// Attach a bunch of functions for handling common AJAX events
jQuery.each( [
	"ajaxStart",
	"ajaxStop",
	"ajaxComplete",
	"ajaxError",
	"ajaxSuccess",
	"ajaxSend"
], function( i, type ) {
	jQuery.fn[ type ] = function( fn ) {
		return this.on( type, fn );
	};
} );




jQuery.expr.pseudos.animated = function( elem ) {
	return jQuery.grep( jQuery.timers, function( fn ) {
		return elem === fn.elem;
	} ).length;
};




jQuery.offset = {
	setOffset: function( elem, options, i ) {
		var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition,
			position = jQuery.css( elem, "position" ),
			curElem = jQuery( elem ),
			props = {};

		// Set position first, in-case top/left are set even on static elem
		if ( position === "static" ) {
			elem.style.position = "relative";
		}

		curOffset = curElem.offset();
		curCSSTop = jQuery.css( elem, "top" );
		curCSSLeft = jQuery.css( elem, "left" );
		calculatePosition = ( position === "absolute" || position === "fixed" ) &&
			( curCSSTop + curCSSLeft ).indexOf( "auto" ) > -1;

		// Need to be able to calculate position if either
		// top or left is auto and position is either absolute or fixed
		if ( calculatePosition ) {
			curPosition = curElem.position();
			curTop = curPosition.top;
			curLeft = curPosition.left;

		} else {
			curTop = parseFloat( curCSSTop ) || 0;
			curLeft = parseFloat( curCSSLeft ) || 0;
		}

		if ( jQuery.isFunction( options ) ) {

			// Use jQuery.extend here to allow modification of coordinates argument (gh-1848)
			options = options.call( elem, i, jQuery.extend( {}, curOffset ) );
		}

		if ( options.top != null ) {
			props.top = ( options.top - curOffset.top ) + curTop;
		}
		if ( options.left != null ) {
			props.left = ( options.left - curOffset.left ) + curLeft;
		}

		if ( "using" in options ) {
			options.using.call( elem, props );

		} else {
			curElem.css( props );
		}
	}
};

jQuery.fn.extend( {
	offset: function( options ) {

		// Preserve chaining for setter
		if ( arguments.length ) {
			return options === undefined ?
				this :
				this.each( function( i ) {
					jQuery.offset.setOffset( this, options, i );
				} );
		}

		var doc, docElem, rect, win,
			elem = this[ 0 ];

		if ( !elem ) {
			return;
		}

		// Return zeros for disconnected and hidden (display: none) elements (gh-2310)
		// Support: IE <=11 only
		// Running getBoundingClientRect on a
		// disconnected node in IE throws an error
		if ( !elem.getClientRects().length ) {
			return { top: 0, left: 0 };
		}

		rect = elem.getBoundingClientRect();

		doc = elem.ownerDocument;
		docElem = doc.documentElement;
		win = doc.defaultView;

		return {
			top: rect.top + win.pageYOffset - docElem.clientTop,
			left: rect.left + win.pageXOffset - docElem.clientLeft
		};
	},

	position: function() {
		if ( !this[ 0 ] ) {
			return;
		}

		var offsetParent, offset,
			elem = this[ 0 ],
			parentOffset = { top: 0, left: 0 };

		// Fixed elements are offset from window (parentOffset = {top:0, left: 0},
		// because it is its only offset parent
		if ( jQuery.css( elem, "position" ) === "fixed" ) {

			// Assume getBoundingClientRect is there when computed position is fixed
			offset = elem.getBoundingClientRect();

		} else {

			// Get *real* offsetParent
			offsetParent = this.offsetParent();

			// Get correct offsets
			offset = this.offset();
			if ( !nodeName( offsetParent[ 0 ], "html" ) ) {
				parentOffset = offsetParent.offset();
			}

			// Add offsetParent borders
			parentOffset = {
				top: parentOffset.top + jQuery.css( offsetParent[ 0 ], "borderTopWidth", true ),
				left: parentOffset.left + jQuery.css( offsetParent[ 0 ], "borderLeftWidth", true )
			};
		}

		// Subtract parent offsets and element margins
		return {
			top: offset.top - parentOffset.top - jQuery.css( elem, "marginTop", true ),
			left: offset.left - parentOffset.left - jQuery.css( elem, "marginLeft", true )
		};
	},

	// This method will return documentElement in the following cases:
	// 1) For the element inside the iframe without offsetParent, this method will return
	//    documentElement of the parent window
	// 2) For the hidden or detached element
	// 3) For body or html element, i.e. in case of the html node - it will return itself
	//
	// but those exceptions were never presented as a real life use-cases
	// and might be considered as more preferable results.
	//
	// This logic, however, is not guaranteed and can change at any point in the future
	offsetParent: function() {
		return this.map( function() {
			var offsetParent = this.offsetParent;

			while ( offsetParent && jQuery.css( offsetParent, "position" ) === "static" ) {
				offsetParent = offsetParent.offsetParent;
			}

			return offsetParent || documentElement;
		} );
	}
} );

// Create scrollLeft and scrollTop methods
jQuery.each( { scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function( method, prop ) {
	var top = "pageYOffset" === prop;

	jQuery.fn[ method ] = function( val ) {
		return access( this, function( elem, method, val ) {

			// Coalesce documents and windows
			var win;
			if ( jQuery.isWindow( elem ) ) {
				win = elem;
			} else if ( elem.nodeType === 9 ) {
				win = elem.defaultView;
			}

			if ( val === undefined ) {
				return win ? win[ prop ] : elem[ method ];
			}

			if ( win ) {
				win.scrollTo(
					!top ? val : win.pageXOffset,
					top ? val : win.pageYOffset
				);

			} else {
				elem[ method ] = val;
			}
		}, method, val, arguments.length );
	};
} );

// Support: Safari <=7 - 9.1, Chrome <=37 - 49
// Add the top/left cssHooks using jQuery.fn.position
// Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084
// Blink bug: https://bugs.chromium.org/p/chromium/issues/detail?id=589347
// getComputedStyle returns percent when specified for top/left/bottom/right;
// rather than make the css module depend on the offset module, just check for it here
jQuery.each( [ "top", "left" ], function( i, prop ) {
	jQuery.cssHooks[ prop ] = addGetHookIf( support.pixelPosition,
		function( elem, computed ) {
			if ( computed ) {
				computed = curCSS( elem, prop );

				// If curCSS returns percentage, fallback to offset
				return rnumnonpx.test( computed ) ?
					jQuery( elem ).position()[ prop ] + "px" :
					computed;
			}
		}
	);
} );


// Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods
jQuery.each( { Height: "height", Width: "width" }, function( name, type ) {
	jQuery.each( { padding: "inner" + name, content: type, "": "outer" + name },
		function( defaultExtra, funcName ) {

		// Margin is only for outerHeight, outerWidth
		jQuery.fn[ funcName ] = function( margin, value ) {
			var chainable = arguments.length && ( defaultExtra || typeof margin !== "boolean" ),
				extra = defaultExtra || ( margin === true || value === true ? "margin" : "border" );

			return access( this, function( elem, type, value ) {
				var doc;

				if ( jQuery.isWindow( elem ) ) {

					// $( window ).outerWidth/Height return w/h including scrollbars (gh-1729)
					return funcName.indexOf( "outer" ) === 0 ?
						elem[ "inner" + name ] :
						elem.document.documentElement[ "client" + name ];
				}

				// Get document width or height
				if ( elem.nodeType === 9 ) {
					doc = elem.documentElement;

					// Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height],
					// whichever is greatest
					return Math.max(
						elem.body[ "scroll" + name ], doc[ "scroll" + name ],
						elem.body[ "offset" + name ], doc[ "offset" + name ],
						doc[ "client" + name ]
					);
				}

				return value === undefined ?

					// Get width or height on the element, requesting but not forcing parseFloat
					jQuery.css( elem, type, extra ) :

					// Set width or height on the element
					jQuery.style( elem, type, value, extra );
			}, type, chainable ? margin : undefined, chainable );
		};
	} );
} );


jQuery.fn.extend( {

	bind: function( types, data, fn ) {
		return this.on( types, null, data, fn );
	},
	unbind: function( types, fn ) {
		return this.off( types, null, fn );
	},

	delegate: function( selector, types, data, fn ) {
		return this.on( types, selector, data, fn );
	},
	undelegate: function( selector, types, fn ) {

		// ( namespace ) or ( selector, types [, fn] )
		return arguments.length === 1 ?
			this.off( selector, "**" ) :
			this.off( types, selector || "**", fn );
	}
} );

jQuery.holdReady = function( hold ) {
	if ( hold ) {
		jQuery.readyWait++;
	} else {
		jQuery.ready( true );
	}
};
jQuery.isArray = Array.isArray;
jQuery.parseJSON = JSON.parse;
jQuery.nodeName = nodeName;




// Register as a named AMD module, since jQuery can be concatenated with other
// files that may use define, but not via a proper concatenation script that
// understands anonymous AMD modules. A named AMD is safest and most robust
// way to register. Lowercase jquery is used because AMD module names are
// derived from file names, and jQuery is normally delivered in a lowercase
// file name. Do this after creating the global so that if an AMD module wants
// to call noConflict to hide this version of jQuery, it will work.

// Note that for maximum portability, libraries that are not jQuery should
// declare themselves as anonymous modules, and avoid setting a global if an
// AMD loader is present. jQuery is a special case. For more information, see
// https://github.com/jrburke/requirejs/wiki/Updating-existing-libraries#wiki-anon

if ( typeof define === "function" && define.amd ) {
	define( "jquery", [], function() {
		return jQuery;
	} );
}




var

	// Map over jQuery in case of overwrite
	_jQuery = window.jQuery,

	// Map over the $ in case of overwrite
	_$ = window.$;

jQuery.noConflict = function( deep ) {
	if ( window.$ === jQuery ) {
		window.$ = _$;
	}

	if ( deep && window.jQuery === jQuery ) {
		window.jQuery = _jQuery;
	}

	return jQuery;
};

// Expose jQuery and $ identifiers, even in AMD
// (#7102#comment:10, https://github.com/jquery/jquery/pull/557)
// and CommonJS for browser emulators (#13566)
if ( !noGlobal ) {
	window.jQuery = window.$ = jQuery;
}




return jQuery;
} );

/*!
 * Select2 4.0.4
 * https://select2.github.io
 *
 * Released under the MIT license
 * https://github.com/select2/select2/blob/master/LICENSE.md
 */
(function (factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['jquery'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // Node/CommonJS
    module.exports = function (root, jQuery) {
      if (jQuery === undefined) {
        // require('jQuery') returns a factory that requires window to
        // build a jQuery instance, we normalize how we use modules
        // that require this pattern but the window provided is a noop
        // if it's defined (how jquery works)
        if (typeof window !== 'undefined') {
          jQuery = require('jquery');
        }
        else {
          jQuery = require('jquery')(root);
        }
      }
      factory(jQuery);
      return jQuery;
    };
  } else {
    // Browser globals
    factory(jQuery);
  }
} (function (jQuery) {
  // This is needed so we can catch the AMD loader configuration and use it
  // The inner file should be wrapped (by `banner.start.js`) in a function that
  // returns the AMD loader references.
  var S2 =(function () {
  // Restore the Select2 AMD loader so it can be used
  // Needed mostly in the language files, where the loader is not inserted
  if (jQuery && jQuery.fn && jQuery.fn.select2 && jQuery.fn.select2.amd) {
    var S2 = jQuery.fn.select2.amd;
  }
var S2;(function () { if (!S2 || !S2.requirejs) {
if (!S2) { S2 = {}; } else { require = S2; }
/**
 * @license almond 0.3.3 Copyright jQuery Foundation and other contributors.
 * Released under MIT license, http://github.com/requirejs/almond/LICENSE
 */
//Going sloppy to avoid 'use strict' string cost, but strict practices should
//be followed.
/*global setTimeout: false */

var requirejs, require, define;
(function (undef) {
    var main, req, makeMap, handlers,
        defined = {},
        waiting = {},
        config = {},
        defining = {},
        hasOwn = Object.prototype.hasOwnProperty,
        aps = [].slice,
        jsSuffixRegExp = /\.js$/;

    function hasProp(obj, prop) {
        return hasOwn.call(obj, prop);
    }

    /**
     * Given a relative module name, like ./something, normalize it to
     * a real name that can be mapped to a path.
     * @param {String} name the relative name
     * @param {String} baseName a real name that the name arg is relative
     * to.
     * @returns {String} normalized name
     */
    function normalize(name, baseName) {
        var nameParts, nameSegment, mapValue, foundMap, lastIndex,
            foundI, foundStarMap, starI, i, j, part, normalizedBaseParts,
            baseParts = baseName && baseName.split("/"),
            map = config.map,
            starMap = (map && map['*']) || {};

        //Adjust any relative paths.
        if (name) {
            name = name.split('/');
            lastIndex = name.length - 1;

            // If wanting node ID compatibility, strip .js from end
            // of IDs. Have to do this here, and not in nameToUrl
            // because node allows either .js or non .js to map
            // to same file.
            if (config.nodeIdCompat && jsSuffixRegExp.test(name[lastIndex])) {
                name[lastIndex] = name[lastIndex].replace(jsSuffixRegExp, '');
            }

            // Starts with a '.' so need the baseName
            if (name[0].charAt(0) === '.' && baseParts) {
                //Convert baseName to array, and lop off the last part,
                //so that . matches that 'directory' and not name of the baseName's
                //module. For instance, baseName of 'one/two/three', maps to
                //'one/two/three.js', but we want the directory, 'one/two' for
                //this normalization.
                normalizedBaseParts = baseParts.slice(0, baseParts.length - 1);
                name = normalizedBaseParts.concat(name);
            }

            //start trimDots
            for (i = 0; i < name.length; i++) {
                part = name[i];
                if (part === '.') {
                    name.splice(i, 1);
                    i -= 1;
                } else if (part === '..') {
                    // If at the start, or previous value is still ..,
                    // keep them so that when converted to a path it may
                    // still work when converted to a path, even though
                    // as an ID it is less than ideal. In larger point
                    // releases, may be better to just kick out an error.
                    if (i === 0 || (i === 1 && name[2] === '..') || name[i - 1] === '..') {
                        continue;
                    } else if (i > 0) {
                        name.splice(i - 1, 2);
                        i -= 2;
                    }
                }
            }
            //end trimDots

            name = name.join('/');
        }

        //Apply map config if available.
        if ((baseParts || starMap) && map) {
            nameParts = name.split('/');

            for (i = nameParts.length; i > 0; i -= 1) {
                nameSegment = nameParts.slice(0, i).join("/");

                if (baseParts) {
                    //Find the longest baseName segment match in the config.
                    //So, do joins on the biggest to smallest lengths of baseParts.
                    for (j = baseParts.length; j > 0; j -= 1) {
                        mapValue = map[baseParts.slice(0, j).join('/')];

                        //baseName segment has  config, find if it has one for
                        //this name.
                        if (mapValue) {
                            mapValue = mapValue[nameSegment];
                            if (mapValue) {
                                //Match, update name to the new value.
                                foundMap = mapValue;
                                foundI = i;
                                break;
                            }
                        }
                    }
                }

                if (foundMap) {
                    break;
                }

                //Check for a star map match, but just hold on to it,
                //if there is a shorter segment match later in a matching
                //config, then favor over this star map.
                if (!foundStarMap && starMap && starMap[nameSegment]) {
                    foundStarMap = starMap[nameSegment];
                    starI = i;
                }
            }

            if (!foundMap && foundStarMap) {
                foundMap = foundStarMap;
                foundI = starI;
            }

            if (foundMap) {
                nameParts.splice(0, foundI, foundMap);
                name = nameParts.join('/');
            }
        }

        return name;
    }

    function makeRequire(relName, forceSync) {
        return function () {
            //A version of a require function that passes a moduleName
            //value for items that may need to
            //look up paths relative to the moduleName
            var args = aps.call(arguments, 0);

            //If first arg is not require('string'), and there is only
            //one arg, it is the array form without a callback. Insert
            //a null so that the following concat is correct.
            if (typeof args[0] !== 'string' && args.length === 1) {
                args.push(null);
            }
            return req.apply(undef, args.concat([relName, forceSync]));
        };
    }

    function makeNormalize(relName) {
        return function (name) {
            return normalize(name, relName);
        };
    }

    function makeLoad(depName) {
        return function (value) {
            defined[depName] = value;
        };
    }

    function callDep(name) {
        if (hasProp(waiting, name)) {
            var args = waiting[name];
            delete waiting[name];
            defining[name] = true;
            main.apply(undef, args);
        }

        if (!hasProp(defined, name) && !hasProp(defining, name)) {
            throw new Error('No ' + name);
        }
        return defined[name];
    }

    //Turns a plugin!resource to [plugin, resource]
    //with the plugin being undefined if the name
    //did not have a plugin prefix.
    function splitPrefix(name) {
        var prefix,
            index = name ? name.indexOf('!') : -1;
        if (index > -1) {
            prefix = name.substring(0, index);
            name = name.substring(index + 1, name.length);
        }
        return [prefix, name];
    }

    //Creates a parts array for a relName where first part is plugin ID,
    //second part is resource ID. Assumes relName has already been normalized.
    function makeRelParts(relName) {
        return relName ? splitPrefix(relName) : [];
    }

    /**
     * Makes a name map, normalizing the name, and using a plugin
     * for normalization if necessary. Grabs a ref to plugin
     * too, as an optimization.
     */
    makeMap = function (name, relParts) {
        var plugin,
            parts = splitPrefix(name),
            prefix = parts[0],
            relResourceName = relParts[1];

        name = parts[1];

        if (prefix) {
            prefix = normalize(prefix, relResourceName);
            plugin = callDep(prefix);
        }

        //Normalize according
        if (prefix) {
            if (plugin && plugin.normalize) {
                name = plugin.normalize(name, makeNormalize(relResourceName));
            } else {
                name = normalize(name, relResourceName);
            }
        } else {
            name = normalize(name, relResourceName);
            parts = splitPrefix(name);
            prefix = parts[0];
            name = parts[1];
            if (prefix) {
                plugin = callDep(prefix);
            }
        }

        //Using ridiculous property names for space reasons
        return {
            f: prefix ? prefix + '!' + name : name, //fullName
            n: name,
            pr: prefix,
            p: plugin
        };
    };

    function makeConfig(name) {
        return function () {
            return (config && config.config && config.config[name]) || {};
        };
    }

    handlers = {
        require: function (name) {
            return makeRequire(name);
        },
        exports: function (name) {
            var e = defined[name];
            if (typeof e !== 'undefined') {
                return e;
            } else {
                return (defined[name] = {});
            }
        },
        module: function (name) {
            return {
                id: name,
                uri: '',
                exports: defined[name],
                config: makeConfig(name)
            };
        }
    };

    main = function (name, deps, callback, relName) {
        var cjsModule, depName, ret, map, i, relParts,
            args = [],
            callbackType = typeof callback,
            usingExports;

        //Use name if no relName
        relName = relName || name;
        relParts = makeRelParts(relName);

        //Call the callback to define the module, if necessary.
        if (callbackType === 'undefined' || callbackType === 'function') {
            //Pull out the defined dependencies and pass the ordered
            //values to the callback.
            //Default to [require, exports, module] if no deps
            deps = !deps.length && callback.length ? ['require', 'exports', 'module'] : deps;
            for (i = 0; i < deps.length; i += 1) {
                map = makeMap(deps[i], relParts);
                depName = map.f;

                //Fast path CommonJS standard dependencies.
                if (depName === "require") {
                    args[i] = handlers.require(name);
                } else if (depName === "exports") {
                    //CommonJS module spec 1.1
                    args[i] = handlers.exports(name);
                    usingExports = true;
                } else if (depName === "module") {
                    //CommonJS module spec 1.1
                    cjsModule = args[i] = handlers.module(name);
                } else if (hasProp(defined, depName) ||
                           hasProp(waiting, depName) ||
                           hasProp(defining, depName)) {
                    args[i] = callDep(depName);
                } else if (map.p) {
                    map.p.load(map.n, makeRequire(relName, true), makeLoad(depName), {});
                    args[i] = defined[depName];
                } else {
                    throw new Error(name + ' missing ' + depName);
                }
            }

            ret = callback ? callback.apply(defined[name], args) : undefined;

            if (name) {
                //If setting exports via "module" is in play,
                //favor that over return value and exports. After that,
                //favor a non-undefined return value over exports use.
                if (cjsModule && cjsModule.exports !== undef &&
                        cjsModule.exports !== defined[name]) {
                    defined[name] = cjsModule.exports;
                } else if (ret !== undef || !usingExports) {
                    //Use the return value from the function.
                    defined[name] = ret;
                }
            }
        } else if (name) {
            //May just be an object definition for the module. Only
            //worry about defining if have a module name.
            defined[name] = callback;
        }
    };

    requirejs = require = req = function (deps, callback, relName, forceSync, alt) {
        if (typeof deps === "string") {
            if (handlers[deps]) {
                //callback in this case is really relName
                return handlers[deps](callback);
            }
            //Just return the module wanted. In this scenario, the
            //deps arg is the module name, and second arg (if passed)
            //is just the relName.
            //Normalize module name, if it contains . or ..
            return callDep(makeMap(deps, makeRelParts(callback)).f);
        } else if (!deps.splice) {
            //deps is a config object, not an array.
            config = deps;
            if (config.deps) {
                req(config.deps, config.callback);
            }
            if (!callback) {
                return;
            }

            if (callback.splice) {
                //callback is an array, which means it is a dependency list.
                //Adjust args if there are dependencies
                deps = callback;
                callback = relName;
                relName = null;
            } else {
                deps = undef;
            }
        }

        //Support require(['a'])
        callback = callback || function () {};

        //If relName is a function, it is an errback handler,
        //so remove it.
        if (typeof relName === 'function') {
            relName = forceSync;
            forceSync = alt;
        }

        //Simulate async callback;
        if (forceSync) {
            main(undef, deps, callback, relName);
        } else {
            //Using a non-zero value because of concern for what old browsers
            //do, and latest browsers "upgrade" to 4 if lower value is used:
            //http://www.whatwg.org/specs/web-apps/current-work/multipage/timers.html#dom-windowtimers-settimeout:
            //If want a value immediately, use require('id') instead -- something
            //that works in almond on the global level, but not guaranteed and
            //unlikely to work in other AMD implementations.
            setTimeout(function () {
                main(undef, deps, callback, relName);
            }, 4);
        }

        return req;
    };

    /**
     * Just drops the config on the floor, but returns req in case
     * the config return value is used.
     */
    req.config = function (cfg) {
        return req(cfg);
    };

    /**
     * Expose module registry for debugging and tooling
     */
    requirejs._defined = defined;

    define = function (name, deps, callback) {
        if (typeof name !== 'string') {
            throw new Error('See almond README: incorrect module build, no module name');
        }

        //This module may not have dependencies
        if (!deps.splice) {
            //deps is not an array, so probably means
            //an object literal or factory function for
            //the value. Adjust args.
            callback = deps;
            deps = [];
        }

        if (!hasProp(defined, name) && !hasProp(waiting, name)) {
            waiting[name] = [name, deps, callback];
        }
    };

    define.amd = {
        jQuery: true
    };
}());

S2.requirejs = requirejs;S2.require = require;S2.define = define;
}
}());
S2.define("almond", function(){});

/* global jQuery:false, $:false */
S2.define('jquery',[],function () {
  var _$ = jQuery || $;

  if (_$ == null && console && console.error) {
    console.error(
      'Select2: An instance of jQuery or a jQuery-compatible library was not ' +
      'found. Make sure that you are including jQuery before Select2 on your ' +
      'web page.'
    );
  }

  return _$;
});

S2.define('select2/utils',[
  'jquery'
], function ($) {
  var Utils = {};

  Utils.Extend = function (ChildClass, SuperClass) {
    var __hasProp = {}.hasOwnProperty;

    function BaseConstructor () {
      this.constructor = ChildClass;
    }

    for (var key in SuperClass) {
      if (__hasProp.call(SuperClass, key)) {
        ChildClass[key] = SuperClass[key];
      }
    }

    BaseConstructor.prototype = SuperClass.prototype;
    ChildClass.prototype = new BaseConstructor();
    ChildClass.__super__ = SuperClass.prototype;

    return ChildClass;
  };

  function getMethods (theClass) {
    var proto = theClass.prototype;

    var methods = [];

    for (var methodName in proto) {
      var m = proto[methodName];

      if (typeof m !== 'function') {
        continue;
      }

      if (methodName === 'constructor') {
        continue;
      }

      methods.push(methodName);
    }

    return methods;
  }

  Utils.Decorate = function (SuperClass, DecoratorClass) {
    var decoratedMethods = getMethods(DecoratorClass);
    var superMethods = getMethods(SuperClass);

    function DecoratedClass () {
      var unshift = Array.prototype.unshift;

      var argCount = DecoratorClass.prototype.constructor.length;

      var calledConstructor = SuperClass.prototype.constructor;

      if (argCount > 0) {
        unshift.call(arguments, SuperClass.prototype.constructor);

        calledConstructor = DecoratorClass.prototype.constructor;
      }

      calledConstructor.apply(this, arguments);
    }

    DecoratorClass.displayName = SuperClass.displayName;

    function ctr () {
      this.constructor = DecoratedClass;
    }

    DecoratedClass.prototype = new ctr();

    for (var m = 0; m < superMethods.length; m++) {
        var superMethod = superMethods[m];

        DecoratedClass.prototype[superMethod] =
          SuperClass.prototype[superMethod];
    }

    var calledMethod = function (methodName) {
      // Stub out the original method if it's not decorating an actual method
      var originalMethod = function () {};

      if (methodName in DecoratedClass.prototype) {
        originalMethod = DecoratedClass.prototype[methodName];
      }

      var decoratedMethod = DecoratorClass.prototype[methodName];

      return function () {
        var unshift = Array.prototype.unshift;

        unshift.call(arguments, originalMethod);

        return decoratedMethod.apply(this, arguments);
      };
    };

    for (var d = 0; d < decoratedMethods.length; d++) {
      var decoratedMethod = decoratedMethods[d];

      DecoratedClass.prototype[decoratedMethod] = calledMethod(decoratedMethod);
    }

    return DecoratedClass;
  };

  var Observable = function () {
    this.listeners = {};
  };

  Observable.prototype.on = function (event, callback) {
    this.listeners = this.listeners || {};

    if (event in this.listeners) {
      this.listeners[event].push(callback);
    } else {
      this.listeners[event] = [callback];
    }
  };

  Observable.prototype.trigger = function (event) {
    var slice = Array.prototype.slice;
    var params = slice.call(arguments, 1);

    this.listeners = this.listeners || {};

    // Params should always come in as an array
    if (params == null) {
      params = [];
    }

    // If there are no arguments to the event, use a temporary object
    if (params.length === 0) {
      params.push({});
    }

    // Set the `_type` of the first object to the event
    params[0]._type = event;

    if (event in this.listeners) {
      this.invoke(this.listeners[event], slice.call(arguments, 1));
    }

    if ('*' in this.listeners) {
      this.invoke(this.listeners['*'], arguments);
    }
  };

  Observable.prototype.invoke = function (listeners, params) {
    for (var i = 0, len = listeners.length; i < len; i++) {
      listeners[i].apply(this, params);
    }
  };

  Utils.Observable = Observable;

  Utils.generateChars = function (length) {
    var chars = '';

    for (var i = 0; i < length; i++) {
      var randomChar = Math.floor(Math.random() * 36);
      chars += randomChar.toString(36);
    }

    return chars;
  };

  Utils.bind = function (func, context) {
    return function () {
      func.apply(context, arguments);
    };
  };

  Utils._convertData = function (data) {
    for (var originalKey in data) {
      var keys = originalKey.split('-');

      var dataLevel = data;

      if (keys.length === 1) {
        continue;
      }

      for (var k = 0; k < keys.length; k++) {
        var key = keys[k];

        // Lowercase the first letter
        // By default, dash-separated becomes camelCase
        key = key.substring(0, 1).toLowerCase() + key.substring(1);

        if (!(key in dataLevel)) {
          dataLevel[key] = {};
        }

        if (k == keys.length - 1) {
          dataLevel[key] = data[originalKey];
        }

        dataLevel = dataLevel[key];
      }

      delete data[originalKey];
    }

    return data;
  };

  Utils.hasScroll = function (index, el) {
    // Adapted from the function created by @ShadowScripter
    // and adapted by @BillBarry on the Stack Exchange Code Review website.
    // The original code can be found at
    // http://codereview.stackexchange.com/q/13338
    // and was designed to be used with the Sizzle selector engine.

    var $el = $(el);
    var overflowX = el.style.overflowX;
    var overflowY = el.style.overflowY;

    //Check both x and y declarations
    if (overflowX === overflowY &&
        (overflowY === 'hidden' || overflowY === 'visible')) {
      return false;
    }

    if (overflowX === 'scroll' || overflowY === 'scroll') {
      return true;
    }

    return ($el.innerHeight() < el.scrollHeight ||
      $el.innerWidth() < el.scrollWidth);
  };

  Utils.escapeMarkup = function (markup) {
    var replaceMap = {
      '\\': '&#92;',
      '&': '&amp;',
      '<': '&lt;',
      '>': '&gt;',
      '"': '&quot;',
      '\'': '&#39;',
      '/': '&#47;'
    };

    // Do not try to escape the markup if it's not a string
    if (typeof markup !== 'string') {
      return markup;
    }

    return String(markup).replace(/[&<>"'\/\\]/g, function (match) {
      return replaceMap[match];
    });
  };

  // Append an array of jQuery nodes to a given element.
  Utils.appendMany = function ($element, $nodes) {
    // jQuery 1.7.x does not support $.fn.append() with an array
    // Fall back to a jQuery object collection using $.fn.add()
    if ($.fn.jquery.substr(0, 3) === '1.7') {
      var $jqNodes = $();

      $.map($nodes, function (node) {
        $jqNodes = $jqNodes.add(node);
      });

      $nodes = $jqNodes;
    }

    $element.append($nodes);
  };

  return Utils;
});

S2.define('select2/results',[
  'jquery',
  './utils'
], function ($, Utils) {
  function Results ($element, options, dataAdapter) {
    this.$element = $element;
    this.data = dataAdapter;
    this.options = options;

    Results.__super__.constructor.call(this);
  }

  Utils.Extend(Results, Utils.Observable);

  Results.prototype.render = function () {
    var $results = $(
      '<ul class="select2-results__options" role="tree"></ul>'
    );

    if (this.options.get('multiple')) {
      $results.attr('aria-multiselectable', 'true');
    }

    this.$results = $results;

    return $results;
  };

  Results.prototype.clear = function () {
    this.$results.empty();
  };

  Results.prototype.displayMessage = function (params) {
    var escapeMarkup = this.options.get('escapeMarkup');

    this.clear();
    this.hideLoading();

    var $message = $(
      '<li role="treeitem" aria-live="assertive"' +
      ' class="select2-results__option"></li>'
    );

    var message = this.options.get('translations').get(params.message);

    $message.append(
      escapeMarkup(
        message(params.args)
      )
    );

    $message[0].className += ' select2-results__message';

    this.$results.append($message);
  };

  Results.prototype.hideMessages = function () {
    this.$results.find('.select2-results__message').remove();
  };

  Results.prototype.append = function (data) {
    this.hideLoading();

    var $options = [];

    if (data.results == null || data.results.length === 0) {
      if (this.$results.children().length === 0) {
        this.trigger('results:message', {
          message: 'noResults'
        });
      }

      return;
    }

    data.results = this.sort(data.results);

    for (var d = 0; d < data.results.length; d++) {
      var item = data.results[d];

      var $option = this.option(item);

      $options.push($option);
    }

    this.$results.append($options);
  };

  Results.prototype.position = function ($results, $dropdown) {
    var $resultsContainer = $dropdown.find('.select2-results');
    $resultsContainer.append($results);
  };

  Results.prototype.sort = function (data) {
    var sorter = this.options.get('sorter');

    return sorter(data);
  };

  Results.prototype.highlightFirstItem = function () {
    var $options = this.$results
      .find('.select2-results__option[aria-selected]');

    var $selected = $options.filter('[aria-selected=true]');

    // Check if there are any selected options
    if ($selected.length > 0) {
      // If there are selected options, highlight the first
      $selected.first().trigger('mouseenter');
    } else {
      // If there are no selected options, highlight the first option
      // in the dropdown
      $options.first().trigger('mouseenter');
    }

    this.ensureHighlightVisible();
  };

  Results.prototype.setClasses = function () {
    var self = this;

    this.data.current(function (selected) {
      var selectedIds = $.map(selected, function (s) {
        return s.id.toString();
      });

      var $options = self.$results
        .find('.select2-results__option[aria-selected]');

      $options.each(function () {
        var $option = $(this);

        var item = $.data(this, 'data');

        // id needs to be converted to a string when comparing
        var id = '' + item.id;

        if ((item.element != null && item.element.selected) ||
            (item.element == null && $.inArray(id, selectedIds) > -1)) {
          $option.attr('aria-selected', 'true');
        } else {
          $option.attr('aria-selected', 'false');
        }
      });

    });
  };

  Results.prototype.showLoading = function (params) {
    this.hideLoading();

    var loadingMore = this.options.get('translations').get('searching');

    var loading = {
      disabled: true,
      loading: true,
      text: loadingMore(params)
    };
    var $loading = this.option(loading);
    $loading.className += ' loading-results';

    this.$results.prepend($loading);
  };

  Results.prototype.hideLoading = function () {
    this.$results.find('.loading-results').remove();
  };

  Results.prototype.option = function (data) {
    var option = document.createElement('li');
    option.className = 'select2-results__option';

    var attrs = {
      'role': 'treeitem',
      'aria-selected': 'false'
    };

    if (data.disabled) {
      delete attrs['aria-selected'];
      attrs['aria-disabled'] = 'true';
    }

    if (data.id == null) {
      delete attrs['aria-selected'];
    }

    if (data._resultId != null) {
      option.id = data._resultId;
    }

    if (data.title) {
      option.title = data.title;
    }

    if (data.children) {
      attrs.role = 'group';
      attrs['aria-label'] = data.text;
      delete attrs['aria-selected'];
    }

    for (var attr in attrs) {
      var val = attrs[attr];

      option.setAttribute(attr, val);
    }

    if (data.children) {
      var $option = $(option);

      var label = document.createElement('strong');
      label.className = 'select2-results__group';

      var $label = $(label);
      this.template(data, label);

      var $children = [];

      for (var c = 0; c < data.children.length; c++) {
        var child = data.children[c];

        var $child = this.option(child);

        $children.push($child);
      }

      var $childrenContainer = $('<ul></ul>', {
        'class': 'select2-results__options select2-results__options--nested'
      });

      $childrenContainer.append($children);

      $option.append(label);
      $option.append($childrenContainer);
    } else {
      this.template(data, option);
    }

    $.data(option, 'data', data);

    return option;
  };

  Results.prototype.bind = function (container, $container) {
    var self = this;

    var id = container.id + '-results';

    this.$results.attr('id', id);

    container.on('results:all', function (params) {
      self.clear();
      self.append(params.data);

      if (container.isOpen()) {
        self.setClasses();
        self.highlightFirstItem();
      }
    });

    container.on('results:append', function (params) {
      self.append(params.data);

      if (container.isOpen()) {
        self.setClasses();
      }
    });

    container.on('query', function (params) {
      self.hideMessages();
      self.showLoading(params);
    });

    container.on('select', function () {
      if (!container.isOpen()) {
        return;
      }

      self.setClasses();
      self.highlightFirstItem();
    });

    container.on('unselect', function () {
      if (!container.isOpen()) {
        return;
      }

      self.setClasses();
      self.highlightFirstItem();
    });

    container.on('open', function () {
      // When the dropdown is open, aria-expended="true"
      self.$results.attr('aria-expanded', 'true');
      self.$results.attr('aria-hidden', 'false');

      self.setClasses();
      self.ensureHighlightVisible();
    });

    container.on('close', function () {
      // When the dropdown is closed, aria-expended="false"
      self.$results.attr('aria-expanded', 'false');
      self.$results.attr('aria-hidden', 'true');
      self.$results.removeAttr('aria-activedescendant');
    });

    container.on('results:toggle', function () {
      var $highlighted = self.getHighlightedResults();

      if ($highlighted.length === 0) {
        return;
      }

      $highlighted.trigger('mouseup');
    });

    container.on('results:select', function () {
      var $highlighted = self.getHighlightedResults();

      if ($highlighted.length === 0) {
        return;
      }

      var data = $highlighted.data('data');

      if ($highlighted.attr('aria-selected') == 'true') {
        self.trigger('close', {});
      } else {
        self.trigger('select', {
          data: data
        });
      }
    });

    container.on('results:previous', function () {
      var $highlighted = self.getHighlightedResults();

      var $options = self.$results.find('[aria-selected]');

      var currentIndex = $options.index($highlighted);

      // If we are already at te top, don't move further
      if (currentIndex === 0) {
        return;
      }

      var nextIndex = currentIndex - 1;

      // If none are highlighted, highlight the first
      if ($highlighted.length === 0) {
        nextIndex = 0;
      }

      var $next = $options.eq(nextIndex);

      $next.trigger('mouseenter');

      var currentOffset = self.$results.offset().top;
      var nextTop = $next.offset().top;
      var nextOffset = self.$results.scrollTop() + (nextTop - currentOffset);

      if (nextIndex === 0) {
        self.$results.scrollTop(0);
      } else if (nextTop - currentOffset < 0) {
        self.$results.scrollTop(nextOffset);
      }
    });

    container.on('results:next', function () {
      var $highlighted = self.getHighlightedResults();

      var $options = self.$results.find('[aria-selected]');

      var currentIndex = $options.index($highlighted);

      var nextIndex = currentIndex + 1;

      // If we are at the last option, stay there
      if (nextIndex >= $options.length) {
        return;
      }

      var $next = $options.eq(nextIndex);

      $next.trigger('mouseenter');

      var currentOffset = self.$results.offset().top +
        self.$results.outerHeight(false);
      var nextBottom = $next.offset().top + $next.outerHeight(false);
      var nextOffset = self.$results.scrollTop() + nextBottom - currentOffset;

      if (nextIndex === 0) {
        self.$results.scrollTop(0);
      } else if (nextBottom > currentOffset) {
        self.$results.scrollTop(nextOffset);
      }
    });

    container.on('results:focus', function (params) {
      params.element.addClass('select2-results__option--highlighted');
    });

    container.on('results:message', function (params) {
      self.displayMessage(params);
    });

    if ($.fn.mousewheel) {
      this.$results.on('mousewheel', function (e) {
        var top = self.$results.scrollTop();

        var bottom = self.$results.get(0).scrollHeight - top + e.deltaY;

        var isAtTop = e.deltaY > 0 && top - e.deltaY <= 0;
        var isAtBottom = e.deltaY < 0 && bottom <= self.$results.height();

        if (isAtTop) {
          self.$results.scrollTop(0);

          e.preventDefault();
          e.stopPropagation();
        } else if (isAtBottom) {
          self.$results.scrollTop(
            self.$results.get(0).scrollHeight - self.$results.height()
          );

          e.preventDefault();
          e.stopPropagation();
        }
      });
    }

    this.$results.on('mouseup', '.select2-results__option[aria-selected]',
      function (evt) {
      var $this = $(this);

      var data = $this.data('data');

      if ($this.attr('aria-selected') === 'true') {
        if (self.options.get('multiple')) {
          self.trigger('unselect', {
            originalEvent: evt,
            data: data
          });
        } else {
          self.trigger('close', {});
        }

        return;
      }

      self.trigger('select', {
        originalEvent: evt,
        data: data
      });
    });

    this.$results.on('mouseenter', '.select2-results__option[aria-selected]',
      function (evt) {
      var data = $(this).data('data');

      self.getHighlightedResults()
          .removeClass('select2-results__option--highlighted');

      self.trigger('results:focus', {
        data: data,
        element: $(this)
      });
    });
  };

  Results.prototype.getHighlightedResults = function () {
    var $highlighted = this.$results
    .find('.select2-results__option--highlighted');

    return $highlighted;
  };

  Results.prototype.destroy = function () {
    this.$results.remove();
  };

  Results.prototype.ensureHighlightVisible = function () {
    var $highlighted = this.getHighlightedResults();

    if ($highlighted.length === 0) {
      return;
    }

    var $options = this.$results.find('[aria-selected]');

    var currentIndex = $options.index($highlighted);

    var currentOffset = this.$results.offset().top;
    var nextTop = $highlighted.offset().top;
    var nextOffset = this.$results.scrollTop() + (nextTop - currentOffset);

    var offsetDelta = nextTop - currentOffset;
    nextOffset -= $highlighted.outerHeight(false) * 2;

    if (currentIndex <= 2) {
      this.$results.scrollTop(0);
    } else if (offsetDelta > this.$results.outerHeight() || offsetDelta < 0) {
      this.$results.scrollTop(nextOffset);
    }
  };

  Results.prototype.template = function (result, container) {
    var template = this.options.get('templateResult');
    var escapeMarkup = this.options.get('escapeMarkup');

    var content = template(result, container);

    if (content == null) {
      container.style.display = 'none';
    } else if (typeof content === 'string') {
      container.innerHTML = escapeMarkup(content);
    } else {
      $(container).append(content);
    }
  };

  return Results;
});

S2.define('select2/keys',[

], function () {
  var KEYS = {
    BACKSPACE: 8,
    TAB: 9,
    ENTER: 13,
    SHIFT: 16,
    CTRL: 17,
    ALT: 18,
    ESC: 27,
    SPACE: 32,
    PAGE_UP: 33,
    PAGE_DOWN: 34,
    END: 35,
    HOME: 36,
    LEFT: 37,
    UP: 38,
    RIGHT: 39,
    DOWN: 40,
    DELETE: 46
  };

  return KEYS;
});

S2.define('select2/selection/base',[
  'jquery',
  '../utils',
  '../keys'
], function ($, Utils, KEYS) {
  function BaseSelection ($element, options) {
    this.$element = $element;
    this.options = options;

    BaseSelection.__super__.constructor.call(this);
  }

  Utils.Extend(BaseSelection, Utils.Observable);

  BaseSelection.prototype.render = function () {
    var $selection = $(
      '<span class="select2-selection" role="combobox" ' +
      ' aria-haspopup="true" aria-expanded="false">' +
      '</span>'
    );

    this._tabindex = 0;

    if (this.$element.data('old-tabindex') != null) {
      this._tabindex = this.$element.data('old-tabindex');
    } else if (this.$element.attr('tabindex') != null) {
      this._tabindex = this.$element.attr('tabindex');
    }

    $selection.attr('title', this.$element.attr('title'));
    $selection.attr('tabindex', this._tabindex);

    this.$selection = $selection;

    return $selection;
  };

  BaseSelection.prototype.bind = function (container, $container) {
    var self = this;

    var id = container.id + '-container';
    var resultsId = container.id + '-results';

    this.container = container;

    this.$selection.on('focus', function (evt) {
      self.trigger('focus', evt);
    });

    this.$selection.on('blur', function (evt) {
      self._handleBlur(evt);
    });

    this.$selection.on('keydown', function (evt) {
      self.trigger('keypress', evt);

      if (evt.which === KEYS.SPACE) {
        evt.preventDefault();
      }
    });

    container.on('results:focus', function (params) {
      self.$selection.attr('aria-activedescendant', params.data._resultId);
    });

    container.on('selection:update', function (params) {
      self.update(params.data);
    });

    container.on('open', function () {
      // When the dropdown is open, aria-expanded="true"
      self.$selection.attr('aria-expanded', 'true');
      self.$selection.attr('aria-owns', resultsId);

      self._attachCloseHandler(container);
    });

    container.on('close', function () {
      // When the dropdown is closed, aria-expanded="false"
      self.$selection.attr('aria-expanded', 'false');
      self.$selection.removeAttr('aria-activedescendant');
      self.$selection.removeAttr('aria-owns');

      self.$selection.focus();

      self._detachCloseHandler(container);
    });

    container.on('enable', function () {
      self.$selection.attr('tabindex', self._tabindex);
    });

    container.on('disable', function () {
      self.$selection.attr('tabindex', '-1');
    });
  };

  BaseSelection.prototype._handleBlur = function (evt) {
    var self = this;

    // This needs to be delayed as the active element is the body when the tab
    // key is pressed, possibly along with others.
    window.setTimeout(function () {
      // Don't trigger `blur` if the focus is still in the selection
      if (
        (document.activeElement == self.$selection[0]) ||
        ($.contains(self.$selection[0], document.activeElement))
      ) {
        return;
      }

      self.trigger('blur', evt);
    }, 1);
  };

  BaseSelection.prototype._attachCloseHandler = function (container) {
    var self = this;

    $(document.body).on('mousedown.select2.' + container.id, function (e) {
      var $target = $(e.target);

      var $select = $target.closest('.select2');

      var $all = $('.select2.select2-container--open');

      $all.each(function () {
        var $this = $(this);

        if (this == $select[0]) {
          return;
        }

        var $element = $this.data('element');

        $element.select2('close');
      });
    });
  };

  BaseSelection.prototype._detachCloseHandler = function (container) {
    $(document.body).off('mousedown.select2.' + container.id);
  };

  BaseSelection.prototype.position = function ($selection, $container) {
    var $selectionContainer = $container.find('.selection');
    $selectionContainer.append($selection);
  };

  BaseSelection.prototype.destroy = function () {
    this._detachCloseHandler(this.container);
  };

  BaseSelection.prototype.update = function (data) {
    throw new Error('The `update` method must be defined in child classes.');
  };

  return BaseSelection;
});

S2.define('select2/selection/single',[
  'jquery',
  './base',
  '../utils',
  '../keys'
], function ($, BaseSelection, Utils, KEYS) {
  function SingleSelection () {
    SingleSelection.__super__.constructor.apply(this, arguments);
  }

  Utils.Extend(SingleSelection, BaseSelection);

  SingleSelection.prototype.render = function () {
    var $selection = SingleSelection.__super__.render.call(this);

    $selection.addClass('select2-selection--single');

    $selection.html(
      '<span class="select2-selection__rendered"></span>' +
      '<span class="select2-selection__arrow" role="presentation">' +
        '<b role="presentation"></b>' +
      '</span>'
    );

    return $selection;
  };

  SingleSelection.prototype.bind = function (container, $container) {
    var self = this;

    SingleSelection.__super__.bind.apply(this, arguments);

    var id = container.id + '-container';

    this.$selection.find('.select2-selection__rendered').attr('id', id);
    this.$selection.attr('aria-labelledby', id);

    this.$selection.on('mousedown', function (evt) {
      // Only respond to left clicks
      if (evt.which !== 1) {
        return;
      }

      self.trigger('toggle', {
        originalEvent: evt
      });
    });

    this.$selection.on('focus', function (evt) {
      // User focuses on the container
    });

    this.$selection.on('blur', function (evt) {
      // User exits the container
    });

    container.on('focus', function (evt) {
      if (!container.isOpen()) {
        self.$selection.focus();
      }
    });

    container.on('selection:update', function (params) {
      self.update(params.data);
    });
  };

  SingleSelection.prototype.clear = function () {
    this.$selection.find('.select2-selection__rendered').empty();
  };

  SingleSelection.prototype.display = function (data, container) {
    var template = this.options.get('templateSelection');
    var escapeMarkup = this.options.get('escapeMarkup');

    return escapeMarkup(template(data, container));
  };

  SingleSelection.prototype.selectionContainer = function () {
    return $('<span></span>');
  };

  SingleSelection.prototype.update = function (data) {
    if (data.length === 0) {
      this.clear();
      return;
    }

    var selection = data[0];

    var $rendered = this.$selection.find('.select2-selection__rendered');
    var formatted = this.display(selection, $rendered);

    $rendered.empty().append(formatted);
    $rendered.prop('title', selection.title || selection.text);
  };

  return SingleSelection;
});

S2.define('select2/selection/multiple',[
  'jquery',
  './base',
  '../utils'
], function ($, BaseSelection, Utils) {
  function MultipleSelection ($element, options) {
    MultipleSelection.__super__.constructor.apply(this, arguments);
  }

  Utils.Extend(MultipleSelection, BaseSelection);

  MultipleSelection.prototype.render = function () {
    var $selection = MultipleSelection.__super__.render.call(this);

    $selection.addClass('select2-selection--multiple');

    $selection.html(
      '<ul class="select2-selection__rendered"></ul>'
    );

    return $selection;
  };

  MultipleSelection.prototype.bind = function (container, $container) {
    var self = this;

    MultipleSelection.__super__.bind.apply(this, arguments);

    this.$selection.on('click', function (evt) {
      self.trigger('toggle', {
        originalEvent: evt
      });
    });

    this.$selection.on(
      'click',
      '.select2-selection__choice__remove',
      function (evt) {
        // Ignore the event if it is disabled
        if (self.options.get('disabled')) {
          return;
        }

        var $remove = $(this);
        var $selection = $remove.parent();

        var data = $selection.data('data');

        self.trigger('unselect', {
          originalEvent: evt,
          data: data
        });
      }
    );
  };

  MultipleSelection.prototype.clear = function () {
    this.$selection.find('.select2-selection__rendered').empty();
  };

  MultipleSelection.prototype.display = function (data, container) {
    var template = this.options.get('templateSelection');
    var escapeMarkup = this.options.get('escapeMarkup');

    return escapeMarkup(template(data, container));
  };

  MultipleSelection.prototype.selectionContainer = function () {
    var $container = $(
      '<li class="select2-selection__choice">' +
        '<span class="select2-selection__choice__remove" role="presentation">' +
          '&times;' +
        '</span>' +
      '</li>'
    );

    return $container;
  };

  MultipleSelection.prototype.update = function (data) {
    this.clear();

    if (data.length === 0) {
      return;
    }

    var $selections = [];

    for (var d = 0; d < data.length; d++) {
      var selection = data[d];

      var $selection = this.selectionContainer();
      var formatted = this.display(selection, $selection);

      $selection.append(formatted);
      $selection.prop('title', selection.title || selection.text);

      $selection.data('data', selection);

      $selections.push($selection);
    }

    var $rendered = this.$selection.find('.select2-selection__rendered');

    Utils.appendMany($rendered, $selections);
  };

  return MultipleSelection;
});

S2.define('select2/selection/placeholder',[
  '../utils'
], function (Utils) {
  function Placeholder (decorated, $element, options) {
    this.placeholder = this.normalizePlaceholder(options.get('placeholder'));

    decorated.call(this, $element, options);
  }

  Placeholder.prototype.normalizePlaceholder = function (_, placeholder) {
    if (typeof placeholder === 'string') {
      placeholder = {
        id: '',
        text: placeholder
      };
    }

    return placeholder;
  };

  Placeholder.prototype.createPlaceholder = function (decorated, placeholder) {
    var $placeholder = this.selectionContainer();

    $placeholder.html(this.display(placeholder));
    $placeholder.addClass('select2-selection__placeholder')
                .removeClass('select2-selection__choice');

    return $placeholder;
  };

  Placeholder.prototype.update = function (decorated, data) {
    var singlePlaceholder = (
      data.length == 1 && data[0].id != this.placeholder.id
    );
    var multipleSelections = data.length > 1;

    if (multipleSelections || singlePlaceholder) {
      return decorated.call(this, data);
    }

    this.clear();

    var $placeholder = this.createPlaceholder(this.placeholder);

    this.$selection.find('.select2-selection__rendered').append($placeholder);
  };

  return Placeholder;
});

S2.define('select2/selection/allowClear',[
  'jquery',
  '../keys'
], function ($, KEYS) {
  function AllowClear () { }

  AllowClear.prototype.bind = function (decorated, container, $container) {
    var self = this;

    decorated.call(this, container, $container);

    if (this.placeholder == null) {
      if (this.options.get('debug') && window.console && console.error) {
        console.error(
          'Select2: The `allowClear` option should be used in combination ' +
          'with the `placeholder` option.'
        );
      }
    }

    this.$selection.on('mousedown', '.select2-selection__clear',
      function (evt) {
        self._handleClear(evt);
    });

    container.on('keypress', function (evt) {
      self._handleKeyboardClear(evt, container);
    });
  };

  AllowClear.prototype._handleClear = function (_, evt) {
    // Ignore the event if it is disabled
    if (this.options.get('disabled')) {
      return;
    }

    var $clear = this.$selection.find('.select2-selection__clear');

    // Ignore the event if nothing has been selected
    if ($clear.length === 0) {
      return;
    }

    evt.stopPropagation();

    var data = $clear.data('data');

    for (var d = 0; d < data.length; d++) {
      var unselectData = {
        data: data[d]
      };

      // Trigger the `unselect` event, so people can prevent it from being
      // cleared.
      this.trigger('unselect', unselectData);

      // If the event was prevented, don't clear it out.
      if (unselectData.prevented) {
        return;
      }
    }

    this.$element.val(this.placeholder.id).trigger('change');

    this.trigger('toggle', {});
  };

  AllowClear.prototype._handleKeyboardClear = function (_, evt, container) {
    if (container.isOpen()) {
      return;
    }

    if (evt.which == KEYS.DELETE || evt.which == KEYS.BACKSPACE) {
      this._handleClear(evt);
    }
  };

  AllowClear.prototype.update = function (decorated, data) {
    decorated.call(this, data);

    if (this.$selection.find('.select2-selection__placeholder').length > 0 ||
        data.length === 0) {
      return;
    }

    var $remove = $(
      '<span class="select2-selection__clear">' +
        '&times;' +
      '</span>'
    );
    $remove.data('data', data);

    this.$selection.find('.select2-selection__rendered').prepend($remove);
  };

  return AllowClear;
});

S2.define('select2/selection/search',[
  'jquery',
  '../utils',
  '../keys'
], function ($, Utils, KEYS) {
  function Search (decorated, $element, options) {
    decorated.call(this, $element, options);
  }

  Search.prototype.render = function (decorated) {
    var $search = $(
      '<li class="select2-search select2-search--inline">' +
        '<input class="select2-search__field" type="search" tabindex="-1"' +
        ' autocomplete="off" autocorrect="off" autocapitalize="off"' +
        ' spellcheck="false" role="textbox" aria-autocomplete="list" />' +
      '</li>'
    );

    this.$searchContainer = $search;
    this.$search = $search.find('input');

    var $rendered = decorated.call(this);

    this._transferTabIndex();

    return $rendered;
  };

  Search.prototype.bind = function (decorated, container, $container) {
    var self = this;

    decorated.call(this, container, $container);

    container.on('open', function () {
      self.$search.trigger('focus');
    });

    container.on('close', function () {
      self.$search.val('');
      self.$search.removeAttr('aria-activedescendant');
      self.$search.trigger('focus');
    });

    container.on('enable', function () {
      self.$search.prop('disabled', false);

      self._transferTabIndex();
    });

    container.on('disable', function () {
      self.$search.prop('disabled', true);
    });

    container.on('focus', function (evt) {
      self.$search.trigger('focus');
    });

    container.on('results:focus', function (params) {
      self.$search.attr('aria-activedescendant', params.id);
    });

    this.$selection.on('focusin', '.select2-search--inline', function (evt) {
      self.trigger('focus', evt);
    });

    this.$selection.on('focusout', '.select2-search--inline', function (evt) {
      self._handleBlur(evt);
    });

    this.$selection.on('keydown', '.select2-search--inline', function (evt) {
      evt.stopPropagation();

      self.trigger('keypress', evt);

      self._keyUpPrevented = evt.isDefaultPrevented();

      var key = evt.which;

      if (key === KEYS.BACKSPACE && self.$search.val() === '') {
        var $previousChoice = self.$searchContainer
          .prev('.select2-selection__choice');

        if ($previousChoice.length > 0) {
          var item = $previousChoice.data('data');

          self.searchRemoveChoice(item);

          evt.preventDefault();
        }
      }
    });

    // Try to detect the IE version should the `documentMode` property that
    // is stored on the document. This is only implemented in IE and is
    // slightly cleaner than doing a user agent check.
    // This property is not available in Edge, but Edge also doesn't have
    // this bug.
    var msie = document.documentMode;
    var disableInputEvents = msie && msie <= 11;

    // Workaround for browsers which do not support the `input` event
    // This will prevent double-triggering of events for browsers which support
    // both the `keyup` and `input` events.
    this.$selection.on(
      'input.searchcheck',
      '.select2-search--inline',
      function (evt) {
        // IE will trigger the `input` event when a placeholder is used on a
        // search box. To get around this issue, we are forced to ignore all
        // `input` events in IE and keep using `keyup`.
        if (disableInputEvents) {
          self.$selection.off('input.search input.searchcheck');
          return;
        }

        // Unbind the duplicated `keyup` event
        self.$selection.off('keyup.search');
      }
    );

    this.$selection.on(
      'keyup.search input.search',
      '.select2-search--inline',
      function (evt) {
        // IE will trigger the `input` event when a placeholder is used on a
        // search box. To get around this issue, we are forced to ignore all
        // `input` events in IE and keep using `keyup`.
        if (disableInputEvents && evt.type === 'input') {
          self.$selection.off('input.search input.searchcheck');
          return;
        }

        var key = evt.which;

        // We can freely ignore events from modifier keys
        if (key == KEYS.SHIFT || key == KEYS.CTRL || key == KEYS.ALT) {
          return;
        }

        // Tabbing will be handled during the `keydown` phase
        if (key == KEYS.TAB) {
          return;
        }

        self.handleSearch(evt);
      }
    );
  };

  /**
   * This method will transfer the tabindex attribute from the rendered
   * selection to the search box. This allows for the search box to be used as
   * the primary focus instead of the selection container.
   *
   * @private
   */
  Search.prototype._transferTabIndex = function (decorated) {
    this.$search.attr('tabindex', this.$selection.attr('tabindex'));
    this.$selection.attr('tabindex', '-1');
  };

  Search.prototype.createPlaceholder = function (decorated, placeholder) {
    this.$search.attr('placeholder', placeholder.text);
  };

  Search.prototype.update = function (decorated, data) {
    var searchHadFocus = this.$search[0] == document.activeElement;

    this.$search.attr('placeholder', '');

    decorated.call(this, data);

    this.$selection.find('.select2-selection__rendered')
                   .append(this.$searchContainer);

    this.resizeSearch();
    if (searchHadFocus) {
      this.$search.focus();
    }
  };

  Search.prototype.handleSearch = function () {
    this.resizeSearch();

    if (!this._keyUpPrevented) {
      var input = this.$search.val();

      this.trigger('query', {
        term: input
      });
    }

    this._keyUpPrevented = false;
  };

  Search.prototype.searchRemoveChoice = function (decorated, item) {
    this.trigger('unselect', {
      data: item
    });

    this.$search.val(item.text);
    this.handleSearch();
  };

  Search.prototype.resizeSearch = function () {
    this.$search.css('width', '25px');

    var width = '';

    if (this.$search.attr('placeholder') !== '') {
      width = this.$selection.find('.select2-selection__rendered').innerWidth();
    } else {
      var minimumWidth = this.$search.val().length + 1;

      width = (minimumWidth * 0.75) + 'em';
    }

    this.$search.css('width', width);
  };

  return Search;
});

S2.define('select2/selection/eventRelay',[
  'jquery'
], function ($) {
  function EventRelay () { }

  EventRelay.prototype.bind = function (decorated, container, $container) {
    var self = this;
    var relayEvents = [
      'open', 'opening',
      'close', 'closing',
      'select', 'selecting',
      'unselect', 'unselecting'
    ];

    var preventableEvents = ['opening', 'closing', 'selecting', 'unselecting'];

    decorated.call(this, container, $container);

    container.on('*', function (name, params) {
      // Ignore events that should not be relayed
      if ($.inArray(name, relayEvents) === -1) {
        return;
      }

      // The parameters should always be an object
      params = params || {};

      // Generate the jQuery event for the Select2 event
      var evt = $.Event('select2:' + name, {
        params: params
      });

      self.$element.trigger(evt);

      // Only handle preventable events if it was one
      if ($.inArray(name, preventableEvents) === -1) {
        return;
      }

      params.prevented = evt.isDefaultPrevented();
    });
  };

  return EventRelay;
});

S2.define('select2/translation',[
  'jquery',
  'require'
], function ($, require) {
  function Translation (dict) {
    this.dict = dict || {};
  }

  Translation.prototype.all = function () {
    return this.dict;
  };

  Translation.prototype.get = function (key) {
    return this.dict[key];
  };

  Translation.prototype.extend = function (translation) {
    this.dict = $.extend({}, translation.all(), this.dict);
  };

  // Static functions

  Translation._cache = {};

  Translation.loadPath = function (path) {
    if (!(path in Translation._cache)) {
      var translations = require(path);

      Translation._cache[path] = translations;
    }

    return new Translation(Translation._cache[path]);
  };

  return Translation;
});

S2.define('select2/diacritics',[

], function () {
  var diacritics = {
    '\u24B6': 'A',
    '\uFF21': 'A',
    '\u00C0': 'A',
    '\u00C1': 'A',
    '\u00C2': 'A',
    '\u1EA6': 'A',
    '\u1EA4': 'A',
    '\u1EAA': 'A',
    '\u1EA8': 'A',
    '\u00C3': 'A',
    '\u0100': 'A',
    '\u0102': 'A',
    '\u1EB0': 'A',
    '\u1EAE': 'A',
    '\u1EB4': 'A',
    '\u1EB2': 'A',
    '\u0226': 'A',
    '\u01E0': 'A',
    '\u00C4': 'A',
    '\u01DE': 'A',
    '\u1EA2': 'A',
    '\u00C5': 'A',
    '\u01FA': 'A',
    '\u01CD': 'A',
    '\u0200': 'A',
    '\u0202': 'A',
    '\u1EA0': 'A',
    '\u1EAC': 'A',
    '\u1EB6': 'A',
    '\u1E00': 'A',
    '\u0104': 'A',
    '\u023A': 'A',
    '\u2C6F': 'A',
    '\uA732': 'AA',
    '\u00C6': 'AE',
    '\u01FC': 'AE',
    '\u01E2': 'AE',
    '\uA734': 'AO',
    '\uA736': 'AU',
    '\uA738': 'AV',
    '\uA73A': 'AV',
    '\uA73C': 'AY',
    '\u24B7': 'B',
    '\uFF22': 'B',
    '\u1E02': 'B',
    '\u1E04': 'B',
    '\u1E06': 'B',
    '\u0243': 'B',
    '\u0182': 'B',
    '\u0181': 'B',
    '\u24B8': 'C',
    '\uFF23': 'C',
    '\u0106': 'C',
    '\u0108': 'C',
    '\u010A': 'C',
    '\u010C': 'C',
    '\u00C7': 'C',
    '\u1E08': 'C',
    '\u0187': 'C',
    '\u023B': 'C',
    '\uA73E': 'C',
    '\u24B9': 'D',
    '\uFF24': 'D',
    '\u1E0A': 'D',
    '\u010E': 'D',
    '\u1E0C': 'D',
    '\u1E10': 'D',
    '\u1E12': 'D',
    '\u1E0E': 'D',
    '\u0110': 'D',
    '\u018B': 'D',
    '\u018A': 'D',
    '\u0189': 'D',
    '\uA779': 'D',
    '\u01F1': 'DZ',
    '\u01C4': 'DZ',
    '\u01F2': 'Dz',
    '\u01C5': 'Dz',
    '\u24BA': 'E',
    '\uFF25': 'E',
    '\u00C8': 'E',
    '\u00C9': 'E',
    '\u00CA': 'E',
    '\u1EC0': 'E',
    '\u1EBE': 'E',
    '\u1EC4': 'E',
    '\u1EC2': 'E',
    '\u1EBC': 'E',
    '\u0112': 'E',
    '\u1E14': 'E',
    '\u1E16': 'E',
    '\u0114': 'E',
    '\u0116': 'E',
    '\u00CB': 'E',
    '\u1EBA': 'E',
    '\u011A': 'E',
    '\u0204': 'E',
    '\u0206': 'E',
    '\u1EB8': 'E',
    '\u1EC6': 'E',
    '\u0228': 'E',
    '\u1E1C': 'E',
    '\u0118': 'E',
    '\u1E18': 'E',
    '\u1E1A': 'E',
    '\u0190': 'E',
    '\u018E': 'E',
    '\u24BB': 'F',
    '\uFF26': 'F',
    '\u1E1E': 'F',
    '\u0191': 'F',
    '\uA77B': 'F',
    '\u24BC': 'G',
    '\uFF27': 'G',
    '\u01F4': 'G',
    '\u011C': 'G',
    '\u1E20': 'G',
    '\u011E': 'G',
    '\u0120': 'G',
    '\u01E6': 'G',
    '\u0122': 'G',
    '\u01E4': 'G',
    '\u0193': 'G',
    '\uA7A0': 'G',
    '\uA77D': 'G',
    '\uA77E': 'G',
    '\u24BD': 'H',
    '\uFF28': 'H',
    '\u0124': 'H',
    '\u1E22': 'H',
    '\u1E26': 'H',
    '\u021E': 'H',
    '\u1E24': 'H',
    '\u1E28': 'H',
    '\u1E2A': 'H',
    '\u0126': 'H',
    '\u2C67': 'H',
    '\u2C75': 'H',
    '\uA78D': 'H',
    '\u24BE': 'I',
    '\uFF29': 'I',
    '\u00CC': 'I',
    '\u00CD': 'I',
    '\u00CE': 'I',
    '\u0128': 'I',
    '\u012A': 'I',
    '\u012C': 'I',
    '\u0130': 'I',
    '\u00CF': 'I',
    '\u1E2E': 'I',
    '\u1EC8': 'I',
    '\u01CF': 'I',
    '\u0208': 'I',
    '\u020A': 'I',
    '\u1ECA': 'I',
    '\u012E': 'I',
    '\u1E2C': 'I',
    '\u0197': 'I',
    '\u24BF': 'J',
    '\uFF2A': 'J',
    '\u0134': 'J',
    '\u0248': 'J',
    '\u24C0': 'K',
    '\uFF2B': 'K',
    '\u1E30': 'K',
    '\u01E8': 'K',
    '\u1E32': 'K',
    '\u0136': 'K',
    '\u1E34': 'K',
    '\u0198': 'K',
    '\u2C69': 'K',
    '\uA740': 'K',
    '\uA742': 'K',
    '\uA744': 'K',
    '\uA7A2': 'K',
    '\u24C1': 'L',
    '\uFF2C': 'L',
    '\u013F': 'L',
    '\u0139': 'L',
    '\u013D': 'L',
    '\u1E36': 'L',
    '\u1E38': 'L',
    '\u013B': 'L',
    '\u1E3C': 'L',
    '\u1E3A': 'L',
    '\u0141': 'L',
    '\u023D': 'L',
    '\u2C62': 'L',
    '\u2C60': 'L',
    '\uA748': 'L',
    '\uA746': 'L',
    '\uA780': 'L',
    '\u01C7': 'LJ',
    '\u01C8': 'Lj',
    '\u24C2': 'M',
    '\uFF2D': 'M',
    '\u1E3E': 'M',
    '\u1E40': 'M',
    '\u1E42': 'M',
    '\u2C6E': 'M',
    '\u019C': 'M',
    '\u24C3': 'N',
    '\uFF2E': 'N',
    '\u01F8': 'N',
    '\u0143': 'N',
    '\u00D1': 'N',
    '\u1E44': 'N',
    '\u0147': 'N',
    '\u1E46': 'N',
    '\u0145': 'N',
    '\u1E4A': 'N',
    '\u1E48': 'N',
    '\u0220': 'N',
    '\u019D': 'N',
    '\uA790': 'N',
    '\uA7A4': 'N',
    '\u01CA': 'NJ',
    '\u01CB': 'Nj',
    '\u24C4': 'O',
    '\uFF2F': 'O',
    '\u00D2': 'O',
    '\u00D3': 'O',
    '\u00D4': 'O',
    '\u1ED2': 'O',
    '\u1ED0': 'O',
    '\u1ED6': 'O',
    '\u1ED4': 'O',
    '\u00D5': 'O',
    '\u1E4C': 'O',
    '\u022C': 'O',
    '\u1E4E': 'O',
    '\u014C': 'O',
    '\u1E50': 'O',
    '\u1E52': 'O',
    '\u014E': 'O',
    '\u022E': 'O',
    '\u0230': 'O',
    '\u00D6': 'O',
    '\u022A': 'O',
    '\u1ECE': 'O',
    '\u0150': 'O',
    '\u01D1': 'O',
    '\u020C': 'O',
    '\u020E': 'O',
    '\u01A0': 'O',
    '\u1EDC': 'O',
    '\u1EDA': 'O',
    '\u1EE0': 'O',
    '\u1EDE': 'O',
    '\u1EE2': 'O',
    '\u1ECC': 'O',
    '\u1ED8': 'O',
    '\u01EA': 'O',
    '\u01EC': 'O',
    '\u00D8': 'O',
    '\u01FE': 'O',
    '\u0186': 'O',
    '\u019F': 'O',
    '\uA74A': 'O',
    '\uA74C': 'O',
    '\u01A2': 'OI',
    '\uA74E': 'OO',
    '\u0222': 'OU',
    '\u24C5': 'P',
    '\uFF30': 'P',
    '\u1E54': 'P',
    '\u1E56': 'P',
    '\u01A4': 'P',
    '\u2C63': 'P',
    '\uA750': 'P',
    '\uA752': 'P',
    '\uA754': 'P',
    '\u24C6': 'Q',
    '\uFF31': 'Q',
    '\uA756': 'Q',
    '\uA758': 'Q',
    '\u024A': 'Q',
    '\u24C7': 'R',
    '\uFF32': 'R',
    '\u0154': 'R',
    '\u1E58': 'R',
    '\u0158': 'R',
    '\u0210': 'R',
    '\u0212': 'R',
    '\u1E5A': 'R',
    '\u1E5C': 'R',
    '\u0156': 'R',
    '\u1E5E': 'R',
    '\u024C': 'R',
    '\u2C64': 'R',
    '\uA75A': 'R',
    '\uA7A6': 'R',
    '\uA782': 'R',
    '\u24C8': 'S',
    '\uFF33': 'S',
    '\u1E9E': 'S',
    '\u015A': 'S',
    '\u1E64': 'S',
    '\u015C': 'S',
    '\u1E60': 'S',
    '\u0160': 'S',
    '\u1E66': 'S',
    '\u1E62': 'S',
    '\u1E68': 'S',
    '\u0218': 'S',
    '\u015E': 'S',
    '\u2C7E': 'S',
    '\uA7A8': 'S',
    '\uA784': 'S',
    '\u24C9': 'T',
    '\uFF34': 'T',
    '\u1E6A': 'T',
    '\u0164': 'T',
    '\u1E6C': 'T',
    '\u021A': 'T',
    '\u0162': 'T',
    '\u1E70': 'T',
    '\u1E6E': 'T',
    '\u0166': 'T',
    '\u01AC': 'T',
    '\u01AE': 'T',
    '\u023E': 'T',
    '\uA786': 'T',
    '\uA728': 'TZ',
    '\u24CA': 'U',
    '\uFF35': 'U',
    '\u00D9': 'U',
    '\u00DA': 'U',
    '\u00DB': 'U',
    '\u0168': 'U',
    '\u1E78': 'U',
    '\u016A': 'U',
    '\u1E7A': 'U',
    '\u016C': 'U',
    '\u00DC': 'U',
    '\u01DB': 'U',
    '\u01D7': 'U',
    '\u01D5': 'U',
    '\u01D9': 'U',
    '\u1EE6': 'U',
    '\u016E': 'U',
    '\u0170': 'U',
    '\u01D3': 'U',
    '\u0214': 'U',
    '\u0216': 'U',
    '\u01AF': 'U',
    '\u1EEA': 'U',
    '\u1EE8': 'U',
    '\u1EEE': 'U',
    '\u1EEC': 'U',
    '\u1EF0': 'U',
    '\u1EE4': 'U',
    '\u1E72': 'U',
    '\u0172': 'U',
    '\u1E76': 'U',
    '\u1E74': 'U',
    '\u0244': 'U',
    '\u24CB': 'V',
    '\uFF36': 'V',
    '\u1E7C': 'V',
    '\u1E7E': 'V',
    '\u01B2': 'V',
    '\uA75E': 'V',
    '\u0245': 'V',
    '\uA760': 'VY',
    '\u24CC': 'W',
    '\uFF37': 'W',
    '\u1E80': 'W',
    '\u1E82': 'W',
    '\u0174': 'W',
    '\u1E86': 'W',
    '\u1E84': 'W',
    '\u1E88': 'W',
    '\u2C72': 'W',
    '\u24CD': 'X',
    '\uFF38': 'X',
    '\u1E8A': 'X',
    '\u1E8C': 'X',
    '\u24CE': 'Y',
    '\uFF39': 'Y',
    '\u1EF2': 'Y',
    '\u00DD': 'Y',
    '\u0176': 'Y',
    '\u1EF8': 'Y',
    '\u0232': 'Y',
    '\u1E8E': 'Y',
    '\u0178': 'Y',
    '\u1EF6': 'Y',
    '\u1EF4': 'Y',
    '\u01B3': 'Y',
    '\u024E': 'Y',
    '\u1EFE': 'Y',
    '\u24CF': 'Z',
    '\uFF3A': 'Z',
    '\u0179': 'Z',
    '\u1E90': 'Z',
    '\u017B': 'Z',
    '\u017D': 'Z',
    '\u1E92': 'Z',
    '\u1E94': 'Z',
    '\u01B5': 'Z',
    '\u0224': 'Z',
    '\u2C7F': 'Z',
    '\u2C6B': 'Z',
    '\uA762': 'Z',
    '\u24D0': 'a',
    '\uFF41': 'a',
    '\u1E9A': 'a',
    '\u00E0': 'a',
    '\u00E1': 'a',
    '\u00E2': 'a',
    '\u1EA7': 'a',
    '\u1EA5': 'a',
    '\u1EAB': 'a',
    '\u1EA9': 'a',
    '\u00E3': 'a',
    '\u0101': 'a',
    '\u0103': 'a',
    '\u1EB1': 'a',
    '\u1EAF': 'a',
    '\u1EB5': 'a',
    '\u1EB3': 'a',
    '\u0227': 'a',
    '\u01E1': 'a',
    '\u00E4': 'a',
    '\u01DF': 'a',
    '\u1EA3': 'a',
    '\u00E5': 'a',
    '\u01FB': 'a',
    '\u01CE': 'a',
    '\u0201': 'a',
    '\u0203': 'a',
    '\u1EA1': 'a',
    '\u1EAD': 'a',
    '\u1EB7': 'a',
    '\u1E01': 'a',
    '\u0105': 'a',
    '\u2C65': 'a',
    '\u0250': 'a',
    '\uA733': 'aa',
    '\u00E6': 'ae',
    '\u01FD': 'ae',
    '\u01E3': 'ae',
    '\uA735': 'ao',
    '\uA737': 'au',
    '\uA739': 'av',
    '\uA73B': 'av',
    '\uA73D': 'ay',
    '\u24D1': 'b',
    '\uFF42': 'b',
    '\u1E03': 'b',
    '\u1E05': 'b',
    '\u1E07': 'b',
    '\u0180': 'b',
    '\u0183': 'b',
    '\u0253': 'b',
    '\u24D2': 'c',
    '\uFF43': 'c',
    '\u0107': 'c',
    '\u0109': 'c',
    '\u010B': 'c',
    '\u010D': 'c',
    '\u00E7': 'c',
    '\u1E09': 'c',
    '\u0188': 'c',
    '\u023C': 'c',
    '\uA73F': 'c',
    '\u2184': 'c',
    '\u24D3': 'd',
    '\uFF44': 'd',
    '\u1E0B': 'd',
    '\u010F': 'd',
    '\u1E0D': 'd',
    '\u1E11': 'd',
    '\u1E13': 'd',
    '\u1E0F': 'd',
    '\u0111': 'd',
    '\u018C': 'd',
    '\u0256': 'd',
    '\u0257': 'd',
    '\uA77A': 'd',
    '\u01F3': 'dz',
    '\u01C6': 'dz',
    '\u24D4': 'e',
    '\uFF45': 'e',
    '\u00E8': 'e',
    '\u00E9': 'e',
    '\u00EA': 'e',
    '\u1EC1': 'e',
    '\u1EBF': 'e',
    '\u1EC5': 'e',
    '\u1EC3': 'e',
    '\u1EBD': 'e',
    '\u0113': 'e',
    '\u1E15': 'e',
    '\u1E17': 'e',
    '\u0115': 'e',
    '\u0117': 'e',
    '\u00EB': 'e',
    '\u1EBB': 'e',
    '\u011B': 'e',
    '\u0205': 'e',
    '\u0207': 'e',
    '\u1EB9': 'e',
    '\u1EC7': 'e',
    '\u0229': 'e',
    '\u1E1D': 'e',
    '\u0119': 'e',
    '\u1E19': 'e',
    '\u1E1B': 'e',
    '\u0247': 'e',
    '\u025B': 'e',
    '\u01DD': 'e',
    '\u24D5': 'f',
    '\uFF46': 'f',
    '\u1E1F': 'f',
    '\u0192': 'f',
    '\uA77C': 'f',
    '\u24D6': 'g',
    '\uFF47': 'g',
    '\u01F5': 'g',
    '\u011D': 'g',
    '\u1E21': 'g',
    '\u011F': 'g',
    '\u0121': 'g',
    '\u01E7': 'g',
    '\u0123': 'g',
    '\u01E5': 'g',
    '\u0260': 'g',
    '\uA7A1': 'g',
    '\u1D79': 'g',
    '\uA77F': 'g',
    '\u24D7': 'h',
    '\uFF48': 'h',
    '\u0125': 'h',
    '\u1E23': 'h',
    '\u1E27': 'h',
    '\u021F': 'h',
    '\u1E25': 'h',
    '\u1E29': 'h',
    '\u1E2B': 'h',
    '\u1E96': 'h',
    '\u0127': 'h',
    '\u2C68': 'h',
    '\u2C76': 'h',
    '\u0265': 'h',
    '\u0195': 'hv',
    '\u24D8': 'i',
    '\uFF49': 'i',
    '\u00EC': 'i',
    '\u00ED': 'i',
    '\u00EE': 'i',
    '\u0129': 'i',
    '\u012B': 'i',
    '\u012D': 'i',
    '\u00EF': 'i',
    '\u1E2F': 'i',
    '\u1EC9': 'i',
    '\u01D0': 'i',
    '\u0209': 'i',
    '\u020B': 'i',
    '\u1ECB': 'i',
    '\u012F': 'i',
    '\u1E2D': 'i',
    '\u0268': 'i',
    '\u0131': 'i',
    '\u24D9': 'j',
    '\uFF4A': 'j',
    '\u0135': 'j',
    '\u01F0': 'j',
    '\u0249': 'j',
    '\u24DA': 'k',
    '\uFF4B': 'k',
    '\u1E31': 'k',
    '\u01E9': 'k',
    '\u1E33': 'k',
    '\u0137': 'k',
    '\u1E35': 'k',
    '\u0199': 'k',
    '\u2C6A': 'k',
    '\uA741': 'k',
    '\uA743': 'k',
    '\uA745': 'k',
    '\uA7A3': 'k',
    '\u24DB': 'l',
    '\uFF4C': 'l',
    '\u0140': 'l',
    '\u013A': 'l',
    '\u013E': 'l',
    '\u1E37': 'l',
    '\u1E39': 'l',
    '\u013C': 'l',
    '\u1E3D': 'l',
    '\u1E3B': 'l',
    '\u017F': 'l',
    '\u0142': 'l',
    '\u019A': 'l',
    '\u026B': 'l',
    '\u2C61': 'l',
    '\uA749': 'l',
    '\uA781': 'l',
    '\uA747': 'l',
    '\u01C9': 'lj',
    '\u24DC': 'm',
    '\uFF4D': 'm',
    '\u1E3F': 'm',
    '\u1E41': 'm',
    '\u1E43': 'm',
    '\u0271': 'm',
    '\u026F': 'm',
    '\u24DD': 'n',
    '\uFF4E': 'n',
    '\u01F9': 'n',
    '\u0144': 'n',
    '\u00F1': 'n',
    '\u1E45': 'n',
    '\u0148': 'n',
    '\u1E47': 'n',
    '\u0146': 'n',
    '\u1E4B': 'n',
    '\u1E49': 'n',
    '\u019E': 'n',
    '\u0272': 'n',
    '\u0149': 'n',
    '\uA791': 'n',
    '\uA7A5': 'n',
    '\u01CC': 'nj',
    '\u24DE': 'o',
    '\uFF4F': 'o',
    '\u00F2': 'o',
    '\u00F3': 'o',
    '\u00F4': 'o',
    '\u1ED3': 'o',
    '\u1ED1': 'o',
    '\u1ED7': 'o',
    '\u1ED5': 'o',
    '\u00F5': 'o',
    '\u1E4D': 'o',
    '\u022D': 'o',
    '\u1E4F': 'o',
    '\u014D': 'o',
    '\u1E51': 'o',
    '\u1E53': 'o',
    '\u014F': 'o',
    '\u022F': 'o',
    '\u0231': 'o',
    '\u00F6': 'o',
    '\u022B': 'o',
    '\u1ECF': 'o',
    '\u0151': 'o',
    '\u01D2': 'o',
    '\u020D': 'o',
    '\u020F': 'o',
    '\u01A1': 'o',
    '\u1EDD': 'o',
    '\u1EDB': 'o',
    '\u1EE1': 'o',
    '\u1EDF': 'o',
    '\u1EE3': 'o',
    '\u1ECD': 'o',
    '\u1ED9': 'o',
    '\u01EB': 'o',
    '\u01ED': 'o',
    '\u00F8': 'o',
    '\u01FF': 'o',
    '\u0254': 'o',
    '\uA74B': 'o',
    '\uA74D': 'o',
    '\u0275': 'o',
    '\u01A3': 'oi',
    '\u0223': 'ou',
    '\uA74F': 'oo',
    '\u24DF': 'p',
    '\uFF50': 'p',
    '\u1E55': 'p',
    '\u1E57': 'p',
    '\u01A5': 'p',
    '\u1D7D': 'p',
    '\uA751': 'p',
    '\uA753': 'p',
    '\uA755': 'p',
    '\u24E0': 'q',
    '\uFF51': 'q',
    '\u024B': 'q',
    '\uA757': 'q',
    '\uA759': 'q',
    '\u24E1': 'r',
    '\uFF52': 'r',
    '\u0155': 'r',
    '\u1E59': 'r',
    '\u0159': 'r',
    '\u0211': 'r',
    '\u0213': 'r',
    '\u1E5B': 'r',
    '\u1E5D': 'r',
    '\u0157': 'r',
    '\u1E5F': 'r',
    '\u024D': 'r',
    '\u027D': 'r',
    '\uA75B': 'r',
    '\uA7A7': 'r',
    '\uA783': 'r',
    '\u24E2': 's',
    '\uFF53': 's',
    '\u00DF': 's',
    '\u015B': 's',
    '\u1E65': 's',
    '\u015D': 's',
    '\u1E61': 's',
    '\u0161': 's',
    '\u1E67': 's',
    '\u1E63': 's',
    '\u1E69': 's',
    '\u0219': 's',
    '\u015F': 's',
    '\u023F': 's',
    '\uA7A9': 's',
    '\uA785': 's',
    '\u1E9B': 's',
    '\u24E3': 't',
    '\uFF54': 't',
    '\u1E6B': 't',
    '\u1E97': 't',
    '\u0165': 't',
    '\u1E6D': 't',
    '\u021B': 't',
    '\u0163': 't',
    '\u1E71': 't',
    '\u1E6F': 't',
    '\u0167': 't',
    '\u01AD': 't',
    '\u0288': 't',
    '\u2C66': 't',
    '\uA787': 't',
    '\uA729': 'tz',
    '\u24E4': 'u',
    '\uFF55': 'u',
    '\u00F9': 'u',
    '\u00FA': 'u',
    '\u00FB': 'u',
    '\u0169': 'u',
    '\u1E79': 'u',
    '\u016B': 'u',
    '\u1E7B': 'u',
    '\u016D': 'u',
    '\u00FC': 'u',
    '\u01DC': 'u',
    '\u01D8': 'u',
    '\u01D6': 'u',
    '\u01DA': 'u',
    '\u1EE7': 'u',
    '\u016F': 'u',
    '\u0171': 'u',
    '\u01D4': 'u',
    '\u0215': 'u',
    '\u0217': 'u',
    '\u01B0': 'u',
    '\u1EEB': 'u',
    '\u1EE9': 'u',
    '\u1EEF': 'u',
    '\u1EED': 'u',
    '\u1EF1': 'u',
    '\u1EE5': 'u',
    '\u1E73': 'u',
    '\u0173': 'u',
    '\u1E77': 'u',
    '\u1E75': 'u',
    '\u0289': 'u',
    '\u24E5': 'v',
    '\uFF56': 'v',
    '\u1E7D': 'v',
    '\u1E7F': 'v',
    '\u028B': 'v',
    '\uA75F': 'v',
    '\u028C': 'v',
    '\uA761': 'vy',
    '\u24E6': 'w',
    '\uFF57': 'w',
    '\u1E81': 'w',
    '\u1E83': 'w',
    '\u0175': 'w',
    '\u1E87': 'w',
    '\u1E85': 'w',
    '\u1E98': 'w',
    '\u1E89': 'w',
    '\u2C73': 'w',
    '\u24E7': 'x',
    '\uFF58': 'x',
    '\u1E8B': 'x',
    '\u1E8D': 'x',
    '\u24E8': 'y',
    '\uFF59': 'y',
    '\u1EF3': 'y',
    '\u00FD': 'y',
    '\u0177': 'y',
    '\u1EF9': 'y',
    '\u0233': 'y',
    '\u1E8F': 'y',
    '\u00FF': 'y',
    '\u1EF7': 'y',
    '\u1E99': 'y',
    '\u1EF5': 'y',
    '\u01B4': 'y',
    '\u024F': 'y',
    '\u1EFF': 'y',
    '\u24E9': 'z',
    '\uFF5A': 'z',
    '\u017A': 'z',
    '\u1E91': 'z',
    '\u017C': 'z',
    '\u017E': 'z',
    '\u1E93': 'z',
    '\u1E95': 'z',
    '\u01B6': 'z',
    '\u0225': 'z',
    '\u0240': 'z',
    '\u2C6C': 'z',
    '\uA763': 'z',
    '\u0386': '\u0391',
    '\u0388': '\u0395',
    '\u0389': '\u0397',
    '\u038A': '\u0399',
    '\u03AA': '\u0399',
    '\u038C': '\u039F',
    '\u038E': '\u03A5',
    '\u03AB': '\u03A5',
    '\u038F': '\u03A9',
    '\u03AC': '\u03B1',
    '\u03AD': '\u03B5',
    '\u03AE': '\u03B7',
    '\u03AF': '\u03B9',
    '\u03CA': '\u03B9',
    '\u0390': '\u03B9',
    '\u03CC': '\u03BF',
    '\u03CD': '\u03C5',
    '\u03CB': '\u03C5',
    '\u03B0': '\u03C5',
    '\u03C9': '\u03C9',
    '\u03C2': '\u03C3'
  };

  return diacritics;
});

S2.define('select2/data/base',[
  '../utils'
], function (Utils) {
  function BaseAdapter ($element, options) {
    BaseAdapter.__super__.constructor.call(this);
  }

  Utils.Extend(BaseAdapter, Utils.Observable);

  BaseAdapter.prototype.current = function (callback) {
    throw new Error('The `current` method must be defined in child classes.');
  };

  BaseAdapter.prototype.query = function (params, callback) {
    throw new Error('The `query` method must be defined in child classes.');
  };

  BaseAdapter.prototype.bind = function (container, $container) {
    // Can be implemented in subclasses
  };

  BaseAdapter.prototype.destroy = function () {
    // Can be implemented in subclasses
  };

  BaseAdapter.prototype.generateResultId = function (container, data) {
    var id = container.id + '-result-';

    id += Utils.generateChars(4);

    if (data.id != null) {
      id += '-' + data.id.toString();
    } else {
      id += '-' + Utils.generateChars(4);
    }
    return id;
  };

  return BaseAdapter;
});

S2.define('select2/data/select',[
  './base',
  '../utils',
  'jquery'
], function (BaseAdapter, Utils, $) {
  function SelectAdapter ($element, options) {
    this.$element = $element;
    this.options = options;

    SelectAdapter.__super__.constructor.call(this);
  }

  Utils.Extend(SelectAdapter, BaseAdapter);

  SelectAdapter.prototype.current = function (callback) {
    var data = [];
    var self = this;

    this.$element.find(':selected').each(function () {
      var $option = $(this);

      var option = self.item($option);

      data.push(option);
    });

    callback(data);
  };

  SelectAdapter.prototype.select = function (data) {
    var self = this;

    data.selected = true;

    // If data.element is a DOM node, use it instead
    if ($(data.element).is('option')) {
      data.element.selected = true;

      this.$element.trigger('change');

      return;
    }

    if (this.$element.prop('multiple')) {
      this.current(function (currentData) {
        var val = [];

        data = [data];
        data.push.apply(data, currentData);

        for (var d = 0; d < data.length; d++) {
          var id = data[d].id;

          if ($.inArray(id, val) === -1) {
            val.push(id);
          }
        }

        self.$element.val(val);
        self.$element.trigger('change');
      });
    } else {
      var val = data.id;

      this.$element.val(val);
      this.$element.trigger('change');
    }
  };

  SelectAdapter.prototype.unselect = function (data) {
    var self = this;

    if (!this.$element.prop('multiple')) {
      return;
    }

    data.selected = false;

    if ($(data.element).is('option')) {
      data.element.selected = false;

      this.$element.trigger('change');

      return;
    }

    this.current(function (currentData) {
      var val = [];

      for (var d = 0; d < currentData.length; d++) {
        var id = currentData[d].id;

        if (id !== data.id && $.inArray(id, val) === -1) {
          val.push(id);
        }
      }

      self.$element.val(val);

      self.$element.trigger('change');
    });
  };

  SelectAdapter.prototype.bind = function (container, $container) {
    var self = this;

    this.container = container;

    container.on('select', function (params) {
      self.select(params.data);
    });

    container.on('unselect', function (params) {
      self.unselect(params.data);
    });
  };

  SelectAdapter.prototype.destroy = function () {
    // Remove anything added to child elements
    this.$element.find('*').each(function () {
      // Remove any custom data set by Select2
      $.removeData(this, 'data');
    });
  };

  SelectAdapter.prototype.query = function (params, callback) {
    var data = [];
    var self = this;

    var $options = this.$element.children();

    $options.each(function () {
      var $option = $(this);

      if (!$option.is('option') && !$option.is('optgroup')) {
        return;
      }

      var option = self.item($option);

      var matches = self.matches(params, option);

      if (matches !== null) {
        data.push(matches);
      }
    });

    callback({
      results: data
    });
  };

  SelectAdapter.prototype.addOptions = function ($options) {
    Utils.appendMany(this.$element, $options);
  };

  SelectAdapter.prototype.option = function (data) {
    var option;

    if (data.children) {
      option = document.createElement('optgroup');
      option.label = data.text;
    } else {
      option = document.createElement('option');

      if (option.textContent !== undefined) {
        option.textContent = data.text;
      } else {
        option.innerText = data.text;
      }
    }

    if (data.id !== undefined) {
      option.value = data.id;
    }

    if (data.disabled) {
      option.disabled = true;
    }

    if (data.selected) {
      option.selected = true;
    }

    if (data.title) {
      option.title = data.title;
    }

    var $option = $(option);

    var normalizedData = this._normalizeItem(data);
    normalizedData.element = option;

    // Override the option's data with the combined data
    $.data(option, 'data', normalizedData);

    return $option;
  };

  SelectAdapter.prototype.item = function ($option) {
    var data = {};

    data = $.data($option[0], 'data');

    if (data != null) {
      return data;
    }

    if ($option.is('option')) {
      data = {
        id: $option.val(),
        text: $option.text(),
        disabled: $option.prop('disabled'),
        selected: $option.prop('selected'),
        title: $option.prop('title')
      };
    } else if ($option.is('optgroup')) {
      data = {
        text: $option.prop('label'),
        children: [],
        title: $option.prop('title')
      };

      var $children = $option.children('option');
      var children = [];

      for (var c = 0; c < $children.length; c++) {
        var $child = $($children[c]);

        var child = this.item($child);

        children.push(child);
      }

      data.children = children;
    }

    data = this._normalizeItem(data);
    data.element = $option[0];

    $.data($option[0], 'data', data);

    return data;
  };

  SelectAdapter.prototype._normalizeItem = function (item) {
    if (!$.isPlainObject(item)) {
      item = {
        id: item,
        text: item
      };
    }

    item = $.extend({}, {
      text: ''
    }, item);

    var defaults = {
      selected: false,
      disabled: false
    };

    if (item.id != null) {
      item.id = item.id.toString();
    }

    if (item.text != null) {
      item.text = item.text.toString();
    }

    if (item._resultId == null && item.id && this.container != null) {
      item._resultId = this.generateResultId(this.container, item);
    }

    return $.extend({}, defaults, item);
  };

  SelectAdapter.prototype.matches = function (params, data) {
    var matcher = this.options.get('matcher');

    return matcher(params, data);
  };

  return SelectAdapter;
});

S2.define('select2/data/array',[
  './select',
  '../utils',
  'jquery'
], function (SelectAdapter, Utils, $) {
  function ArrayAdapter ($element, options) {
    var data = options.get('data') || [];

    ArrayAdapter.__super__.constructor.call(this, $element, options);

    this.addOptions(this.convertToOptions(data));
  }

  Utils.Extend(ArrayAdapter, SelectAdapter);

  ArrayAdapter.prototype.select = function (data) {
    var $option = this.$element.find('option').filter(function (i, elm) {
      return elm.value == data.id.toString();
    });

    if ($option.length === 0) {
      $option = this.option(data);

      this.addOptions($option);
    }

    ArrayAdapter.__super__.select.call(this, data);
  };

  ArrayAdapter.prototype.convertToOptions = function (data) {
    var self = this;

    var $existing = this.$element.find('option');
    var existingIds = $existing.map(function () {
      return self.item($(this)).id;
    }).get();

    var $options = [];

    // Filter out all items except for the one passed in the argument
    function onlyItem (item) {
      return function () {
        return $(this).val() == item.id;
      };
    }

    for (var d = 0; d < data.length; d++) {
      var item = this._normalizeItem(data[d]);

      // Skip items which were pre-loaded, only merge the data
      if ($.inArray(item.id, existingIds) >= 0) {
        var $existingOption = $existing.filter(onlyItem(item));

        var existingData = this.item($existingOption);
        var newData = $.extend(true, {}, item, existingData);

        var $newOption = this.option(newData);

        $existingOption.replaceWith($newOption);

        continue;
      }

      var $option = this.option(item);

      if (item.children) {
        var $children = this.convertToOptions(item.children);

        Utils.appendMany($option, $children);
      }

      $options.push($option);
    }

    return $options;
  };

  return ArrayAdapter;
});

S2.define('select2/data/ajax',[
  './array',
  '../utils',
  'jquery'
], function (ArrayAdapter, Utils, $) {
  function AjaxAdapter ($element, options) {
    this.ajaxOptions = this._applyDefaults(options.get('ajax'));

    if (this.ajaxOptions.processResults != null) {
      this.processResults = this.ajaxOptions.processResults;
    }

    AjaxAdapter.__super__.constructor.call(this, $element, options);
  }

  Utils.Extend(AjaxAdapter, ArrayAdapter);

  AjaxAdapter.prototype._applyDefaults = function (options) {
    var defaults = {
      data: function (params) {
        return $.extend({}, params, {
          q: params.term
        });
      },
      transport: function (params, success, failure) {
        var $request = $.ajax(params);

        $request.then(success);
        $request.fail(failure);

        return $request;
      }
    };

    return $.extend({}, defaults, options, true);
  };

  AjaxAdapter.prototype.processResults = function (results) {
    return results;
  };

  AjaxAdapter.prototype.query = function (params, callback) {
    var matches = [];
    var self = this;

    if (this._request != null) {
      // JSONP requests cannot always be aborted
      if ($.isFunction(this._request.abort)) {
        this._request.abort();
      }

      this._request = null;
    }

    var options = $.extend({
      type: 'GET'
    }, this.ajaxOptions);

    if (typeof options.url === 'function') {
      options.url = options.url.call(this.$element, params);
    }

    if (typeof options.data === 'function') {
      options.data = options.data.call(this.$element, params);
    }

    function request () {
      var $request = options.transport(options, function (data) {
        var results = self.processResults(data, params);

        if (self.options.get('debug') && window.console && console.error) {
          // Check to make sure that the response included a `results` key.
          if (!results || !results.results || !$.isArray(results.results)) {
            console.error(
              'Select2: The AJAX results did not return an array in the ' +
              '`results` key of the response.'
            );
          }
        }

        callback(results);
      }, function () {
        // Attempt to detect if a request was aborted
        // Only works if the transport exposes a status property
        if ($request.status && $request.status === '0') {
          return;
        }

        self.trigger('results:message', {
          message: 'errorLoading'
        });
      });

      self._request = $request;
    }

    if (this.ajaxOptions.delay && params.term != null) {
      if (this._queryTimeout) {
        window.clearTimeout(this._queryTimeout);
      }

      this._queryTimeout = window.setTimeout(request, this.ajaxOptions.delay);
    } else {
      request();
    }
  };

  return AjaxAdapter;
});

S2.define('select2/data/tags',[
  'jquery'
], function ($) {
  function Tags (decorated, $element, options) {
    var tags = options.get('tags');

    var createTag = options.get('createTag');

    if (createTag !== undefined) {
      this.createTag = createTag;
    }

    var insertTag = options.get('insertTag');

    if (insertTag !== undefined) {
        this.insertTag = insertTag;
    }

    decorated.call(this, $element, options);

    if ($.isArray(tags)) {
      for (var t = 0; t < tags.length; t++) {
        var tag = tags[t];
        var item = this._normalizeItem(tag);

        var $option = this.option(item);

        this.$element.append($option);
      }
    }
  }

  Tags.prototype.query = function (decorated, params, callback) {
    var self = this;

    this._removeOldTags();

    if (params.term == null || params.page != null) {
      decorated.call(this, params, callback);
      return;
    }

    function wrapper (obj, child) {
      var data = obj.results;

      for (var i = 0; i < data.length; i++) {
        var option = data[i];

        var checkChildren = (
          option.children != null &&
          !wrapper({
            results: option.children
          }, true)
        );

        var optionText = (option.text || '').toUpperCase();
        var paramsTerm = (params.term || '').toUpperCase();

        var checkText = optionText === paramsTerm;

        if (checkText || checkChildren) {
          if (child) {
            return false;
          }

          obj.data = data;
          callback(obj);

          return;
        }
      }

      if (child) {
        return true;
      }

      var tag = self.createTag(params);

      if (tag != null) {
        var $option = self.option(tag);
        $option.attr('data-select2-tag', true);

        self.addOptions([$option]);

        self.insertTag(data, tag);
      }

      obj.results = data;

      callback(obj);
    }

    decorated.call(this, params, wrapper);
  };

  Tags.prototype.createTag = function (decorated, params) {
    var term = $.trim(params.term);

    if (term === '') {
      return null;
    }

    return {
      id: term,
      text: term
    };
  };

  Tags.prototype.insertTag = function (_, data, tag) {
    data.unshift(tag);
  };

  Tags.prototype._removeOldTags = function (_) {
    var tag = this._lastTag;

    var $options = this.$element.find('option[data-select2-tag]');

    $options.each(function () {
      if (this.selected) {
        return;
      }

      $(this).remove();
    });
  };

  return Tags;
});

S2.define('select2/data/tokenizer',[
  'jquery'
], function ($) {
  function Tokenizer (decorated, $element, options) {
    var tokenizer = options.get('tokenizer');

    if (tokenizer !== undefined) {
      this.tokenizer = tokenizer;
    }

    decorated.call(this, $element, options);
  }

  Tokenizer.prototype.bind = function (decorated, container, $container) {
    decorated.call(this, container, $container);

    this.$search =  container.dropdown.$search || container.selection.$search ||
      $container.find('.select2-search__field');
  };

  Tokenizer.prototype.query = function (decorated, params, callback) {
    var self = this;

    function createAndSelect (data) {
      // Normalize the data object so we can use it for checks
      var item = self._normalizeItem(data);

      // Check if the data object already exists as a tag
      // Select it if it doesn't
      var $existingOptions = self.$element.find('option').filter(function () {
        return $(this).val() === item.id;
      });

      // If an existing option wasn't found for it, create the option
      if (!$existingOptions.length) {
        var $option = self.option(item);
        $option.attr('data-select2-tag', true);

        self._removeOldTags();
        self.addOptions([$option]);
      }

      // Select the item, now that we know there is an option for it
      select(item);
    }

    function select (data) {
      self.trigger('select', {
        data: data
      });
    }

    params.term = params.term || '';

    var tokenData = this.tokenizer(params, this.options, createAndSelect);

    if (tokenData.term !== params.term) {
      // Replace the search term if we have the search box
      if (this.$search.length) {
        this.$search.val(tokenData.term);
        this.$search.focus();
      }

      params.term = tokenData.term;
    }

    decorated.call(this, params, callback);
  };

  Tokenizer.prototype.tokenizer = function (_, params, options, callback) {
    var separators = options.get('tokenSeparators') || [];
    var term = params.term;
    var i = 0;

    var createTag = this.createTag || function (params) {
      return {
        id: params.term,
        text: params.term
      };
    };

    while (i < term.length) {
      var termChar = term[i];

      if ($.inArray(termChar, separators) === -1) {
        i++;

        continue;
      }

      var part = term.substr(0, i);
      var partParams = $.extend({}, params, {
        term: part
      });

      var data = createTag(partParams);

      if (data == null) {
        i++;
        continue;
      }

      callback(data);

      // Reset the term to not include the tokenized portion
      term = term.substr(i + 1) || '';
      i = 0;
    }

    return {
      term: term
    };
  };

  return Tokenizer;
});

S2.define('select2/data/minimumInputLength',[

], function () {
  function MinimumInputLength (decorated, $e, options) {
    this.minimumInputLength = options.get('minimumInputLength');

    decorated.call(this, $e, options);
  }

  MinimumInputLength.prototype.query = function (decorated, params, callback) {
    params.term = params.term || '';

    if (params.term.length < this.minimumInputLength) {
      this.trigger('results:message', {
        message: 'inputTooShort',
        args: {
          minimum: this.minimumInputLength,
          input: params.term,
          params: params
        }
      });

      return;
    }

    decorated.call(this, params, callback);
  };

  return MinimumInputLength;
});

S2.define('select2/data/maximumInputLength',[

], function () {
  function MaximumInputLength (decorated, $e, options) {
    this.maximumInputLength = options.get('maximumInputLength');

    decorated.call(this, $e, options);
  }

  MaximumInputLength.prototype.query = function (decorated, params, callback) {
    params.term = params.term || '';

    if (this.maximumInputLength > 0 &&
        params.term.length > this.maximumInputLength) {
      this.trigger('results:message', {
        message: 'inputTooLong',
        args: {
          maximum: this.maximumInputLength,
          input: params.term,
          params: params
        }
      });

      return;
    }

    decorated.call(this, params, callback);
  };

  return MaximumInputLength;
});

S2.define('select2/data/maximumSelectionLength',[

], function (){
  function MaximumSelectionLength (decorated, $e, options) {
    this.maximumSelectionLength = options.get('maximumSelectionLength');

    decorated.call(this, $e, options);
  }

  MaximumSelectionLength.prototype.query =
    function (decorated, params, callback) {
      var self = this;

      this.current(function (currentData) {
        var count = currentData != null ? currentData.length : 0;
        if (self.maximumSelectionLength > 0 &&
          count >= self.maximumSelectionLength) {
          self.trigger('results:message', {
            message: 'maximumSelected',
            args: {
              maximum: self.maximumSelectionLength
            }
          });
          return;
        }
        decorated.call(self, params, callback);
      });
  };

  return MaximumSelectionLength;
});

S2.define('select2/dropdown',[
  'jquery',
  './utils'
], function ($, Utils) {
  function Dropdown ($element, options) {
    this.$element = $element;
    this.options = options;

    Dropdown.__super__.constructor.call(this);
  }

  Utils.Extend(Dropdown, Utils.Observable);

  Dropdown.prototype.render = function () {
    var $dropdown = $(
      '<span class="select2-dropdown">' +
        '<span class="select2-results"></span>' +
      '</span>'
    );

    $dropdown.attr('dir', this.options.get('dir'));

    this.$dropdown = $dropdown;

    return $dropdown;
  };

  Dropdown.prototype.bind = function () {
    // Should be implemented in subclasses
  };

  Dropdown.prototype.position = function ($dropdown, $container) {
    // Should be implmented in subclasses
  };

  Dropdown.prototype.destroy = function () {
    // Remove the dropdown from the DOM
    this.$dropdown.remove();
  };

  return Dropdown;
});

S2.define('select2/dropdown/search',[
  'jquery',
  '../utils'
], function ($, Utils) {
  function Search () { }

  Search.prototype.render = function (decorated) {
    var $rendered = decorated.call(this);

    var $search = $(
      '<span class="select2-search select2-search--dropdown">' +
        '<input class="select2-search__field" type="search" tabindex="-1"' +
        ' autocomplete="off" autocorrect="off" autocapitalize="off"' +
        ' spellcheck="false" role="textbox" />' +
      '</span>'
    );

    this.$searchContainer = $search;
    this.$search = $search.find('input');

    $rendered.prepend($search);

    return $rendered;
  };

  Search.prototype.bind = function (decorated, container, $container) {
    var self = this;

    decorated.call(this, container, $container);

    this.$search.on('keydown', function (evt) {
      self.trigger('keypress', evt);

      self._keyUpPrevented = evt.isDefaultPrevented();
    });

    // Workaround for browsers which do not support the `input` event
    // This will prevent double-triggering of events for browsers which support
    // both the `keyup` and `input` events.
    this.$search.on('input', function (evt) {
      // Unbind the duplicated `keyup` event
      $(this).off('keyup');
    });

    this.$search.on('keyup input', function (evt) {
      self.handleSearch(evt);
    });

    container.on('open', function () {
      self.$search.attr('tabindex', 0);

      self.$search.focus();

      window.setTimeout(function () {
        self.$search.focus();
      }, 0);
    });

    container.on('close', function () {
      self.$search.attr('tabindex', -1);

      self.$search.val('');
    });

    container.on('focus', function () {
      if (!container.isOpen()) {
        self.$search.focus();
      }
    });

    container.on('results:all', function (params) {
      if (params.query.term == null || params.query.term === '') {
        var showSearch = self.showSearch(params);

        if (showSearch) {
          self.$searchContainer.removeClass('select2-search--hide');
        } else {
          self.$searchContainer.addClass('select2-search--hide');
        }
      }
    });
  };

  Search.prototype.handleSearch = function (evt) {
    if (!this._keyUpPrevented) {
      var input = this.$search.val();

      this.trigger('query', {
        term: input
      });
    }

    this._keyUpPrevented = false;
  };

  Search.prototype.showSearch = function (_, params) {
    return true;
  };

  return Search;
});

S2.define('select2/dropdown/hidePlaceholder',[

], function () {
  function HidePlaceholder (decorated, $element, options, dataAdapter) {
    this.placeholder = this.normalizePlaceholder(options.get('placeholder'));

    decorated.call(this, $element, options, dataAdapter);
  }

  HidePlaceholder.prototype.append = function (decorated, data) {
    data.results = this.removePlaceholder(data.results);

    decorated.call(this, data);
  };

  HidePlaceholder.prototype.normalizePlaceholder = function (_, placeholder) {
    if (typeof placeholder === 'string') {
      placeholder = {
        id: '',
        text: placeholder
      };
    }

    return placeholder;
  };

  HidePlaceholder.prototype.removePlaceholder = function (_, data) {
    var modifiedData = data.slice(0);

    for (var d = data.length - 1; d >= 0; d--) {
      var item = data[d];

      if (this.placeholder.id === item.id) {
        modifiedData.splice(d, 1);
      }
    }

    return modifiedData;
  };

  return HidePlaceholder;
});

S2.define('select2/dropdown/infiniteScroll',[
  'jquery'
], function ($) {
  function InfiniteScroll (decorated, $element, options, dataAdapter) {
    this.lastParams = {};

    decorated.call(this, $element, options, dataAdapter);

    this.$loadingMore = this.createLoadingMore();
    this.loading = false;
  }

  InfiniteScroll.prototype.append = function (decorated, data) {
    this.$loadingMore.remove();
    this.loading = false;

    decorated.call(this, data);

    if (this.showLoadingMore(data)) {
      this.$results.append(this.$loadingMore);
    }
  };

  InfiniteScroll.prototype.bind = function (decorated, container, $container) {
    var self = this;

    decorated.call(this, container, $container);

    container.on('query', function (params) {
      self.lastParams = params;
      self.loading = true;
    });

    container.on('query:append', function (params) {
      self.lastParams = params;
      self.loading = true;
    });

    this.$results.on('scroll', function () {
      var isLoadMoreVisible = $.contains(
        document.documentElement,
        self.$loadingMore[0]
      );

      if (self.loading || !isLoadMoreVisible) {
        return;
      }

      var currentOffset = self.$results.offset().top +
        self.$results.outerHeight(false);
      var loadingMoreOffset = self.$loadingMore.offset().top +
        self.$loadingMore.outerHeight(false);

      if (currentOffset + 50 >= loadingMoreOffset) {
        self.loadMore();
      }
    });
  };

  InfiniteScroll.prototype.loadMore = function () {
    this.loading = true;

    var params = $.extend({}, {page: 1}, this.lastParams);

    params.page++;

    this.trigger('query:append', params);
  };

  InfiniteScroll.prototype.showLoadingMore = function (_, data) {
    return data.pagination && data.pagination.more;
  };

  InfiniteScroll.prototype.createLoadingMore = function () {
    var $option = $(
      '<li ' +
      'class="select2-results__option select2-results__option--load-more"' +
      'role="treeitem" aria-disabled="true"></li>'
    );

    var message = this.options.get('translations').get('loadingMore');

    $option.html(message(this.lastParams));

    return $option;
  };

  return InfiniteScroll;
});

S2.define('select2/dropdown/attachBody',[
  'jquery',
  '../utils'
], function ($, Utils) {
  function AttachBody (decorated, $element, options) {
    this.$dropdownParent = options.get('dropdownParent') || $(document.body);

    decorated.call(this, $element, options);
  }

  AttachBody.prototype.bind = function (decorated, container, $container) {
    var self = this;

    var setupResultsEvents = false;

    decorated.call(this, container, $container);

    container.on('open', function () {
      self._showDropdown();
      self._attachPositioningHandler(container);

      if (!setupResultsEvents) {
        setupResultsEvents = true;

        container.on('results:all', function () {
          self._positionDropdown();
          self._resizeDropdown();
        });

        container.on('results:append', function () {
          self._positionDropdown();
          self._resizeDropdown();
        });
      }
    });

    container.on('close', function () {
      self._hideDropdown();
      self._detachPositioningHandler(container);
    });

    this.$dropdownContainer.on('mousedown', function (evt) {
      evt.stopPropagation();
    });
  };

  AttachBody.prototype.destroy = function (decorated) {
    decorated.call(this);

    this.$dropdownContainer.remove();
  };

  AttachBody.prototype.position = function (decorated, $dropdown, $container) {
    // Clone all of the container classes
    $dropdown.attr('class', $container.attr('class'));

    $dropdown.removeClass('select2');
    $dropdown.addClass('select2-container--open');

    $dropdown.css({
      position: 'absolute',
      top: -999999
    });

    this.$container = $container;
  };

  AttachBody.prototype.render = function (decorated) {
    var $container = $('<span></span>');

    var $dropdown = decorated.call(this);
    $container.append($dropdown);

    this.$dropdownContainer = $container;

    return $container;
  };

  AttachBody.prototype._hideDropdown = function (decorated) {
    this.$dropdownContainer.detach();
  };

  AttachBody.prototype._attachPositioningHandler =
      function (decorated, container) {
    var self = this;

    var scrollEvent = 'scroll.select2.' + container.id;
    var resizeEvent = 'resize.select2.' + container.id;
    var orientationEvent = 'orientationchange.select2.' + container.id;

    var $watchers = this.$container.parents().filter(Utils.hasScroll);
    $watchers.each(function () {
      $(this).data('select2-scroll-position', {
        x: $(this).scrollLeft(),
        y: $(this).scrollTop()
      });
    });

    $watchers.on(scrollEvent, function (ev) {
      var position = $(this).data('select2-scroll-position');
      $(this).scrollTop(position.y);
    });

    $(window).on(scrollEvent + ' ' + resizeEvent + ' ' + orientationEvent,
      function (e) {
      self._positionDropdown();
      self._resizeDropdown();
    });
  };

  AttachBody.prototype._detachPositioningHandler =
      function (decorated, container) {
    var scrollEvent = 'scroll.select2.' + container.id;
    var resizeEvent = 'resize.select2.' + container.id;
    var orientationEvent = 'orientationchange.select2.' + container.id;

    var $watchers = this.$container.parents().filter(Utils.hasScroll);
    $watchers.off(scrollEvent);

    $(window).off(scrollEvent + ' ' + resizeEvent + ' ' + orientationEvent);
  };

  AttachBody.prototype._positionDropdown = function () {
    var $window = $(window);

    var isCurrentlyAbove = this.$dropdown.hasClass('select2-dropdown--above');
    var isCurrentlyBelow = this.$dropdown.hasClass('select2-dropdown--below');

    var newDirection = null;

    var offset = this.$container.offset();

    offset.bottom = offset.top + this.$container.outerHeight(false);

    var container = {
      height: this.$container.outerHeight(false)
    };

    container.top = offset.top;
    container.bottom = offset.top + container.height;

    var dropdown = {
      height: this.$dropdown.outerHeight(false)
    };

    var viewport = {
      top: $window.scrollTop(),
      bottom: $window.scrollTop() + $window.height()
    };

    var enoughRoomAbove = viewport.top < (offset.top - dropdown.height);
    var enoughRoomBelow = viewport.bottom > (offset.bottom + dropdown.height);

    var css = {
      left: offset.left,
      top: container.bottom
    };

    // Determine what the parent element is to use for calciulating the offset
    var $offsetParent = this.$dropdownParent;

    // For statically positoned elements, we need to get the element
    // that is determining the offset
    if ($offsetParent.css('position') === 'static') {
      $offsetParent = $offsetParent.offsetParent();
    }

    var parentOffset = $offsetParent.offset();

    css.top -= parentOffset.top;
    css.left -= parentOffset.left;

    if (!isCurrentlyAbove && !isCurrentlyBelow) {
      newDirection = 'below';
    }

    if (!enoughRoomBelow && enoughRoomAbove && !isCurrentlyAbove) {
      newDirection = 'above';
    } else if (!enoughRoomAbove && enoughRoomBelow && isCurrentlyAbove) {
      newDirection = 'below';
    }

    if (newDirection == 'above' ||
      (isCurrentlyAbove && newDirection !== 'below')) {
      css.top = container.top - parentOffset.top - dropdown.height;
    }

    if (newDirection != null) {
      this.$dropdown
        .removeClass('select2-dropdown--below select2-dropdown--above')
        .addClass('select2-dropdown--' + newDirection);
      this.$container
        .removeClass('select2-container--below select2-container--above')
        .addClass('select2-container--' + newDirection);
    }

    this.$dropdownContainer.css(css);
  };

  AttachBody.prototype._resizeDropdown = function () {
    var css = {
      width: this.$container.outerWidth(false) + 'px'
    };

    if (this.options.get('dropdownAutoWidth')) {
      css.minWidth = css.width;
      css.position = 'relative';
      css.width = 'auto';
    }

    this.$dropdown.css(css);
  };

  AttachBody.prototype._showDropdown = function (decorated) {
    this.$dropdownContainer.appendTo(this.$dropdownParent);

    this._positionDropdown();
    this._resizeDropdown();
  };

  return AttachBody;
});

S2.define('select2/dropdown/minimumResultsForSearch',[

], function () {
  function countResults (data) {
    var count = 0;

    for (var d = 0; d < data.length; d++) {
      var item = data[d];

      if (item.children) {
        count += countResults(item.children);
      } else {
        count++;
      }
    }

    return count;
  }

  function MinimumResultsForSearch (decorated, $element, options, dataAdapter) {
    this.minimumResultsForSearch = options.get('minimumResultsForSearch');

    if (this.minimumResultsForSearch < 0) {
      this.minimumResultsForSearch = Infinity;
    }

    decorated.call(this, $element, options, dataAdapter);
  }

  MinimumResultsForSearch.prototype.showSearch = function (decorated, params) {
    if (countResults(params.data.results) < this.minimumResultsForSearch) {
      return false;
    }

    return decorated.call(this, params);
  };

  return MinimumResultsForSearch;
});

S2.define('select2/dropdown/selectOnClose',[

], function () {
  function SelectOnClose () { }

  SelectOnClose.prototype.bind = function (decorated, container, $container) {
    var self = this;

    decorated.call(this, container, $container);

    container.on('close', function (params) {
      self._handleSelectOnClose(params);
    });
  };

  SelectOnClose.prototype._handleSelectOnClose = function (_, params) {
    if (params && params.originalSelect2Event != null) {
      var event = params.originalSelect2Event;

      // Don't select an item if the close event was triggered from a select or
      // unselect event
      if (event._type === 'select' || event._type === 'unselect') {
        return;
      }
    }

    var $highlightedResults = this.getHighlightedResults();

    // Only select highlighted results
    if ($highlightedResults.length < 1) {
      return;
    }

    var data = $highlightedResults.data('data');

    // Don't re-select already selected resulte
    if (
      (data.element != null && data.element.selected) ||
      (data.element == null && data.selected)
    ) {
      return;
    }

    this.trigger('select', {
        data: data
    });
  };

  return SelectOnClose;
});

S2.define('select2/dropdown/closeOnSelect',[

], function () {
  function CloseOnSelect () { }

  CloseOnSelect.prototype.bind = function (decorated, container, $container) {
    var self = this;

    decorated.call(this, container, $container);

    container.on('select', function (evt) {
      self._selectTriggered(evt);
    });

    container.on('unselect', function (evt) {
      self._selectTriggered(evt);
    });
  };

  CloseOnSelect.prototype._selectTriggered = function (_, evt) {
    var originalEvent = evt.originalEvent;

    // Don't close if the control key is being held
    if (originalEvent && originalEvent.ctrlKey) {
      return;
    }

    this.trigger('close', {
      originalEvent: originalEvent,
      originalSelect2Event: evt
    });
  };

  return CloseOnSelect;
});

S2.define('select2/i18n/en',[],function () {
  // English
  return {
    errorLoading: function () {
      return 'The results could not be loaded.';
    },
    inputTooLong: function (args) {
      var overChars = args.input.length - args.maximum;

      var message = 'Please delete ' + overChars + ' character';

      if (overChars != 1) {
        message += 's';
      }

      return message;
    },
    inputTooShort: function (args) {
      var remainingChars = args.minimum - args.input.length;

      var message = 'Please enter ' + remainingChars + ' or more characters';

      return message;
    },
    loadingMore: function () {
      return 'Loading more results…';
    },
    maximumSelected: function (args) {
      var message = 'You can only select ' + args.maximum + ' item';

      if (args.maximum != 1) {
        message += 's';
      }

      return message;
    },
    noResults: function () {
      return 'No results found';
    },
    searching: function () {
      return 'Searching…';
    }
  };
});

S2.define('select2/defaults',[
  'jquery',
  'require',

  './results',

  './selection/single',
  './selection/multiple',
  './selection/placeholder',
  './selection/allowClear',
  './selection/search',
  './selection/eventRelay',

  './utils',
  './translation',
  './diacritics',

  './data/select',
  './data/array',
  './data/ajax',
  './data/tags',
  './data/tokenizer',
  './data/minimumInputLength',
  './data/maximumInputLength',
  './data/maximumSelectionLength',

  './dropdown',
  './dropdown/search',
  './dropdown/hidePlaceholder',
  './dropdown/infiniteScroll',
  './dropdown/attachBody',
  './dropdown/minimumResultsForSearch',
  './dropdown/selectOnClose',
  './dropdown/closeOnSelect',

  './i18n/en'
], function ($, require,

             ResultsList,

             SingleSelection, MultipleSelection, Placeholder, AllowClear,
             SelectionSearch, EventRelay,

             Utils, Translation, DIACRITICS,

             SelectData, ArrayData, AjaxData, Tags, Tokenizer,
             MinimumInputLength, MaximumInputLength, MaximumSelectionLength,

             Dropdown, DropdownSearch, HidePlaceholder, InfiniteScroll,
             AttachBody, MinimumResultsForSearch, SelectOnClose, CloseOnSelect,

             EnglishTranslation) {
  function Defaults () {
    this.reset();
  }

  Defaults.prototype.apply = function (options) {
    options = $.extend(true, {}, this.defaults, options);

    if (options.dataAdapter == null) {
      if (options.ajax != null) {
        options.dataAdapter = AjaxData;
      } else if (options.data != null) {
        options.dataAdapter = ArrayData;
      } else {
        options.dataAdapter = SelectData;
      }

      if (options.minimumInputLength > 0) {
        options.dataAdapter = Utils.Decorate(
          options.dataAdapter,
          MinimumInputLength
        );
      }

      if (options.maximumInputLength > 0) {
        options.dataAdapter = Utils.Decorate(
          options.dataAdapter,
          MaximumInputLength
        );
      }

      if (options.maximumSelectionLength > 0) {
        options.dataAdapter = Utils.Decorate(
          options.dataAdapter,
          MaximumSelectionLength
        );
      }

      if (options.tags) {
        options.dataAdapter = Utils.Decorate(options.dataAdapter, Tags);
      }

      if (options.tokenSeparators != null || options.tokenizer != null) {
        options.dataAdapter = Utils.Decorate(
          options.dataAdapter,
          Tokenizer
        );
      }

      if (options.query != null) {
        var Query = require(options.amdBase + 'compat/query');

        options.dataAdapter = Utils.Decorate(
          options.dataAdapter,
          Query
        );
      }

      if (options.initSelection != null) {
        var InitSelection = require(options.amdBase + 'compat/initSelection');

        options.dataAdapter = Utils.Decorate(
          options.dataAdapter,
          InitSelection
        );
      }
    }

    if (options.resultsAdapter == null) {
      options.resultsAdapter = ResultsList;

      if (options.ajax != null) {
        options.resultsAdapter = Utils.Decorate(
          options.resultsAdapter,
          InfiniteScroll
        );
      }

      if (options.placeholder != null) {
        options.resultsAdapter = Utils.Decorate(
          options.resultsAdapter,
          HidePlaceholder
        );
      }

      if (options.selectOnClose) {
        options.resultsAdapter = Utils.Decorate(
          options.resultsAdapter,
          SelectOnClose
        );
      }
    }

    if (options.dropdownAdapter == null) {
      if (options.multiple) {
        options.dropdownAdapter = Dropdown;
      } else {
        var SearchableDropdown = Utils.Decorate(Dropdown, DropdownSearch);

        options.dropdownAdapter = SearchableDropdown;
      }

      if (options.minimumResultsForSearch !== 0) {
        options.dropdownAdapter = Utils.Decorate(
          options.dropdownAdapter,
          MinimumResultsForSearch
        );
      }

      if (options.closeOnSelect) {
        options.dropdownAdapter = Utils.Decorate(
          options.dropdownAdapter,
          CloseOnSelect
        );
      }

      if (
        options.dropdownCssClass != null ||
        options.dropdownCss != null ||
        options.adaptDropdownCssClass != null
      ) {
        var DropdownCSS = require(options.amdBase + 'compat/dropdownCss');

        options.dropdownAdapter = Utils.Decorate(
          options.dropdownAdapter,
          DropdownCSS
        );
      }

      options.dropdownAdapter = Utils.Decorate(
        options.dropdownAdapter,
        AttachBody
      );
    }

    if (options.selectionAdapter == null) {
      if (options.multiple) {
        options.selectionAdapter = MultipleSelection;
      } else {
        options.selectionAdapter = SingleSelection;
      }

      // Add the placeholder mixin if a placeholder was specified
      if (options.placeholder != null) {
        options.selectionAdapter = Utils.Decorate(
          options.selectionAdapter,
          Placeholder
        );
      }

      if (options.allowClear) {
        options.selectionAdapter = Utils.Decorate(
          options.selectionAdapter,
          AllowClear
        );
      }

      if (options.multiple) {
        options.selectionAdapter = Utils.Decorate(
          options.selectionAdapter,
          SelectionSearch
        );
      }

      if (
        options.containerCssClass != null ||
        options.containerCss != null ||
        options.adaptContainerCssClass != null
      ) {
        var ContainerCSS = require(options.amdBase + 'compat/containerCss');

        options.selectionAdapter = Utils.Decorate(
          options.selectionAdapter,
          ContainerCSS
        );
      }

      options.selectionAdapter = Utils.Decorate(
        options.selectionAdapter,
        EventRelay
      );
    }

    if (typeof options.language === 'string') {
      // Check if the language is specified with a region
      if (options.language.indexOf('-') > 0) {
        // Extract the region information if it is included
        var languageParts = options.language.split('-');
        var baseLanguage = languageParts[0];

        options.language = [options.language, baseLanguage];
      } else {
        options.language = [options.language];
      }
    }

    if ($.isArray(options.language)) {
      var languages = new Translation();
      options.language.push('en');

      var languageNames = options.language;

      for (var l = 0; l < languageNames.length; l++) {
        var name = languageNames[l];
        var language = {};

        try {
          // Try to load it with the original name
          language = Translation.loadPath(name);
        } catch (e) {
          try {
            // If we couldn't load it, check if it wasn't the full path
            name = this.defaults.amdLanguageBase + name;
            language = Translation.loadPath(name);
          } catch (ex) {
            // The translation could not be loaded at all. Sometimes this is
            // because of a configuration problem, other times this can be
            // because of how Select2 helps load all possible translation files.
            if (options.debug && window.console && console.warn) {
              console.warn(
                'Select2: The language file for "' + name + '" could not be ' +
                'automatically loaded. A fallback will be used instead.'
              );
            }

            continue;
          }
        }

        languages.extend(language);
      }

      options.translations = languages;
    } else {
      var baseTranslation = Translation.loadPath(
        this.defaults.amdLanguageBase + 'en'
      );
      var customTranslation = new Translation(options.language);

      customTranslation.extend(baseTranslation);

      options.translations = customTranslation;
    }

    return options;
  };

  Defaults.prototype.reset = function () {
    function stripDiacritics (text) {
      // Used 'uni range + named function' from http://jsperf.com/diacritics/18
      function match(a) {
        return DIACRITICS[a] || a;
      }

      return text.replace(/[^\u0000-\u007E]/g, match);
    }

    function matcher (params, data) {
      // Always return the object if there is nothing to compare
      if ($.trim(params.term) === '') {
        return data;
      }

      // Do a recursive check for options with children
      if (data.children && data.children.length > 0) {
        // Clone the data object if there are children
        // This is required as we modify the object to remove any non-matches
        var match = $.extend(true, {}, data);

        // Check each child of the option
        for (var c = data.children.length - 1; c >= 0; c--) {
          var child = data.children[c];

          var matches = matcher(params, child);

          // If there wasn't a match, remove the object in the array
          if (matches == null) {
            match.children.splice(c, 1);
          }
        }

        // If any children matched, return the new object
        if (match.children.length > 0) {
          return match;
        }

        // If there were no matching children, check just the plain object
        return matcher(params, match);
      }

      var original = stripDiacritics(data.text).toUpperCase();
      var term = stripDiacritics(params.term).toUpperCase();

      // Check if the text contains the term
      if (original.indexOf(term) > -1) {
        return data;
      }

      // If it doesn't contain the term, don't return anything
      return null;
    }

    this.defaults = {
      amdBase: './',
      amdLanguageBase: './i18n/',
      closeOnSelect: true,
      debug: false,
      dropdownAutoWidth: false,
      escapeMarkup: Utils.escapeMarkup,
      language: EnglishTranslation,
      matcher: matcher,
      minimumInputLength: 0,
      maximumInputLength: 0,
      maximumSelectionLength: 0,
      minimumResultsForSearch: 0,
      selectOnClose: false,
      sorter: function (data) {
        return data;
      },
      templateResult: function (result) {
        return result.text;
      },
      templateSelection: function (selection) {
        return selection.text;
      },
      theme: 'default',
      width: 'resolve'
    };
  };

  Defaults.prototype.set = function (key, value) {
    var camelKey = $.camelCase(key);

    var data = {};
    data[camelKey] = value;

    var convertedData = Utils._convertData(data);

    $.extend(this.defaults, convertedData);
  };

  var defaults = new Defaults();

  return defaults;
});

S2.define('select2/options',[
  'require',
  'jquery',
  './defaults',
  './utils'
], function (require, $, Defaults, Utils) {
  function Options (options, $element) {
    this.options = options;

    if ($element != null) {
      this.fromElement($element);
    }

    this.options = Defaults.apply(this.options);

    if ($element && $element.is('input')) {
      var InputCompat = require(this.get('amdBase') + 'compat/inputData');

      this.options.dataAdapter = Utils.Decorate(
        this.options.dataAdapter,
        InputCompat
      );
    }
  }

  Options.prototype.fromElement = function ($e) {
    var excludedData = ['select2'];

    if (this.options.multiple == null) {
      this.options.multiple = $e.prop('multiple');
    }

    if (this.options.disabled == null) {
      this.options.disabled = $e.prop('disabled');
    }

    if (this.options.language == null) {
      if ($e.prop('lang')) {
        this.options.language = $e.prop('lang').toLowerCase();
      } else if ($e.closest('[lang]').prop('lang')) {
        this.options.language = $e.closest('[lang]').prop('lang');
      }
    }

    if (this.options.dir == null) {
      if ($e.prop('dir')) {
        this.options.dir = $e.prop('dir');
      } else if ($e.closest('[dir]').prop('dir')) {
        this.options.dir = $e.closest('[dir]').prop('dir');
      } else {
        this.options.dir = 'ltr';
      }
    }

    $e.prop('disabled', this.options.disabled);
    $e.prop('multiple', this.options.multiple);

    if ($e.data('select2Tags')) {
      if (this.options.debug && window.console && console.warn) {
        console.warn(
          'Select2: The `data-select2-tags` attribute has been changed to ' +
          'use the `data-data` and `data-tags="true"` attributes and will be ' +
          'removed in future versions of Select2.'
        );
      }

      $e.data('data', $e.data('select2Tags'));
      $e.data('tags', true);
    }

    if ($e.data('ajaxUrl')) {
      if (this.options.debug && window.console && console.warn) {
        console.warn(
          'Select2: The `data-ajax-url` attribute has been changed to ' +
          '`data-ajax--url` and support for the old attribute will be removed' +
          ' in future versions of Select2.'
        );
      }

      $e.attr('ajax--url', $e.data('ajaxUrl'));
      $e.data('ajax--url', $e.data('ajaxUrl'));
    }

    var dataset = {};

    // Prefer the element's `dataset` attribute if it exists
    // jQuery 1.x does not correctly handle data attributes with multiple dashes
    if ($.fn.jquery && $.fn.jquery.substr(0, 2) == '1.' && $e[0].dataset) {
      dataset = $.extend(true, {}, $e[0].dataset, $e.data());
    } else {
      dataset = $e.data();
    }

    var data = $.extend(true, {}, dataset);

    data = Utils._convertData(data);

    for (var key in data) {
      if ($.inArray(key, excludedData) > -1) {
        continue;
      }

      if ($.isPlainObject(this.options[key])) {
        $.extend(this.options[key], data[key]);
      } else {
        this.options[key] = data[key];
      }
    }

    return this;
  };

  Options.prototype.get = function (key) {
    return this.options[key];
  };

  Options.prototype.set = function (key, val) {
    this.options[key] = val;
  };

  return Options;
});

S2.define('select2/core',[
  'jquery',
  './options',
  './utils',
  './keys'
], function ($, Options, Utils, KEYS) {
  var Select2 = function ($element, options) {
    if ($element.data('select2') != null) {
      $element.data('select2').destroy();
    }

    this.$element = $element;

    this.id = this._generateId($element);

    options = options || {};

    this.options = new Options(options, $element);

    Select2.__super__.constructor.call(this);

    // Set up the tabindex

    var tabindex = $element.attr('tabindex') || 0;
    $element.data('old-tabindex', tabindex);
    $element.attr('tabindex', '-1');

    // Set up containers and adapters

    var DataAdapter = this.options.get('dataAdapter');
    this.dataAdapter = new DataAdapter($element, this.options);

    var $container = this.render();

    this._placeContainer($container);

    var SelectionAdapter = this.options.get('selectionAdapter');
    this.selection = new SelectionAdapter($element, this.options);
    this.$selection = this.selection.render();

    this.selection.position(this.$selection, $container);

    var DropdownAdapter = this.options.get('dropdownAdapter');
    this.dropdown = new DropdownAdapter($element, this.options);
    this.$dropdown = this.dropdown.render();

    this.dropdown.position(this.$dropdown, $container);

    var ResultsAdapter = this.options.get('resultsAdapter');
    this.results = new ResultsAdapter($element, this.options, this.dataAdapter);
    this.$results = this.results.render();

    this.results.position(this.$results, this.$dropdown);

    // Bind events

    var self = this;

    // Bind the container to all of the adapters
    this._bindAdapters();

    // Register any DOM event handlers
    this._registerDomEvents();

    // Register any internal event handlers
    this._registerDataEvents();
    this._registerSelectionEvents();
    this._registerDropdownEvents();
    this._registerResultsEvents();
    this._registerEvents();

    // Set the initial state
    this.dataAdapter.current(function (initialData) {
      self.trigger('selection:update', {
        data: initialData
      });
    });

    // Hide the original select
    $element.addClass('select2-hidden-accessible');
    $element.attr('aria-hidden', 'true');

    // Synchronize any monitored attributes
    this._syncAttributes();

    $element.data('select2', this);
  };

  Utils.Extend(Select2, Utils.Observable);

  Select2.prototype._generateId = function ($element) {
    var id = '';

    if ($element.attr('id') != null) {
      id = $element.attr('id');
    } else if ($element.attr('name') != null) {
      id = $element.attr('name') + '-' + Utils.generateChars(2);
    } else {
      id = Utils.generateChars(4);
    }

    id = id.replace(/(:|\.|\[|\]|,)/g, '');
    id = 'select2-' + id;

    return id;
  };

  Select2.prototype._placeContainer = function ($container) {
    $container.insertAfter(this.$element);

    var width = this._resolveWidth(this.$element, this.options.get('width'));

    if (width != null) {
      $container.css('width', width);
    }
  };

  Select2.prototype._resolveWidth = function ($element, method) {
    var WIDTH = /^width:(([-+]?([0-9]*\.)?[0-9]+)(px|em|ex|%|in|cm|mm|pt|pc))/i;

    if (method == 'resolve') {
      var styleWidth = this._resolveWidth($element, 'style');

      if (styleWidth != null) {
        return styleWidth;
      }

      return this._resolveWidth($element, 'element');
    }

    if (method == 'element') {
      var elementWidth = $element.outerWidth(false);

      if (elementWidth <= 0) {
        return 'auto';
      }

      return elementWidth + 'px';
    }

    if (method == 'style') {
      var style = $element.attr('style');

      if (typeof(style) !== 'string') {
        return null;
      }

      var attrs = style.split(';');

      for (var i = 0, l = attrs.length; i < l; i = i + 1) {
        var attr = attrs[i].replace(/\s/g, '');
        var matches = attr.match(WIDTH);

        if (matches !== null && matches.length >= 1) {
          return matches[1];
        }
      }

      return null;
    }

    return method;
  };

  Select2.prototype._bindAdapters = function () {
    this.dataAdapter.bind(this, this.$container);
    this.selection.bind(this, this.$container);

    this.dropdown.bind(this, this.$container);
    this.results.bind(this, this.$container);
  };

  Select2.prototype._registerDomEvents = function () {
    var self = this;

    this.$element.on('change.select2', function () {
      self.dataAdapter.current(function (data) {
        self.trigger('selection:update', {
          data: data
        });
      });
    });

    this.$element.on('focus.select2', function (evt) {
      self.trigger('focus', evt);
    });

    this._syncA = Utils.bind(this._syncAttributes, this);
    this._syncS = Utils.bind(this._syncSubtree, this);

    if (this.$element[0].attachEvent) {
      this.$element[0].attachEvent('onpropertychange', this._syncA);
    }

    var observer = window.MutationObserver ||
      window.WebKitMutationObserver ||
      window.MozMutationObserver
    ;

    if (observer != null) {
      this._observer = new observer(function (mutations) {
        $.each(mutations, self._syncA);
        $.each(mutations, self._syncS);
      });
      this._observer.observe(this.$element[0], {
        attributes: true,
        childList: true,
        subtree: false
      });
    } else if (this.$element[0].addEventListener) {
      this.$element[0].addEventListener(
        'DOMAttrModified',
        self._syncA,
        false
      );
      this.$element[0].addEventListener(
        'DOMNodeInserted',
        self._syncS,
        false
      );
      this.$element[0].addEventListener(
        'DOMNodeRemoved',
        self._syncS,
        false
      );
    }
  };

  Select2.prototype._registerDataEvents = function () {
    var self = this;

    this.dataAdapter.on('*', function (name, params) {
      self.trigger(name, params);
    });
  };

  Select2.prototype._registerSelectionEvents = function () {
    var self = this;
    var nonRelayEvents = ['toggle', 'focus'];

    this.selection.on('toggle', function () {
      self.toggleDropdown();
    });

    this.selection.on('focus', function (params) {
      self.focus(params);
    });

    this.selection.on('*', function (name, params) {
      if ($.inArray(name, nonRelayEvents) !== -1) {
        return;
      }

      self.trigger(name, params);
    });
  };

  Select2.prototype._registerDropdownEvents = function () {
    var self = this;

    this.dropdown.on('*', function (name, params) {
      self.trigger(name, params);
    });
  };

  Select2.prototype._registerResultsEvents = function () {
    var self = this;

    this.results.on('*', function (name, params) {
      self.trigger(name, params);
    });
  };

  Select2.prototype._registerEvents = function () {
    var self = this;

    this.on('open', function () {
      self.$container.addClass('select2-container--open');
    });

    this.on('close', function () {
      self.$container.removeClass('select2-container--open');
    });

    this.on('enable', function () {
      self.$container.removeClass('select2-container--disabled');
    });

    this.on('disable', function () {
      self.$container.addClass('select2-container--disabled');
    });

    this.on('blur', function () {
      self.$container.removeClass('select2-container--focus');
    });

    this.on('query', function (params) {
      if (!self.isOpen()) {
        self.trigger('open', {});
      }

      this.dataAdapter.query(params, function (data) {
        self.trigger('results:all', {
          data: data,
          query: params
        });
      });
    });

    this.on('query:append', function (params) {
      this.dataAdapter.query(params, function (data) {
        self.trigger('results:append', {
          data: data,
          query: params
        });
      });
    });

    this.on('keypress', function (evt) {
      var key = evt.which;

      if (self.isOpen()) {
        if (key === KEYS.ESC || key === KEYS.TAB ||
            (key === KEYS.UP && evt.altKey)) {
          self.close();

          evt.preventDefault();
        } else if (key === KEYS.ENTER) {
          self.trigger('results:select', {});

          evt.preventDefault();
        } else if ((key === KEYS.SPACE && evt.ctrlKey)) {
          self.trigger('results:toggle', {});

          evt.preventDefault();
        } else if (key === KEYS.UP) {
          self.trigger('results:previous', {});

          evt.preventDefault();
        } else if (key === KEYS.DOWN) {
          self.trigger('results:next', {});

          evt.preventDefault();
        }
      } else {
        if (key === KEYS.ENTER || key === KEYS.SPACE ||
            (key === KEYS.DOWN && evt.altKey)) {
          self.open();

          evt.preventDefault();
        }
      }
    });
  };

  Select2.prototype._syncAttributes = function () {
    this.options.set('disabled', this.$element.prop('disabled'));

    if (this.options.get('disabled')) {
      if (this.isOpen()) {
        this.close();
      }

      this.trigger('disable', {});
    } else {
      this.trigger('enable', {});
    }
  };

  Select2.prototype._syncSubtree = function (evt, mutations) {
    var changed = false;
    var self = this;

    // Ignore any mutation events raised for elements that aren't options or
    // optgroups. This handles the case when the select element is destroyed
    if (
      evt && evt.target && (
        evt.target.nodeName !== 'OPTION' && evt.target.nodeName !== 'OPTGROUP'
      )
    ) {
      return;
    }

    if (!mutations) {
      // If mutation events aren't supported, then we can only assume that the
      // change affected the selections
      changed = true;
    } else if (mutations.addedNodes && mutations.addedNodes.length > 0) {
      for (var n = 0; n < mutations.addedNodes.length; n++) {
        var node = mutations.addedNodes[n];

        if (node.selected) {
          changed = true;
        }
      }
    } else if (mutations.removedNodes && mutations.removedNodes.length > 0) {
      changed = true;
    }

    // Only re-pull the data if we think there is a change
    if (changed) {
      this.dataAdapter.current(function (currentData) {
        self.trigger('selection:update', {
          data: currentData
        });
      });
    }
  };

  /**
   * Override the trigger method to automatically trigger pre-events when
   * there are events that can be prevented.
   */
  Select2.prototype.trigger = function (name, args) {
    var actualTrigger = Select2.__super__.trigger;
    var preTriggerMap = {
      'open': 'opening',
      'close': 'closing',
      'select': 'selecting',
      'unselect': 'unselecting'
    };

    if (args === undefined) {
      args = {};
    }

    if (name in preTriggerMap) {
      var preTriggerName = preTriggerMap[name];
      var preTriggerArgs = {
        prevented: false,
        name: name,
        args: args
      };

      actualTrigger.call(this, preTriggerName, preTriggerArgs);

      if (preTriggerArgs.prevented) {
        args.prevented = true;

        return;
      }
    }

    actualTrigger.call(this, name, args);
  };

  Select2.prototype.toggleDropdown = function () {
    if (this.options.get('disabled')) {
      return;
    }

    if (this.isOpen()) {
      this.close();
    } else {
      this.open();
    }
  };

  Select2.prototype.open = function () {
    if (this.isOpen()) {
      return;
    }

    this.trigger('query', {});
  };

  Select2.prototype.close = function () {
    if (!this.isOpen()) {
      return;
    }

    this.trigger('close', {});
  };

  Select2.prototype.isOpen = function () {
    return this.$container.hasClass('select2-container--open');
  };

  Select2.prototype.hasFocus = function () {
    return this.$container.hasClass('select2-container--focus');
  };

  Select2.prototype.focus = function (data) {
    // No need to re-trigger focus events if we are already focused
    if (this.hasFocus()) {
      return;
    }

    this.$container.addClass('select2-container--focus');
    this.trigger('focus', {});
  };

  Select2.prototype.enable = function (args) {
    if (this.options.get('debug') && window.console && console.warn) {
      console.warn(
        'Select2: The `select2("enable")` method has been deprecated and will' +
        ' be removed in later Select2 versions. Use $element.prop("disabled")' +
        ' instead.'
      );
    }

    if (args == null || args.length === 0) {
      args = [true];
    }

    var disabled = !args[0];

    this.$element.prop('disabled', disabled);
  };

  Select2.prototype.data = function () {
    if (this.options.get('debug') &&
        arguments.length > 0 && window.console && console.warn) {
      console.warn(
        'Select2: Data can no longer be set using `select2("data")`. You ' +
        'should consider setting the value instead using `$element.val()`.'
      );
    }

    var data = [];

    this.dataAdapter.current(function (currentData) {
      data = currentData;
    });

    return data;
  };

  Select2.prototype.val = function (args) {
    if (this.options.get('debug') && window.console && console.warn) {
      console.warn(
        'Select2: The `select2("val")` method has been deprecated and will be' +
        ' removed in later Select2 versions. Use $element.val() instead.'
      );
    }

    if (args == null || args.length === 0) {
      return this.$element.val();
    }

    var newVal = args[0];

    if ($.isArray(newVal)) {
      newVal = $.map(newVal, function (obj) {
        return obj.toString();
      });
    }

    this.$element.val(newVal).trigger('change');
  };

  Select2.prototype.destroy = function () {
    this.$container.remove();

    if (this.$element[0].detachEvent) {
      this.$element[0].detachEvent('onpropertychange', this._syncA);
    }

    if (this._observer != null) {
      this._observer.disconnect();
      this._observer = null;
    } else if (this.$element[0].removeEventListener) {
      this.$element[0]
        .removeEventListener('DOMAttrModified', this._syncA, false);
      this.$element[0]
        .removeEventListener('DOMNodeInserted', this._syncS, false);
      this.$element[0]
        .removeEventListener('DOMNodeRemoved', this._syncS, false);
    }

    this._syncA = null;
    this._syncS = null;

    this.$element.off('.select2');
    this.$element.attr('tabindex', this.$element.data('old-tabindex'));

    this.$element.removeClass('select2-hidden-accessible');
    this.$element.attr('aria-hidden', 'false');
    this.$element.removeData('select2');

    this.dataAdapter.destroy();
    this.selection.destroy();
    this.dropdown.destroy();
    this.results.destroy();

    this.dataAdapter = null;
    this.selection = null;
    this.dropdown = null;
    this.results = null;
  };

  Select2.prototype.render = function () {
    var $container = $(
      '<span class="select2 select2-container">' +
        '<span class="selection"></span>' +
        '<span class="dropdown-wrapper" aria-hidden="true"></span>' +
      '</span>'
    );

    $container.attr('dir', this.options.get('dir'));

    this.$container = $container;

    this.$container.addClass('select2-container--' + this.options.get('theme'));

    $container.data('element', this.$element);

    return $container;
  };

  return Select2;
});

S2.define('jquery-mousewheel',[
  'jquery'
], function ($) {
  // Used to shim jQuery.mousewheel for non-full builds.
  return $;
});

S2.define('jquery.select2',[
  'jquery',
  'jquery-mousewheel',

  './select2/core',
  './select2/defaults'
], function ($, _, Select2, Defaults) {
  if ($.fn.select2 == null) {
    // All methods that should return the element
    var thisMethods = ['open', 'close', 'destroy'];

    $.fn.select2 = function (options) {
      options = options || {};

      if (typeof options === 'object') {
        this.each(function () {
          var instanceOptions = $.extend(true, {}, options);

          var instance = new Select2($(this), instanceOptions);
        });

        return this;
      } else if (typeof options === 'string') {
        var ret;
        var args = Array.prototype.slice.call(arguments, 1);

        this.each(function () {
          var instance = $(this).data('select2');

          if (instance == null && window.console && console.error) {
            console.error(
              'The select2(\'' + options + '\') method was called on an ' +
              'element that is not using Select2.'
            );
          }

          ret = instance[options].apply(instance, args);
        });

        // Check if we should be returning `this`
        if ($.inArray(options, thisMethods) > -1) {
          return this;
        }

        return ret;
      } else {
        throw new Error('Invalid arguments for Select2: ' + options);
      }
    };
  }

  if ($.fn.select2.defaults == null) {
    $.fn.select2.defaults = Defaults;
  }

  return Select2;
});

  // Return the AMD loader configuration so it can be used outside of this file
  return {
    define: S2.define,
    require: S2.require
  };
}());

  // Autoload the jQuery bindings
  // We know that all of the modules exist above this, so we're safe
  var select2 = S2.require('jquery.select2');

  // Hold the AMD module references on the jQuery function that was just loaded
  // This allows Select2 to use the internal loader outside of this file, such
  // as in the language files.
  jQuery.fn.select2.amd = S2;

  // Return the Select2 instance for anyone who is importing it.
  return select2;
}));

/*
 Highstock JS v5.0.14 (2017-07-28)

 (c) 2009-2016 Torstein Honsi

 License: www.highcharts.com/license
*/
(function(M,S){"object"===typeof module&&module.exports?module.exports=M.document?S(M):S:M.Highcharts=S(M)})("undefined"!==typeof window?window:this,function(M){M=function(){var a=window,D=a.document,B=a.navigator&&a.navigator.userAgent||"",G=D&&D.createElementNS&&!!D.createElementNS("http://www.w3.org/2000/svg","svg").createSVGRect,E=/(edge|msie|trident)/i.test(B)&&!window.opera,r=!G,g=/Firefox/.test(B),p=g&&4>parseInt(B.split("Firefox/")[1],10);return a.Highcharts?a.Highcharts.error(16,!0):{product:"Highstock",
version:"5.0.14",deg2rad:2*Math.PI/360,doc:D,hasBidiBug:p,hasTouch:D&&void 0!==D.documentElement.ontouchstart,isMS:E,isWebKit:/AppleWebKit/.test(B),isFirefox:g,isTouchDevice:/(Mobile|Android|Windows Phone)/.test(B),SVG_NS:"http://www.w3.org/2000/svg",chartCount:0,seriesTypes:{},symbolSizes:{},svg:G,vml:r,win:a,marginNames:["plotTop","marginRight","marginBottom","plotLeft"],noop:function(){},charts:[]}}();(function(a){var D=[],B=a.charts,G=a.doc,E=a.win;a.error=function(r,g){r=a.isNumber(r)?"Highcharts error #"+
r+": www.highcharts.com/errors/"+r:r;if(g)throw Error(r);E.console&&console.log(r)};a.Fx=function(a,g,p){this.options=g;this.elem=a;this.prop=p};a.Fx.prototype={dSetter:function(){var a=this.paths[0],g=this.paths[1],p=[],t=this.now,v=a.length,u;if(1===t)p=this.toD;else if(v===g.length&&1>t)for(;v--;)u=parseFloat(a[v]),p[v]=isNaN(u)?a[v]:t*parseFloat(g[v]-u)+u;else p=g;this.elem.attr("d",p,null,!0)},update:function(){var a=this.elem,g=this.prop,p=this.now,t=this.options.step;if(this[g+"Setter"])this[g+
"Setter"]();else a.attr?a.element&&a.attr(g,p,null,!0):a.style[g]=p+this.unit;t&&t.call(a,p,this)},run:function(a,g,p){var t=this,r=function(a){return r.stopped?!1:t.step(a)},u;this.startTime=+new Date;this.start=a;this.end=g;this.unit=p;this.now=this.start;this.pos=0;r.elem=this.elem;r.prop=this.prop;r()&&1===D.push(r)&&(r.timerId=setInterval(function(){for(u=0;u<D.length;u++)D[u]()||D.splice(u--,1);D.length||clearInterval(r.timerId)},13))},step:function(r){var g=+new Date,p,t=this.options,v=this.elem,
u=t.complete,l=t.duration,e=t.curAnim;v.attr&&!v.element?r=!1:r||g>=l+this.startTime?(this.now=this.end,this.pos=1,this.update(),p=e[this.prop]=!0,a.objectEach(e,function(a){!0!==a&&(p=!1)}),p&&u&&u.call(v),r=!1):(this.pos=t.easing((g-this.startTime)/l),this.now=this.start+(this.end-this.start)*this.pos,this.update(),r=!0);return r},initPath:function(r,g,p){function t(a){var b,m;for(c=a.length;c--;)b="M"===a[c]||"L"===a[c],m=/[a-zA-Z]/.test(a[c+3]),b&&m&&a.splice(c+1,0,a[c+1],a[c+2],a[c+1],a[c+2])}
function v(a,b){for(;a.length<x;){a[0]=b[x-a.length];var m=a.slice(0,d);[].splice.apply(a,[0,0].concat(m));q&&(m=a.slice(a.length-d),[].splice.apply(a,[a.length,0].concat(m)),c--)}a[0]="M"}function u(a,b){for(var m=(x-a.length)/d;0<m&&m--;)C=a.slice().splice(a.length/I-d,d*I),C[0]=b[x-d-m*d],f&&(C[d-6]=C[d-2],C[d-5]=C[d-1]),[].splice.apply(a,[a.length/I,0].concat(C)),q&&m--}g=g||"";var l,e=r.startX,k=r.endX,f=-1<g.indexOf("C"),d=f?7:3,x,C,c;g=g.split(" ");p=p.slice();var q=r.isArea,I=q?2:1,m;f&&(t(g),
t(p));if(e&&k){for(c=0;c<e.length;c++)if(e[c]===k[0]){l=c;break}else if(e[0]===k[k.length-e.length+c]){l=c;m=!0;break}void 0===l&&(g=[])}g.length&&a.isNumber(l)&&(x=p.length+l*I*d,m?(v(g,p),u(p,g)):(v(p,g),u(g,p)));return[g,p]}};a.Fx.prototype.fillSetter=a.Fx.prototype.strokeSetter=function(){this.elem.attr(this.prop,a.color(this.start).tweenTo(a.color(this.end),this.pos),null,!0)};a.extend=function(a,g){var r;a||(a={});for(r in g)a[r]=g[r];return a};a.merge=function(){var r,g=arguments,p,t={},v=
function(g,l){"object"!==typeof g&&(g={});a.objectEach(l,function(e,k){!a.isObject(e,!0)||a.isClass(e)||a.isDOMElement(e)?g[k]=l[k]:g[k]=v(g[k]||{},e)});return g};!0===g[0]&&(t=g[1],g=Array.prototype.slice.call(g,2));p=g.length;for(r=0;r<p;r++)t=v(t,g[r]);return t};a.pInt=function(a,g){return parseInt(a,g||10)};a.isString=function(a){return"string"===typeof a};a.isArray=function(a){a=Object.prototype.toString.call(a);return"[object Array]"===a||"[object Array Iterator]"===a};a.isObject=function(r,
g){return!!r&&"object"===typeof r&&(!g||!a.isArray(r))};a.isDOMElement=function(r){return a.isObject(r)&&"number"===typeof r.nodeType};a.isClass=function(r){var g=r&&r.constructor;return!(!a.isObject(r,!0)||a.isDOMElement(r)||!g||!g.name||"Object"===g.name)};a.isNumber=function(a){return"number"===typeof a&&!isNaN(a)};a.erase=function(a,g){for(var r=a.length;r--;)if(a[r]===g){a.splice(r,1);break}};a.defined=function(a){return void 0!==a&&null!==a};a.attr=function(r,g,p){var t;a.isString(g)?a.defined(p)?
r.setAttribute(g,p):r&&r.getAttribute&&(t=r.getAttribute(g)):a.defined(g)&&a.isObject(g)&&a.objectEach(g,function(a,g){r.setAttribute(g,a)});return t};a.splat=function(r){return a.isArray(r)?r:[r]};a.syncTimeout=function(a,g,p){if(g)return setTimeout(a,g,p);a.call(0,p)};a.pick=function(){var a=arguments,g,p,t=a.length;for(g=0;g<t;g++)if(p=a[g],void 0!==p&&null!==p)return p};a.css=function(r,g){a.isMS&&!a.svg&&g&&void 0!==g.opacity&&(g.filter="alpha(opacity\x3d"+100*g.opacity+")");a.extend(r.style,
g)};a.createElement=function(r,g,p,t,v){r=G.createElement(r);var u=a.css;g&&a.extend(r,g);v&&u(r,{padding:0,border:"none",margin:0});p&&u(r,p);t&&t.appendChild(r);return r};a.extendClass=function(r,g){var p=function(){};p.prototype=new r;a.extend(p.prototype,g);return p};a.pad=function(a,g,p){return Array((g||2)+1-String(a).length).join(p||0)+a};a.relativeLength=function(a,g,p){return/%$/.test(a)?g*parseFloat(a)/100+(p||0):parseFloat(a)};a.wrap=function(a,g,p){var t=a[g];a[g]=function(){var a=Array.prototype.slice.call(arguments),
g=arguments,l=this;l.proceed=function(){t.apply(l,arguments.length?arguments:g)};a.unshift(t);a=p.apply(this,a);l.proceed=null;return a}};a.getTZOffset=function(r){var g=a.Date;return 6E4*(g.hcGetTimezoneOffset&&g.hcGetTimezoneOffset(r)||g.hcTimezoneOffset||0)};a.dateFormat=function(r,g,p){if(!a.defined(g)||isNaN(g))return a.defaultOptions.lang.invalidDate||"";r=a.pick(r,"%Y-%m-%d %H:%M:%S");var t=a.Date,v=new t(g-a.getTZOffset(g)),u=v[t.hcGetHours](),l=v[t.hcGetDay](),e=v[t.hcGetDate](),k=v[t.hcGetMonth](),
f=v[t.hcGetFullYear](),d=a.defaultOptions.lang,x=d.weekdays,C=d.shortWeekdays,c=a.pad,t=a.extend({a:C?C[l]:x[l].substr(0,3),A:x[l],d:c(e),e:c(e,2," "),w:l,b:d.shortMonths[k],B:d.months[k],m:c(k+1),y:f.toString().substr(2,2),Y:f,H:c(u),k:u,I:c(u%12||12),l:u%12||12,M:c(v[t.hcGetMinutes]()),p:12>u?"AM":"PM",P:12>u?"am":"pm",S:c(v.getSeconds()),L:c(Math.round(g%1E3),3)},a.dateFormats);a.objectEach(t,function(a,f){for(;-1!==r.indexOf("%"+f);)r=r.replace("%"+f,"function"===typeof a?a(g):a)});return p?r.substr(0,
1).toUpperCase()+r.substr(1):r};a.formatSingle=function(r,g){var p=/\.([0-9])/,t=a.defaultOptions.lang;/f$/.test(r)?(p=(p=r.match(p))?p[1]:-1,null!==g&&(g=a.numberFormat(g,p,t.decimalPoint,-1<r.indexOf(",")?t.thousandsSep:""))):g=a.dateFormat(r,g);return g};a.format=function(r,g){for(var p="{",t=!1,v,u,l,e,k=[],f;r;){p=r.indexOf(p);if(-1===p)break;v=r.slice(0,p);if(t){v=v.split(":");u=v.shift().split(".");e=u.length;f=g;for(l=0;l<e;l++)f=f[u[l]];v.length&&(f=a.formatSingle(v.join(":"),f));k.push(f)}else k.push(v);
r=r.slice(p+1);p=(t=!t)?"}":"{"}k.push(r);return k.join("")};a.getMagnitude=function(a){return Math.pow(10,Math.floor(Math.log(a)/Math.LN10))};a.normalizeTickInterval=function(r,g,p,t,v){var u,l=r;p=a.pick(p,1);u=r/p;g||(g=v?[1,1.2,1.5,2,2.5,3,4,5,6,8,10]:[1,2,2.5,5,10],!1===t&&(1===p?g=a.grep(g,function(a){return 0===a%1}):.1>=p&&(g=[1/p])));for(t=0;t<g.length&&!(l=g[t],v&&l*p>=r||!v&&u<=(g[t]+(g[t+1]||g[t]))/2);t++);return l=a.correctFloat(l*p,-Math.round(Math.log(.001)/Math.LN10))};a.stableSort=
function(a,g){var p=a.length,t,v;for(v=0;v<p;v++)a[v].safeI=v;a.sort(function(a,l){t=g(a,l);return 0===t?a.safeI-l.safeI:t});for(v=0;v<p;v++)delete a[v].safeI};a.arrayMin=function(a){for(var g=a.length,p=a[0];g--;)a[g]<p&&(p=a[g]);return p};a.arrayMax=function(a){for(var g=a.length,p=a[0];g--;)a[g]>p&&(p=a[g]);return p};a.destroyObjectProperties=function(r,g){a.objectEach(r,function(a,t){a&&a!==g&&a.destroy&&a.destroy();delete r[t]})};a.discardElement=function(r){var g=a.garbageBin;g||(g=a.createElement("div"));
r&&g.appendChild(r);g.innerHTML=""};a.correctFloat=function(a,g){return parseFloat(a.toPrecision(g||14))};a.setAnimation=function(r,g){g.renderer.globalAnimation=a.pick(r,g.options.chart.animation,!0)};a.animObject=function(r){return a.isObject(r)?a.merge(r):{duration:r?500:0}};a.timeUnits={millisecond:1,second:1E3,minute:6E4,hour:36E5,day:864E5,week:6048E5,month:24192E5,year:314496E5};a.numberFormat=function(r,g,p,t){r=+r||0;g=+g;var v=a.defaultOptions.lang,u=(r.toString().split(".")[1]||"").split("e")[0].length,
l,e,k=r.toString().split("e");-1===g?g=Math.min(u,20):a.isNumber(g)||(g=2);e=(Math.abs(k[1]?k[0]:r)+Math.pow(10,-Math.max(g,u)-1)).toFixed(g);u=String(a.pInt(e));l=3<u.length?u.length%3:0;p=a.pick(p,v.decimalPoint);t=a.pick(t,v.thousandsSep);r=(0>r?"-":"")+(l?u.substr(0,l)+t:"");r+=u.substr(l).replace(/(\d{3})(?=\d)/g,"$1"+t);g&&(r+=p+e.slice(-g));k[1]&&(r+="e"+k[1]);return r};Math.easeInOutSine=function(a){return-.5*(Math.cos(Math.PI*a)-1)};a.getStyle=function(r,g,p){if("width"===g)return Math.min(r.offsetWidth,
r.scrollWidth)-a.getStyle(r,"padding-left")-a.getStyle(r,"padding-right");if("height"===g)return Math.min(r.offsetHeight,r.scrollHeight)-a.getStyle(r,"padding-top")-a.getStyle(r,"padding-bottom");if(r=E.getComputedStyle(r,void 0))r=r.getPropertyValue(g),a.pick(p,!0)&&(r=a.pInt(r));return r};a.inArray=function(a,g){return g.indexOf?g.indexOf(a):[].indexOf.call(g,a)};a.grep=function(a,g){return[].filter.call(a,g)};a.find=function(a,g){return[].find.call(a,g)};a.map=function(a,g){for(var p=[],t=0,v=
a.length;t<v;t++)p[t]=g.call(a[t],a[t],t,a);return p};a.offset=function(a){var g=G.documentElement;a=a.getBoundingClientRect();return{top:a.top+(E.pageYOffset||g.scrollTop)-(g.clientTop||0),left:a.left+(E.pageXOffset||g.scrollLeft)-(g.clientLeft||0)}};a.stop=function(a,g){for(var p=D.length;p--;)D[p].elem!==a||g&&g!==D[p].prop||(D[p].stopped=!0)};a.each=function(a,g,p){return Array.prototype.forEach.call(a,g,p)};a.objectEach=function(a,g,p){for(var t in a)a.hasOwnProperty(t)&&g.call(p,a[t],t,a)};
a.addEvent=function(r,g,p){function t(a){a.target=a.srcElement||E;p.call(r,a)}var v=r.hcEvents=r.hcEvents||{};r.addEventListener?r.addEventListener(g,p,!1):r.attachEvent&&(r.hcEventsIE||(r.hcEventsIE={}),p.hcGetKey||(p.hcGetKey=a.uniqueKey()),r.hcEventsIE[p.hcGetKey]=t,r.attachEvent("on"+g,t));v[g]||(v[g]=[]);v[g].push(p);return function(){a.removeEvent(r,g,p)}};a.removeEvent=function(r,g,p){function t(a,f){r.removeEventListener?r.removeEventListener(a,f,!1):r.attachEvent&&(f=r.hcEventsIE[f.hcGetKey],
r.detachEvent("on"+a,f))}function v(){var e,f;r.nodeName&&(g?(e={},e[g]=!0):e=l,a.objectEach(e,function(a,e){if(l[e])for(f=l[e].length;f--;)t(e,l[e][f])}))}var u,l=r.hcEvents,e;l&&(g?(u=l[g]||[],p?(e=a.inArray(p,u),-1<e&&(u.splice(e,1),l[g]=u),t(g,p)):(v(),l[g]=[])):(v(),r.hcEvents={}))};a.fireEvent=function(r,g,p,t){var v;v=r.hcEvents;var u,l;p=p||{};if(G.createEvent&&(r.dispatchEvent||r.fireEvent))v=G.createEvent("Events"),v.initEvent(g,!0,!0),a.extend(v,p),r.dispatchEvent?r.dispatchEvent(v):r.fireEvent(g,
v);else if(v)for(v=v[g]||[],u=v.length,p.target||a.extend(p,{preventDefault:function(){p.defaultPrevented=!0},target:r,type:g}),g=0;g<u;g++)(l=v[g])&&!1===l.call(r,p)&&p.preventDefault();t&&!p.defaultPrevented&&t(p)};a.animate=function(r,g,p){var t,v="",u,l,e;a.isObject(p)||(e=arguments,p={duration:e[2],easing:e[3],complete:e[4]});a.isNumber(p.duration)||(p.duration=400);p.easing="function"===typeof p.easing?p.easing:Math[p.easing]||Math.easeInOutSine;p.curAnim=a.merge(g);a.objectEach(g,function(e,
f){a.stop(r,f);l=new a.Fx(r,p,f);u=null;"d"===f?(l.paths=l.initPath(r,r.d,g.d),l.toD=g.d,t=0,u=1):r.attr?t=r.attr(f):(t=parseFloat(a.getStyle(r,f))||0,"opacity"!==f&&(v="px"));u||(u=e);u&&u.match&&u.match("px")&&(u=u.replace(/px/g,""));l.run(t,u,v)})};a.seriesType=function(r,g,p,t,v){var u=a.getOptions(),l=a.seriesTypes;u.plotOptions[r]=a.merge(u.plotOptions[g],p);l[r]=a.extendClass(l[g]||function(){},t);l[r].prototype.type=r;v&&(l[r].prototype.pointClass=a.extendClass(a.Point,v));return l[r]};a.uniqueKey=
function(){var a=Math.random().toString(36).substring(2,9),g=0;return function(){return"highcharts-"+a+"-"+g++}}();E.jQuery&&(E.jQuery.fn.highcharts=function(){var r=[].slice.call(arguments);if(this[0])return r[0]?(new (a[a.isString(r[0])?r.shift():"Chart"])(this[0],r[0],r[1]),this):B[a.attr(this[0],"data-highcharts-chart")]});G&&!G.defaultView&&(a.getStyle=function(r,g){var p={width:"clientWidth",height:"clientHeight"}[g];if(r.style[g])return a.pInt(r.style[g]);"opacity"===g&&(g="filter");if(p)return r.style.zoom=
1,Math.max(r[p]-2*a.getStyle(r,"padding"),0);r=r.currentStyle[g.replace(/\-(\w)/g,function(a,g){return g.toUpperCase()})];"filter"===g&&(r=r.replace(/alpha\(opacity=([0-9]+)\)/,function(a,g){return g/100}));return""===r?1:a.pInt(r)});Array.prototype.forEach||(a.each=function(a,g,p){for(var t=0,v=a.length;t<v;t++)if(!1===g.call(p,a[t],t,a))return t});Array.prototype.indexOf||(a.inArray=function(a,g){var p,t=0;if(g)for(p=g.length;t<p;t++)if(g[t]===a)return t;return-1});Array.prototype.filter||(a.grep=
function(a,g){for(var p=[],t=0,v=a.length;t<v;t++)g(a[t],t)&&p.push(a[t]);return p});Array.prototype.find||(a.find=function(a,g){var p,t=a.length;for(p=0;p<t;p++)if(g(a[p],p))return a[p]})})(M);(function(a){var D=a.each,B=a.isNumber,G=a.map,E=a.merge,r=a.pInt;a.Color=function(g){if(!(this instanceof a.Color))return new a.Color(g);this.init(g)};a.Color.prototype={parsers:[{regex:/rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]?(?:\.[0-9]+)?)\s*\)/,parse:function(a){return[r(a[1]),
r(a[2]),r(a[3]),parseFloat(a[4],10)]}},{regex:/rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/,parse:function(a){return[r(a[1]),r(a[2]),r(a[3]),1]}}],names:{none:"rgba(255,255,255,0)",white:"#ffffff",black:"#000000"},init:function(g){var p,t,v,u;if((this.input=g=this.names[g&&g.toLowerCase?g.toLowerCase():""]||g)&&g.stops)this.stops=G(g.stops,function(l){return new a.Color(l[1])});else if(g&&"#"===g.charAt()&&(p=g.length,g=parseInt(g.substr(1),16),7===p?t=[(g&16711680)>>16,(g&65280)>>
8,g&255,1]:4===p&&(t=[(g&3840)>>4|(g&3840)>>8,(g&240)>>4|g&240,(g&15)<<4|g&15,1])),!t)for(v=this.parsers.length;v--&&!t;)u=this.parsers[v],(p=u.regex.exec(g))&&(t=u.parse(p));this.rgba=t||[]},get:function(a){var g=this.input,t=this.rgba,v;this.stops?(v=E(g),v.stops=[].concat(v.stops),D(this.stops,function(g,l){v.stops[l]=[v.stops[l][0],g.get(a)]})):v=t&&B(t[0])?"rgb"===a||!a&&1===t[3]?"rgb("+t[0]+","+t[1]+","+t[2]+")":"a"===a?t[3]:"rgba("+t.join(",")+")":g;return v},brighten:function(a){var g,t=this.rgba;
if(this.stops)D(this.stops,function(g){g.brighten(a)});else if(B(a)&&0!==a)for(g=0;3>g;g++)t[g]+=r(255*a),0>t[g]&&(t[g]=0),255<t[g]&&(t[g]=255);return this},setOpacity:function(a){this.rgba[3]=a;return this},tweenTo:function(a,p){var g,v;a.rgba.length?(g=this.rgba,a=a.rgba,v=1!==a[3]||1!==g[3],a=(v?"rgba(":"rgb(")+Math.round(a[0]+(g[0]-a[0])*(1-p))+","+Math.round(a[1]+(g[1]-a[1])*(1-p))+","+Math.round(a[2]+(g[2]-a[2])*(1-p))+(v?","+(a[3]+(g[3]-a[3])*(1-p)):"")+")"):a=a.input||"none";return a}};a.color=
function(g){return new a.Color(g)}})(M);(function(a){var D,B,G=a.addEvent,E=a.animate,r=a.attr,g=a.charts,p=a.color,t=a.css,v=a.createElement,u=a.defined,l=a.deg2rad,e=a.destroyObjectProperties,k=a.doc,f=a.each,d=a.extend,x=a.erase,C=a.grep,c=a.hasTouch,q=a.inArray,I=a.isArray,m=a.isFirefox,J=a.isMS,b=a.isObject,z=a.isString,K=a.isWebKit,y=a.merge,A=a.noop,n=a.objectEach,H=a.pick,h=a.pInt,w=a.removeEvent,P=a.stop,L=a.svg,Q=a.SVG_NS,N=a.symbolSizes,O=a.win;D=a.SVGElement=function(){return this};d(D.prototype,
{opacity:1,SVG_NS:Q,textProps:"direction fontSize fontWeight fontFamily fontStyle color lineHeight width textAlign textDecoration textOverflow textOutline".split(" "),init:function(a,h){this.element="span"===h?v(h):k.createElementNS(this.SVG_NS,h);this.renderer=a},animate:function(F,h,b){h=a.animObject(H(h,this.renderer.globalAnimation,!0));0!==h.duration?(b&&(h.complete=b),E(this,F,h)):(this.attr(F,null,b),h.step&&h.step.call(this));return this},colorGradient:function(F,h,b){var w=this.renderer,
m,d,c,e,k,L,R,z,q,A,H=[],x;F.radialGradient?d="radialGradient":F.linearGradient&&(d="linearGradient");d&&(c=F[d],k=w.gradients,R=F.stops,A=b.radialReference,I(c)&&(F[d]=c={x1:c[0],y1:c[1],x2:c[2],y2:c[3],gradientUnits:"userSpaceOnUse"}),"radialGradient"===d&&A&&!u(c.gradientUnits)&&(e=c,c=y(c,w.getRadialAttr(A,e),{gradientUnits:"userSpaceOnUse"})),n(c,function(a,F){"id"!==F&&H.push(F,a)}),n(R,function(a){H.push(a)}),H=H.join(","),k[H]?A=k[H].attr("id"):(c.id=A=a.uniqueKey(),k[H]=L=w.createElement(d).attr(c).add(w.defs),
L.radAttr=e,L.stops=[],f(R,function(F){0===F[1].indexOf("rgba")?(m=a.color(F[1]),z=m.get("rgb"),q=m.get("a")):(z=F[1],q=1);F=w.createElement("stop").attr({offset:F[0],"stop-color":z,"stop-opacity":q}).add(L);L.stops.push(F)})),x="url("+w.url+"#"+A+")",b.setAttribute(h,x),b.gradient=H,F.toString=function(){return x})},applyTextOutline:function(F){var h=this.element,b,n,w,m,d;-1!==F.indexOf("contrast")&&(F=F.replace(/contrast/g,this.renderer.getContrast(h.style.fill)));F=F.split(" ");n=F[F.length-1];
if((w=F[0])&&"none"!==w&&a.svg){this.fakeTS=!0;F=[].slice.call(h.getElementsByTagName("tspan"));this.ySetter=this.xSetter;w=w.replace(/(^[\d\.]+)(.*?)$/g,function(a,F,h){return 2*F+h});for(d=F.length;d--;)b=F[d],"highcharts-text-outline"===b.getAttribute("class")&&x(F,h.removeChild(b));m=h.firstChild;f(F,function(a,F){0===F&&(a.setAttribute("x",h.getAttribute("x")),F=h.getAttribute("y"),a.setAttribute("y",F||0),null===F&&h.setAttribute("y",0));a=a.cloneNode(1);r(a,{"class":"highcharts-text-outline",
fill:n,stroke:n,"stroke-width":w,"stroke-linejoin":"round"});h.insertBefore(a,m)})}},attr:function(a,h,b,w){var F,m=this.element,d,f=this,c,y;"string"===typeof a&&void 0!==h&&(F=a,a={},a[F]=h);"string"===typeof a?f=(this[a+"Getter"]||this._defaultGetter).call(this,a,m):(n(a,function(F,h){c=!1;w||P(this,h);this.symbolName&&/^(x|y|width|height|r|start|end|innerR|anchorX|anchorY)$/.test(h)&&(d||(this.symbolAttr(a),d=!0),c=!0);!this.rotation||"x"!==h&&"y"!==h||(this.doTransform=!0);c||(y=this[h+"Setter"]||
this._defaultSetter,y.call(this,F,h,m),this.shadows&&/^(width|height|visibility|x|y|d|transform|cx|cy|r)$/.test(h)&&this.updateShadows(h,F,y))},this),this.afterSetters());b&&b();return f},afterSetters:function(){this.doTransform&&(this.updateTransform(),this.doTransform=!1)},updateShadows:function(a,h,b){for(var F=this.shadows,w=F.length;w--;)b.call(F[w],"height"===a?Math.max(h-(F[w].cutHeight||0),0):"d"===a?this.d:h,a,F[w])},addClass:function(a,h){var F=this.attr("class")||"";-1===F.indexOf(a)&&
(h||(a=(F+(F?" ":"")+a).replace("  "," ")),this.attr("class",a));return this},hasClass:function(a){return-1!==q(a,(this.attr("class")||"").split(" "))},removeClass:function(a){return this.attr("class",(this.attr("class")||"").replace(a,""))},symbolAttr:function(a){var h=this;f("x y r start end width height innerR anchorX anchorY".split(" "),function(F){h[F]=H(a[F],h[F])});h.attr({d:h.renderer.symbols[h.symbolName](h.x,h.y,h.width,h.height,h)})},clip:function(a){return this.attr("clip-path",a?"url("+
this.renderer.url+"#"+a.id+")":"none")},crisp:function(a,h){var F=this,b={},w;h=h||a.strokeWidth||0;w=Math.round(h)%2/2;a.x=Math.floor(a.x||F.x||0)+w;a.y=Math.floor(a.y||F.y||0)+w;a.width=Math.floor((a.width||F.width||0)-2*w);a.height=Math.floor((a.height||F.height||0)-2*w);u(a.strokeWidth)&&(a.strokeWidth=h);n(a,function(a,h){F[h]!==a&&(F[h]=b[h]=a)});return b},css:function(a){var F=this.styles,b={},w=this.element,m,c="",f,y=!F,e=["textOutline","textOverflow","width"];a&&a.color&&(a.fill=a.color);
F&&n(a,function(a,h){a!==F[h]&&(b[h]=a,y=!0)});y&&(F&&(a=d(F,b)),m=this.textWidth=a&&a.width&&"auto"!==a.width&&"text"===w.nodeName.toLowerCase()&&h(a.width),this.styles=a,m&&!L&&this.renderer.forExport&&delete a.width,J&&!L?t(this.element,a):(f=function(a,h){return"-"+h.toLowerCase()},n(a,function(a,h){-1===q(h,e)&&(c+=h.replace(/([A-Z])/g,f)+":"+a+";")}),c&&r(w,"style",c)),this.added&&("text"===this.element.nodeName&&this.renderer.buildText(this),a&&a.textOutline&&this.applyTextOutline(a.textOutline)));
return this},strokeWidth:function(){return this["stroke-width"]||0},on:function(a,h){var F=this,b=F.element;c&&"click"===a?(b.ontouchstart=function(a){F.touchEventFired=Date.now();a.preventDefault();h.call(b,a)},b.onclick=function(a){(-1===O.navigator.userAgent.indexOf("Android")||1100<Date.now()-(F.touchEventFired||0))&&h.call(b,a)}):b["on"+a]=h;return this},setRadialReference:function(a){var h=this.renderer.gradients[this.element.gradient];this.element.radialReference=a;h&&h.radAttr&&h.animate(this.renderer.getRadialAttr(a,
h.radAttr));return this},translate:function(a,h){return this.attr({translateX:a,translateY:h})},invert:function(a){this.inverted=a;this.updateTransform();return this},updateTransform:function(){var a=this.translateX||0,h=this.translateY||0,b=this.scaleX,w=this.scaleY,n=this.inverted,m=this.rotation,d=this.element;n&&(a+=this.width,h+=this.height);a=["translate("+a+","+h+")"];n?a.push("rotate(90) scale(-1,1)"):m&&a.push("rotate("+m+" "+(d.getAttribute("x")||0)+" "+(d.getAttribute("y")||0)+")");(u(b)||
u(w))&&a.push("scale("+H(b,1)+" "+H(w,1)+")");a.length&&d.setAttribute("transform",a.join(" "))},toFront:function(){var a=this.element;a.parentNode.appendChild(a);return this},align:function(a,h,b){var F,w,n,m,d={};w=this.renderer;n=w.alignedObjects;var c,f;if(a){if(this.alignOptions=a,this.alignByTranslate=h,!b||z(b))this.alignTo=F=b||"renderer",x(n,this),n.push(this),b=null}else a=this.alignOptions,h=this.alignByTranslate,F=this.alignTo;b=H(b,w[F],w);F=a.align;w=a.verticalAlign;n=(b.x||0)+(a.x||
0);m=(b.y||0)+(a.y||0);"right"===F?c=1:"center"===F&&(c=2);c&&(n+=(b.width-(a.width||0))/c);d[h?"translateX":"x"]=Math.round(n);"bottom"===w?f=1:"middle"===w&&(f=2);f&&(m+=(b.height-(a.height||0))/f);d[h?"translateY":"y"]=Math.round(m);this[this.placed?"animate":"attr"](d);this.placed=!0;this.alignAttr=d;return this},getBBox:function(a,h){var F,b=this.renderer,w,n=this.element,m=this.styles,c,y=this.textStr,e,k=b.cache,L=b.cacheKeys,z;h=H(h,this.rotation);w=h*l;c=m&&m.fontSize;void 0!==y&&(z=y.toString(),
-1===z.indexOf("\x3c")&&(z=z.replace(/[0-9]/g,"0")),z+=["",h||0,c,m&&m.width,m&&m.textOverflow].join());z&&!a&&(F=k[z]);if(!F){if(n.namespaceURI===this.SVG_NS||b.forExport){try{(e=this.fakeTS&&function(a){f(n.querySelectorAll(".highcharts-text-outline"),function(h){h.style.display=a})})&&e("none"),F=n.getBBox?d({},n.getBBox()):{width:n.offsetWidth,height:n.offsetHeight},e&&e("")}catch(V){}if(!F||0>F.width)F={width:0,height:0}}else F=this.htmlGetBBox();b.isSVG&&(a=F.width,b=F.height,m&&"11px"===m.fontSize&&
17===Math.round(b)&&(F.height=b=14),h&&(F.width=Math.abs(b*Math.sin(w))+Math.abs(a*Math.cos(w)),F.height=Math.abs(b*Math.cos(w))+Math.abs(a*Math.sin(w))));if(z&&0<F.height){for(;250<L.length;)delete k[L.shift()];k[z]||L.push(z);k[z]=F}}return F},show:function(a){return this.attr({visibility:a?"inherit":"visible"})},hide:function(){return this.attr({visibility:"hidden"})},fadeOut:function(a){var h=this;h.animate({opacity:0},{duration:a||150,complete:function(){h.attr({y:-9999})}})},add:function(a){var h=
this.renderer,F=this.element,b;a&&(this.parentGroup=a);this.parentInverted=a&&a.inverted;void 0!==this.textStr&&h.buildText(this);this.added=!0;if(!a||a.handleZ||this.zIndex)b=this.zIndexSetter();b||(a?a.element:h.box).appendChild(F);if(this.onAdd)this.onAdd();return this},safeRemoveChild:function(a){var h=a.parentNode;h&&h.removeChild(a)},destroy:function(){var a=this,h=a.element||{},b=a.renderer.isSVG&&"SPAN"===h.nodeName&&a.parentGroup,w=h.ownerSVGElement;h.onclick=h.onmouseout=h.onmouseover=h.onmousemove=
h.point=null;P(a);a.clipPath&&w&&(f(w.querySelectorAll("[clip-path]"),function(h){-1<h.getAttribute("clip-path").indexOf(a.clipPath.element.id+")")&&h.removeAttribute("clip-path")}),a.clipPath=a.clipPath.destroy());if(a.stops){for(w=0;w<a.stops.length;w++)a.stops[w]=a.stops[w].destroy();a.stops=null}a.safeRemoveChild(h);for(a.destroyShadows();b&&b.div&&0===b.div.childNodes.length;)h=b.parentGroup,a.safeRemoveChild(b.div),delete b.div,b=h;a.alignTo&&x(a.renderer.alignedObjects,a);n(a,function(h,b){delete a[b]});
return null},shadow:function(a,h,b){var F=[],w,n,m=this.element,d,c,f,y;if(!a)this.destroyShadows();else if(!this.shadows){c=H(a.width,3);f=(a.opacity||.15)/c;y=this.parentInverted?"(-1,-1)":"("+H(a.offsetX,1)+", "+H(a.offsetY,1)+")";for(w=1;w<=c;w++)n=m.cloneNode(0),d=2*c+1-2*w,r(n,{isShadow:"true",stroke:a.color||"#000000","stroke-opacity":f*w,"stroke-width":d,transform:"translate"+y,fill:"none"}),b&&(r(n,"height",Math.max(r(n,"height")-d,0)),n.cutHeight=d),h?h.element.appendChild(n):m.parentNode.insertBefore(n,
m),F.push(n);this.shadows=F}return this},destroyShadows:function(){f(this.shadows||[],function(a){this.safeRemoveChild(a)},this);this.shadows=void 0},xGetter:function(a){"circle"===this.element.nodeName&&("x"===a?a="cx":"y"===a&&(a="cy"));return this._defaultGetter(a)},_defaultGetter:function(a){a=H(this[a],this.element?this.element.getAttribute(a):null,0);/^[\-0-9\.]+$/.test(a)&&(a=parseFloat(a));return a},dSetter:function(a,h,b){a&&a.join&&(a=a.join(" "));/(NaN| {2}|^$)/.test(a)&&(a="M 0 0");this[h]!==
a&&(b.setAttribute(h,a),this[h]=a)},dashstyleSetter:function(a){var b,w=this["stroke-width"];"inherit"===w&&(w=1);if(a=a&&a.toLowerCase()){a=a.replace("shortdashdotdot","3,1,1,1,1,1,").replace("shortdashdot","3,1,1,1").replace("shortdot","1,1,").replace("shortdash","3,1,").replace("longdash","8,3,").replace(/dot/g,"1,3,").replace("dash","4,3,").replace(/,$/,"").split(",");for(b=a.length;b--;)a[b]=h(a[b])*w;a=a.join(",").replace(/NaN/g,"none");this.element.setAttribute("stroke-dasharray",a)}},alignSetter:function(a){this.element.setAttribute("text-anchor",
{left:"start",center:"middle",right:"end"}[a])},opacitySetter:function(a,h,b){this[h]=a;b.setAttribute(h,a)},titleSetter:function(a){var h=this.element.getElementsByTagName("title")[0];h||(h=k.createElementNS(this.SVG_NS,"title"),this.element.appendChild(h));h.firstChild&&h.removeChild(h.firstChild);h.appendChild(k.createTextNode(String(H(a),"").replace(/<[^>]*>/g,"")))},textSetter:function(a){a!==this.textStr&&(delete this.bBox,this.textStr=a,this.added&&this.renderer.buildText(this))},fillSetter:function(a,
h,b){"string"===typeof a?b.setAttribute(h,a):a&&this.colorGradient(a,h,b)},visibilitySetter:function(a,h,b){"inherit"===a?b.removeAttribute(h):this[h]!==a&&b.setAttribute(h,a);this[h]=a},zIndexSetter:function(a,b){var w=this.renderer,n=this.parentGroup,m=(n||w).element||w.box,F,d=this.element,c;F=this.added;var f;u(a)&&(d.zIndex=a,a=+a,this[b]===a&&(F=!1),this[b]=a);if(F){(a=this.zIndex)&&n&&(n.handleZ=!0);b=m.childNodes;for(f=0;f<b.length&&!c;f++)n=b[f],F=n.zIndex,n!==d&&(h(F)>a||!u(a)&&u(F)||0>
a&&!u(F)&&m!==w.box)&&(m.insertBefore(d,n),c=!0);c||m.appendChild(d)}return c},_defaultSetter:function(a,h,b){b.setAttribute(h,a)}});D.prototype.yGetter=D.prototype.xGetter;D.prototype.translateXSetter=D.prototype.translateYSetter=D.prototype.rotationSetter=D.prototype.verticalAlignSetter=D.prototype.scaleXSetter=D.prototype.scaleYSetter=function(a,h){this[h]=a;this.doTransform=!0};D.prototype["stroke-widthSetter"]=D.prototype.strokeSetter=function(a,h,b){this[h]=a;this.stroke&&this["stroke-width"]?
(D.prototype.fillSetter.call(this,this.stroke,"stroke",b),b.setAttribute("stroke-width",this["stroke-width"]),this.hasStroke=!0):"stroke-width"===h&&0===a&&this.hasStroke&&(b.removeAttribute("stroke"),this.hasStroke=!1)};B=a.SVGRenderer=function(){this.init.apply(this,arguments)};d(B.prototype,{Element:D,SVG_NS:Q,init:function(a,h,b,w,n,d){var F;w=this.createElement("svg").attr({version:"1.1","class":"highcharts-root"}).css(this.getStyle(w));F=w.element;a.appendChild(F);-1===a.innerHTML.indexOf("xmlns")&&
r(F,"xmlns",this.SVG_NS);this.isSVG=!0;this.box=F;this.boxWrapper=w;this.alignedObjects=[];this.url=(m||K)&&k.getElementsByTagName("base").length?O.location.href.replace(/#.*?$/,"").replace(/<[^>]*>/g,"").replace(/([\('\)])/g,"\\$1").replace(/ /g,"%20"):"";this.createElement("desc").add().element.appendChild(k.createTextNode("Created with Highstock 5.0.14"));this.defs=this.createElement("defs").add();this.allowHTML=d;this.forExport=n;this.gradients={};this.cache={};this.cacheKeys=[];this.imgCount=
0;this.setSize(h,b,!1);var c;m&&a.getBoundingClientRect&&(h=function(){t(a,{left:0,top:0});c=a.getBoundingClientRect();t(a,{left:Math.ceil(c.left)-c.left+"px",top:Math.ceil(c.top)-c.top+"px"})},h(),this.unSubPixelFix=G(O,"resize",h))},getStyle:function(a){return this.style=d({fontFamily:'"Lucida Grande", "Lucida Sans Unicode", Arial, Helvetica, sans-serif',fontSize:"12px"},a)},setStyle:function(a){this.boxWrapper.css(this.getStyle(a))},isHidden:function(){return!this.boxWrapper.getBBox().width},destroy:function(){var a=
this.defs;this.box=null;this.boxWrapper=this.boxWrapper.destroy();e(this.gradients||{});this.gradients=null;a&&(this.defs=a.destroy());this.unSubPixelFix&&this.unSubPixelFix();return this.alignedObjects=null},createElement:function(a){var h=new this.Element;h.init(this,a);return h},draw:A,getRadialAttr:function(a,h){return{cx:a[0]-a[2]/2+h.cx*a[2],cy:a[1]-a[2]/2+h.cy*a[2],r:h.r*a[2]}},getSpanWidth:function(a,h){var b=a.getBBox(!0).width;!L&&this.forExport&&(b=this.measureSpanWidth(h.firstChild.data,
a.styles));return b},applyEllipsis:function(a,h,b,w){var n=a.rotation,m=b,c,d=0,F=b.length,f=function(a){h.removeChild(h.firstChild);a&&h.appendChild(k.createTextNode(a))},y;a.rotation=0;m=this.getSpanWidth(a,h);if(y=m>w){for(;d<=F;)c=Math.ceil((d+F)/2),m=b.substring(0,c)+"\u2026",f(m),m=this.getSpanWidth(a,h),d===F?d=F+1:m>w?F=c-1:d=c;0===F&&f("")}a.rotation=n;return y},buildText:function(a){var b=a.element,w=this,n=w.forExport,m=H(a.textStr,"").toString(),c=-1!==m.indexOf("\x3c"),d=b.childNodes,
F,y,e,z,q=r(b,"x"),A=a.styles,x=a.textWidth,J=A&&A.lineHeight,l=A&&A.textOutline,P=A&&"ellipsis"===A.textOverflow,g=A&&"nowrap"===A.whiteSpace,K=A&&A.fontSize,I,v,p=d.length,A=x&&!a.added&&this.box,u=function(a){var n;n=/(px|em)$/.test(a&&a.style.fontSize)?a.style.fontSize:K||w.style.fontSize||12;return J?h(J):w.fontMetrics(n,a.getAttribute("style")?a:b).h};I=[m,P,g,J,l,K,x].join();if(I!==a.textCache){for(a.textCache=I;p--;)b.removeChild(d[p]);c||l||P||x||-1!==m.indexOf(" ")?(F=/<.*class="([^"]+)".*>/,
y=/<.*style="([^"]+)".*>/,e=/<.*href="([^"]+)".*>/,A&&A.appendChild(b),m=c?m.replace(/<(b|strong)>/g,'\x3cspan style\x3d"font-weight:bold"\x3e').replace(/<(i|em)>/g,'\x3cspan style\x3d"font-style:italic"\x3e').replace(/<a/g,"\x3cspan").replace(/<\/(b|strong|i|em|a)>/g,"\x3c/span\x3e").split(/<br.*?>/g):[m],m=C(m,function(a){return""!==a}),f(m,function(h,m){var c,d=0;h=h.replace(/^\s+|\s+$/g,"").replace(/<span/g,"|||\x3cspan").replace(/<\/span>/g,"\x3c/span\x3e|||");c=h.split("|||");f(c,function(h){if(""!==
h||1===c.length){var f={},A=k.createElementNS(w.SVG_NS,"tspan"),H,J;F.test(h)&&(H=h.match(F)[1],r(A,"class",H));y.test(h)&&(J=h.match(y)[1].replace(/(;| |^)color([ :])/,"$1fill$2"),r(A,"style",J));e.test(h)&&!n&&(r(A,"onclick",'location.href\x3d"'+h.match(e)[1]+'"'),t(A,{cursor:"pointer"}));h=(h.replace(/<(.|\n)*?>/g,"")||" ").replace(/&lt;/g,"\x3c").replace(/&gt;/g,"\x3e");if(" "!==h){A.appendChild(k.createTextNode(h));d?f.dx=0:m&&null!==q&&(f.x=q);r(A,f);b.appendChild(A);!d&&v&&(!L&&n&&t(A,{display:"block"}),
r(A,"dy",u(A)));if(x){f=h.replace(/([^\^])-/g,"$1- ").split(" ");H=1<c.length||m||1<f.length&&!g;var l=[],C,R=u(A),K=a.rotation;for(P&&(z=w.applyEllipsis(a,A,h,x));!P&&H&&(f.length||l.length);)a.rotation=0,C=w.getSpanWidth(a,A),h=C>x,void 0===z&&(z=h),h&&1!==f.length?(A.removeChild(A.firstChild),l.unshift(f.pop())):(f=l,l=[],f.length&&!g&&(A=k.createElementNS(Q,"tspan"),r(A,{dy:R,x:q}),J&&r(A,"style",J),b.appendChild(A)),C>x&&(x=C)),f.length&&A.appendChild(k.createTextNode(f.join(" ").replace(/- /g,
"-")));a.rotation=K}d++}}});v=v||b.childNodes.length}),z&&a.attr("title",a.textStr),A&&A.removeChild(b),l&&a.applyTextOutline&&a.applyTextOutline(l)):b.appendChild(k.createTextNode(m.replace(/&lt;/g,"\x3c").replace(/&gt;/g,"\x3e")))}},getContrast:function(a){a=p(a).rgba;return 510<a[0]+a[1]+a[2]?"#000000":"#FFFFFF"},button:function(a,h,b,w,n,m,c,f,e){var F=this.label(a,h,b,e,null,null,null,null,"button"),A=0;F.attr(y({padding:8,r:2},n));var k,z,L,q;n=y({fill:"#f7f7f7",stroke:"#cccccc","stroke-width":1,
style:{color:"#333333",cursor:"pointer",fontWeight:"normal"}},n);k=n.style;delete n.style;m=y(n,{fill:"#e6e6e6"},m);z=m.style;delete m.style;c=y(n,{fill:"#e6ebf5",style:{color:"#000000",fontWeight:"bold"}},c);L=c.style;delete c.style;f=y(n,{style:{color:"#cccccc"}},f);q=f.style;delete f.style;G(F.element,J?"mouseover":"mouseenter",function(){3!==A&&F.setState(1)});G(F.element,J?"mouseout":"mouseleave",function(){3!==A&&F.setState(A)});F.setState=function(a){1!==a&&(F.state=A=a);F.removeClass(/highcharts-button-(normal|hover|pressed|disabled)/).addClass("highcharts-button-"+
["normal","hover","pressed","disabled"][a||0]);F.attr([n,m,c,f][a||0]).css([k,z,L,q][a||0])};F.attr(n).css(d({cursor:"default"},k));return F.on("click",function(a){3!==A&&w.call(F,a)})},crispLine:function(a,h){a[1]===a[4]&&(a[1]=a[4]=Math.round(a[1])-h%2/2);a[2]===a[5]&&(a[2]=a[5]=Math.round(a[2])+h%2/2);return a},path:function(a){var h={fill:"none"};I(a)?h.d=a:b(a)&&d(h,a);return this.createElement("path").attr(h)},circle:function(a,h,w){a=b(a)?a:{x:a,y:h,r:w};h=this.createElement("circle");h.xSetter=
h.ySetter=function(a,h,b){b.setAttribute("c"+h,a)};return h.attr(a)},arc:function(a,h,w,n,m,c){b(a)?(n=a,h=n.y,w=n.r,a=n.x):n={innerR:n,start:m,end:c};a=this.symbol("arc",a,h,w,w,n);a.r=w;return a},rect:function(a,h,w,n,m,c){m=b(a)?a.r:m;var d=this.createElement("rect");a=b(a)?a:void 0===a?{}:{x:a,y:h,width:Math.max(w,0),height:Math.max(n,0)};void 0!==c&&(a.strokeWidth=c,a=d.crisp(a));a.fill="none";m&&(a.r=m);d.rSetter=function(a,h,b){r(b,{rx:a,ry:a})};return d.attr(a)},setSize:function(a,h,b){var w=
this.alignedObjects,n=w.length;this.width=a;this.height=h;for(this.boxWrapper.animate({width:a,height:h},{step:function(){this.attr({viewBox:"0 0 "+this.attr("width")+" "+this.attr("height")})},duration:H(b,!0)?void 0:0});n--;)w[n].align()},g:function(a){var h=this.createElement("g");return a?h.attr({"class":"highcharts-"+a}):h},image:function(a,h,b,w,n){var m={preserveAspectRatio:"none"};1<arguments.length&&d(m,{x:h,y:b,width:w,height:n});m=this.createElement("image").attr(m);m.element.setAttributeNS?
m.element.setAttributeNS("http://www.w3.org/1999/xlink","href",a):m.element.setAttribute("hc-svg-href",a);return m},symbol:function(a,h,b,w,n,m){var c=this,y,F=/^url\((.*?)\)$/,e=F.test(a),A=!e&&(this.symbols[a]?a:"circle"),z=A&&this.symbols[A],L=u(h)&&z&&z.call(this.symbols,Math.round(h),Math.round(b),w,n,m),q,x;z?(y=this.path(L),y.attr("fill","none"),d(y,{symbolName:A,x:h,y:b,width:w,height:n}),m&&d(y,m)):e&&(q=a.match(F)[1],y=this.image(q),y.imgwidth=H(N[q]&&N[q].width,m&&m.width),y.imgheight=
H(N[q]&&N[q].height,m&&m.height),x=function(){y.attr({width:y.width,height:y.height})},f(["width","height"],function(a){y[a+"Setter"]=function(a,h){var b={},w=this["img"+h],n="width"===h?"translateX":"translateY";this[h]=a;u(w)&&(this.element&&this.element.setAttribute(h,w),this.alignByTranslate||(b[n]=((this[h]||0)-w)/2,this.attr(b)))}}),u(h)&&y.attr({x:h,y:b}),y.isImg=!0,u(y.imgwidth)&&u(y.imgheight)?x():(y.attr({width:0,height:0}),v("img",{onload:function(){var a=g[c.chartIndex];0===this.width&&
(t(this,{position:"absolute",top:"-999em"}),k.body.appendChild(this));N[q]={width:this.width,height:this.height};y.imgwidth=this.width;y.imgheight=this.height;y.element&&x();this.parentNode&&this.parentNode.removeChild(this);c.imgCount--;if(!c.imgCount&&a&&a.onload)a.onload()},src:q}),this.imgCount++));return y},symbols:{circle:function(a,h,b,w){return this.arc(a+b/2,h+w/2,b/2,w/2,{start:0,end:2*Math.PI,open:!1})},square:function(a,h,b,w){return["M",a,h,"L",a+b,h,a+b,h+w,a,h+w,"Z"]},triangle:function(a,
h,b,w){return["M",a+b/2,h,"L",a+b,h+w,a,h+w,"Z"]},"triangle-down":function(a,h,b,w){return["M",a,h,"L",a+b,h,a+b/2,h+w,"Z"]},diamond:function(a,h,b,w){return["M",a+b/2,h,"L",a+b,h+w/2,a+b/2,h+w,a,h+w/2,"Z"]},arc:function(a,h,b,w,n){var m=n.start,c=n.r||b,d=n.r||w||b,f=n.end-.001;b=n.innerR;w=H(n.open,.001>Math.abs(n.end-n.start-2*Math.PI));var y=Math.cos(m),e=Math.sin(m),F=Math.cos(f),f=Math.sin(f);n=.001>n.end-m-Math.PI?0:1;c=["M",a+c*y,h+d*e,"A",c,d,0,n,1,a+c*F,h+d*f];u(b)&&c.push(w?"M":"L",a+b*
F,h+b*f,"A",b,b,0,n,0,a+b*y,h+b*e);c.push(w?"":"Z");return c},callout:function(a,h,b,w,n){var m=Math.min(n&&n.r||0,b,w),c=m+6,d=n&&n.anchorX;n=n&&n.anchorY;var f;f=["M",a+m,h,"L",a+b-m,h,"C",a+b,h,a+b,h,a+b,h+m,"L",a+b,h+w-m,"C",a+b,h+w,a+b,h+w,a+b-m,h+w,"L",a+m,h+w,"C",a,h+w,a,h+w,a,h+w-m,"L",a,h+m,"C",a,h,a,h,a+m,h];d&&d>b?n>h+c&&n<h+w-c?f.splice(13,3,"L",a+b,n-6,a+b+6,n,a+b,n+6,a+b,h+w-m):f.splice(13,3,"L",a+b,w/2,d,n,a+b,w/2,a+b,h+w-m):d&&0>d?n>h+c&&n<h+w-c?f.splice(33,3,"L",a,n+6,a-6,n,a,n-6,
a,h+m):f.splice(33,3,"L",a,w/2,d,n,a,w/2,a,h+m):n&&n>w&&d>a+c&&d<a+b-c?f.splice(23,3,"L",d+6,h+w,d,h+w+6,d-6,h+w,a+m,h+w):n&&0>n&&d>a+c&&d<a+b-c&&f.splice(3,3,"L",d-6,h,d,h-6,d+6,h,b-m,h);return f}},clipRect:function(h,b,w,n){var m=a.uniqueKey(),c=this.createElement("clipPath").attr({id:m}).add(this.defs);h=this.rect(h,b,w,n,0).add(c);h.id=m;h.clipPath=c;h.count=0;return h},text:function(a,h,b,w){var n=!L&&this.forExport,m={};if(w&&(this.allowHTML||!this.forExport))return this.html(a,h,b);m.x=Math.round(h||
0);b&&(m.y=Math.round(b));if(a||0===a)m.text=a;a=this.createElement("text").attr(m);n&&a.css({position:"absolute"});w||(a.xSetter=function(a,h,b){var w=b.getElementsByTagName("tspan"),n,m=b.getAttribute(h),c;for(c=0;c<w.length;c++)n=w[c],n.getAttribute(h)===m&&n.setAttribute(h,a);b.setAttribute(h,a)});return a},fontMetrics:function(a,b){a=a||b&&b.style&&b.style.fontSize||this.style&&this.style.fontSize;a=/px/.test(a)?h(a):/em/.test(a)?parseFloat(a)*(b?this.fontMetrics(null,b.parentNode).f:16):12;
b=24>a?a+3:Math.round(1.2*a);return{h:b,b:Math.round(.8*b),f:a}},rotCorr:function(a,h,b){var w=a;h&&b&&(w=Math.max(w*Math.cos(h*l),4));return{x:-a/3*Math.sin(h*l),y:w}},label:function(h,b,n,m,c,e,A,k,z){var L=this,q=L.g("button"!==z&&"label"),H=q.text=L.text("",0,0,A).attr({zIndex:1}),x,F,J=0,l=3,C=0,P,g,K,I,t,Q={},v,p,r=/^url\((.*?)\)$/.test(m),R=r,N,U,T,O;z&&q.addClass("highcharts-"+z);R=r;N=function(){return(v||0)%2/2};U=function(){var a=H.element.style,h={};F=(void 0===P||void 0===g||t)&&u(H.textStr)&&
H.getBBox();q.width=(P||F.width||0)+2*l+C;q.height=(g||F.height||0)+2*l;p=l+L.fontMetrics(a&&a.fontSize,H).b;R&&(x||(q.box=x=L.symbols[m]||r?L.symbol(m):L.rect(),x.addClass(("button"===z?"":"highcharts-label-box")+(z?" highcharts-"+z+"-box":"")),x.add(q),a=N(),h.x=a,h.y=(k?-p:0)+a),h.width=Math.round(q.width),h.height=Math.round(q.height),x.attr(d(h,Q)),Q={})};T=function(){var a=C+l,h;h=k?0:p;u(P)&&F&&("center"===t||"right"===t)&&(a+={center:.5,right:1}[t]*(P-F.width));if(a!==H.x||h!==H.y)H.attr("x",
a),void 0!==h&&H.attr("y",h);H.x=a;H.y=h};O=function(a,h){x?x.attr(a,h):Q[a]=h};q.onAdd=function(){H.add(q);q.attr({text:h||0===h?h:"",x:b,y:n});x&&u(c)&&q.attr({anchorX:c,anchorY:e})};q.widthSetter=function(h){P=a.isNumber(h)?h:null};q.heightSetter=function(a){g=a};q["text-alignSetter"]=function(a){t=a};q.paddingSetter=function(a){u(a)&&a!==l&&(l=q.padding=a,T())};q.paddingLeftSetter=function(a){u(a)&&a!==C&&(C=a,T())};q.alignSetter=function(a){a={left:0,center:.5,right:1}[a];a!==J&&(J=a,F&&q.attr({x:K}))};
q.textSetter=function(a){void 0!==a&&H.textSetter(a);U();T()};q["stroke-widthSetter"]=function(a,h){a&&(R=!0);v=this["stroke-width"]=a;O(h,a)};q.strokeSetter=q.fillSetter=q.rSetter=function(a,h){"r"!==h&&("fill"===h&&a&&(R=!0),q[h]=a);O(h,a)};q.anchorXSetter=function(a,h){c=q.anchorX=a;O(h,Math.round(a)-N()-K)};q.anchorYSetter=function(a,h){e=q.anchorY=a;O(h,a-I)};q.xSetter=function(a){q.x=a;J&&(a-=J*((P||F.width)+2*l));K=Math.round(a);q.attr("translateX",K)};q.ySetter=function(a){I=q.y=Math.round(a);
q.attr("translateY",I)};var B=q.css;return d(q,{css:function(a){if(a){var h={};a=y(a);f(q.textProps,function(b){void 0!==a[b]&&(h[b]=a[b],delete a[b])});H.css(h)}return B.call(q,a)},getBBox:function(){return{width:F.width+2*l,height:F.height+2*l,x:F.x-l,y:F.y-l}},shadow:function(a){a&&(U(),x&&x.shadow(a));return q},destroy:function(){w(q.element,"mouseenter");w(q.element,"mouseleave");H&&(H=H.destroy());x&&(x=x.destroy());D.prototype.destroy.call(q);q=L=U=T=O=null}})}});a.Renderer=B})(M);(function(a){var D=
a.attr,B=a.createElement,G=a.css,E=a.defined,r=a.each,g=a.extend,p=a.isFirefox,t=a.isMS,v=a.isWebKit,u=a.pInt,l=a.SVGRenderer,e=a.win,k=a.wrap;g(a.SVGElement.prototype,{htmlCss:function(a){var d=this.element;if(d=a&&"SPAN"===d.tagName&&a.width)delete a.width,this.textWidth=d,this.updateTransform();a&&"ellipsis"===a.textOverflow&&(a.whiteSpace="nowrap",a.overflow="hidden");this.styles=g(this.styles,a);G(this.element,a);return this},htmlGetBBox:function(){var a=this.element;"text"===a.nodeName&&(a.style.position=
"absolute");return{x:a.offsetLeft,y:a.offsetTop,width:a.offsetWidth,height:a.offsetHeight}},htmlUpdateTransform:function(){if(this.added){var a=this.renderer,d=this.element,e=this.translateX||0,k=this.translateY||0,c=this.x||0,q=this.y||0,l=this.textAlign||"left",m={left:0,center:.5,right:1}[l],J=this.styles;G(d,{marginLeft:e,marginTop:k});this.shadows&&r(this.shadows,function(a){G(a,{marginLeft:e+1,marginTop:k+1})});this.inverted&&r(d.childNodes,function(b){a.invertChild(b,d)});if("SPAN"===d.tagName){var b=
this.rotation,z=u(this.textWidth),g=J&&J.whiteSpace,y=[b,l,d.innerHTML,this.textWidth,this.textAlign].join();y!==this.cTT&&(J=a.fontMetrics(d.style.fontSize).b,E(b)&&this.setSpanRotation(b,m,J),G(d,{width:"",whiteSpace:g||"nowrap"}),d.offsetWidth>z&&/[ \-]/.test(d.textContent||d.innerText)&&G(d,{width:z+"px",display:"block",whiteSpace:g||"normal"}),this.getSpanCorrection(d.offsetWidth,J,m,b,l));G(d,{left:c+(this.xCorr||0)+"px",top:q+(this.yCorr||0)+"px"});v&&(J=d.offsetHeight);this.cTT=y}}else this.alignOnAdd=
!0},setSpanRotation:function(a,d,k){var f={},c=t?"-ms-transform":v?"-webkit-transform":p?"MozTransform":e.opera?"-o-transform":"";f[c]=f.transform="rotate("+a+"deg)";f[c+(p?"Origin":"-origin")]=f.transformOrigin=100*d+"% "+k+"px";G(this.element,f)},getSpanCorrection:function(a,d,e){this.xCorr=-a*e;this.yCorr=-d}});g(l.prototype,{html:function(a,d,e){var f=this.createElement("span"),c=f.element,q=f.renderer,x=q.isSVG,m=function(a,b){r(["opacity","visibility"],function(m){k(a,m+"Setter",function(a,
m,c,n){a.call(this,m,c,n);b[c]=m})})};f.textSetter=function(a){a!==c.innerHTML&&delete this.bBox;c.innerHTML=this.textStr=a;f.htmlUpdateTransform()};x&&m(f,f.element.style);f.xSetter=f.ySetter=f.alignSetter=f.rotationSetter=function(a,b){"align"===b&&(b="textAlign");f[b]=a;f.htmlUpdateTransform()};f.attr({text:a,x:Math.round(d),y:Math.round(e)}).css({fontFamily:this.style.fontFamily,fontSize:this.style.fontSize,position:"absolute"});c.style.whiteSpace="nowrap";f.css=f.htmlCss;x&&(f.add=function(a){var b,
d=q.box.parentNode,e=[];if(this.parentGroup=a){if(b=a.div,!b){for(;a;)e.push(a),a=a.parentGroup;r(e.reverse(),function(a){var c,n=D(a.element,"class");n&&(n={className:n});b=a.div=a.div||B("div",n,{position:"absolute",left:(a.translateX||0)+"px",top:(a.translateY||0)+"px",display:a.display,opacity:a.opacity,pointerEvents:a.styles&&a.styles.pointerEvents},b||d);c=b.style;g(a,{classSetter:function(a){this.element.setAttribute("class",a);b.className=a},on:function(){e[0].div&&f.on.apply({element:e[0].div},
arguments);return a},translateXSetter:function(b,h){c.left=b+"px";a[h]=b;a.doTransform=!0},translateYSetter:function(b,h){c.top=b+"px";a[h]=b;a.doTransform=!0}});m(a,c)})}}else b=d;b.appendChild(c);f.added=!0;f.alignOnAdd&&f.htmlUpdateTransform();return f});return f}})})(M);(function(a){var D,B,G=a.createElement,E=a.css,r=a.defined,g=a.deg2rad,p=a.discardElement,t=a.doc,v=a.each,u=a.erase,l=a.extend;D=a.extendClass;var e=a.isArray,k=a.isNumber,f=a.isObject,d=a.merge;B=a.noop;var x=a.pick,C=a.pInt,
c=a.SVGElement,q=a.SVGRenderer,I=a.win;a.svg||(B={docMode8:t&&8===t.documentMode,init:function(a,c){var b=["\x3c",c,' filled\x3d"f" stroked\x3d"f"'],m=["position: ","absolute",";"],d="div"===c;("shape"===c||d)&&m.push("left:0;top:0;width:1px;height:1px;");m.push("visibility: ",d?"hidden":"visible");b.push(' style\x3d"',m.join(""),'"/\x3e');c&&(b=d||"span"===c||"img"===c?b.join(""):a.prepVML(b),this.element=G(b));this.renderer=a},add:function(a){var m=this.renderer,b=this.element,c=m.box,d=a&&a.inverted,
c=a?a.element||a:c;a&&(this.parentGroup=a);d&&m.invertChild(b,c);c.appendChild(b);this.added=!0;this.alignOnAdd&&!this.deferUpdateTransform&&this.updateTransform();if(this.onAdd)this.onAdd();this.className&&this.attr("class",this.className);return this},updateTransform:c.prototype.htmlUpdateTransform,setSpanRotation:function(){var a=this.rotation,c=Math.cos(a*g),b=Math.sin(a*g);E(this.element,{filter:a?["progid:DXImageTransform.Microsoft.Matrix(M11\x3d",c,", M12\x3d",-b,", M21\x3d",b,", M22\x3d",
c,", sizingMethod\x3d'auto expand')"].join(""):"none"})},getSpanCorrection:function(a,c,b,d,f){var m=d?Math.cos(d*g):1,e=d?Math.sin(d*g):0,n=x(this.elemHeight,this.element.offsetHeight),q;this.xCorr=0>m&&-a;this.yCorr=0>e&&-n;q=0>m*e;this.xCorr+=e*c*(q?1-b:b);this.yCorr-=m*c*(d?q?b:1-b:1);f&&"left"!==f&&(this.xCorr-=a*b*(0>m?-1:1),d&&(this.yCorr-=n*b*(0>e?-1:1)),E(this.element,{textAlign:f}))},pathToVML:function(a){for(var m=a.length,b=[];m--;)k(a[m])?b[m]=Math.round(10*a[m])-5:"Z"===a[m]?b[m]="x":
(b[m]=a[m],!a.isArc||"wa"!==a[m]&&"at"!==a[m]||(b[m+5]===b[m+7]&&(b[m+7]+=a[m+7]>a[m+5]?1:-1),b[m+6]===b[m+8]&&(b[m+8]+=a[m+8]>a[m+6]?1:-1)));return b.join(" ")||"x"},clip:function(a){var m=this,b;a?(b=a.members,u(b,m),b.push(m),m.destroyClip=function(){u(b,m)},a=a.getCSS(m)):(m.destroyClip&&m.destroyClip(),a={clip:m.docMode8?"inherit":"rect(auto)"});return m.css(a)},css:c.prototype.htmlCss,safeRemoveChild:function(a){a.parentNode&&p(a)},destroy:function(){this.destroyClip&&this.destroyClip();return c.prototype.destroy.apply(this)},
on:function(a,c){this.element["on"+a]=function(){var a=I.event;a.target=a.srcElement;c(a)};return this},cutOffPath:function(a,c){var b;a=a.split(/[ ,]/);b=a.length;if(9===b||11===b)a[b-4]=a[b-2]=C(a[b-2])-10*c;return a.join(" ")},shadow:function(a,c,b){var m=[],d,f=this.element,e=this.renderer,n,q=f.style,h,w=f.path,k,L,l,J;w&&"string"!==typeof w.value&&(w="x");L=w;if(a){l=x(a.width,3);J=(a.opacity||.15)/l;for(d=1;3>=d;d++)k=2*l+1-2*d,b&&(L=this.cutOffPath(w.value,k+.5)),h=['\x3cshape isShadow\x3d"true" strokeweight\x3d"',
k,'" filled\x3d"false" path\x3d"',L,'" coordsize\x3d"10 10" style\x3d"',f.style.cssText,'" /\x3e'],n=G(e.prepVML(h),null,{left:C(q.left)+x(a.offsetX,1),top:C(q.top)+x(a.offsetY,1)}),b&&(n.cutOff=k+1),h=['\x3cstroke color\x3d"',a.color||"#000000",'" opacity\x3d"',J*d,'"/\x3e'],G(e.prepVML(h),null,null,n),c?c.element.appendChild(n):f.parentNode.insertBefore(n,f),m.push(n);this.shadows=m}return this},updateShadows:B,setAttr:function(a,c){this.docMode8?this.element[a]=c:this.element.setAttribute(a,c)},
classSetter:function(a){(this.added?this.element:this).className=a},dashstyleSetter:function(a,c,b){(b.getElementsByTagName("stroke")[0]||G(this.renderer.prepVML(["\x3cstroke/\x3e"]),null,null,b))[c]=a||"solid";this[c]=a},dSetter:function(a,c,b){var m=this.shadows;a=a||[];this.d=a.join&&a.join(" ");b.path=a=this.pathToVML(a);if(m)for(b=m.length;b--;)m[b].path=m[b].cutOff?this.cutOffPath(a,m[b].cutOff):a;this.setAttr(c,a)},fillSetter:function(a,c,b){var m=b.nodeName;"SPAN"===m?b.style.color=a:"IMG"!==
m&&(b.filled="none"!==a,this.setAttr("fillcolor",this.renderer.color(a,b,c,this)))},"fill-opacitySetter":function(a,c,b){G(this.renderer.prepVML(["\x3c",c.split("-")[0],' opacity\x3d"',a,'"/\x3e']),null,null,b)},opacitySetter:B,rotationSetter:function(a,c,b){b=b.style;this[c]=b[c]=a;b.left=-Math.round(Math.sin(a*g)+1)+"px";b.top=Math.round(Math.cos(a*g))+"px"},strokeSetter:function(a,c,b){this.setAttr("strokecolor",this.renderer.color(a,b,c,this))},"stroke-widthSetter":function(a,c,b){b.stroked=!!a;
this[c]=a;k(a)&&(a+="px");this.setAttr("strokeweight",a)},titleSetter:function(a,c){this.setAttr(c,a)},visibilitySetter:function(a,c,b){"inherit"===a&&(a="visible");this.shadows&&v(this.shadows,function(b){b.style[c]=a});"DIV"===b.nodeName&&(a="hidden"===a?"-999em":0,this.docMode8||(b.style[c]=a?"visible":"hidden"),c="top");b.style[c]=a},xSetter:function(a,c,b){this[c]=a;"x"===c?c="left":"y"===c&&(c="top");this.updateClipping?(this[c]=a,this.updateClipping()):b.style[c]=a},zIndexSetter:function(a,
c,b){b.style[c]=a}},B["stroke-opacitySetter"]=B["fill-opacitySetter"],a.VMLElement=B=D(c,B),B.prototype.ySetter=B.prototype.widthSetter=B.prototype.heightSetter=B.prototype.xSetter,B={Element:B,isIE8:-1<I.navigator.userAgent.indexOf("MSIE 8.0"),init:function(a,c,b){var m,d;this.alignedObjects=[];m=this.createElement("div").css({position:"relative"});d=m.element;a.appendChild(m.element);this.isVML=!0;this.box=d;this.boxWrapper=m;this.gradients={};this.cache={};this.cacheKeys=[];this.imgCount=0;this.setSize(c,
b,!1);if(!t.namespaces.hcv){t.namespaces.add("hcv","urn:schemas-microsoft-com:vml");try{t.createStyleSheet().cssText="hcv\\:fill, hcv\\:path, hcv\\:shape, hcv\\:stroke{ behavior:url(#default#VML); display: inline-block; } "}catch(y){t.styleSheets[0].cssText+="hcv\\:fill, hcv\\:path, hcv\\:shape, hcv\\:stroke{ behavior:url(#default#VML); display: inline-block; } "}}},isHidden:function(){return!this.box.offsetWidth},clipRect:function(a,c,b,d){var m=this.createElement(),y=f(a);return l(m,{members:[],
count:0,left:(y?a.x:a)+1,top:(y?a.y:c)+1,width:(y?a.width:b)-1,height:(y?a.height:d)-1,getCSS:function(a){var b=a.element,c=b.nodeName,h=a.inverted,w=this.top-("shape"===c?b.offsetTop:0),m=this.left,b=m+this.width,d=w+this.height,w={clip:"rect("+Math.round(h?m:w)+"px,"+Math.round(h?d:b)+"px,"+Math.round(h?b:d)+"px,"+Math.round(h?w:m)+"px)"};!h&&a.docMode8&&"DIV"===c&&l(w,{width:b+"px",height:d+"px"});return w},updateClipping:function(){v(m.members,function(a){a.element&&a.css(m.getCSS(a))})}})},color:function(c,
d,b,f){var m=this,y,e=/^rgba/,n,q,h="none";c&&c.linearGradient?q="gradient":c&&c.radialGradient&&(q="pattern");if(q){var w,k,L=c.linearGradient||c.radialGradient,x,z,l,F,C,g="";c=c.stops;var J,t=[],I=function(){n=['\x3cfill colors\x3d"'+t.join(",")+'" opacity\x3d"',l,'" o:opacity2\x3d"',z,'" type\x3d"',q,'" ',g,'focus\x3d"100%" method\x3d"any" /\x3e'];G(m.prepVML(n),null,null,d)};x=c[0];J=c[c.length-1];0<x[0]&&c.unshift([0,x[1]]);1>J[0]&&c.push([1,J[1]]);v(c,function(h,b){e.test(h[1])?(y=a.color(h[1]),
w=y.get("rgb"),k=y.get("a")):(w=h[1],k=1);t.push(100*h[0]+"% "+w);b?(l=k,F=w):(z=k,C=w)});if("fill"===b)if("gradient"===q)b=L.x1||L[0]||0,c=L.y1||L[1]||0,x=L.x2||L[2]||0,L=L.y2||L[3]||0,g='angle\x3d"'+(90-180*Math.atan((L-c)/(x-b))/Math.PI)+'"',I();else{var h=L.r,p=2*h,u=2*h,r=L.cx,B=L.cy,D=d.radialReference,E,h=function(){D&&(E=f.getBBox(),r+=(D[0]-E.x)/E.width-.5,B+=(D[1]-E.y)/E.height-.5,p*=D[2]/E.width,u*=D[2]/E.height);g='src\x3d"'+a.getOptions().global.VMLRadialGradientURL+'" size\x3d"'+p+","+
u+'" origin\x3d"0.5,0.5" position\x3d"'+r+","+B+'" color2\x3d"'+C+'" ';I()};f.added?h():f.onAdd=h;h=F}else h=w}else e.test(c)&&"IMG"!==d.tagName?(y=a.color(c),f[b+"-opacitySetter"](y.get("a"),b,d),h=y.get("rgb")):(h=d.getElementsByTagName(b),h.length&&(h[0].opacity=1,h[0].type="solid"),h=c);return h},prepVML:function(a){var c=this.isIE8;a=a.join("");c?(a=a.replace("/\x3e",' xmlns\x3d"urn:schemas-microsoft-com:vml" /\x3e'),a=-1===a.indexOf('style\x3d"')?a.replace("/\x3e",' style\x3d"display:inline-block;behavior:url(#default#VML);" /\x3e'):
a.replace('style\x3d"','style\x3d"display:inline-block;behavior:url(#default#VML);')):a=a.replace("\x3c","\x3chcv:");return a},text:q.prototype.html,path:function(a){var c={coordsize:"10 10"};e(a)?c.d=a:f(a)&&l(c,a);return this.createElement("shape").attr(c)},circle:function(a,c,b){var d=this.symbol("circle");f(a)&&(b=a.r,c=a.y,a=a.x);d.isCircle=!0;d.r=b;return d.attr({x:a,y:c})},g:function(a){var c;a&&(c={className:"highcharts-"+a,"class":"highcharts-"+a});return this.createElement("div").attr(c)},
image:function(a,c,b,d,f){var m=this.createElement("img").attr({src:a});1<arguments.length&&m.attr({x:c,y:b,width:d,height:f});return m},createElement:function(a){return"rect"===a?this.symbol(a):q.prototype.createElement.call(this,a)},invertChild:function(a,c){var b=this;c=c.style;var d="IMG"===a.tagName&&a.style;E(a,{flip:"x",left:C(c.width)-(d?C(d.top):1),top:C(c.height)-(d?C(d.left):1),rotation:-90});v(a.childNodes,function(c){b.invertChild(c,a)})},symbols:{arc:function(a,c,b,d,f){var m=f.start,
e=f.end,n=f.r||b||d;b=f.innerR;d=Math.cos(m);var q=Math.sin(m),h=Math.cos(e),w=Math.sin(e);if(0===e-m)return["x"];m=["wa",a-n,c-n,a+n,c+n,a+n*d,c+n*q,a+n*h,c+n*w];f.open&&!b&&m.push("e","M",a,c);m.push("at",a-b,c-b,a+b,c+b,a+b*h,c+b*w,a+b*d,c+b*q,"x","e");m.isArc=!0;return m},circle:function(a,c,b,d,f){f&&r(f.r)&&(b=d=2*f.r);f&&f.isCircle&&(a-=b/2,c-=d/2);return["wa",a,c,a+b,c+d,a+b,c+d/2,a+b,c+d/2,"e"]},rect:function(a,c,b,d,f){return q.prototype.symbols[r(f)&&f.r?"callout":"square"].call(0,a,c,
b,d,f)}}},a.VMLRenderer=D=function(){this.init.apply(this,arguments)},D.prototype=d(q.prototype,B),a.Renderer=D);q.prototype.measureSpanWidth=function(a,c){var b=t.createElement("span");a=t.createTextNode(a);b.appendChild(a);E(b,c);this.box.appendChild(b);c=b.offsetWidth;p(b);return c}})(M);(function(a){function D(){var g=a.defaultOptions.global,p=t.moment;if(g.timezone){if(p)return function(a){return-p.tz(a,g.timezone).utcOffset()};a.error(25)}return g.useUTC&&g.getTimezoneOffset}function B(){var g=
a.defaultOptions.global,u,l=g.useUTC,e=l?"getUTC":"get",k=l?"setUTC":"set";a.Date=u=g.Date||t.Date;u.hcTimezoneOffset=l&&g.timezoneOffset;u.hcGetTimezoneOffset=D();u.hcMakeTime=function(a,d,e,k,c,q){var f;l?(f=u.UTC.apply(0,arguments),f+=r(f)):f=(new u(a,d,p(e,1),p(k,0),p(c,0),p(q,0))).getTime();return f};E("Minutes Hours Day Date Month FullYear".split(" "),function(a){u["hcGet"+a]=e+a});E("Milliseconds Seconds Minutes Hours Date Month FullYear".split(" "),function(a){u["hcSet"+a]=k+a})}var G=a.color,
E=a.each,r=a.getTZOffset,g=a.merge,p=a.pick,t=a.win;a.defaultOptions={colors:"#7cb5ec #434348 #90ed7d #f7a35c #8085e9 #f15c80 #e4d354 #2b908f #f45b5b #91e8e1".split(" "),symbols:["circle","diamond","square","triangle","triangle-down"],lang:{loading:"Loading...",months:"January February March April May June July August September October November December".split(" "),shortMonths:"Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split(" "),weekdays:"Sunday Monday Tuesday Wednesday Thursday Friday Saturday".split(" "),
decimalPoint:".",numericSymbols:"kMGTPE".split(""),resetZoom:"Reset zoom",resetZoomTitle:"Reset zoom level 1:1",thousandsSep:" "},global:{useUTC:!0,VMLRadialGradientURL:"http://code.highcharts.com/5.0.14/gfx/vml-radial-gradient.png"},chart:{borderRadius:0,defaultSeriesType:"line",ignoreHiddenSeries:!0,spacing:[10,10,15,10],resetZoomButton:{theme:{zIndex:20},position:{align:"right",x:-10,y:10}},width:null,height:null,borderColor:"#335cad",backgroundColor:"#ffffff",plotBorderColor:"#cccccc"},title:{text:"Chart title",
align:"center",margin:15,widthAdjust:-44},subtitle:{text:"",align:"center",widthAdjust:-44},plotOptions:{},labels:{style:{position:"absolute",color:"#333333"}},legend:{enabled:!0,align:"center",layout:"horizontal",labelFormatter:function(){return this.name},borderColor:"#999999",borderRadius:0,navigation:{activeColor:"#003399",inactiveColor:"#cccccc"},itemStyle:{color:"#333333",fontSize:"12px",fontWeight:"bold",textOverflow:"ellipsis"},itemHoverStyle:{color:"#000000"},itemHiddenStyle:{color:"#cccccc"},
shadow:!1,itemCheckboxStyle:{position:"absolute",width:"13px",height:"13px"},squareSymbol:!0,symbolPadding:5,verticalAlign:"bottom",x:0,y:0,title:{style:{fontWeight:"bold"}}},loading:{labelStyle:{fontWeight:"bold",position:"relative",top:"45%"},style:{position:"absolute",backgroundColor:"#ffffff",opacity:.5,textAlign:"center"}},tooltip:{enabled:!0,animation:a.svg,borderRadius:3,dateTimeLabelFormats:{millisecond:"%A, %b %e, %H:%M:%S.%L",second:"%A, %b %e, %H:%M:%S",minute:"%A, %b %e, %H:%M",hour:"%A, %b %e, %H:%M",
day:"%A, %b %e, %Y",week:"Week from %A, %b %e, %Y",month:"%B %Y",year:"%Y"},footerFormat:"",padding:8,snap:a.isTouchDevice?25:10,backgroundColor:G("#f7f7f7").setOpacity(.85).get(),borderWidth:1,headerFormat:'\x3cspan style\x3d"font-size: 10px"\x3e{point.key}\x3c/span\x3e\x3cbr/\x3e',pointFormat:'\x3cspan style\x3d"color:{point.color}"\x3e\u25cf\x3c/span\x3e {series.name}: \x3cb\x3e{point.y}\x3c/b\x3e\x3cbr/\x3e',shadow:!0,style:{color:"#333333",cursor:"default",fontSize:"12px",pointerEvents:"none",
whiteSpace:"nowrap"}},credits:{enabled:!0,href:"http://www.highcharts.com",position:{align:"right",x:-10,verticalAlign:"bottom",y:-5},style:{cursor:"pointer",color:"#999999",fontSize:"9px"},text:"Highcharts.com"}};a.setOptions=function(t){a.defaultOptions=g(!0,a.defaultOptions,t);B();return a.defaultOptions};a.getOptions=function(){return a.defaultOptions};a.defaultPlotOptions=a.defaultOptions.plotOptions;B()})(M);(function(a){var D=a.correctFloat,B=a.defined,G=a.destroyObjectProperties,E=a.isNumber,
r=a.merge,g=a.pick,p=a.deg2rad;a.Tick=function(a,g,p,l){this.axis=a;this.pos=g;this.type=p||"";this.isNewLabel=this.isNew=!0;p||l||this.addLabel()};a.Tick.prototype={addLabel:function(){var a=this.axis,p=a.options,u=a.chart,l=a.categories,e=a.names,k=this.pos,f=p.labels,d=a.tickPositions,x=k===d[0],C=k===d[d.length-1],e=l?g(l[k],e[k],k):k,l=this.label,d=d.info,c;a.isDatetimeAxis&&d&&(c=p.dateTimeLabelFormats[d.higherRanks[k]||d.unitName]);this.isFirst=x;this.isLast=C;p=a.labelFormatter.call({axis:a,
chart:u,isFirst:x,isLast:C,dateTimeLabelFormat:c,value:a.isLog?D(a.lin2log(e)):e,pos:k});B(l)?l&&l.attr({text:p}):(this.labelLength=(this.label=l=B(p)&&f.enabled?u.renderer.text(p,0,0,f.useHTML).css(r(f.style)).add(a.labelGroup):null)&&l.getBBox().width,this.rotation=0)},getLabelSize:function(){return this.label?this.label.getBBox()[this.axis.horiz?"height":"width"]:0},handleOverflow:function(a){var t=this.axis,u=a.x,l=t.chart.chartWidth,e=t.chart.spacing,k=g(t.labelLeft,Math.min(t.pos,e[3])),e=g(t.labelRight,
Math.max(t.pos+t.len,l-e[1])),f=this.label,d=this.rotation,x={left:0,center:.5,right:1}[t.labelAlign],C=f.getBBox().width,c=t.getSlotWidth(),q=c,I=1,m,J={};if(d)0>d&&u-x*C<k?m=Math.round(u/Math.cos(d*p)-k):0<d&&u+x*C>e&&(m=Math.round((l-u)/Math.cos(d*p)));else if(l=u+(1-x)*C,u-x*C<k?q=a.x+q*(1-x)-k:l>e&&(q=e-a.x+q*x,I=-1),q=Math.min(c,q),q<c&&"center"===t.labelAlign&&(a.x+=I*(c-q-x*(c-Math.min(C,q)))),C>q||t.autoRotation&&(f.styles||{}).width)m=q;m&&(J.width=m,(t.options.labels.style||{}).textOverflow||
(J.textOverflow="ellipsis"),f.css(J))},getPosition:function(a,g,p,l){var e=this.axis,k=e.chart,f=l&&k.oldChartHeight||k.chartHeight;return{x:a?e.translate(g+p,null,null,l)+e.transB:e.left+e.offset+(e.opposite?(l&&k.oldChartWidth||k.chartWidth)-e.right-e.left:0),y:a?f-e.bottom+e.offset-(e.opposite?e.height:0):f-e.translate(g+p,null,null,l)-e.transB}},getLabelPosition:function(a,g,u,l,e,k,f,d){var x=this.axis,C=x.transA,c=x.reversed,q=x.staggerLines,t=x.tickRotCorr||{x:0,y:0},m=e.y;B(m)||(m=0===x.side?
u.rotation?-8:-u.getBBox().height:2===x.side?t.y+8:Math.cos(u.rotation*p)*(t.y-u.getBBox(!1,0).height/2));a=a+e.x+t.x-(k&&l?k*C*(c?-1:1):0);g=g+m-(k&&!l?k*C*(c?1:-1):0);q&&(u=f/(d||1)%q,x.opposite&&(u=q-u-1),g+=x.labelOffset/q*u);return{x:a,y:Math.round(g)}},getMarkPath:function(a,g,p,l,e,k){return k.crispLine(["M",a,g,"L",a+(e?0:-p),g+(e?p:0)],l)},renderGridLine:function(a,g,p){var l=this.axis,e=l.options,k=this.gridLine,f={},d=this.pos,x=this.type,C=l.tickmarkOffset,c=l.chart.renderer,q=x?x+"Grid":
"grid",I=e[q+"LineWidth"],m=e[q+"LineColor"],e=e[q+"LineDashStyle"];k||(f.stroke=m,f["stroke-width"]=I,e&&(f.dashstyle=e),x||(f.zIndex=1),a&&(f.opacity=0),this.gridLine=k=c.path().attr(f).addClass("highcharts-"+(x?x+"-":"")+"grid-line").add(l.gridGroup));if(!a&&k&&(a=l.getPlotLinePath(d+C,k.strokeWidth()*p,a,!0)))k[this.isNew?"attr":"animate"]({d:a,opacity:g})},renderMark:function(a,p,u){var l=this.axis,e=l.options,k=l.chart.renderer,f=this.type,d=f?f+"Tick":"tick",x=l.tickSize(d),C=this.mark,c=!C,
q=a.x;a=a.y;var I=g(e[d+"Width"],!f&&l.isXAxis?1:0),e=e[d+"Color"];x&&(l.opposite&&(x[0]=-x[0]),c&&(this.mark=C=k.path().addClass("highcharts-"+(f?f+"-":"")+"tick").add(l.axisGroup),C.attr({stroke:e,"stroke-width":I})),C[c?"attr":"animate"]({d:this.getMarkPath(q,a,x[0],C.strokeWidth()*u,l.horiz,k),opacity:p}))},renderLabel:function(a,p,u,l){var e=this.axis,k=e.horiz,f=e.options,d=this.label,x=f.labels,C=x.step,c=e.tickmarkOffset,q=!0,I=a.x;a=a.y;d&&E(I)&&(d.xy=a=this.getLabelPosition(I,a,d,k,x,c,
l,C),this.isFirst&&!this.isLast&&!g(f.showFirstLabel,1)||this.isLast&&!this.isFirst&&!g(f.showLastLabel,1)?q=!1:!k||e.isRadial||x.step||x.rotation||p||0===u||this.handleOverflow(a),C&&l%C&&(q=!1),q&&E(a.y)?(a.opacity=u,d[this.isNewLabel?"attr":"animate"](a),this.isNewLabel=!1):(d.attr("y",-9999),this.isNewLabel=!0),this.isNew=!1)},render:function(a,p,u){var l=this.axis,e=l.horiz,k=this.getPosition(e,this.pos,l.tickmarkOffset,p),f=k.x,d=k.y,l=e&&f===l.pos+l.len||!e&&d===l.pos?-1:1;u=g(u,1);this.isActive=
!0;this.renderGridLine(p,u,l);this.renderMark(k,u,l);this.renderLabel(k,p,u,a)},destroy:function(){G(this,this.axis)}}})(M);var S=function(a){var D=a.addEvent,B=a.animObject,G=a.arrayMax,E=a.arrayMin,r=a.color,g=a.correctFloat,p=a.defaultOptions,t=a.defined,v=a.deg2rad,u=a.destroyObjectProperties,l=a.each,e=a.extend,k=a.fireEvent,f=a.format,d=a.getMagnitude,x=a.grep,C=a.inArray,c=a.isArray,q=a.isNumber,I=a.isString,m=a.merge,J=a.normalizeTickInterval,b=a.objectEach,z=a.pick,K=a.removeEvent,y=a.splat,
A=a.syncTimeout,n=a.Tick,H=function(){this.init.apply(this,arguments)};a.extend(H.prototype,{defaultOptions:{dateTimeLabelFormats:{millisecond:"%H:%M:%S.%L",second:"%H:%M:%S",minute:"%H:%M",hour:"%H:%M",day:"%e. %b",week:"%e. %b",month:"%b '%y",year:"%Y"},endOnTick:!1,labels:{enabled:!0,style:{color:"#666666",cursor:"default",fontSize:"11px"},x:0},minPadding:.01,maxPadding:.01,minorTickLength:2,minorTickPosition:"outside",startOfWeek:1,startOnTick:!1,tickLength:10,tickmarkPlacement:"between",tickPixelInterval:100,
tickPosition:"outside",title:{align:"middle",style:{color:"#666666"}},type:"linear",minorGridLineColor:"#f2f2f2",minorGridLineWidth:1,minorTickColor:"#999999",lineColor:"#ccd6eb",lineWidth:1,gridLineColor:"#e6e6e6",tickColor:"#ccd6eb"},defaultYAxisOptions:{endOnTick:!0,tickPixelInterval:72,showLastLabel:!0,labels:{x:-8},maxPadding:.05,minPadding:.05,startOnTick:!0,title:{rotation:270,text:"Values"},stackLabels:{allowOverlap:!1,enabled:!1,formatter:function(){return a.numberFormat(this.total,-1)},
style:{fontSize:"11px",fontWeight:"bold",color:"#000000",textOutline:"1px contrast"}},gridLineWidth:1,lineWidth:0},defaultLeftAxisOptions:{labels:{x:-15},title:{rotation:270}},defaultRightAxisOptions:{labels:{x:15},title:{rotation:90}},defaultBottomAxisOptions:{labels:{autoRotation:[-45],x:0},title:{rotation:0}},defaultTopAxisOptions:{labels:{autoRotation:[-45],x:0},title:{rotation:0}},init:function(a,c){var h=c.isX,w=this;w.chart=a;w.horiz=a.inverted&&!w.isZAxis?!h:h;w.isXAxis=h;w.coll=w.coll||(h?
"xAxis":"yAxis");w.opposite=c.opposite;w.side=c.side||(w.horiz?w.opposite?0:2:w.opposite?1:3);w.setOptions(c);var n=this.options,d=n.type;w.labelFormatter=n.labels.formatter||w.defaultLabelFormatter;w.userOptions=c;w.minPixelPadding=0;w.reversed=n.reversed;w.visible=!1!==n.visible;w.zoomEnabled=!1!==n.zoomEnabled;w.hasNames="category"===d||!0===n.categories;w.categories=n.categories||w.hasNames;w.names=w.names||[];w.plotLinesAndBandsGroups={};w.isLog="logarithmic"===d;w.isDatetimeAxis="datetime"===
d;w.positiveValuesOnly=w.isLog&&!w.allowNegativeLog;w.isLinked=t(n.linkedTo);w.ticks={};w.labelEdge=[];w.minorTicks={};w.plotLinesAndBands=[];w.alternateBands={};w.len=0;w.minRange=w.userMinRange=n.minRange||n.maxZoom;w.range=n.range;w.offset=n.offset||0;w.stacks={};w.oldStacks={};w.stacksTouched=0;w.max=null;w.min=null;w.crosshair=z(n.crosshair,y(a.options.tooltip.crosshairs)[h?0:1],!1);c=w.options.events;-1===C(w,a.axes)&&(h?a.axes.splice(a.xAxis.length,0,w):a.axes.push(w),a[w.coll].push(w));w.series=
w.series||[];a.inverted&&!w.isZAxis&&h&&void 0===w.reversed&&(w.reversed=!0);b(c,function(a,h){D(w,h,a)});w.lin2log=n.linearToLogConverter||w.lin2log;w.isLog&&(w.val2lin=w.log2lin,w.lin2val=w.lin2log)},setOptions:function(a){this.options=m(this.defaultOptions,"yAxis"===this.coll&&this.defaultYAxisOptions,[this.defaultTopAxisOptions,this.defaultRightAxisOptions,this.defaultBottomAxisOptions,this.defaultLeftAxisOptions][this.side],m(p[this.coll],a))},defaultLabelFormatter:function(){var h=this.axis,
b=this.value,c=h.categories,n=this.dateTimeLabelFormat,d=p.lang,m=d.numericSymbols,d=d.numericSymbolMagnitude||1E3,e=m&&m.length,y,q=h.options.labels.format,h=h.isLog?Math.abs(b):h.tickInterval;if(q)y=f(q,this);else if(c)y=b;else if(n)y=a.dateFormat(n,b);else if(e&&1E3<=h)for(;e--&&void 0===y;)c=Math.pow(d,e+1),h>=c&&0===10*b%c&&null!==m[e]&&0!==b&&(y=a.numberFormat(b/c,-1)+m[e]);void 0===y&&(y=1E4<=Math.abs(b)?a.numberFormat(b,-1):a.numberFormat(b,-1,void 0,""));return y},getSeriesExtremes:function(){var a=
this,b=a.chart;a.hasVisibleSeries=!1;a.dataMin=a.dataMax=a.threshold=null;a.softThreshold=!a.isXAxis;a.buildStacks&&a.buildStacks();l(a.series,function(h){if(h.visible||!b.options.chart.ignoreHiddenSeries){var w=h.options,c=w.threshold,n;a.hasVisibleSeries=!0;a.positiveValuesOnly&&0>=c&&(c=null);if(a.isXAxis)w=h.xData,w.length&&(h=E(w),q(h)||h instanceof Date||(w=x(w,function(a){return q(a)}),h=E(w)),a.dataMin=Math.min(z(a.dataMin,w[0]),h),a.dataMax=Math.max(z(a.dataMax,w[0]),G(w)));else if(h.getExtremes(),
n=h.dataMax,h=h.dataMin,t(h)&&t(n)&&(a.dataMin=Math.min(z(a.dataMin,h),h),a.dataMax=Math.max(z(a.dataMax,n),n)),t(c)&&(a.threshold=c),!w.softThreshold||a.positiveValuesOnly)a.softThreshold=!1}})},translate:function(a,b,c,n,d,f){var h=this.linkedParent||this,w=1,m=0,y=n?h.oldTransA:h.transA;n=n?h.oldMin:h.min;var e=h.minPixelPadding;d=(h.isOrdinal||h.isBroken||h.isLog&&d)&&h.lin2val;y||(y=h.transA);c&&(w*=-1,m=h.len);h.reversed&&(w*=-1,m-=w*(h.sector||h.len));b?(a=(a*w+m-e)/y+n,d&&(a=h.lin2val(a))):
(d&&(a=h.val2lin(a)),a=w*(a-n)*y+m+w*e+(q(f)?y*f:0));return a},toPixels:function(a,b){return this.translate(a,!1,!this.horiz,null,!0)+(b?0:this.pos)},toValue:function(a,b){return this.translate(a-(b?0:this.pos),!0,!this.horiz,null,!0)},getPlotLinePath:function(a,b,c,n,d){var h=this.chart,w=this.left,f=this.top,m,y,e=c&&h.oldChartHeight||h.chartHeight,k=c&&h.oldChartWidth||h.chartWidth,A;m=this.transB;var x=function(a,h,b){if(a<h||a>b)n?a=Math.min(Math.max(h,a),b):A=!0;return a};d=z(d,this.translate(a,
null,null,c));a=c=Math.round(d+m);m=y=Math.round(e-d-m);q(d)?this.horiz?(m=f,y=e-this.bottom,a=c=x(a,w,w+this.width)):(a=w,c=k-this.right,m=y=x(m,f,f+this.height)):A=!0;return A&&!n?null:h.renderer.crispLine(["M",a,m,"L",c,y],b||1)},getLinearTickPositions:function(a,b,c){var h,w=g(Math.floor(b/a)*a);c=g(Math.ceil(c/a)*a);var n=[];if(this.single)return[b];for(b=w;b<=c;){n.push(b);b=g(b+a);if(b===h)break;h=b}return n},getMinorTickPositions:function(){var a=this,b=a.options,c=a.tickPositions,n=a.minorTickInterval,
d=[],f=a.pointRangePadding||0,m=a.min-f,f=a.max+f,y=f-m;if(y&&y/n<a.len/3)if(a.isLog)l(this.paddedTicks,function(h,b,c){b&&d.push.apply(d,a.getLogTickPositions(n,c[b-1],c[b],!0))});else if(a.isDatetimeAxis&&"auto"===b.minorTickInterval)d=d.concat(a.getTimeTicks(a.normalizeTimeTickInterval(n),m,f,b.startOfWeek));else for(b=m+(c[0]-m)%n;b<=f&&b!==d[0];b+=n)d.push(b);0!==d.length&&a.trimTicks(d);return d},adjustForMinRange:function(){var a=this.options,b=this.min,c=this.max,n,d,f,m,y,e,q,k;this.isXAxis&&
void 0===this.minRange&&!this.isLog&&(t(a.min)||t(a.max)?this.minRange=null:(l(this.series,function(a){e=a.xData;for(m=q=a.xIncrement?1:e.length-1;0<m;m--)if(y=e[m]-e[m-1],void 0===f||y<f)f=y}),this.minRange=Math.min(5*f,this.dataMax-this.dataMin)));c-b<this.minRange&&(d=this.dataMax-this.dataMin>=this.minRange,k=this.minRange,n=(k-c+b)/2,n=[b-n,z(a.min,b-n)],d&&(n[2]=this.isLog?this.log2lin(this.dataMin):this.dataMin),b=G(n),c=[b+k,z(a.max,b+k)],d&&(c[2]=this.isLog?this.log2lin(this.dataMax):this.dataMax),
c=E(c),c-b<k&&(n[0]=c-k,n[1]=z(a.min,c-k),b=G(n)));this.min=b;this.max=c},getClosest:function(){var a;this.categories?a=1:l(this.series,function(h){var b=h.closestPointRange,c=h.visible||!h.chart.options.chart.ignoreHiddenSeries;!h.noSharedTooltip&&t(b)&&c&&(a=t(a)?Math.min(a,b):b)});return a},nameToX:function(a){var h=c(this.categories),b=h?this.categories:this.names,n=a.options.x,d;a.series.requireSorting=!1;t(n)||(n=!1===this.options.uniqueNames?a.series.autoIncrement():C(a.name,b));-1===n?h||
(d=b.length):d=n;void 0!==d&&(this.names[d]=a.name);return d},updateNames:function(){var a=this;0<this.names.length&&(this.names.length=0,this.minRange=this.userMinRange,l(this.series||[],function(h){h.xIncrement=null;if(!h.points||h.isDirtyData)h.processData(),h.generatePoints();l(h.points,function(b,c){var n;b.options&&(n=a.nameToX(b),void 0!==n&&n!==b.x&&(b.x=n,h.xData[c]=n))})}))},setAxisTranslation:function(a){var h=this,b=h.max-h.min,c=h.axisPointRange||0,n,d=0,f=0,m=h.linkedParent,y=!!h.categories,
e=h.transA,q=h.isXAxis;if(q||y||c)n=h.getClosest(),m?(d=m.minPointOffset,f=m.pointRangePadding):l(h.series,function(a){var b=y?1:q?z(a.options.pointRange,n,0):h.axisPointRange||0;a=a.options.pointPlacement;c=Math.max(c,b);h.single||(d=Math.max(d,I(a)?0:b/2),f=Math.max(f,"on"===a?0:b))}),m=h.ordinalSlope&&n?h.ordinalSlope/n:1,h.minPointOffset=d*=m,h.pointRangePadding=f*=m,h.pointRange=Math.min(c,b),q&&(h.closestPointRange=n);a&&(h.oldTransA=e);h.translationSlope=h.transA=e=h.options.staticScale||h.len/
(b+f||1);h.transB=h.horiz?h.left:h.bottom;h.minPixelPadding=e*d},minFromRange:function(){return this.max-this.range},setTickInterval:function(h){var b=this,c=b.chart,n=b.options,f=b.isLog,m=b.log2lin,y=b.isDatetimeAxis,e=b.isXAxis,A=b.isLinked,x=n.maxPadding,H=n.minPadding,C=n.tickInterval,p=n.tickPixelInterval,I=b.categories,K=b.threshold,u=b.softThreshold,r,v,B,D;y||I||A||this.getTickAmount();B=z(b.userMin,n.min);D=z(b.userMax,n.max);A?(b.linkedParent=c[b.coll][n.linkedTo],c=b.linkedParent.getExtremes(),
b.min=z(c.min,c.dataMin),b.max=z(c.max,c.dataMax),n.type!==b.linkedParent.options.type&&a.error(11,1)):(!u&&t(K)&&(b.dataMin>=K?(r=K,H=0):b.dataMax<=K&&(v=K,x=0)),b.min=z(B,r,b.dataMin),b.max=z(D,v,b.dataMax));f&&(b.positiveValuesOnly&&!h&&0>=Math.min(b.min,z(b.dataMin,b.min))&&a.error(10,1),b.min=g(m(b.min),15),b.max=g(m(b.max),15));b.range&&t(b.max)&&(b.userMin=b.min=B=Math.max(b.dataMin,b.minFromRange()),b.userMax=D=b.max,b.range=null);k(b,"foundExtremes");b.beforePadding&&b.beforePadding();b.adjustForMinRange();
!(I||b.axisPointRange||b.usePercentage||A)&&t(b.min)&&t(b.max)&&(m=b.max-b.min)&&(!t(B)&&H&&(b.min-=m*H),!t(D)&&x&&(b.max+=m*x));q(n.softMin)&&(b.min=Math.min(b.min,n.softMin));q(n.softMax)&&(b.max=Math.max(b.max,n.softMax));q(n.floor)&&(b.min=Math.max(b.min,n.floor));q(n.ceiling)&&(b.max=Math.min(b.max,n.ceiling));u&&t(b.dataMin)&&(K=K||0,!t(B)&&b.min<K&&b.dataMin>=K?b.min=K:!t(D)&&b.max>K&&b.dataMax<=K&&(b.max=K));b.tickInterval=b.min===b.max||void 0===b.min||void 0===b.max?1:A&&!C&&p===b.linkedParent.options.tickPixelInterval?
C=b.linkedParent.tickInterval:z(C,this.tickAmount?(b.max-b.min)/Math.max(this.tickAmount-1,1):void 0,I?1:(b.max-b.min)*p/Math.max(b.len,p));e&&!h&&l(b.series,function(a){a.processData(b.min!==b.oldMin||b.max!==b.oldMax)});b.setAxisTranslation(!0);b.beforeSetTickPositions&&b.beforeSetTickPositions();b.postProcessTickInterval&&(b.tickInterval=b.postProcessTickInterval(b.tickInterval));b.pointRange&&!C&&(b.tickInterval=Math.max(b.pointRange,b.tickInterval));h=z(n.minTickInterval,b.isDatetimeAxis&&b.closestPointRange);
!C&&b.tickInterval<h&&(b.tickInterval=h);y||f||C||(b.tickInterval=J(b.tickInterval,null,d(b.tickInterval),z(n.allowDecimals,!(.5<b.tickInterval&&5>b.tickInterval&&1E3<b.max&&9999>b.max)),!!this.tickAmount));this.tickAmount||(b.tickInterval=b.unsquish());this.setTickPositions()},setTickPositions:function(){var a=this.options,b,c=a.tickPositions,n=a.tickPositioner,d=a.startOnTick,f=a.endOnTick;this.tickmarkOffset=this.categories&&"between"===a.tickmarkPlacement&&1===this.tickInterval?.5:0;this.minorTickInterval=
"auto"===a.minorTickInterval&&this.tickInterval?this.tickInterval/5:a.minorTickInterval;this.single=this.min===this.max&&t(this.min)&&!this.tickAmount&&(parseInt(this.min,10)===this.min||!1!==a.allowDecimals);this.tickPositions=b=c&&c.slice();!b&&(b=this.isDatetimeAxis?this.getTimeTicks(this.normalizeTimeTickInterval(this.tickInterval,a.units),this.min,this.max,a.startOfWeek,this.ordinalPositions,this.closestPointRange,!0):this.isLog?this.getLogTickPositions(this.tickInterval,this.min,this.max):this.getLinearTickPositions(this.tickInterval,
this.min,this.max),b.length>this.len&&(b=[b[0],b.pop()]),this.tickPositions=b,n&&(n=n.apply(this,[this.min,this.max])))&&(this.tickPositions=b=n);this.paddedTicks=b.slice(0);this.trimTicks(b,d,f);this.isLinked||(this.single&&2>b.length&&(this.min-=.5,this.max+=.5),c||n||this.adjustTickAmount())},trimTicks:function(a,b,c){var h=a[0],n=a[a.length-1],d=this.minPointOffset||0;if(!this.isLinked){if(b&&-Infinity!==h)this.min=h;else for(;this.min-d>a[0];)a.shift();if(c)this.max=n;else for(;this.max+d<a[a.length-
1];)a.pop();0===a.length&&t(h)&&a.push((n+h)/2)}},alignToOthers:function(){var a={},b,c=this.options;!1===this.chart.options.chart.alignTicks||!1===c.alignTicks||this.isLog||l(this.chart[this.coll],function(h){var c=h.options,c=[h.horiz?c.left:c.top,c.width,c.height,c.pane].join();h.series.length&&(a[c]?b=!0:a[c]=1)});return b},getTickAmount:function(){var a=this.options,b=a.tickAmount,c=a.tickPixelInterval;!t(a.tickInterval)&&this.len<c&&!this.isRadial&&!this.isLog&&a.startOnTick&&a.endOnTick&&(b=
2);!b&&this.alignToOthers()&&(b=Math.ceil(this.len/c)+1);4>b&&(this.finalTickAmt=b,b=5);this.tickAmount=b},adjustTickAmount:function(){var a=this.tickInterval,b=this.tickPositions,c=this.tickAmount,n=this.finalTickAmt,d=b&&b.length;if(d<c){for(;b.length<c;)b.push(g(b[b.length-1]+a));this.transA*=(d-1)/(c-1);this.max=b[b.length-1]}else d>c&&(this.tickInterval*=2,this.setTickPositions());if(t(n)){for(a=c=b.length;a--;)(3===n&&1===a%2||2>=n&&0<a&&a<c-1)&&b.splice(a,1);this.finalTickAmt=void 0}},setScale:function(){var a,
b;this.oldMin=this.min;this.oldMax=this.max;this.oldAxisLength=this.len;this.setAxisSize();b=this.len!==this.oldAxisLength;l(this.series,function(b){if(b.isDirtyData||b.isDirty||b.xAxis.isDirty)a=!0});b||a||this.isLinked||this.forceRedraw||this.userMin!==this.oldUserMin||this.userMax!==this.oldUserMax||this.alignToOthers()?(this.resetStacks&&this.resetStacks(),this.forceRedraw=!1,this.getSeriesExtremes(),this.setTickInterval(),this.oldUserMin=this.userMin,this.oldUserMax=this.userMax,this.isDirty||
(this.isDirty=b||this.min!==this.oldMin||this.max!==this.oldMax)):this.cleanStacks&&this.cleanStacks()},setExtremes:function(a,b,c,n,d){var h=this,f=h.chart;c=z(c,!0);l(h.series,function(a){delete a.kdTree});d=e(d,{min:a,max:b});k(h,"setExtremes",d,function(){h.userMin=a;h.userMax=b;h.eventArgs=d;c&&f.redraw(n)})},zoom:function(a,b){var h=this.dataMin,c=this.dataMax,n=this.options,d=Math.min(h,z(n.min,h)),n=Math.max(c,z(n.max,c));if(a!==this.min||b!==this.max)this.allowZoomOutside||(t(h)&&(a<d&&(a=
d),a>n&&(a=n)),t(c)&&(b<d&&(b=d),b>n&&(b=n))),this.displayBtn=void 0!==a||void 0!==b,this.setExtremes(a,b,!1,void 0,{trigger:"zoom"});return!0},setAxisSize:function(){var b=this.chart,c=this.options,n=c.offsets||[0,0,0,0],d=this.horiz,f=this.width=Math.round(a.relativeLength(z(c.width,b.plotWidth-n[3]+n[1]),b.plotWidth)),m=this.height=Math.round(a.relativeLength(z(c.height,b.plotHeight-n[0]+n[2]),b.plotHeight)),y=this.top=Math.round(a.relativeLength(z(c.top,b.plotTop+n[0]),b.plotHeight,b.plotTop)),
c=this.left=Math.round(a.relativeLength(z(c.left,b.plotLeft+n[3]),b.plotWidth,b.plotLeft));this.bottom=b.chartHeight-m-y;this.right=b.chartWidth-f-c;this.len=Math.max(d?f:m,0);this.pos=d?c:y},getExtremes:function(){var a=this.isLog,b=this.lin2log;return{min:a?g(b(this.min)):this.min,max:a?g(b(this.max)):this.max,dataMin:this.dataMin,dataMax:this.dataMax,userMin:this.userMin,userMax:this.userMax}},getThreshold:function(a){var b=this.isLog,h=this.lin2log,c=b?h(this.min):this.min,b=b?h(this.max):this.max;
null===a?a=c:c>a?a=c:b<a&&(a=b);return this.translate(a,0,1,0,1)},autoLabelAlign:function(a){a=(z(a,0)-90*this.side+720)%360;return 15<a&&165>a?"right":195<a&&345>a?"left":"center"},tickSize:function(a){var b=this.options,h=b[a+"Length"],c=z(b[a+"Width"],"tick"===a&&this.isXAxis?1:0);if(c&&h)return"inside"===b[a+"Position"]&&(h=-h),[h,c]},labelMetrics:function(){var a=this.tickPositions&&this.tickPositions[0]||0;return this.chart.renderer.fontMetrics(this.options.labels.style&&this.options.labels.style.fontSize,
this.ticks[a]&&this.ticks[a].label)},unsquish:function(){var a=this.options.labels,b=this.horiz,c=this.tickInterval,n=c,d=this.len/(((this.categories?1:0)+this.max-this.min)/c),f,m=a.rotation,y=this.labelMetrics(),e,q=Number.MAX_VALUE,k,A=function(a){a/=d||1;a=1<a?Math.ceil(a):1;return a*c};b?(k=!a.staggerLines&&!a.step&&(t(m)?[m]:d<z(a.autoRotationLimit,80)&&a.autoRotation))&&l(k,function(a){var b;if(a===m||a&&-90<=a&&90>=a)e=A(Math.abs(y.h/Math.sin(v*a))),b=e+Math.abs(a/360),b<q&&(q=b,f=a,n=e)}):
a.step||(n=A(y.h));this.autoRotation=k;this.labelRotation=z(f,m);return n},getSlotWidth:function(){var a=this.chart,b=this.horiz,c=this.options.labels,n=Math.max(this.tickPositions.length-(this.categories?0:1),1),d=a.margin[3];return b&&2>(c.step||0)&&!c.rotation&&(this.staggerLines||1)*this.len/n||!b&&(d&&d-a.spacing[3]||.33*a.chartWidth)},renderUnsquish:function(){var a=this.chart,b=a.renderer,c=this.tickPositions,n=this.ticks,d=this.options.labels,f=this.horiz,y=this.getSlotWidth(),e=Math.max(1,
Math.round(y-2*(d.padding||5))),q={},k=this.labelMetrics(),A=d.style&&d.style.textOverflow,x,H=0,z,C;I(d.rotation)||(q.rotation=d.rotation||0);l(c,function(a){(a=n[a])&&a.labelLength>H&&(H=a.labelLength)});this.maxLabelLength=H;if(this.autoRotation)H>e&&H>k.h?q.rotation=this.labelRotation:this.labelRotation=0;else if(y&&(x={width:e+"px"},!A))for(x.textOverflow="clip",z=c.length;!f&&z--;)if(C=c[z],e=n[C].label)e.styles&&"ellipsis"===e.styles.textOverflow?e.css({textOverflow:"clip"}):n[C].labelLength>
y&&e.css({width:y+"px"}),e.getBBox().height>this.len/c.length-(k.h-k.f)&&(e.specCss={textOverflow:"ellipsis"});q.rotation&&(x={width:(H>.5*a.chartHeight?.33*a.chartHeight:a.chartHeight)+"px"},A||(x.textOverflow="ellipsis"));if(this.labelAlign=d.align||this.autoLabelAlign(this.labelRotation))q.align=this.labelAlign;l(c,function(a){var b=(a=n[a])&&a.label;b&&(b.attr(q),x&&b.css(m(x,b.specCss)),delete b.specCss,a.rotation=q.rotation)});this.tickRotCorr=b.rotCorr(k.b,this.labelRotation||0,0!==this.side)},
hasData:function(){return this.hasVisibleSeries||t(this.min)&&t(this.max)&&!!this.tickPositions},addTitle:function(a){var b=this.chart.renderer,h=this.horiz,c=this.opposite,n=this.options.title,d;this.axisTitle||((d=n.textAlign)||(d=(h?{low:"left",middle:"center",high:"right"}:{low:c?"right":"left",middle:"center",high:c?"left":"right"})[n.align]),this.axisTitle=b.text(n.text,0,0,n.useHTML).attr({zIndex:7,rotation:n.rotation||0,align:d}).addClass("highcharts-axis-title").css(n.style).add(this.axisGroup),
this.axisTitle.isNew=!0);n.style.width||this.isRadial||this.axisTitle.css({width:this.len});this.axisTitle[a?"show":"hide"](!0)},generateTick:function(a){var b=this.ticks;b[a]?b[a].addLabel():b[a]=new n(this,a)},getOffset:function(){var a=this,c=a.chart,n=c.renderer,d=a.options,f=a.tickPositions,m=a.ticks,y=a.horiz,e=a.side,q=c.inverted&&!a.isZAxis?[1,0,3,2][e]:e,k,A,x=0,H,C=0,g=d.title,p=d.labels,I=0,J=c.axisOffset,c=c.clipOffset,K=[-1,1,1,-1][e],u=d.className,r=a.axisParent,v=this.tickSize("tick");
k=a.hasData();a.showAxis=A=k||z(d.showEmpty,!0);a.staggerLines=a.horiz&&p.staggerLines;a.axisGroup||(a.gridGroup=n.g("grid").attr({zIndex:d.gridZIndex||1}).addClass("highcharts-"+this.coll.toLowerCase()+"-grid "+(u||"")).add(r),a.axisGroup=n.g("axis").attr({zIndex:d.zIndex||2}).addClass("highcharts-"+this.coll.toLowerCase()+" "+(u||"")).add(r),a.labelGroup=n.g("axis-labels").attr({zIndex:p.zIndex||7}).addClass("highcharts-"+a.coll.toLowerCase()+"-labels "+(u||"")).add(r));k||a.isLinked?(l(f,function(b,
h){a.generateTick(b,h)}),a.renderUnsquish(),!1===p.reserveSpace||0!==e&&2!==e&&{1:"left",3:"right"}[e]!==a.labelAlign&&"center"!==a.labelAlign||l(f,function(a){I=Math.max(m[a].getLabelSize(),I)}),a.staggerLines&&(I*=a.staggerLines,a.labelOffset=I*(a.opposite?-1:1))):b(m,function(a,b){a.destroy();delete m[b]});g&&g.text&&!1!==g.enabled&&(a.addTitle(A),A&&!1!==g.reserveSpace&&(a.titleOffset=x=a.axisTitle.getBBox()[y?"height":"width"],H=g.offset,C=t(H)?0:z(g.margin,y?5:10)));a.renderLine();a.offset=
K*z(d.offset,J[e]);a.tickRotCorr=a.tickRotCorr||{x:0,y:0};n=0===e?-a.labelMetrics().h:2===e?a.tickRotCorr.y:0;C=Math.abs(I)+C;I&&(C=C-n+K*(y?z(p.y,a.tickRotCorr.y+8*K):p.x));a.axisTitleMargin=z(H,C);J[e]=Math.max(J[e],a.axisTitleMargin+x+K*a.offset,C,k&&f.length&&v?v[0]+K*a.offset:0);f=2*Math.floor(a.axisLine.strokeWidth()/2);0<d.offset&&(f-=2*d.offset);c[q]=Math.max(c[q]||f,f)},getLinePath:function(a){var b=this.chart,h=this.opposite,c=this.offset,n=this.horiz,d=this.left+(h?this.width:0)+c,c=b.chartHeight-
this.bottom-(h?this.height:0)+c;h&&(a*=-1);return b.renderer.crispLine(["M",n?this.left:d,n?c:this.top,"L",n?b.chartWidth-this.right:d,n?c:b.chartHeight-this.bottom],a)},renderLine:function(){this.axisLine||(this.axisLine=this.chart.renderer.path().addClass("highcharts-axis-line").add(this.axisGroup),this.axisLine.attr({stroke:this.options.lineColor,"stroke-width":this.options.lineWidth,zIndex:7}))},getTitlePosition:function(){var a=this.horiz,b=this.left,c=this.top,n=this.len,d=this.options.title,
f=a?b:c,m=this.opposite,e=this.offset,y=d.x||0,q=d.y||0,k=this.axisTitle,A=this.chart.renderer.fontMetrics(d.style&&d.style.fontSize,k),k=Math.max(k.getBBox(null,0).height-A.h-1,0),n={low:f+(a?0:n),middle:f+n/2,high:f+(a?n:0)}[d.align],b=(a?c+this.height:b)+(a?1:-1)*(m?-1:1)*this.axisTitleMargin+[-k,k,A.f,-k][this.side];return{x:a?n+y:b+(m?this.width:0)+e+y,y:a?b+q-(m?this.height:0)+e:n+q}},renderMinorTick:function(a){var b=this.chart.hasRendered&&q(this.oldMin),c=this.minorTicks;c[a]||(c[a]=new n(this,
a,"minor"));b&&c[a].isNew&&c[a].render(null,!0);c[a].render(null,!1,1)},renderTick:function(a,b){var c=this.isLinked,h=this.ticks,d=this.chart.hasRendered&&q(this.oldMin);if(!c||a>=this.min&&a<=this.max)h[a]||(h[a]=new n(this,a)),d&&h[a].isNew&&h[a].render(b,!0,.1),h[a].render(b)},render:function(){var c=this,d=c.chart,f=c.options,m=c.isLog,e=c.lin2log,y=c.isLinked,k=c.tickPositions,x=c.axisTitle,H=c.ticks,z=c.minorTicks,C=c.alternateBands,g=f.stackLabels,p=f.alternateGridColor,I=c.tickmarkOffset,
J=c.axisLine,K=c.showAxis,t=B(d.renderer.globalAnimation),u,r;c.labelEdge.length=0;c.overlap=!1;l([H,z,C],function(a){b(a,function(a){a.isActive=!1})});if(c.hasData()||y)c.minorTickInterval&&!c.categories&&l(c.getMinorTickPositions(),function(a){c.renderMinorTick(a)}),k.length&&(l(k,function(a,b){c.renderTick(a,b)}),I&&(0===c.min||c.single)&&(H[-1]||(H[-1]=new n(c,-1,null,!0)),H[-1].render(-1))),p&&l(k,function(b,h){r=void 0!==k[h+1]?k[h+1]+I:c.max-I;0===h%2&&b<c.max&&r<=c.max+(d.polar?-I:I)&&(C[b]||
(C[b]=new a.PlotLineOrBand(c)),u=b+I,C[b].options={from:m?e(u):u,to:m?e(r):r,color:p},C[b].render(),C[b].isActive=!0)}),c._addedPlotLB||(l((f.plotLines||[]).concat(f.plotBands||[]),function(a){c.addPlotBandOrLine(a)}),c._addedPlotLB=!0);l([H,z,C],function(a){var c,h=[],n=t.duration;b(a,function(a,b){a.isActive||(a.render(b,!1,0),a.isActive=!1,h.push(b))});A(function(){for(c=h.length;c--;)a[h[c]]&&!a[h[c]].isActive&&(a[h[c]].destroy(),delete a[h[c]])},a!==C&&d.hasRendered&&n?n:0)});J&&(J[J.isPlaced?
"animate":"attr"]({d:this.getLinePath(J.strokeWidth())}),J.isPlaced=!0,J[K?"show":"hide"](!0));x&&K&&(f=c.getTitlePosition(),q(f.y)?(x[x.isNew?"attr":"animate"](f),x.isNew=!1):(x.attr("y",-9999),x.isNew=!0));g&&g.enabled&&c.renderStackTotals();c.isDirty=!1},redraw:function(){this.visible&&(this.render(),l(this.plotLinesAndBands,function(a){a.render()}));l(this.series,function(a){a.isDirty=!0})},keepProps:"extKey hcEvents names series userMax userMin".split(" "),destroy:function(a){var c=this,h=c.stacks,
n=c.plotLinesAndBands,d;a||K(c);b(h,function(a,b){u(a);h[b]=null});l([c.ticks,c.minorTicks,c.alternateBands],function(a){u(a)});if(n)for(a=n.length;a--;)n[a].destroy();l("stackTotalGroup axisLine axisTitle axisGroup gridGroup labelGroup cross".split(" "),function(a){c[a]&&(c[a]=c[a].destroy())});for(d in c.plotLinesAndBandsGroups)c.plotLinesAndBandsGroups[d]=c.plotLinesAndBandsGroups[d].destroy();b(c,function(a,b){-1===C(b,c.keepProps)&&delete c[b]})},drawCrosshair:function(a,b){var c,h=this.crosshair,
n=z(h.snap,!0),d,f=this.cross;a||(a=this.cross&&this.cross.e);this.crosshair&&!1!==(t(b)||!n)?(n?t(b)&&(d=this.isXAxis?b.plotX:this.len-b.plotY):d=a&&(this.horiz?a.chartX-this.pos:this.len-a.chartY+this.pos),t(d)&&(c=this.getPlotLinePath(b&&(this.isXAxis?b.x:z(b.stackY,b.y)),null,null,null,d)||null),t(c)?(b=this.categories&&!this.isRadial,f||(this.cross=f=this.chart.renderer.path().addClass("highcharts-crosshair highcharts-crosshair-"+(b?"category ":"thin ")+h.className).attr({zIndex:z(h.zIndex,2)}).add(),
f.attr({stroke:h.color||(b?r("#ccd6eb").setOpacity(.25).get():"#cccccc"),"stroke-width":z(h.width,1)}),h.dashStyle&&f.attr({dashstyle:h.dashStyle})),f.show().attr({d:c}),b&&!h.width&&f.attr({"stroke-width":this.transA}),this.cross.e=a):this.hideCrosshair()):this.hideCrosshair()},hideCrosshair:function(){this.cross&&this.cross.hide()}});return a.Axis=H}(M);(function(a){var D=a.Axis,B=a.Date,G=a.dateFormat,E=a.defaultOptions,r=a.defined,g=a.each,p=a.extend,t=a.getMagnitude,v=a.getTZOffset,u=a.normalizeTickInterval,
l=a.pick,e=a.timeUnits;D.prototype.getTimeTicks=function(a,f,d,x){var k=[],c={},q=E.global.useUTC,I,m=new B(f-Math.max(v(f),v(d))),J=B.hcMakeTime,b=a.unitRange,z=a.count,K,y;if(r(f)){m[B.hcSetMilliseconds](b>=e.second?0:z*Math.floor(m.getMilliseconds()/z));if(b>=e.second)m[B.hcSetSeconds](b>=e.minute?0:z*Math.floor(m.getSeconds()/z));if(b>=e.minute)m[B.hcSetMinutes](b>=e.hour?0:z*Math.floor(m[B.hcGetMinutes]()/z));if(b>=e.hour)m[B.hcSetHours](b>=e.day?0:z*Math.floor(m[B.hcGetHours]()/z));if(b>=e.day)m[B.hcSetDate](b>=
e.month?1:z*Math.floor(m[B.hcGetDate]()/z));b>=e.month&&(m[B.hcSetMonth](b>=e.year?0:z*Math.floor(m[B.hcGetMonth]()/z)),I=m[B.hcGetFullYear]());if(b>=e.year)m[B.hcSetFullYear](I-I%z);if(b===e.week)m[B.hcSetDate](m[B.hcGetDate]()-m[B.hcGetDay]()+l(x,1));I=m[B.hcGetFullYear]();x=m[B.hcGetMonth]();var A=m[B.hcGetDate](),n=m[B.hcGetHours]();if(B.hcTimezoneOffset||B.hcGetTimezoneOffset)y=(!q||!!B.hcGetTimezoneOffset)&&(d-f>4*e.month||v(f)!==v(d)),m=m.getTime(),K=v(m),m=new B(m+K);q=m.getTime();for(f=1;q<
d;)k.push(q),q=b===e.year?J(I+f*z,0):b===e.month?J(I,x+f*z):!y||b!==e.day&&b!==e.week?y&&b===e.hour?J(I,x,A,n+f*z,0,0,K)-K:q+b*z:J(I,x,A+f*z*(b===e.day?1:7)),f++;k.push(q);b<=e.hour&&1E4>k.length&&g(k,function(a){0===a%18E5&&"000000000"===G("%H%M%S%L",a)&&(c[a]="day")})}k.info=p(a,{higherRanks:c,totalRange:b*z});return k};D.prototype.normalizeTimeTickInterval=function(a,f){var d=f||[["millisecond",[1,2,5,10,20,25,50,100,200,500]],["second",[1,2,5,10,15,30]],["minute",[1,2,5,10,15,30]],["hour",[1,
2,3,4,6,8,12]],["day",[1,2]],["week",[1,2]],["month",[1,2,3,4,6]],["year",null]];f=d[d.length-1];var k=e[f[0]],l=f[1],c;for(c=0;c<d.length&&!(f=d[c],k=e[f[0]],l=f[1],d[c+1]&&a<=(k*l[l.length-1]+e[d[c+1][0]])/2);c++);k===e.year&&a<5*k&&(l=[1,2,5]);a=u(a/k,l,"year"===f[0]?Math.max(t(a/k),1):1);return{unitRange:k,count:a,unitName:f[0]}}})(M);(function(a){var D=a.Axis,B=a.getMagnitude,G=a.map,E=a.normalizeTickInterval,r=a.pick;D.prototype.getLogTickPositions=function(a,p,t,v){var g=this.options,l=this.len,
e=this.lin2log,k=this.log2lin,f=[];v||(this._minorAutoInterval=null);if(.5<=a)a=Math.round(a),f=this.getLinearTickPositions(a,p,t);else if(.08<=a)for(var l=Math.floor(p),d,x,C,c,q,g=.3<a?[1,2,4]:.15<a?[1,2,4,6,8]:[1,2,3,4,5,6,7,8,9];l<t+1&&!q;l++)for(x=g.length,d=0;d<x&&!q;d++)C=k(e(l)*g[d]),C>p&&(!v||c<=t)&&void 0!==c&&f.push(c),c>t&&(q=!0),c=C;else p=e(p),t=e(t),a=g[v?"minorTickInterval":"tickInterval"],a=r("auto"===a?null:a,this._minorAutoInterval,g.tickPixelInterval/(v?5:1)*(t-p)/((v?l/this.tickPositions.length:
l)||1)),a=E(a,null,B(a)),f=G(this.getLinearTickPositions(a,p,t),k),v||(this._minorAutoInterval=a/5);v||(this.tickInterval=a);return f};D.prototype.log2lin=function(a){return Math.log(a)/Math.LN10};D.prototype.lin2log=function(a){return Math.pow(10,a)}})(M);(function(a,D){var B=a.arrayMax,G=a.arrayMin,E=a.defined,r=a.destroyObjectProperties,g=a.each,p=a.erase,t=a.merge,v=a.pick;a.PlotLineOrBand=function(a,l){this.axis=a;l&&(this.options=l,this.id=l.id)};a.PlotLineOrBand.prototype={render:function(){var g=
this,l=g.axis,e=l.horiz,k=g.options,f=k.label,d=g.label,x=k.to,C=k.from,c=k.value,q=E(C)&&E(x),p=E(c),m=g.svgElem,J=!m,b=[],z=k.color,K=v(k.zIndex,0),y=k.events,b={"class":"highcharts-plot-"+(q?"band ":"line ")+(k.className||"")},A={},n=l.chart.renderer,H=q?"bands":"lines",h=l.log2lin;l.isLog&&(C=h(C),x=h(x),c=h(c));p?(b={stroke:z,"stroke-width":k.width},k.dashStyle&&(b.dashstyle=k.dashStyle)):q&&(z&&(b.fill=z),k.borderWidth&&(b.stroke=k.borderColor,b["stroke-width"]=k.borderWidth));A.zIndex=K;H+=
"-"+K;(z=l.plotLinesAndBandsGroups[H])||(l.plotLinesAndBandsGroups[H]=z=n.g("plot-"+H).attr(A).add());J&&(g.svgElem=m=n.path().attr(b).add(z));if(p)b=l.getPlotLinePath(c,m.strokeWidth());else if(q)b=l.getPlotBandPath(C,x,k);else return;J&&b&&b.length?(m.attr({d:b}),y&&a.objectEach(y,function(a,b){m.on(b,function(a){y[b].apply(g,[a])})})):m&&(b?(m.show(),m.animate({d:b})):(m.hide(),d&&(g.label=d=d.destroy())));f&&E(f.text)&&b&&b.length&&0<l.width&&0<l.height&&!b.flat?(f=t({align:e&&q&&"center",x:e?
!q&&4:10,verticalAlign:!e&&q&&"middle",y:e?q?16:10:q?6:-4,rotation:e&&!q&&90},f),this.renderLabel(f,b,q,K)):d&&d.hide();return g},renderLabel:function(a,l,e,k){var f=this.label,d=this.axis.chart.renderer;f||(f={align:a.textAlign||a.align,rotation:a.rotation,"class":"highcharts-plot-"+(e?"band":"line")+"-label "+(a.className||"")},f.zIndex=k,this.label=f=d.text(a.text,0,0,a.useHTML).attr(f).add(),f.css(a.style));k=[l[1],l[4],e?l[6]:l[1]];l=[l[2],l[5],e?l[7]:l[2]];e=G(k);d=G(l);f.align(a,!1,{x:e,y:d,
width:B(k)-e,height:B(l)-d});f.show()},destroy:function(){p(this.axis.plotLinesAndBands,this);delete this.axis;r(this)}};a.extend(D.prototype,{getPlotBandPath:function(a,l){var e=this.getPlotLinePath(l,null,null,!0),k=this.getPlotLinePath(a,null,null,!0),f=this.horiz,d=1;a=a<this.min&&l<this.min||a>this.max&&l>this.max;k&&e?(a&&(k.flat=k.toString()===e.toString(),d=0),k.push(f&&e[4]===k[4]?e[4]+d:e[4],f||e[5]!==k[5]?e[5]:e[5]+d,f&&e[1]===k[1]?e[1]+d:e[1],f||e[2]!==k[2]?e[2]:e[2]+d)):k=null;return k},
addPlotBand:function(a){return this.addPlotBandOrLine(a,"plotBands")},addPlotLine:function(a){return this.addPlotBandOrLine(a,"plotLines")},addPlotBandOrLine:function(g,l){var e=(new a.PlotLineOrBand(this,g)).render(),k=this.userOptions;e&&(l&&(k[l]=k[l]||[],k[l].push(g)),this.plotLinesAndBands.push(e));return e},removePlotBandOrLine:function(a){for(var l=this.plotLinesAndBands,e=this.options,k=this.userOptions,f=l.length;f--;)l[f].id===a&&l[f].destroy();g([e.plotLines||[],k.plotLines||[],e.plotBands||
[],k.plotBands||[]],function(d){for(f=d.length;f--;)d[f].id===a&&p(d,d[f])})},removePlotBand:function(a){this.removePlotBandOrLine(a)},removePlotLine:function(a){this.removePlotBandOrLine(a)}})})(M,S);(function(a){var D=a.dateFormat,B=a.each,G=a.extend,E=a.format,r=a.isNumber,g=a.map,p=a.merge,t=a.pick,v=a.splat,u=a.syncTimeout,l=a.timeUnits;a.Tooltip=function(){this.init.apply(this,arguments)};a.Tooltip.prototype={init:function(a,k){this.chart=a;this.options=k;this.crosshairs=[];this.now={x:0,y:0};
this.isHidden=!0;this.split=k.split&&!a.inverted;this.shared=k.shared||this.split},cleanSplit:function(a){B(this.chart.series,function(e){var f=e&&e.tt;f&&(!f.isActive||a?e.tt=f.destroy():f.isActive=!1)})},getLabel:function(){var a=this.chart.renderer,k=this.options;this.label||(this.split?this.label=a.g("tooltip"):(this.label=a.label("",0,0,k.shape||"callout",null,null,k.useHTML,null,"tooltip").attr({padding:k.padding,r:k.borderRadius}),this.label.attr({fill:k.backgroundColor,"stroke-width":k.borderWidth}).css(k.style).shadow(k.shadow)),
this.label.attr({zIndex:8}).add());return this.label},update:function(a){this.destroy();p(!0,this.chart.options.tooltip.userOptions,a);this.init(this.chart,p(!0,this.options,a))},destroy:function(){this.label&&(this.label=this.label.destroy());this.split&&this.tt&&(this.cleanSplit(this.chart,!0),this.tt=this.tt.destroy());clearTimeout(this.hideTimer);clearTimeout(this.tooltipTimeout)},move:function(a,k,f,d){var e=this,l=e.now,c=!1!==e.options.animation&&!e.isHidden&&(1<Math.abs(a-l.x)||1<Math.abs(k-
l.y)),q=e.followPointer||1<e.len;G(l,{x:c?(2*l.x+a)/3:a,y:c?(l.y+k)/2:k,anchorX:q?void 0:c?(2*l.anchorX+f)/3:f,anchorY:q?void 0:c?(l.anchorY+d)/2:d});e.getLabel().attr(l);c&&(clearTimeout(this.tooltipTimeout),this.tooltipTimeout=setTimeout(function(){e&&e.move(a,k,f,d)},32))},hide:function(a){var e=this;clearTimeout(this.hideTimer);a=t(a,this.options.hideDelay,500);this.isHidden||(this.hideTimer=u(function(){e.getLabel()[a?"fadeOut":"hide"]();e.isHidden=!0},a))},getAnchor:function(a,k){var f,d=this.chart,
e=d.inverted,l=d.plotTop,c=d.plotLeft,q=0,p=0,m,J;a=v(a);f=a[0].tooltipPos;this.followPointer&&k&&(void 0===k.chartX&&(k=d.pointer.normalize(k)),f=[k.chartX-d.plotLeft,k.chartY-l]);f||(B(a,function(a){m=a.series.yAxis;J=a.series.xAxis;q+=a.plotX+(!e&&J?J.left-c:0);p+=(a.plotLow?(a.plotLow+a.plotHigh)/2:a.plotY)+(!e&&m?m.top-l:0)}),q/=a.length,p/=a.length,f=[e?d.plotWidth-p:q,this.shared&&!e&&1<a.length&&k?k.chartY-l:e?d.plotHeight-q:p]);return g(f,Math.round)},getPosition:function(a,k,f){var d=this.chart,
e=this.distance,l={},c=f.h||0,q,g=["y",d.chartHeight,k,f.plotY+d.plotTop,d.plotTop,d.plotTop+d.plotHeight],m=["x",d.chartWidth,a,f.plotX+d.plotLeft,d.plotLeft,d.plotLeft+d.plotWidth],p=!this.followPointer&&t(f.ttBelow,!d.inverted===!!f.negative),b=function(a,b,d,h,f,m){var n=d<h-e,y=h+e+d<b,q=h-e-d;h+=e;if(p&&y)l[a]=h;else if(!p&&n)l[a]=q;else if(n)l[a]=Math.min(m-d,0>q-c?q:q-c);else if(y)l[a]=Math.max(f,h+c+d>b?h:h+c);else return!1},z=function(a,b,c,h){var n;h<e||h>b-e?n=!1:l[a]=h<c/2?1:h>b-c/2?
b-c-2:h-c/2;return n},K=function(a){var b=g;g=m;m=b;q=a},y=function(){!1!==b.apply(0,g)?!1!==z.apply(0,m)||q||(K(!0),y()):q?l.x=l.y=0:(K(!0),y())};(d.inverted||1<this.len)&&K();y();return l},defaultFormatter:function(a){var e=this.points||v(this),f;f=[a.tooltipFooterHeaderFormatter(e[0])];f=f.concat(a.bodyFormatter(e));f.push(a.tooltipFooterHeaderFormatter(e[0],!0));return f},refresh:function(a,k){var f,d=this.options,e,l=a,c,q={},g=[];f=d.formatter||this.defaultFormatter;var q=this.shared,m;d.enabled&&
(clearTimeout(this.hideTimer),this.followPointer=v(l)[0].series.tooltipOptions.followPointer,c=this.getAnchor(l,k),k=c[0],e=c[1],!q||l.series&&l.series.noSharedTooltip?q=l.getLabelConfig():(B(l,function(a){a.setState("hover");g.push(a.getLabelConfig())}),q={x:l[0].category,y:l[0].y},q.points=g,l=l[0]),this.len=g.length,q=f.call(q,this),m=l.series,this.distance=t(m.tooltipOptions.distance,16),!1===q?this.hide():(f=this.getLabel(),this.isHidden&&f.attr({opacity:1}).show(),this.split?this.renderSplit(q,
a):(d.style.width||f.css({width:this.chart.spacingBox.width}),f.attr({text:q&&q.join?q.join(""):q}),f.removeClass(/highcharts-color-[\d]+/g).addClass("highcharts-color-"+t(l.colorIndex,m.colorIndex)),f.attr({stroke:d.borderColor||l.color||m.color||"#666666"}),this.updatePosition({plotX:k,plotY:e,negative:l.negative,ttBelow:l.ttBelow,h:c[2]||0})),this.isHidden=!1))},renderSplit:function(e,k){var f=this,d=[],l=this.chart,g=l.renderer,c=!0,q=this.options,p=0,m=this.getLabel();B(e.slice(0,k.length+1),
function(a,b){if(!1!==a){b=k[b-1]||{isHeader:!0,plotX:k[0].plotX};var e=b.series||f,x=e.tt,y=b.series||{},A="highcharts-color-"+t(b.colorIndex,y.colorIndex,"none");x||(e.tt=x=g.label(null,null,null,"callout").addClass("highcharts-tooltip-box "+A).attr({padding:q.padding,r:q.borderRadius,fill:q.backgroundColor,stroke:q.borderColor||b.color||y.color||"#333333","stroke-width":q.borderWidth}).add(m));x.isActive=!0;x.attr({text:a});x.css(q.style).shadow(q.shadow);a=x.getBBox();y=a.width+x.strokeWidth();
b.isHeader?(p=a.height,y=Math.max(0,Math.min(b.plotX+l.plotLeft-y/2,l.chartWidth-y))):y=b.plotX+l.plotLeft-t(q.distance,16)-y;0>y&&(c=!1);a=(b.series&&b.series.yAxis&&b.series.yAxis.pos)+(b.plotY||0);a-=l.plotTop;d.push({target:b.isHeader?l.plotHeight+p:a,rank:b.isHeader?1:0,size:e.tt.getBBox().height+1,point:b,x:y,tt:x})}});this.cleanSplit();a.distribute(d,l.plotHeight+p);B(d,function(a){var b=a.point,d=b.series;a.tt.attr({visibility:void 0===a.pos?"hidden":"inherit",x:c||b.isHeader?a.x:b.plotX+
l.plotLeft+t(q.distance,16),y:a.pos+l.plotTop,anchorX:b.isHeader?b.plotX+l.plotLeft:b.plotX+d.xAxis.pos,anchorY:b.isHeader?a.pos+l.plotTop-15:b.plotY+d.yAxis.pos})})},updatePosition:function(a){var e=this.chart,f=this.getLabel(),f=(this.options.positioner||this.getPosition).call(this,f.width,f.height,a);this.move(Math.round(f.x),Math.round(f.y||0),a.plotX+e.plotLeft,a.plotY+e.plotTop)},getDateFormat:function(a,k,f,d){var e=D("%m-%d %H:%M:%S.%L",k),g,c,q={millisecond:15,second:12,minute:9,hour:6,day:3},
p="millisecond";for(c in l){if(a===l.week&&+D("%w",k)===f&&"00:00:00.000"===e.substr(6)){c="week";break}if(l[c]>a){c=p;break}if(q[c]&&e.substr(q[c])!=="01-01 00:00:00.000".substr(q[c]))break;"week"!==c&&(p=c)}c&&(g=d[c]);return g},getXDateFormat:function(a,k,f){k=k.dateTimeLabelFormats;var d=f&&f.closestPointRange;return(d?this.getDateFormat(d,a.x,f.options.startOfWeek,k):k.day)||k.year},tooltipFooterHeaderFormatter:function(a,k){var f=k?"footer":"header";k=a.series;var d=k.tooltipOptions,e=d.xDateFormat,
l=k.xAxis,c=l&&"datetime"===l.options.type&&r(a.key),f=d[f+"Format"];c&&!e&&(e=this.getXDateFormat(a,d,l));c&&e&&(f=f.replace("{point.key}","{point.key:"+e+"}"));return E(f,{point:a,series:k})},bodyFormatter:function(a){return g(a,function(a){var f=a.series.tooltipOptions;return(f.pointFormatter||a.point.tooltipFormatter).call(a.point,f.pointFormat)})}}})(M);(function(a){var D=a.addEvent,B=a.attr,G=a.charts,E=a.color,r=a.css,g=a.defined,p=a.each,t=a.extend,v=a.find,u=a.fireEvent,l=a.isObject,e=a.offset,
k=a.pick,f=a.removeEvent,d=a.splat,x=a.Tooltip,C=a.win;a.Pointer=function(a,d){this.init(a,d)};a.Pointer.prototype={init:function(a,d){this.options=d;this.chart=a;this.runChartClick=d.chart.events&&!!d.chart.events.click;this.pinchDown=[];this.lastValidTouch={};x&&(a.tooltip=new x(a,d.tooltip),this.followTouchMove=k(d.tooltip.followTouchMove,!0));this.setDOMEvents()},zoomOption:function(a){var c=this.chart,d=c.options.chart,f=d.zoomType||"",c=c.inverted;/touch/.test(a.type)&&(f=k(d.pinchType,f));
this.zoomX=a=/x/.test(f);this.zoomY=f=/y/.test(f);this.zoomHor=a&&!c||f&&c;this.zoomVert=f&&!c||a&&c;this.hasZoom=a||f},normalize:function(a,d){var c,f;a=a||C.event;a.target||(a.target=a.srcElement);f=a.touches?a.touches.length?a.touches.item(0):a.changedTouches[0]:a;d||(this.chartPosition=d=e(this.chart.container));void 0===f.pageX?(c=Math.max(a.x,a.clientX-d.left),d=a.y):(c=f.pageX-d.left,d=f.pageY-d.top);return t(a,{chartX:Math.round(c),chartY:Math.round(d)})},getCoordinates:function(a){var c=
{xAxis:[],yAxis:[]};p(this.chart.axes,function(d){c[d.isXAxis?"xAxis":"yAxis"].push({axis:d,value:d.toValue(a[d.horiz?"chartX":"chartY"])})});return c},findNearestKDPoint:function(a,d,f){var c;p(a,function(a){var b=!(a.noSharedTooltip&&d)&&0>a.options.findNearestPointBy.indexOf("y");a=a.searchPoint(f,b);if((b=l(a,!0))&&!(b=!l(c,!0)))var b=c.distX-a.distX,m=c.dist-a.dist,e=(a.series.group&&a.series.group.zIndex)-(c.series.group&&c.series.group.zIndex),b=0<(0!==b&&d?b:0!==m?m:0!==e?e:c.series.index>
a.series.index?-1:1);b&&(c=a)});return c},getPointFromEvent:function(a){a=a.target;for(var c;a&&!c;)c=a.point,a=a.parentNode;return c},getChartCoordinatesFromPoint:function(a,d){var c=a.series,f=c.xAxis,c=c.yAxis;if(f&&c)return d?{chartX:f.len+f.pos-a.clientX,chartY:c.len+c.pos-a.plotY}:{chartX:a.clientX+f.pos,chartY:a.plotY+c.pos}},getHoverData:function(c,d,f,m,e,b){var q,x=[];m=!(!m||!c);var y=d&&!d.stickyTracking?[d]:a.grep(f,function(a){return a.visible&&!(!e&&a.directTouch)&&k(a.options.enableMouseTracking,
!0)&&a.stickyTracking});d=(q=m?c:this.findNearestKDPoint(y,e,b))&&q.series;q&&(e&&!d.noSharedTooltip?(y=a.grep(f,function(a){return a.visible&&!(!e&&a.directTouch)&&k(a.options.enableMouseTracking,!0)&&!a.noSharedTooltip}),p(y,function(a){a=v(a.points,function(a){return a.x===q.x});l(a)&&!a.isNull&&x.push(a)})):x.push(q));return{hoverPoint:q,hoverSeries:d,hoverPoints:x}},runPointActions:function(c,d){var f=this.chart,m=f.tooltip,e=m?m.shared:!1,b=d||f.hoverPoint,q=b&&b.series||f.hoverSeries,q=this.getHoverData(b,
q,f.series,!!d||q&&q.directTouch&&this.isDirectTouch,e,c),l,b=q.hoverPoint;l=q.hoverPoints;d=(q=q.hoverSeries)&&q.tooltipOptions.followPointer;e=e&&q&&!q.noSharedTooltip;if(b&&(b!==f.hoverPoint||m&&m.isHidden)){p(f.hoverPoints||[],function(b){-1===a.inArray(b,l)&&b.setState()});p(l||[],function(a){a.setState("hover")});if(f.hoverSeries!==q)q.onMouseOver();f.hoverPoint&&f.hoverPoint.firePointEvent("mouseOut");b.firePointEvent("mouseOver");f.hoverPoints=l;f.hoverPoint=b;m&&m.refresh(e?l:b,c)}else d&&
m&&!m.isHidden&&(b=m.getAnchor([{}],c),m.updatePosition({plotX:b[0],plotY:b[1]}));this.unDocMouseMove||(this.unDocMouseMove=D(f.container.ownerDocument,"mousemove",function(b){var c=G[a.hoverChartIndex];if(c)c.pointer.onDocumentMouseMove(b)}));p(f.axes,function(b){var d=k(b.crosshair.snap,!0),n=d?a.find(l,function(a){return a.series[b.coll]===b}):void 0;n||!d?b.drawCrosshair(c,n):b.hideCrosshair()})},reset:function(a,f){var c=this.chart,m=c.hoverSeries,e=c.hoverPoint,b=c.hoverPoints,q=c.tooltip,k=
q&&q.shared?b:e;a&&k&&p(d(k),function(b){b.series.isCartesian&&void 0===b.plotX&&(a=!1)});if(a)q&&k&&(q.refresh(k),e&&(e.setState(e.state,!0),p(c.axes,function(a){a.crosshair&&a.drawCrosshair(null,e)})));else{if(e)e.onMouseOut();b&&p(b,function(a){a.setState()});if(m)m.onMouseOut();q&&q.hide(f);this.unDocMouseMove&&(this.unDocMouseMove=this.unDocMouseMove());p(c.axes,function(a){a.hideCrosshair()});this.hoverX=c.hoverPoints=c.hoverPoint=null}},scaleGroups:function(a,d){var c=this.chart,f;p(c.series,
function(m){f=a||m.getPlotBox();m.xAxis&&m.xAxis.zoomEnabled&&m.group&&(m.group.attr(f),m.markerGroup&&(m.markerGroup.attr(f),m.markerGroup.clip(d?c.clipRect:null)),m.dataLabelsGroup&&m.dataLabelsGroup.attr(f))});c.clipRect.attr(d||c.clipBox)},dragStart:function(a){var c=this.chart;c.mouseIsDown=a.type;c.cancelClick=!1;c.mouseDownX=this.mouseDownX=a.chartX;c.mouseDownY=this.mouseDownY=a.chartY},drag:function(a){var c=this.chart,d=c.options.chart,f=a.chartX,e=a.chartY,b=this.zoomHor,k=this.zoomVert,
l=c.plotLeft,y=c.plotTop,A=c.plotWidth,n=c.plotHeight,x,h=this.selectionMarker,w=this.mouseDownX,g=this.mouseDownY,C=d.panKey&&a[d.panKey+"Key"];h&&h.touch||(f<l?f=l:f>l+A&&(f=l+A),e<y?e=y:e>y+n&&(e=y+n),this.hasDragged=Math.sqrt(Math.pow(w-f,2)+Math.pow(g-e,2)),10<this.hasDragged&&(x=c.isInsidePlot(w-l,g-y),c.hasCartesianSeries&&(this.zoomX||this.zoomY)&&x&&!C&&!h&&(this.selectionMarker=h=c.renderer.rect(l,y,b?1:A,k?1:n,0).attr({fill:d.selectionMarkerFill||E("#335cad").setOpacity(.25).get(),"class":"highcharts-selection-marker",
zIndex:7}).add()),h&&b&&(f-=w,h.attr({width:Math.abs(f),x:(0<f?0:f)+w})),h&&k&&(f=e-g,h.attr({height:Math.abs(f),y:(0<f?0:f)+g})),x&&!h&&d.panning&&c.pan(a,d.panning)))},drop:function(a){var c=this,d=this.chart,f=this.hasPinched;if(this.selectionMarker){var e={originalEvent:a,xAxis:[],yAxis:[]},b=this.selectionMarker,k=b.attr?b.attr("x"):b.x,l=b.attr?b.attr("y"):b.y,y=b.attr?b.attr("width"):b.width,A=b.attr?b.attr("height"):b.height,n;if(this.hasDragged||f)p(d.axes,function(b){if(b.zoomEnabled&&g(b.min)&&
(f||c[{xAxis:"zoomX",yAxis:"zoomY"}[b.coll]])){var h=b.horiz,d="touchend"===a.type?b.minPixelPadding:0,m=b.toValue((h?k:l)+d),h=b.toValue((h?k+y:l+A)-d);e[b.coll].push({axis:b,min:Math.min(m,h),max:Math.max(m,h)});n=!0}}),n&&u(d,"selection",e,function(a){d.zoom(t(a,f?{animation:!1}:null))});this.selectionMarker=this.selectionMarker.destroy();f&&this.scaleGroups()}d&&(r(d.container,{cursor:d._cursor}),d.cancelClick=10<this.hasDragged,d.mouseIsDown=this.hasDragged=this.hasPinched=!1,this.pinchDown=
[])},onContainerMouseDown:function(a){a=this.normalize(a);this.zoomOption(a);a.preventDefault&&a.preventDefault();this.dragStart(a)},onDocumentMouseUp:function(c){G[a.hoverChartIndex]&&G[a.hoverChartIndex].pointer.drop(c)},onDocumentMouseMove:function(a){var c=this.chart,d=this.chartPosition;a=this.normalize(a,d);!d||this.inClass(a.target,"highcharts-tracker")||c.isInsidePlot(a.chartX-c.plotLeft,a.chartY-c.plotTop)||this.reset()},onContainerMouseLeave:function(c){var d=G[a.hoverChartIndex];d&&(c.relatedTarget||
c.toElement)&&(d.pointer.reset(),d.pointer.chartPosition=null)},onContainerMouseMove:function(c){var d=this.chart;g(a.hoverChartIndex)&&G[a.hoverChartIndex]&&G[a.hoverChartIndex].mouseIsDown||(a.hoverChartIndex=d.index);c=this.normalize(c);c.returnValue=!1;"mousedown"===d.mouseIsDown&&this.drag(c);!this.inClass(c.target,"highcharts-tracker")&&!d.isInsidePlot(c.chartX-d.plotLeft,c.chartY-d.plotTop)||d.openMenu||this.runPointActions(c)},inClass:function(a,d){for(var c;a;){if(c=B(a,"class")){if(-1!==
c.indexOf(d))return!0;if(-1!==c.indexOf("highcharts-container"))return!1}a=a.parentNode}},onTrackerMouseOut:function(a){var c=this.chart.hoverSeries;a=a.relatedTarget||a.toElement;this.isDirectTouch=!1;if(!(!c||!a||c.stickyTracking||this.inClass(a,"highcharts-tooltip")||this.inClass(a,"highcharts-series-"+c.index)&&this.inClass(a,"highcharts-tracker")))c.onMouseOut()},onContainerClick:function(a){var c=this.chart,d=c.hoverPoint,f=c.plotLeft,e=c.plotTop;a=this.normalize(a);c.cancelClick||(d&&this.inClass(a.target,
"highcharts-tracker")?(u(d.series,"click",t(a,{point:d})),c.hoverPoint&&d.firePointEvent("click",a)):(t(a,this.getCoordinates(a)),c.isInsidePlot(a.chartX-f,a.chartY-e)&&u(c,"click",a)))},setDOMEvents:function(){var c=this,d=c.chart.container,f=d.ownerDocument;d.onmousedown=function(a){c.onContainerMouseDown(a)};d.onmousemove=function(a){c.onContainerMouseMove(a)};d.onclick=function(a){c.onContainerClick(a)};D(d,"mouseleave",c.onContainerMouseLeave);1===a.chartCount&&D(f,"mouseup",c.onDocumentMouseUp);
a.hasTouch&&(d.ontouchstart=function(a){c.onContainerTouchStart(a)},d.ontouchmove=function(a){c.onContainerTouchMove(a)},1===a.chartCount&&D(f,"touchend",c.onDocumentTouchEnd))},destroy:function(){var c=this,d=this.chart.container.ownerDocument;c.unDocMouseMove&&c.unDocMouseMove();f(c.chart.container,"mouseleave",c.onContainerMouseLeave);a.chartCount||(f(d,"mouseup",c.onDocumentMouseUp),a.hasTouch&&f(d,"touchend",c.onDocumentTouchEnd));clearInterval(c.tooltipTimeout);a.objectEach(c,function(a,d){c[d]=
null})}}})(M);(function(a){var D=a.charts,B=a.each,G=a.extend,E=a.map,r=a.noop,g=a.pick;G(a.Pointer.prototype,{pinchTranslate:function(a,g,r,u,l,e){this.zoomHor&&this.pinchTranslateDirection(!0,a,g,r,u,l,e);this.zoomVert&&this.pinchTranslateDirection(!1,a,g,r,u,l,e)},pinchTranslateDirection:function(a,g,r,u,l,e,k,f){var d=this.chart,x=a?"x":"y",C=a?"X":"Y",c="chart"+C,q=a?"width":"height",p=d["plot"+(a?"Left":"Top")],m,t,b=f||1,z=d.inverted,K=d.bounds[a?"h":"v"],y=1===g.length,A=g[0][c],n=r[0][c],
H=!y&&g[1][c],h=!y&&r[1][c],w;r=function(){!y&&20<Math.abs(A-H)&&(b=f||Math.abs(n-h)/Math.abs(A-H));t=(p-n)/b+A;m=d["plot"+(a?"Width":"Height")]/b};r();g=t;g<K.min?(g=K.min,w=!0):g+m>K.max&&(g=K.max-m,w=!0);w?(n-=.8*(n-k[x][0]),y||(h-=.8*(h-k[x][1])),r()):k[x]=[n,h];z||(e[x]=t-p,e[q]=m);e=z?1/b:b;l[q]=m;l[x]=g;u[z?a?"scaleY":"scaleX":"scale"+C]=b;u["translate"+C]=e*p+(n-e*A)},pinch:function(a){var p=this,v=p.chart,u=p.pinchDown,l=a.touches,e=l.length,k=p.lastValidTouch,f=p.hasZoom,d=p.selectionMarker,
x={},C=1===e&&(p.inClass(a.target,"highcharts-tracker")&&v.runTrackerClick||p.runChartClick),c={};1<e&&(p.initiated=!0);f&&p.initiated&&!C&&a.preventDefault();E(l,function(a){return p.normalize(a)});"touchstart"===a.type?(B(l,function(a,c){u[c]={chartX:a.chartX,chartY:a.chartY}}),k.x=[u[0].chartX,u[1]&&u[1].chartX],k.y=[u[0].chartY,u[1]&&u[1].chartY],B(v.axes,function(a){if(a.zoomEnabled){var c=v.bounds[a.horiz?"h":"v"],d=a.minPixelPadding,f=a.toPixels(g(a.options.min,a.dataMin)),b=a.toPixels(g(a.options.max,
a.dataMax)),e=Math.max(f,b);c.min=Math.min(a.pos,Math.min(f,b)-d);c.max=Math.max(a.pos+a.len,e+d)}}),p.res=!0):p.followTouchMove&&1===e?this.runPointActions(p.normalize(a)):u.length&&(d||(p.selectionMarker=d=G({destroy:r,touch:!0},v.plotBox)),p.pinchTranslate(u,l,x,d,c,k),p.hasPinched=f,p.scaleGroups(x,c),p.res&&(p.res=!1,this.reset(!1,0)))},touch:function(p,t){var r=this.chart,u,l;if(r.index!==a.hoverChartIndex)this.onContainerMouseLeave({relatedTarget:!0});a.hoverChartIndex=r.index;1===p.touches.length?
(p=this.normalize(p),(l=r.isInsidePlot(p.chartX-r.plotLeft,p.chartY-r.plotTop))&&!r.openMenu?(t&&this.runPointActions(p),"touchmove"===p.type&&(t=this.pinchDown,u=t[0]?4<=Math.sqrt(Math.pow(t[0].chartX-p.chartX,2)+Math.pow(t[0].chartY-p.chartY,2)):!1),g(u,!0)&&this.pinch(p)):t&&this.reset()):2===p.touches.length&&this.pinch(p)},onContainerTouchStart:function(a){this.zoomOption(a);this.touch(a,!0)},onContainerTouchMove:function(a){this.touch(a)},onDocumentTouchEnd:function(g){D[a.hoverChartIndex]&&
D[a.hoverChartIndex].pointer.drop(g)}})})(M);(function(a){var D=a.addEvent,B=a.charts,G=a.css,E=a.doc,r=a.extend,g=a.noop,p=a.Pointer,t=a.removeEvent,v=a.win,u=a.wrap;if(!a.hasTouch&&(v.PointerEvent||v.MSPointerEvent)){var l={},e=!!v.PointerEvent,k=function(){var d=[];d.item=function(a){return this[a]};a.objectEach(l,function(a){d.push({pageX:a.pageX,pageY:a.pageY,target:a.target})});return d},f=function(d,f,e,c){"touch"!==d.pointerType&&d.pointerType!==d.MSPOINTER_TYPE_TOUCH||!B[a.hoverChartIndex]||
(c(d),c=B[a.hoverChartIndex].pointer,c[f]({type:e,target:d.currentTarget,preventDefault:g,touches:k()}))};r(p.prototype,{onContainerPointerDown:function(a){f(a,"onContainerTouchStart","touchstart",function(a){l[a.pointerId]={pageX:a.pageX,pageY:a.pageY,target:a.currentTarget}})},onContainerPointerMove:function(a){f(a,"onContainerTouchMove","touchmove",function(a){l[a.pointerId]={pageX:a.pageX,pageY:a.pageY};l[a.pointerId].target||(l[a.pointerId].target=a.currentTarget)})},onDocumentPointerUp:function(a){f(a,
"onDocumentTouchEnd","touchend",function(a){delete l[a.pointerId]})},batchMSEvents:function(a){a(this.chart.container,e?"pointerdown":"MSPointerDown",this.onContainerPointerDown);a(this.chart.container,e?"pointermove":"MSPointerMove",this.onContainerPointerMove);a(E,e?"pointerup":"MSPointerUp",this.onDocumentPointerUp)}});u(p.prototype,"init",function(a,f,e){a.call(this,f,e);this.hasZoom&&G(f.container,{"-ms-touch-action":"none","touch-action":"none"})});u(p.prototype,"setDOMEvents",function(a){a.apply(this);
(this.hasZoom||this.followTouchMove)&&this.batchMSEvents(D)});u(p.prototype,"destroy",function(a){this.batchMSEvents(t);a.call(this)})}})(M);(function(a){var D=a.addEvent,B=a.css,G=a.discardElement,E=a.defined,r=a.each,g=a.isFirefox,p=a.marginNames,t=a.merge,v=a.pick,u=a.setAnimation,l=a.stableSort,e=a.win,k=a.wrap;a.Legend=function(a,d){this.init(a,d)};a.Legend.prototype={init:function(a,d){this.chart=a;this.setOptions(d);d.enabled&&(this.render(),D(this.chart,"endResize",function(){this.legend.positionCheckboxes()}))},
setOptions:function(a){var d=v(a.padding,8);this.options=a;this.itemStyle=a.itemStyle;this.itemHiddenStyle=t(this.itemStyle,a.itemHiddenStyle);this.itemMarginTop=a.itemMarginTop||0;this.padding=d;this.initialItemY=d-5;this.itemHeight=this.maxItemWidth=0;this.symbolWidth=v(a.symbolWidth,16);this.pages=[]},update:function(a,d){var f=this.chart;this.setOptions(t(!0,this.options,a));this.destroy();f.isDirtyLegend=f.isDirtyBox=!0;v(d,!0)&&f.redraw()},colorizeItem:function(a,d){a.legendGroup[d?"removeClass":
"addClass"]("highcharts-legend-item-hidden");var f=this.options,e=a.legendItem,c=a.legendLine,k=a.legendSymbol,l=this.itemHiddenStyle.color,f=d?f.itemStyle.color:l,m=d?a.color||l:l,g=a.options&&a.options.marker,b={fill:m};e&&e.css({fill:f,color:f});c&&c.attr({stroke:m});k&&(g&&k.isMarker&&(b=a.pointAttribs(),d||(b.stroke=b.fill=l)),k.attr(b))},positionItem:function(a){var d=this.options,f=d.symbolPadding,d=!d.rtl,e=a._legendItemPos,c=e[0],e=e[1],k=a.checkbox;(a=a.legendGroup)&&a.element&&a.translate(d?
c:this.legendWidth-c-2*f-4,e);k&&(k.x=c,k.y=e)},destroyItem:function(a){var d=a.checkbox;r(["legendItem","legendLine","legendSymbol","legendGroup"],function(d){a[d]&&(a[d]=a[d].destroy())});d&&G(a.checkbox)},destroy:function(){function a(a){this[a]&&(this[a]=this[a].destroy())}r(this.getAllItems(),function(d){r(["legendItem","legendGroup"],a,d)});r("clipRect up down pager nav box title group".split(" "),a,this);this.display=null},positionCheckboxes:function(a){var d=this.group&&this.group.alignAttr,
f,e=this.clipHeight||this.legendHeight,c=this.titleHeight;d&&(f=d.translateY,r(this.allItems,function(k){var l=k.checkbox,m;l&&(m=f+c+l.y+(a||0)+3,B(l,{left:d.translateX+k.checkboxOffset+l.x-20+"px",top:m+"px",display:m>f-6&&m<f+e-6?"":"none"}))}))},renderTitle:function(){var a=this.options,d=this.padding,e=a.title,k=0;e.text&&(this.title||(this.title=this.chart.renderer.label(e.text,d-3,d-4,null,null,null,a.useHTML,null,"legend-title").attr({zIndex:1}).css(e.style).add(this.group)),a=this.title.getBBox(),
k=a.height,this.offsetWidth=a.width,this.contentGroup.attr({translateY:k}));this.titleHeight=k},setText:function(f){var d=this.options;f.legendItem.attr({text:d.labelFormat?a.format(d.labelFormat,f):d.labelFormatter.call(f)})},renderItem:function(a){var d=this.chart,f=d.renderer,e=this.options,c="horizontal"===e.layout,k=this.symbolWidth,l=e.symbolPadding,m=this.itemStyle,g=this.itemHiddenStyle,b=this.padding,z=c?v(e.itemDistance,20):0,p=!e.rtl,y=e.width,A=e.itemMarginBottom||0,n=this.itemMarginTop,
H=a.legendItem,h=!a.series,w=!h&&a.series.drawLegendSymbol?a.series:a,r=w.options,L=this.createCheckboxForItem&&r&&r.showCheckbox,r=k+l+z+(L?20:0),u=e.useHTML,N=a.options.className;H||(a.legendGroup=f.g("legend-item").addClass("highcharts-"+w.type+"-series highcharts-color-"+a.colorIndex+(N?" "+N:"")+(h?" highcharts-series-"+a.index:"")).attr({zIndex:1}).add(this.scrollGroup),a.legendItem=H=f.text("",p?k+l:-l,this.baseline||0,u).css(t(a.visible?m:g)).attr({align:p?"left":"right",zIndex:2}).add(a.legendGroup),
this.baseline||(k=m.fontSize,this.fontMetrics=f.fontMetrics(k,H),this.baseline=this.fontMetrics.f+3+n,H.attr("y",this.baseline)),this.symbolHeight=e.symbolHeight||this.fontMetrics.f,w.drawLegendSymbol(this,a),this.setItemEvents&&this.setItemEvents(a,H,u),L&&this.createCheckboxForItem(a));this.colorizeItem(a,a.visible);m.width||H.css({width:(e.itemWidth||e.width||d.spacingBox.width)-r});this.setText(a);f=H.getBBox();m=a.checkboxOffset=e.itemWidth||a.legendItemWidth||f.width+r;this.itemHeight=f=Math.round(a.legendItemHeight||
f.height||this.symbolHeight);c&&this.itemX-b+m>(y||d.spacingBox.width-2*b-e.x)&&(this.itemX=b,this.itemY+=n+this.lastLineHeight+A,this.lastLineHeight=0);this.maxItemWidth=Math.max(this.maxItemWidth,m);this.lastItemY=n+this.itemY+A;this.lastLineHeight=Math.max(f,this.lastLineHeight);a._legendItemPos=[this.itemX,this.itemY];c?this.itemX+=m:(this.itemY+=n+f+A,this.lastLineHeight=f);this.offsetWidth=y||Math.max((c?this.itemX-b-(a.checkbox?0:z):m)+b,this.offsetWidth)},getAllItems:function(){var a=[];r(this.chart.series,
function(d){var f=d&&d.options;d&&v(f.showInLegend,E(f.linkedTo)?!1:void 0,!0)&&(a=a.concat(d.legendItems||("point"===f.legendType?d.data:d)))});return a},adjustMargins:function(a,d){var f=this.chart,e=this.options,c=e.align.charAt(0)+e.verticalAlign.charAt(0)+e.layout.charAt(0);e.floating||r([/(lth|ct|rth)/,/(rtv|rm|rbv)/,/(rbh|cb|lbh)/,/(lbv|lm|ltv)/],function(k,l){k.test(c)&&!E(a[l])&&(f[p[l]]=Math.max(f[p[l]],f.legend[(l+1)%2?"legendHeight":"legendWidth"]+[1,-1,-1,1][l]*e[l%2?"x":"y"]+v(e.margin,
12)+d[l]))})},render:function(){var a=this,d=a.chart,e=d.renderer,k=a.group,c,q,g,m,p=a.box,b=a.options,z=a.padding;a.itemX=z;a.itemY=a.initialItemY;a.offsetWidth=0;a.lastItemY=0;k||(a.group=k=e.g("legend").attr({zIndex:7}).add(),a.contentGroup=e.g().attr({zIndex:1}).add(k),a.scrollGroup=e.g().add(a.contentGroup));a.renderTitle();c=a.getAllItems();l(c,function(a,b){return(a.options&&a.options.legendIndex||0)-(b.options&&b.options.legendIndex||0)});b.reversed&&c.reverse();a.allItems=c;a.display=q=
!!c.length;a.lastLineHeight=0;r(c,function(b){a.renderItem(b)});g=(b.width||a.offsetWidth)+z;m=a.lastItemY+a.lastLineHeight+a.titleHeight;m=a.handleOverflow(m);m+=z;p||(a.box=p=e.rect().addClass("highcharts-legend-box").attr({r:b.borderRadius}).add(k),p.isNew=!0);p.attr({stroke:b.borderColor,"stroke-width":b.borderWidth||0,fill:b.backgroundColor||"none"}).shadow(b.shadow);0<g&&0<m&&(p[p.isNew?"attr":"animate"](p.crisp({x:0,y:0,width:g,height:m},p.strokeWidth())),p.isNew=!1);p[q?"show":"hide"]();a.legendWidth=
g;a.legendHeight=m;r(c,function(b){a.positionItem(b)});q&&k.align(t(b,{width:g,height:m}),!0,"spacingBox");d.isResizing||this.positionCheckboxes()},handleOverflow:function(a){var d=this,f=this.chart,e=f.renderer,c=this.options,k=c.y,l=this.padding,f=f.spacingBox.height+("top"===c.verticalAlign?-k:k)-l,k=c.maxHeight,m,g=this.clipRect,b=c.navigation,z=v(b.animation,!0),p=b.arrowSize||12,y=this.nav,A=this.pages,n,H=this.allItems,h=function(a){"number"===typeof a?g.attr({height:a}):g&&(d.clipRect=g.destroy(),
d.contentGroup.clip());d.contentGroup.div&&(d.contentGroup.div.style.clip=a?"rect("+l+"px,9999px,"+(l+a)+"px,0)":"auto")};"horizontal"!==c.layout||"middle"===c.verticalAlign||c.floating||(f/=2);k&&(f=Math.min(f,k));A.length=0;a>f&&!1!==b.enabled?(this.clipHeight=m=Math.max(f-20-this.titleHeight-l,0),this.currentPage=v(this.currentPage,1),this.fullHeight=a,r(H,function(a,b){var c=a._legendItemPos[1];a=Math.round(a.legendItem.getBBox().height);var d=A.length;if(!d||c-A[d-1]>m&&(n||c)!==A[d-1])A.push(n||
c),d++;b===H.length-1&&c+a-A[d-1]>m&&A.push(c);c!==n&&(n=c)}),g||(g=d.clipRect=e.clipRect(0,l,9999,0),d.contentGroup.clip(g)),h(m),y||(this.nav=y=e.g().attr({zIndex:1}).add(this.group),this.up=e.symbol("triangle",0,0,p,p).on("click",function(){d.scroll(-1,z)}).add(y),this.pager=e.text("",15,10).addClass("highcharts-legend-navigation").css(b.style).add(y),this.down=e.symbol("triangle-down",0,0,p,p).on("click",function(){d.scroll(1,z)}).add(y)),d.scroll(0),a=f):y&&(h(),this.nav=y.destroy(),this.scrollGroup.attr({translateY:1}),
this.clipHeight=0);return a},scroll:function(a,d){var f=this.pages,e=f.length;a=this.currentPage+a;var c=this.clipHeight,k=this.options.navigation,l=this.pager,m=this.padding;a>e&&(a=e);0<a&&(void 0!==d&&u(d,this.chart),this.nav.attr({translateX:m,translateY:c+this.padding+7+this.titleHeight,visibility:"visible"}),this.up.attr({"class":1===a?"highcharts-legend-nav-inactive":"highcharts-legend-nav-active"}),l.attr({text:a+"/"+e}),this.down.attr({x:18+this.pager.getBBox().width,"class":a===e?"highcharts-legend-nav-inactive":
"highcharts-legend-nav-active"}),this.up.attr({fill:1===a?k.inactiveColor:k.activeColor}).css({cursor:1===a?"default":"pointer"}),this.down.attr({fill:a===e?k.inactiveColor:k.activeColor}).css({cursor:a===e?"default":"pointer"}),d=-f[a-1]+this.initialItemY,this.scrollGroup.animate({translateY:d}),this.currentPage=a,this.positionCheckboxes(d))}};a.LegendSymbolMixin={drawRectangle:function(a,d){var f=a.symbolHeight,e=a.options.squareSymbol;d.legendSymbol=this.chart.renderer.rect(e?(a.symbolWidth-f)/
2:0,a.baseline-f+1,e?f:a.symbolWidth,f,v(a.options.symbolRadius,f/2)).addClass("highcharts-point").attr({zIndex:3}).add(d.legendGroup)},drawLineMarker:function(a){var d=this.options,f=d.marker,e=a.symbolWidth,c=a.symbolHeight,k=c/2,l=this.chart.renderer,m=this.legendGroup;a=a.baseline-Math.round(.3*a.fontMetrics.b);var g;g={"stroke-width":d.lineWidth||0};d.dashStyle&&(g.dashstyle=d.dashStyle);this.legendLine=l.path(["M",0,a,"L",e,a]).addClass("highcharts-graph").attr(g).add(m);f&&!1!==f.enabled&&
(d=Math.min(v(f.radius,k),k),0===this.symbol.indexOf("url")&&(f=t(f,{width:c,height:c}),d=0),this.legendSymbol=f=l.symbol(this.symbol,e/2-d,a-d,2*d,2*d,f).addClass("highcharts-point").add(m),f.isMarker=!0)}};(/Trident\/7\.0/.test(e.navigator.userAgent)||g)&&k(a.Legend.prototype,"positionItem",function(a,d){var f=this,e=function(){d._legendItemPos&&a.call(f,d)};e();setTimeout(e)})})(M);(function(a){var D=a.addEvent,B=a.animate,G=a.animObject,E=a.attr,r=a.doc,g=a.Axis,p=a.createElement,t=a.defaultOptions,
v=a.discardElement,u=a.charts,l=a.css,e=a.defined,k=a.each,f=a.extend,d=a.find,x=a.fireEvent,C=a.getStyle,c=a.grep,q=a.isNumber,I=a.isObject,m=a.isString,J=a.Legend,b=a.marginNames,z=a.merge,K=a.objectEach,y=a.Pointer,A=a.pick,n=a.pInt,H=a.removeEvent,h=a.seriesTypes,w=a.splat,P=a.svg,L=a.syncTimeout,Q=a.win,N=a.Renderer,O=a.Chart=function(){this.getArgs.apply(this,arguments)};a.chart=function(a,b,c){return new O(a,b,c)};f(O.prototype,{callbacks:[],getArgs:function(){var a=[].slice.call(arguments);
if(m(a[0])||a[0].nodeName)this.renderTo=a.shift();this.init(a[0],a[1])},init:function(b,c){var d,h,n=b.series,f=b.plotOptions||{};b.series=null;d=z(t,b);for(h in d.plotOptions)d.plotOptions[h].tooltip=f[h]&&z(f[h].tooltip)||void 0;d.tooltip.userOptions=b.chart&&b.chart.forExport&&b.tooltip.userOptions||b.tooltip;d.series=b.series=n;this.userOptions=b;b=d.chart;h=b.events;this.margin=[];this.spacing=[];this.bounds={h:{},v:{}};this.callback=c;this.isResizing=0;this.options=d;this.axes=[];this.series=
[];this.hasCartesianSeries=b.showAxes;var e=this;e.index=u.length;u.push(e);a.chartCount++;h&&K(h,function(a,b){D(e,b,a)});e.xAxis=[];e.yAxis=[];e.pointCount=e.colorCounter=e.symbolCounter=0;e.firstRender()},initSeries:function(b){var c=this.options.chart;(c=h[b.type||c.type||c.defaultSeriesType])||a.error(17,!0);c=new c;c.init(this,b);return c},orderSeries:function(a){var b=this.series;for(a=a||0;a<b.length;a++)b[a]&&(b[a].index=a,b[a].name=b[a].name||"Series "+(b[a].index+1))},isInsidePlot:function(a,
b,c){var d=c?b:a;a=c?a:b;return 0<=d&&d<=this.plotWidth&&0<=a&&a<=this.plotHeight},redraw:function(b){var c=this.axes,d=this.series,h=this.pointer,n=this.legend,e=this.isDirtyLegend,m,y,l=this.hasCartesianSeries,w=this.isDirtyBox,A,q=this.renderer,g=q.isHidden(),H=[];this.setResponsive&&this.setResponsive(!1);a.setAnimation(b,this);g&&this.temporaryDisplay();this.layOutTitles();for(b=d.length;b--;)if(A=d[b],A.options.stacking&&(m=!0,A.isDirty)){y=!0;break}if(y)for(b=d.length;b--;)A=d[b],A.options.stacking&&
(A.isDirty=!0);k(d,function(a){a.isDirty&&"point"===a.options.legendType&&(a.updateTotals&&a.updateTotals(),e=!0);a.isDirtyData&&x(a,"updatedData")});e&&n.options.enabled&&(n.render(),this.isDirtyLegend=!1);m&&this.getStacks();l&&k(c,function(a){a.updateNames();a.setScale()});this.getMargins();l&&(k(c,function(a){a.isDirty&&(w=!0)}),k(c,function(a){var b=a.min+","+a.max;a.extKey!==b&&(a.extKey=b,H.push(function(){x(a,"afterSetExtremes",f(a.eventArgs,a.getExtremes()));delete a.eventArgs}));(w||m)&&
a.redraw()}));w&&this.drawChartBox();x(this,"predraw");k(d,function(a){(w||a.isDirty)&&a.visible&&a.redraw();a.isDirtyData=!1});h&&h.reset(!0);q.draw();x(this,"redraw");x(this,"render");g&&this.temporaryDisplay(!0);k(H,function(a){a.call()})},get:function(a){function b(b){return b.id===a||b.options&&b.options.id===a}var c,h=this.series,n;c=d(this.axes,b)||d(this.series,b);for(n=0;!c&&n<h.length;n++)c=d(h[n].points||[],b);return c},getAxes:function(){var a=this,b=this.options,c=b.xAxis=w(b.xAxis||
{}),b=b.yAxis=w(b.yAxis||{});k(c,function(a,b){a.index=b;a.isX=!0});k(b,function(a,b){a.index=b});c=c.concat(b);k(c,function(b){new g(a,b)})},getSelectedPoints:function(){var a=[];k(this.series,function(b){a=a.concat(c(b.data||[],function(a){return a.selected}))});return a},getSelectedSeries:function(){return c(this.series,function(a){return a.selected})},setTitle:function(a,b,c){var d=this,h=d.options,n;n=h.title=z({style:{color:"#333333",fontSize:h.isStock?"16px":"18px"}},h.title,a);h=h.subtitle=
z({style:{color:"#666666"}},h.subtitle,b);k([["title",a,n],["subtitle",b,h]],function(a,b){var c=a[0],h=d[c],n=a[1];a=a[2];h&&n&&(d[c]=h=h.destroy());a&&a.text&&!h&&(d[c]=d.renderer.text(a.text,0,0,a.useHTML).attr({align:a.align,"class":"highcharts-"+c,zIndex:a.zIndex||4}).add(),d[c].update=function(a){d.setTitle(!b&&a,b&&a)},d[c].css(a.style))});d.layOutTitles(c)},layOutTitles:function(a){var b=0,c,d=this.renderer,h=this.spacingBox;k(["title","subtitle"],function(a){var c=this[a],n=this.options[a];
a="title"===a?-3:n.verticalAlign?0:b+2;var e;c&&(e=n.style.fontSize,e=d.fontMetrics(e,c).b,c.css({width:(n.width||h.width+n.widthAdjust)+"px"}).align(f({y:a+e},n),!1,"spacingBox"),n.floating||n.verticalAlign||(b=Math.ceil(b+c.getBBox(n.useHTML).height)))},this);c=this.titleOffset!==b;this.titleOffset=b;!this.isDirtyBox&&c&&(this.isDirtyBox=c,this.hasRendered&&A(a,!0)&&this.isDirtyBox&&this.redraw())},getChartSize:function(){var b=this.options.chart,c=b.width,b=b.height,d=this.renderTo;e(c)||(this.containerWidth=
C(d,"width"));e(b)||(this.containerHeight=C(d,"height"));this.chartWidth=Math.max(0,c||this.containerWidth||600);this.chartHeight=Math.max(0,a.relativeLength(b,this.chartWidth)||this.containerHeight||400)},temporaryDisplay:function(b){var c=this.renderTo;if(b)for(;c&&c.style;)c.hcOrigStyle&&(a.css(c,c.hcOrigStyle),delete c.hcOrigStyle),c.hcOrigDetached&&(r.body.removeChild(c),c.hcOrigDetached=!1),c=c.parentNode;else for(;c&&c.style;){r.body.contains(c)||(c.hcOrigDetached=!0,r.body.appendChild(c));
if("none"===C(c,"display",!1)||c.hcOricDetached)c.hcOrigStyle={display:c.style.display,height:c.style.height,overflow:c.style.overflow},b={display:"block",overflow:"hidden"},c!==this.renderTo&&(b.height=0),a.css(c,b),c.offsetWidth||c.style.setProperty("display","block","important");c=c.parentNode;if(c===r.body)break}},setClassName:function(a){this.container.className="highcharts-container "+(a||"")},getContainer:function(){var b,c=this.options,d=c.chart,h,e;b=this.renderTo;var y=a.uniqueKey(),k;b||
(this.renderTo=b=d.renderTo);m(b)&&(this.renderTo=b=r.getElementById(b));b||a.error(13,!0);h=n(E(b,"data-highcharts-chart"));q(h)&&u[h]&&u[h].hasRendered&&u[h].destroy();E(b,"data-highcharts-chart",this.index);b.innerHTML="";d.skipClone||b.offsetWidth||this.temporaryDisplay();this.getChartSize();h=this.chartWidth;e=this.chartHeight;k=f({position:"relative",overflow:"hidden",width:h+"px",height:e+"px",textAlign:"left",lineHeight:"normal",zIndex:0,"-webkit-tap-highlight-color":"rgba(0,0,0,0)"},d.style);
this.container=b=p("div",{id:y},k,b);this._cursor=b.style.cursor;this.renderer=new (a[d.renderer]||N)(b,h,e,null,d.forExport,c.exporting&&c.exporting.allowHTML);this.setClassName(d.className);this.renderer.setStyle(d.style);this.renderer.chartIndex=this.index},getMargins:function(a){var b=this.spacing,c=this.margin,d=this.titleOffset;this.resetMargins();d&&!e(c[0])&&(this.plotTop=Math.max(this.plotTop,d+this.options.title.margin+b[0]));this.legend.display&&this.legend.adjustMargins(c,b);this.extraMargin&&
(this[this.extraMargin.type]=(this[this.extraMargin.type]||0)+this.extraMargin.value);this.extraTopMargin&&(this.plotTop+=this.extraTopMargin);a||this.getAxisMargins()},getAxisMargins:function(){var a=this,c=a.axisOffset=[0,0,0,0],d=a.margin;a.hasCartesianSeries&&k(a.axes,function(a){a.visible&&a.getOffset()});k(b,function(b,h){e(d[h])||(a[b]+=c[h])});a.setChartSize()},reflow:function(a){var b=this,c=b.options.chart,d=b.renderTo,h=e(c.width)&&e(c.height),n=c.width||C(d,"width"),c=c.height||C(d,"height"),
d=a?a.target:Q;if(!h&&!b.isPrinting&&n&&c&&(d===Q||d===r)){if(n!==b.containerWidth||c!==b.containerHeight)clearTimeout(b.reflowTimeout),b.reflowTimeout=L(function(){b.container&&b.setSize(void 0,void 0,!1)},a?100:0);b.containerWidth=n;b.containerHeight=c}},initReflow:function(){var a=this,b;b=D(Q,"resize",function(b){a.reflow(b)});D(a,"destroy",b)},setSize:function(b,c,d){var h=this,n=h.renderer;h.isResizing+=1;a.setAnimation(d,h);h.oldChartHeight=h.chartHeight;h.oldChartWidth=h.chartWidth;void 0!==
b&&(h.options.chart.width=b);void 0!==c&&(h.options.chart.height=c);h.getChartSize();b=n.globalAnimation;(b?B:l)(h.container,{width:h.chartWidth+"px",height:h.chartHeight+"px"},b);h.setChartSize(!0);n.setSize(h.chartWidth,h.chartHeight,d);k(h.axes,function(a){a.isDirty=!0;a.setScale()});h.isDirtyLegend=!0;h.isDirtyBox=!0;h.layOutTitles();h.getMargins();h.redraw(d);h.oldChartHeight=null;x(h,"resize");L(function(){h&&x(h,"endResize",null,function(){--h.isResizing})},G(b).duration)},setChartSize:function(a){function b(a){a=
m[a]||0;return Math.max(q||a,a)/2}var c=this.inverted,h=this.renderer,d=this.chartWidth,n=this.chartHeight,e=this.options.chart,f=this.spacing,m=this.clipOffset,y,l,w,A,q;this.plotLeft=y=Math.round(this.plotLeft);this.plotTop=l=Math.round(this.plotTop);this.plotWidth=w=Math.max(0,Math.round(d-y-this.marginRight));this.plotHeight=A=Math.max(0,Math.round(n-l-this.marginBottom));this.plotSizeX=c?A:w;this.plotSizeY=c?w:A;this.plotBorderWidth=e.plotBorderWidth||0;this.spacingBox=h.spacingBox={x:f[3],y:f[0],
width:d-f[3]-f[1],height:n-f[0]-f[2]};this.plotBox=h.plotBox={x:y,y:l,width:w,height:A};q=2*Math.floor(this.plotBorderWidth/2);c=Math.ceil(b(3));h=Math.ceil(b(0));this.clipBox={x:c,y:h,width:Math.floor(this.plotSizeX-b(1)-c),height:Math.max(0,Math.floor(this.plotSizeY-b(2)-h))};a||k(this.axes,function(a){a.setAxisSize();a.setAxisTranslation()})},resetMargins:function(){var a=this,c=a.options.chart;k(["margin","spacing"],function(b){var h=c[b],d=I(h)?h:[h,h,h,h];k(["Top","Right","Bottom","Left"],function(h,
n){a[b][n]=A(c[b+h],d[n])})});k(b,function(b,c){a[b]=A(a.margin[c],a.spacing[c])});a.axisOffset=[0,0,0,0];a.clipOffset=[]},drawChartBox:function(){var a=this.options.chart,b=this.renderer,c=this.chartWidth,h=this.chartHeight,d=this.chartBackground,n=this.plotBackground,e=this.plotBorder,f,m=this.plotBGImage,y=a.backgroundColor,k=a.plotBackgroundColor,l=a.plotBackgroundImage,w,A=this.plotLeft,q=this.plotTop,g=this.plotWidth,H=this.plotHeight,z=this.plotBox,p=this.clipRect,x=this.clipBox,C="animate";
d||(this.chartBackground=d=b.rect().addClass("highcharts-background").add(),C="attr");f=a.borderWidth||0;w=f+(a.shadow?8:0);y={fill:y||"none"};if(f||d["stroke-width"])y.stroke=a.borderColor,y["stroke-width"]=f;d.attr(y).shadow(a.shadow);d[C]({x:w/2,y:w/2,width:c-w-f%2,height:h-w-f%2,r:a.borderRadius});C="animate";n||(C="attr",this.plotBackground=n=b.rect().addClass("highcharts-plot-background").add());n[C](z);n.attr({fill:k||"none"}).shadow(a.plotShadow);l&&(m?m.animate(z):this.plotBGImage=b.image(l,
A,q,g,H).add());p?p.animate({width:x.width,height:x.height}):this.clipRect=b.clipRect(x);C="animate";e||(C="attr",this.plotBorder=e=b.rect().addClass("highcharts-plot-border").attr({zIndex:1}).add());e.attr({stroke:a.plotBorderColor,"stroke-width":a.plotBorderWidth||0,fill:"none"});e[C](e.crisp({x:A,y:q,width:g,height:H},-e.strokeWidth()));this.isDirtyBox=!1},propFromSeries:function(){var a=this,b=a.options.chart,c,d=a.options.series,n,e;k(["inverted","angular","polar"],function(f){c=h[b.type||b.defaultSeriesType];
e=b[f]||c&&c.prototype[f];for(n=d&&d.length;!e&&n--;)(c=h[d[n].type])&&c.prototype[f]&&(e=!0);a[f]=e})},linkSeries:function(){var a=this,b=a.series;k(b,function(a){a.linkedSeries.length=0});k(b,function(b){var c=b.options.linkedTo;m(c)&&(c=":previous"===c?a.series[b.index-1]:a.get(c))&&c.linkedParent!==b&&(c.linkedSeries.push(b),b.linkedParent=c,b.visible=A(b.options.visible,c.options.visible,b.visible))})},renderSeries:function(){k(this.series,function(a){a.translate();a.render()})},renderLabels:function(){var a=
this,b=a.options.labels;b.items&&k(b.items,function(c){var h=f(b.style,c.style),d=n(h.left)+a.plotLeft,e=n(h.top)+a.plotTop+12;delete h.left;delete h.top;a.renderer.text(c.html,d,e).attr({zIndex:2}).css(h).add()})},render:function(){var a=this.axes,b=this.renderer,c=this.options,h,d,n;this.setTitle();this.legend=new J(this,c.legend);this.getStacks&&this.getStacks();this.getMargins(!0);this.setChartSize();c=this.plotWidth;h=this.plotHeight-=21;k(a,function(a){a.setScale()});this.getAxisMargins();d=
1.1<c/this.plotWidth;n=1.05<h/this.plotHeight;if(d||n)k(a,function(a){(a.horiz&&d||!a.horiz&&n)&&a.setTickInterval(!0)}),this.getMargins();this.drawChartBox();this.hasCartesianSeries&&k(a,function(a){a.visible&&a.render()});this.seriesGroup||(this.seriesGroup=b.g("series-group").attr({zIndex:3}).add());this.renderSeries();this.renderLabels();this.addCredits();this.setResponsive&&this.setResponsive();this.hasRendered=!0},addCredits:function(a){var b=this;a=z(!0,this.options.credits,a);a.enabled&&!this.credits&&
(this.credits=this.renderer.text(a.text+(this.mapCredits||""),0,0).addClass("highcharts-credits").on("click",function(){a.href&&(Q.location.href=a.href)}).attr({align:a.position.align,zIndex:8}).css(a.style).add().align(a.position),this.credits.update=function(a){b.credits=b.credits.destroy();b.addCredits(a)})},destroy:function(){var b=this,c=b.axes,h=b.series,d=b.container,n,e=d&&d.parentNode;x(b,"destroy");b.renderer.forExport?a.erase(u,b):u[b.index]=void 0;a.chartCount--;b.renderTo.removeAttribute("data-highcharts-chart");
H(b);for(n=c.length;n--;)c[n]=c[n].destroy();this.scroller&&this.scroller.destroy&&this.scroller.destroy();for(n=h.length;n--;)h[n]=h[n].destroy();k("title subtitle chartBackground plotBackground plotBGImage plotBorder seriesGroup clipRect credits pointer rangeSelector legend resetZoomButton tooltip renderer".split(" "),function(a){var c=b[a];c&&c.destroy&&(b[a]=c.destroy())});d&&(d.innerHTML="",H(d),e&&v(d));K(b,function(a,c){delete b[c]})},isReadyToRender:function(){var a=this;return P||Q!=Q.top||
"complete"===r.readyState?!0:(r.attachEvent("onreadystatechange",function(){r.detachEvent("onreadystatechange",a.firstRender);"complete"===r.readyState&&a.firstRender()}),!1)},firstRender:function(){var a=this,b=a.options;if(a.isReadyToRender()){a.getContainer();x(a,"init");a.resetMargins();a.setChartSize();a.propFromSeries();a.getAxes();k(b.series||[],function(b){a.initSeries(b)});a.linkSeries();x(a,"beforeRender");y&&(a.pointer=new y(a,b));a.render();if(!a.renderer.imgCount&&a.onload)a.onload();
a.temporaryDisplay(!0)}},onload:function(){k([this.callback].concat(this.callbacks),function(a){a&&void 0!==this.index&&a.apply(this,[this])},this);x(this,"load");x(this,"render");e(this.index)&&!1!==this.options.chart.reflow&&this.initReflow();this.onload=null}})})(M);(function(a){var D,B=a.each,G=a.extend,E=a.erase,r=a.fireEvent,g=a.format,p=a.isArray,t=a.isNumber,v=a.pick,u=a.removeEvent;a.Point=D=function(){};a.Point.prototype={init:function(a,e,k){this.series=a;this.color=a.color;this.applyOptions(e,
k);a.options.colorByPoint?(e=a.options.colors||a.chart.options.colors,this.color=this.color||e[a.colorCounter],e=e.length,k=a.colorCounter,a.colorCounter++,a.colorCounter===e&&(a.colorCounter=0)):k=a.colorIndex;this.colorIndex=v(this.colorIndex,k);a.chart.pointCount++;return this},applyOptions:function(a,e){var k=this.series,f=k.options.pointValKey||k.pointValKey;a=D.prototype.optionsToObject.call(this,a);G(this,a);this.options=this.options?G(this.options,a):a;a.group&&delete this.group;f&&(this.y=
this[f]);this.isNull=v(this.isValid&&!this.isValid(),null===this.x||!t(this.y,!0));this.selected&&(this.state="select");"name"in this&&void 0===e&&k.xAxis&&k.xAxis.hasNames&&(this.x=k.xAxis.nameToX(this));void 0===this.x&&k&&(this.x=void 0===e?k.autoIncrement(this):e);return this},optionsToObject:function(a){var e={},k=this.series,f=k.options.keys,d=f||k.pointArrayMap||["y"],l=d.length,g=0,c=0;if(t(a)||null===a)e[d[0]]=a;else if(p(a))for(!f&&a.length>l&&(k=typeof a[0],"string"===k?e.name=a[0]:"number"===
k&&(e.x=a[0]),g++);c<l;)f&&void 0===a[g]||(e[d[c]]=a[g]),g++,c++;else"object"===typeof a&&(e=a,a.dataLabels&&(k._hasPointLabels=!0),a.marker&&(k._hasPointMarkers=!0));return e},getClassName:function(){return"highcharts-point"+(this.selected?" highcharts-point-select":"")+(this.negative?" highcharts-negative":"")+(this.isNull?" highcharts-null-point":"")+(void 0!==this.colorIndex?" highcharts-color-"+this.colorIndex:"")+(this.options.className?" "+this.options.className:"")+(this.zone&&this.zone.className?
" "+this.zone.className.replace("highcharts-negative",""):"")},getZone:function(){var a=this.series,e=a.zones,a=a.zoneAxis||"y",k=0,f;for(f=e[k];this[a]>=f.value;)f=e[++k];f&&f.color&&!this.options.color&&(this.color=f.color);return f},destroy:function(){var a=this.series.chart,e=a.hoverPoints,k;a.pointCount--;e&&(this.setState(),E(e,this),e.length||(a.hoverPoints=null));if(this===a.hoverPoint)this.onMouseOut();if(this.graphic||this.dataLabel)u(this),this.destroyElements();this.legendItem&&a.legend.destroyItem(this);
for(k in this)this[k]=null},destroyElements:function(){for(var a=["graphic","dataLabel","dataLabelUpper","connector","shadowGroup"],e,k=6;k--;)e=a[k],this[e]&&(this[e]=this[e].destroy())},getLabelConfig:function(){return{x:this.category,y:this.y,color:this.color,colorIndex:this.colorIndex,key:this.name||this.category,series:this.series,point:this,percentage:this.percentage,total:this.total||this.stackTotal}},tooltipFormatter:function(a){var e=this.series,k=e.tooltipOptions,f=v(k.valueDecimals,""),
d=k.valuePrefix||"",l=k.valueSuffix||"";B(e.pointArrayMap||["y"],function(e){e="{point."+e;if(d||l)a=a.replace(e+"}",d+e+"}"+l);a=a.replace(e+"}",e+":,."+f+"f}")});return g(a,{point:this,series:this.series})},firePointEvent:function(a,e,k){var f=this,d=this.series.options;(d.point.events[a]||f.options&&f.options.events&&f.options.events[a])&&this.importEvents();"click"===a&&d.allowPointSelect&&(k=function(a){f.select&&f.select(null,a.ctrlKey||a.metaKey||a.shiftKey)});r(this,a,e,k)},visible:!0}})(M);
(function(a){var D=a.addEvent,B=a.animObject,G=a.arrayMax,E=a.arrayMin,r=a.correctFloat,g=a.Date,p=a.defaultOptions,t=a.defaultPlotOptions,v=a.defined,u=a.each,l=a.erase,e=a.extend,k=a.fireEvent,f=a.grep,d=a.isArray,x=a.isNumber,C=a.isString,c=a.merge,q=a.objectEach,I=a.pick,m=a.removeEvent,J=a.splat,b=a.SVGElement,z=a.syncTimeout,K=a.win;a.Series=a.seriesType("line",null,{lineWidth:2,allowPointSelect:!1,showCheckbox:!1,animation:{duration:1E3},events:{},marker:{lineWidth:0,lineColor:"#ffffff",radius:4,
states:{hover:{animation:{duration:50},enabled:!0,radiusPlus:2,lineWidthPlus:1},select:{fillColor:"#cccccc",lineColor:"#000000",lineWidth:2}}},point:{events:{}},dataLabels:{align:"center",formatter:function(){return null===this.y?"":a.numberFormat(this.y,-1)},style:{fontSize:"11px",fontWeight:"bold",color:"contrast",textOutline:"1px contrast"},verticalAlign:"bottom",x:0,y:0,padding:5},cropThreshold:300,pointRange:0,softThreshold:!0,states:{hover:{animation:{duration:50},lineWidthPlus:1,marker:{},
halo:{size:10,opacity:.25}},select:{marker:{}}},stickyTracking:!0,turboThreshold:1E3,findNearestPointBy:"x"},{isCartesian:!0,pointClass:a.Point,sorted:!0,requireSorting:!0,directTouch:!1,axisTypes:["xAxis","yAxis"],colorCounter:0,parallelArrays:["x","y"],coll:"series",init:function(a,b){var c=this,d,h=a.series,f;c.chart=a;c.options=b=c.setOptions(b);c.linkedSeries=[];c.bindAxes();e(c,{name:b.name,state:"",visible:!1!==b.visible,selected:!0===b.selected});d=b.events;q(d,function(a,b){D(c,b,a)});if(d&&
d.click||b.point&&b.point.events&&b.point.events.click||b.allowPointSelect)a.runTrackerClick=!0;c.getColor();c.getSymbol();u(c.parallelArrays,function(a){c[a+"Data"]=[]});c.setData(b.data,!1);c.isCartesian&&(a.hasCartesianSeries=!0);h.length&&(f=h[h.length-1]);c._i=I(f&&f._i,-1)+1;a.orderSeries(this.insert(h))},insert:function(a){var b=this.options.index,c;if(x(b)){for(c=a.length;c--;)if(b>=I(a[c].options.index,a[c]._i)){a.splice(c+1,0,this);break}-1===c&&a.unshift(this);c+=1}else a.push(this);return I(c,
a.length-1)},bindAxes:function(){var b=this,c=b.options,d=b.chart,e;u(b.axisTypes||[],function(h){u(d[h],function(a){e=a.options;if(c[h]===e.index||void 0!==c[h]&&c[h]===e.id||void 0===c[h]&&0===e.index)b.insert(a.series),b[h]=a,a.isDirty=!0});b[h]||b.optionalAxis===h||a.error(18,!0)})},updateParallelArrays:function(a,b){var c=a.series,d=arguments,h=x(b)?function(h){var d="y"===h&&c.toYData?c.toYData(a):a[h];c[h+"Data"][b]=d}:function(a){Array.prototype[b].apply(c[a+"Data"],Array.prototype.slice.call(d,
2))};u(c.parallelArrays,h)},autoIncrement:function(){var a=this.options,b=this.xIncrement,c,d=a.pointIntervalUnit,b=I(b,a.pointStart,0);this.pointInterval=c=I(this.pointInterval,a.pointInterval,1);d&&(a=new g(b),"day"===d?a=+a[g.hcSetDate](a[g.hcGetDate]()+c):"month"===d?a=+a[g.hcSetMonth](a[g.hcGetMonth]()+c):"year"===d&&(a=+a[g.hcSetFullYear](a[g.hcGetFullYear]()+c)),c=a-b);this.xIncrement=b+c;return b},setOptions:function(a){var b=this.chart,d=b.options,e=d.plotOptions,h=(b.userOptions||{}).plotOptions||
{},f=e[this.type];this.userOptions=a;b=c(f,e.series,a);this.tooltipOptions=c(p.tooltip,p.plotOptions.series&&p.plotOptions.series.tooltip,p.plotOptions[this.type].tooltip,d.tooltip.userOptions,e.series&&e.series.tooltip,e[this.type].tooltip,a.tooltip);this.stickyTracking=I(a.stickyTracking,h[this.type]&&h[this.type].stickyTracking,h.series&&h.series.stickyTracking,this.tooltipOptions.shared&&!this.noSharedTooltip?!0:b.stickyTracking);null===f.marker&&delete b.marker;this.zoneAxis=b.zoneAxis;a=this.zones=
(b.zones||[]).slice();!b.negativeColor&&!b.negativeFillColor||b.zones||a.push({value:b[this.zoneAxis+"Threshold"]||b.threshold||0,className:"highcharts-negative",color:b.negativeColor,fillColor:b.negativeFillColor});a.length&&v(a[a.length-1].value)&&a.push({color:this.color,fillColor:this.fillColor});return b},getCyclic:function(a,b,c){var d,h=this.chart,n=this.userOptions,e=a+"Index",f=a+"Counter",m=c?c.length:I(h.options.chart[a+"Count"],h[a+"Count"]);b||(d=I(n[e],n["_"+e]),v(d)||(h.series.length||
(h[f]=0),n["_"+e]=d=h[f]%m,h[f]+=1),c&&(b=c[d]));void 0!==d&&(this[e]=d);this[a]=b},getColor:function(){this.options.colorByPoint?this.options.color=null:this.getCyclic("color",this.options.color||t[this.type].color,this.chart.options.colors)},getSymbol:function(){this.getCyclic("symbol",this.options.marker.symbol,this.chart.options.symbols)},drawLegendSymbol:a.LegendSymbolMixin.drawLineMarker,setData:function(b,c,n,e){var h=this,f=h.points,m=f&&f.length||0,k,y=h.options,l=h.chart,q=null,g=h.xAxis,
A=y.turboThreshold,H=this.xData,z=this.yData,p=(k=h.pointArrayMap)&&k.length;b=b||[];k=b.length;c=I(c,!0);if(!1!==e&&k&&m===k&&!h.cropped&&!h.hasGroupedData&&h.visible)u(b,function(a,b){f[b].update&&a!==y.data[b]&&f[b].update(a,!1,null,!1)});else{h.xIncrement=null;h.colorCounter=0;u(this.parallelArrays,function(a){h[a+"Data"].length=0});if(A&&k>A){for(n=0;null===q&&n<k;)q=b[n],n++;if(x(q))for(n=0;n<k;n++)H[n]=this.autoIncrement(),z[n]=b[n];else if(d(q))if(p)for(n=0;n<k;n++)q=b[n],H[n]=q[0],z[n]=q.slice(1,
p+1);else for(n=0;n<k;n++)q=b[n],H[n]=q[0],z[n]=q[1];else a.error(12)}else for(n=0;n<k;n++)void 0!==b[n]&&(q={series:h},h.pointClass.prototype.applyOptions.apply(q,[b[n]]),h.updateParallelArrays(q,n));C(z[0])&&a.error(14,!0);h.data=[];h.options.data=h.userOptions.data=b;for(n=m;n--;)f[n]&&f[n].destroy&&f[n].destroy();g&&(g.minRange=g.userMinRange);h.isDirty=l.isDirtyBox=!0;h.isDirtyData=!!f;n=!1}"point"===y.legendType&&(this.processData(),this.generatePoints());c&&l.redraw(n)},processData:function(b){var c=
this.xData,d=this.yData,e=c.length,h;h=0;var f,m,k=this.xAxis,y,l=this.options;y=l.cropThreshold;var q=this.getExtremesFromAll||l.getExtremesFromAll,g=this.isCartesian,l=k&&k.val2lin,z=k&&k.isLog,p,x;if(g&&!this.isDirty&&!k.isDirty&&!this.yAxis.isDirty&&!b)return!1;k&&(b=k.getExtremes(),p=b.min,x=b.max);if(g&&this.sorted&&!q&&(!y||e>y||this.forceCrop))if(c[e-1]<p||c[0]>x)c=[],d=[];else if(c[0]<p||c[e-1]>x)h=this.cropData(this.xData,this.yData,p,x),c=h.xData,d=h.yData,h=h.start,f=!0;for(y=c.length||
1;--y;)e=z?l(c[y])-l(c[y-1]):c[y]-c[y-1],0<e&&(void 0===m||e<m)?m=e:0>e&&this.requireSorting&&a.error(15);this.cropped=f;this.cropStart=h;this.processedXData=c;this.processedYData=d;this.closestPointRange=m},cropData:function(a,b,c,d){var h=a.length,n=0,e=h,f=I(this.cropShoulder,1),m;for(m=0;m<h;m++)if(a[m]>=c){n=Math.max(0,m-f);break}for(c=m;c<h;c++)if(a[c]>d){e=c+f;break}return{xData:a.slice(n,e),yData:b.slice(n,e),start:n,end:e}},generatePoints:function(){var a=this.options,b=a.data,c=this.data,
d,h=this.processedXData,e=this.processedYData,f=this.pointClass,m=h.length,k=this.cropStart||0,l,q=this.hasGroupedData,a=a.keys,g,z=[],p;c||q||(c=[],c.length=b.length,c=this.data=c);a&&q&&(this.options.keys=!1);for(p=0;p<m;p++)l=k+p,q?(g=(new f).init(this,[h[p]].concat(J(e[p]))),g.dataGroup=this.groupMap[p]):(g=c[l])||void 0===b[l]||(c[l]=g=(new f).init(this,b[l],h[p])),g&&(g.index=l,z[p]=g);this.options.keys=a;if(c&&(m!==(d=c.length)||q))for(p=0;p<d;p++)p!==k||q||(p+=m),c[p]&&(c[p].destroyElements(),
c[p].plotX=void 0);this.data=c;this.points=z},getExtremes:function(a){var b=this.yAxis,c=this.processedXData,e,h=[],f=0;e=this.xAxis.getExtremes();var m=e.min,k=e.max,y,l,q,g;a=a||this.stackedYData||this.processedYData||[];e=a.length;for(g=0;g<e;g++)if(l=c[g],q=a[g],y=(x(q,!0)||d(q))&&(!b.positiveValuesOnly||q.length||0<q),l=this.getExtremesFromAll||this.options.getExtremesFromAll||this.cropped||(c[g]||l)>=m&&(c[g]||l)<=k,y&&l)if(y=q.length)for(;y--;)null!==q[y]&&(h[f++]=q[y]);else h[f++]=q;this.dataMin=
E(h);this.dataMax=G(h)},translate:function(){this.processedXData||this.processData();this.generatePoints();var a=this.options,b=a.stacking,c=this.xAxis,d=c.categories,h=this.yAxis,e=this.points,f=e.length,m=!!this.modifyValue,k=a.pointPlacement,l="between"===k||x(k),q=a.threshold,g=a.startFromThreshold?q:0,z,p,C,t,K=Number.MAX_VALUE;"between"===k&&(k=.5);x(k)&&(k*=I(a.pointRange||c.pointRange));for(a=0;a<f;a++){var u=e[a],J=u.x,B=u.y;p=u.low;var D=b&&h.stacks[(this.negStacks&&B<(g?0:q)?"-":"")+this.stackKey],
E;h.positiveValuesOnly&&null!==B&&0>=B&&(u.isNull=!0);u.plotX=z=r(Math.min(Math.max(-1E5,c.translate(J,0,0,0,1,k,"flags"===this.type)),1E5));b&&this.visible&&!u.isNull&&D&&D[J]&&(t=this.getStackIndicator(t,J,this.index),E=D[J],B=E.points[t.key],p=B[0],B=B[1],p===g&&t.key===D[J].base&&(p=I(q,h.min)),h.positiveValuesOnly&&0>=p&&(p=null),u.total=u.stackTotal=E.total,u.percentage=E.total&&u.y/E.total*100,u.stackY=B,E.setOffset(this.pointXOffset||0,this.barW||0));u.yBottom=v(p)?h.translate(p,0,1,0,1):
null;m&&(B=this.modifyValue(B,u));u.plotY=p="number"===typeof B&&Infinity!==B?Math.min(Math.max(-1E5,h.translate(B,0,1,0,1)),1E5):void 0;u.isInside=void 0!==p&&0<=p&&p<=h.len&&0<=z&&z<=c.len;u.clientX=l?r(c.translate(J,0,0,0,1,k)):z;u.negative=u.y<(q||0);u.category=d&&void 0!==d[u.x]?d[u.x]:u.x;u.isNull||(void 0!==C&&(K=Math.min(K,Math.abs(z-C))),C=z);u.zone=this.zones.length&&u.getZone()}this.closestPointRangePx=K},getValidPoints:function(a,b){var c=this.chart;return f(a||this.points||[],function(a){return b&&
!c.isInsidePlot(a.plotX,a.plotY,c.inverted)?!1:!a.isNull})},setClip:function(a){var b=this.chart,c=this.options,d=b.renderer,h=b.inverted,e=this.clipBox,f=e||b.clipBox,m=this.sharedClipKey||["_sharedClip",a&&a.duration,a&&a.easing,f.height,c.xAxis,c.yAxis].join(),k=b[m],l=b[m+"m"];k||(a&&(f.width=0,b[m+"m"]=l=d.clipRect(-99,h?-b.plotLeft:-b.plotTop,99,h?b.chartWidth:b.chartHeight)),b[m]=k=d.clipRect(f),k.count={length:0});a&&!k.count[this.index]&&(k.count[this.index]=!0,k.count.length+=1);!1!==c.clip&&
(this.group.clip(a||e?k:b.clipRect),this.markerGroup.clip(l),this.sharedClipKey=m);a||(k.count[this.index]&&(delete k.count[this.index],--k.count.length),0===k.count.length&&m&&b[m]&&(e||(b[m]=b[m].destroy()),b[m+"m"]&&(b[m+"m"]=b[m+"m"].destroy())))},animate:function(a){var b=this.chart,c=B(this.options.animation),d;a?this.setClip(c):(d=this.sharedClipKey,(a=b[d])&&a.animate({width:b.plotSizeX},c),b[d+"m"]&&b[d+"m"].animate({width:b.plotSizeX+99},c),this.animate=null)},afterAnimate:function(){this.setClip();
k(this,"afterAnimate");this.finishedAnimating=!0},drawPoints:function(){var a=this.points,b=this.chart,c,d,h,e,f=this.options.marker,m,k,l,q,g=this[this.specialGroup]||this.markerGroup,z=I(f.enabled,this.xAxis.isRadial?!0:null,this.closestPointRangePx>=2*f.radius);if(!1!==f.enabled||this._hasPointMarkers)for(d=0;d<a.length;d++)h=a[d],c=h.plotY,e=h.graphic,m=h.marker||{},k=!!h.marker,l=z&&void 0===m.enabled||m.enabled,q=h.isInside,l&&x(c)&&null!==h.y?(c=I(m.symbol,this.symbol),h.hasImage=0===c.indexOf("url"),
l=this.markerAttribs(h,h.selected&&"select"),e?e[q?"show":"hide"](!0).animate(l):q&&(0<l.width||h.hasImage)&&(h.graphic=e=b.renderer.symbol(c,l.x,l.y,l.width,l.height,k?m:f).add(g)),e&&e.attr(this.pointAttribs(h,h.selected&&"select")),e&&e.addClass(h.getClassName(),!0)):e&&(h.graphic=e.destroy())},markerAttribs:function(a,b){var c=this.options.marker,d=a.marker||{},h=I(d.radius,c.radius);b&&(c=c.states[b],b=d.states&&d.states[b],h=I(b&&b.radius,c&&c.radius,h+(c&&c.radiusPlus||0)));a.hasImage&&(h=
0);a={x:Math.floor(a.plotX)-h,y:a.plotY-h};h&&(a.width=a.height=2*h);return a},pointAttribs:function(a,b){var c=this.options.marker,d=a&&a.options,h=d&&d.marker||{},e=this.color,f=d&&d.color,m=a&&a.color,d=I(h.lineWidth,c.lineWidth);a=a&&a.zone&&a.zone.color;e=f||a||m||e;a=h.fillColor||c.fillColor||e;e=h.lineColor||c.lineColor||e;b&&(c=c.states[b],b=h.states&&h.states[b]||{},d=I(b.lineWidth,c.lineWidth,d+I(b.lineWidthPlus,c.lineWidthPlus,0)),a=b.fillColor||c.fillColor||a,e=b.lineColor||c.lineColor||
e);return{stroke:e,"stroke-width":d,fill:a}},destroy:function(){var a=this,c=a.chart,d=/AppleWebKit\/533/.test(K.navigator.userAgent),e,h,f=a.data||[],g,z;k(a,"destroy");m(a);u(a.axisTypes||[],function(b){(z=a[b])&&z.series&&(l(z.series,a),z.isDirty=z.forceRedraw=!0)});a.legendItem&&a.chart.legend.destroyItem(a);for(h=f.length;h--;)(g=f[h])&&g.destroy&&g.destroy();a.points=null;clearTimeout(a.animationTimeout);q(a,function(a,c){a instanceof b&&!a.survive&&(e=d&&"group"===c?"hide":"destroy",a[e]())});
c.hoverSeries===a&&(c.hoverSeries=null);l(c.series,a);c.orderSeries();q(a,function(b,c){delete a[c]})},getGraphPath:function(a,b,c){var d=this,h=d.options,n=h.step,e,f=[],m=[],k;a=a||d.points;(e=a.reversed)&&a.reverse();(n={right:1,center:2}[n]||n&&3)&&e&&(n=4-n);!h.connectNulls||b||c||(a=this.getValidPoints(a));u(a,function(e,l){var q=e.plotX,g=e.plotY,y=a[l-1];(e.leftCliff||y&&y.rightCliff)&&!c&&(k=!0);e.isNull&&!v(b)&&0<l?k=!h.connectNulls:e.isNull&&!b?k=!0:(0===l||k?l=["M",e.plotX,e.plotY]:d.getPointSpline?
l=d.getPointSpline(a,e,l):n?(l=1===n?["L",y.plotX,g]:2===n?["L",(y.plotX+q)/2,y.plotY,"L",(y.plotX+q)/2,g]:["L",q,y.plotY],l.push("L",q,g)):l=["L",q,g],m.push(e.x),n&&m.push(e.x),f.push.apply(f,l),k=!1)});f.xMap=m;return d.graphPath=f},drawGraph:function(){var a=this,b=this.options,c=(this.gappedPath||this.getGraphPath).call(this),d=[["graph","highcharts-graph",b.lineColor||this.color,b.dashStyle]];u(this.zones,function(c,e){d.push(["zone-graph-"+e,"highcharts-graph highcharts-zone-graph-"+e+" "+
(c.className||""),c.color||a.color,c.dashStyle||b.dashStyle])});u(d,function(d,e){var h=d[0],n=a[h];n?(n.endX=c.xMap,n.animate({d:c})):c.length&&(a[h]=a.chart.renderer.path(c).addClass(d[1]).attr({zIndex:1}).add(a.group),n={stroke:d[2],"stroke-width":b.lineWidth,fill:a.fillGraph&&a.color||"none"},d[3]?n.dashstyle=d[3]:"square"!==b.linecap&&(n["stroke-linecap"]=n["stroke-linejoin"]="round"),n=a[h].attr(n).shadow(2>e&&b.shadow));n&&(n.startX=c.xMap,n.isArea=c.isArea)})},applyZones:function(){var a=
this,b=this.chart,c=b.renderer,d=this.zones,h,e,f=this.clips||[],m,k=this.graph,l=this.area,q=Math.max(b.chartWidth,b.chartHeight),g=this[(this.zoneAxis||"y")+"Axis"],z,p,x=b.inverted,C,t,r,K,v=!1;d.length&&(k||l)&&g&&void 0!==g.min&&(p=g.reversed,C=g.horiz,k&&k.hide(),l&&l.hide(),z=g.getExtremes(),u(d,function(d,n){h=p?C?b.plotWidth:0:C?0:g.toPixels(z.min);h=Math.min(Math.max(I(e,h),0),q);e=Math.min(Math.max(Math.round(g.toPixels(I(d.value,z.max),!0)),0),q);v&&(h=e=g.toPixels(z.max));t=Math.abs(h-
e);r=Math.min(h,e);K=Math.max(h,e);g.isXAxis?(m={x:x?K:r,y:0,width:t,height:q},C||(m.x=b.plotHeight-m.x)):(m={x:0,y:x?K:r,width:q,height:t},C&&(m.y=b.plotWidth-m.y));x&&c.isVML&&(m=g.isXAxis?{x:0,y:p?r:K,height:m.width,width:b.chartWidth}:{x:m.y-b.plotLeft-b.spacingBox.x,y:0,width:m.height,height:b.chartHeight});f[n]?f[n].animate(m):(f[n]=c.clipRect(m),k&&a["zone-graph-"+n].clip(f[n]),l&&a["zone-area-"+n].clip(f[n]));v=d.value>z.max}),this.clips=f)},invertGroups:function(a){function b(){u(["group",
"markerGroup"],function(b){c[b]&&(d.renderer.isVML&&c[b].attr({width:c.yAxis.len,height:c.xAxis.len}),c[b].width=c.yAxis.len,c[b].height=c.xAxis.len,c[b].invert(a))})}var c=this,d=c.chart,h;c.xAxis&&(h=D(d,"resize",b),D(c,"destroy",h),b(a),c.invertGroups=b)},plotGroup:function(a,b,c,d,h){var e=this[a],n=!e;n&&(this[a]=e=this.chart.renderer.g().attr({zIndex:d||.1}).add(h));e.addClass("highcharts-"+b+" highcharts-series-"+this.index+" highcharts-"+this.type+"-series highcharts-color-"+this.colorIndex+
" "+(this.options.className||""),!0);e.attr({visibility:c})[n?"attr":"animate"](this.getPlotBox());return e},getPlotBox:function(){var a=this.chart,b=this.xAxis,c=this.yAxis;a.inverted&&(b=c,c=this.xAxis);return{translateX:b?b.left:a.plotLeft,translateY:c?c.top:a.plotTop,scaleX:1,scaleY:1}},render:function(){var a=this,b=a.chart,c,d=a.options,h=!!a.animate&&b.renderer.isSVG&&B(d.animation).duration,e=a.visible?"inherit":"hidden",f=d.zIndex,m=a.hasRendered,k=b.seriesGroup,l=b.inverted;c=a.plotGroup("group",
"series",e,f,k);a.markerGroup=a.plotGroup("markerGroup","markers",e,f,k);h&&a.animate(!0);c.inverted=a.isCartesian?l:!1;a.drawGraph&&(a.drawGraph(),a.applyZones());a.drawDataLabels&&a.drawDataLabels();a.visible&&a.drawPoints();a.drawTracker&&!1!==a.options.enableMouseTracking&&a.drawTracker();a.invertGroups(l);!1===d.clip||a.sharedClipKey||m||c.clip(b.clipRect);h&&a.animate();m||(a.animationTimeout=z(function(){a.afterAnimate()},h));a.isDirty=!1;a.hasRendered=!0},redraw:function(){var a=this.chart,
b=this.isDirty||this.isDirtyData,c=this.group,d=this.xAxis,h=this.yAxis;c&&(a.inverted&&c.attr({width:a.plotWidth,height:a.plotHeight}),c.animate({translateX:I(d&&d.left,a.plotLeft),translateY:I(h&&h.top,a.plotTop)}));this.translate();this.render();b&&delete this.kdTree},kdAxisArray:["clientX","plotY"],searchPoint:function(a,b){var c=this.xAxis,d=this.yAxis,h=this.chart.inverted;return this.searchKDTree({clientX:h?c.len-a.chartY+c.pos:a.chartX-c.pos,plotY:h?d.len-a.chartX+d.pos:a.chartY-d.pos},b)},
buildKDTree:function(){function a(c,d,e){var h,n;if(n=c&&c.length)return h=b.kdAxisArray[d%e],c.sort(function(a,b){return a[h]-b[h]}),n=Math.floor(n/2),{point:c[n],left:a(c.slice(0,n),d+1,e),right:a(c.slice(n+1),d+1,e)}}this.buildingKdTree=!0;var b=this,c=-1<b.options.findNearestPointBy.indexOf("y")?2:1;delete b.kdTree;z(function(){b.kdTree=a(b.getValidPoints(null,!b.directTouch),c,c);b.buildingKdTree=!1},b.options.kdNow?0:1)},searchKDTree:function(a,b){function c(a,b,n,m){var k=b.point,l=d.kdAxisArray[n%
m],q,g,z=k;g=v(a[h])&&v(k[h])?Math.pow(a[h]-k[h],2):null;q=v(a[e])&&v(k[e])?Math.pow(a[e]-k[e],2):null;q=(g||0)+(q||0);k.dist=v(q)?Math.sqrt(q):Number.MAX_VALUE;k.distX=v(g)?Math.sqrt(g):Number.MAX_VALUE;l=a[l]-k[l];q=0>l?"left":"right";g=0>l?"right":"left";b[q]&&(q=c(a,b[q],n+1,m),z=q[f]<z[f]?q:k);b[g]&&Math.sqrt(l*l)<z[f]&&(a=c(a,b[g],n+1,m),z=a[f]<z[f]?a:z);return z}var d=this,h=this.kdAxisArray[0],e=this.kdAxisArray[1],f=b?"distX":"dist";b=-1<d.options.findNearestPointBy.indexOf("y")?2:1;this.kdTree||
this.buildingKdTree||this.buildKDTree();if(this.kdTree)return c(a,this.kdTree,b,b)}})})(M);(function(a){var D=a.Axis,B=a.Chart,G=a.correctFloat,E=a.defined,r=a.destroyObjectProperties,g=a.each,p=a.format,t=a.objectEach,v=a.pick,u=a.Series;a.StackItem=function(a,e,k,f,d){var l=a.chart.inverted;this.axis=a;this.isNegative=k;this.options=e;this.x=f;this.total=null;this.points={};this.stack=d;this.rightCliff=this.leftCliff=0;this.alignOptions={align:e.align||(l?k?"left":"right":"center"),verticalAlign:e.verticalAlign||
(l?"middle":k?"bottom":"top"),y:v(e.y,l?4:k?14:-6),x:v(e.x,l?k?-6:6:0)};this.textAlign=e.textAlign||(l?k?"right":"left":"center")};a.StackItem.prototype={destroy:function(){r(this,this.axis)},render:function(a){var e=this.options,k=e.format,k=k?p(k,this):e.formatter.call(this);this.label?this.label.attr({text:k,visibility:"hidden"}):this.label=this.axis.chart.renderer.text(k,null,null,e.useHTML).css(e.style).attr({align:this.textAlign,rotation:e.rotation,visibility:"hidden"}).add(a)},setOffset:function(a,
e){var k=this.axis,f=k.chart,d=k.translate(k.usePercentage?100:this.total,0,0,0,1),k=k.translate(0),k=Math.abs(d-k);a=f.xAxis[0].translate(this.x)+a;d=this.getStackBox(f,this,a,d,e,k);if(e=this.label)e.align(this.alignOptions,null,d),d=e.alignAttr,e[!1===this.options.crop||f.isInsidePlot(d.x,d.y)?"show":"hide"](!0)},getStackBox:function(a,e,k,f,d,g){var l=e.axis.reversed,c=a.inverted;a=a.plotHeight;e=e.isNegative&&!l||!e.isNegative&&l;return{x:c?e?f:f-g:k,y:c?a-k-d:e?a-f-g:a-f,width:c?g:d,height:c?
d:g}}};B.prototype.getStacks=function(){var a=this;g(a.yAxis,function(a){a.stacks&&a.hasVisibleSeries&&(a.oldStacks=a.stacks)});g(a.series,function(e){!e.options.stacking||!0!==e.visible&&!1!==a.options.chart.ignoreHiddenSeries||(e.stackKey=e.type+v(e.options.stack,""))})};D.prototype.buildStacks=function(){var a=this.series,e=v(this.options.reversedStacks,!0),k=a.length,f;if(!this.isXAxis){this.usePercentage=!1;for(f=k;f--;)a[e?f:k-f-1].setStackedPoints();if(this.usePercentage)for(f=0;f<k;f++)a[f].setPercentStacks()}};
D.prototype.renderStackTotals=function(){var a=this.chart,e=a.renderer,k=this.stacks,f=this.stackTotalGroup;f||(this.stackTotalGroup=f=e.g("stack-labels").attr({visibility:"visible",zIndex:6}).add());f.translate(a.plotLeft,a.plotTop);t(k,function(a){t(a,function(a){a.render(f)})})};D.prototype.resetStacks=function(){var a=this,e=a.stacks;a.isXAxis||t(e,function(e){t(e,function(f,d){f.touched<a.stacksTouched?(f.destroy(),delete e[d]):(f.total=null,f.cum=null)})})};D.prototype.cleanStacks=function(){var a;
this.isXAxis||(this.oldStacks&&(a=this.stacks=this.oldStacks),t(a,function(a){t(a,function(a){a.cum=a.total})}))};u.prototype.setStackedPoints=function(){if(this.options.stacking&&(!0===this.visible||!1===this.chart.options.chart.ignoreHiddenSeries)){var l=this.processedXData,e=this.processedYData,k=[],f=e.length,d=this.options,g=d.threshold,p=d.startFromThreshold?g:0,c=d.stack,d=d.stacking,q=this.stackKey,r="-"+q,m=this.negStacks,t=this.yAxis,b=t.stacks,z=t.oldStacks,K,y,A,n,H,h,w;t.stacksTouched+=
1;for(H=0;H<f;H++)h=l[H],w=e[H],K=this.getStackIndicator(K,h,this.index),n=K.key,A=(y=m&&w<(p?0:g))?r:q,b[A]||(b[A]={}),b[A][h]||(z[A]&&z[A][h]?(b[A][h]=z[A][h],b[A][h].total=null):b[A][h]=new a.StackItem(t,t.options.stackLabels,y,h,c)),A=b[A][h],null!==w&&(A.points[n]=A.points[this.index]=[v(A.cum,p)],E(A.cum)||(A.base=n),A.touched=t.stacksTouched,0<K.index&&!1===this.singleStacks&&(A.points[n][0]=A.points[this.index+","+h+",0"][0])),"percent"===d?(y=y?q:r,m&&b[y]&&b[y][h]?(y=b[y][h],A.total=y.total=
Math.max(y.total,A.total)+Math.abs(w)||0):A.total=G(A.total+(Math.abs(w)||0))):A.total=G(A.total+(w||0)),A.cum=v(A.cum,p)+(w||0),null!==w&&(A.points[n].push(A.cum),k[H]=A.cum);"percent"===d&&(t.usePercentage=!0);this.stackedYData=k;t.oldStacks={}}};u.prototype.setPercentStacks=function(){var a=this,e=a.stackKey,k=a.yAxis.stacks,f=a.processedXData,d;g([e,"-"+e],function(e){for(var l=f.length,c,q;l--;)if(c=f[l],d=a.getStackIndicator(d,c,a.index,e),c=(q=k[e]&&k[e][c])&&q.points[d.key])q=q.total?100/
q.total:0,c[0]=G(c[0]*q),c[1]=G(c[1]*q),a.stackedYData[l]=c[1]})};u.prototype.getStackIndicator=function(a,e,k,f){!E(a)||a.x!==e||f&&a.key!==f?a={x:e,index:0,key:f}:a.index++;a.key=[k,e,a.index].join();return a}})(M);(function(a){var D=a.addEvent,B=a.animate,G=a.Axis,E=a.createElement,r=a.css,g=a.defined,p=a.each,t=a.erase,v=a.extend,u=a.fireEvent,l=a.inArray,e=a.isNumber,k=a.isObject,f=a.isArray,d=a.merge,x=a.objectEach,C=a.pick,c=a.Point,q=a.Series,I=a.seriesTypes,m=a.setAnimation,J=a.splat;v(a.Chart.prototype,
{addSeries:function(a,c,d){var b,e=this;a&&(c=C(c,!0),u(e,"addSeries",{options:a},function(){b=e.initSeries(a);e.isDirtyLegend=!0;e.linkSeries();c&&e.redraw(d)}));return b},addAxis:function(a,c,e,f){var b=c?"xAxis":"yAxis",n=this.options;a=d(a,{index:this[b].length,isX:c});c=new G(this,a);n[b]=J(n[b]||{});n[b].push(a);C(e,!0)&&this.redraw(f);return c},showLoading:function(a){var b=this,c=b.options,d=b.loadingDiv,e=c.loading,n=function(){d&&r(d,{left:b.plotLeft+"px",top:b.plotTop+"px",width:b.plotWidth+
"px",height:b.plotHeight+"px"})};d||(b.loadingDiv=d=E("div",{className:"highcharts-loading highcharts-loading-hidden"},null,b.container),b.loadingSpan=E("span",{className:"highcharts-loading-inner"},null,d),D(b,"redraw",n));d.className="highcharts-loading";b.loadingSpan.innerHTML=a||c.lang.loading;r(d,v(e.style,{zIndex:10}));r(b.loadingSpan,e.labelStyle);b.loadingShown||(r(d,{opacity:0,display:""}),B(d,{opacity:e.style.opacity||.5},{duration:e.showDuration||0}));b.loadingShown=!0;n()},hideLoading:function(){var a=
this.options,c=this.loadingDiv;c&&(c.className="highcharts-loading highcharts-loading-hidden",B(c,{opacity:0},{duration:a.loading.hideDuration||100,complete:function(){r(c,{display:"none"})}}));this.loadingShown=!1},propsRequireDirtyBox:"backgroundColor borderColor borderWidth margin marginTop marginRight marginBottom marginLeft spacing spacingTop spacingRight spacingBottom spacingLeft borderRadius plotBackgroundColor plotBackgroundImage plotBorderColor plotBorderWidth plotShadow shadow".split(" "),
propsRequireUpdateSeries:"chart.inverted chart.polar chart.ignoreHiddenSeries chart.type colors plotOptions tooltip".split(" "),update:function(a,c,f){var b=this,m={credits:"addCredits",title:"setTitle",subtitle:"setSubtitle"},n=a.chart,k,h,q=[];if(n){d(!0,b.options.chart,n);"className"in n&&b.setClassName(n.className);if("inverted"in n||"polar"in n)b.propFromSeries(),k=!0;"alignTicks"in n&&(k=!0);x(n,function(a,c){-1!==l("chart."+c,b.propsRequireUpdateSeries)&&(h=!0);-1!==l(c,b.propsRequireDirtyBox)&&
(b.isDirtyBox=!0)});"style"in n&&b.renderer.setStyle(n.style)}a.colors&&(this.options.colors=a.colors);a.plotOptions&&d(!0,this.options.plotOptions,a.plotOptions);x(a,function(a,c){if(b[c]&&"function"===typeof b[c].update)b[c].update(a,!1);else if("function"===typeof b[m[c]])b[m[c]](a);"chart"!==c&&-1!==l(c,b.propsRequireUpdateSeries)&&(h=!0)});p("xAxis yAxis zAxis series colorAxis pane".split(" "),function(c){a[c]&&(p(J(a[c]),function(a,d){(d=g(a.id)&&b.get(a.id)||b[c][d])&&d.coll===c&&(d.update(a,
!1),f&&(d.touched=!0));if(!d&&f)if("series"===c)b.addSeries(a,!1).touched=!0;else if("xAxis"===c||"yAxis"===c)b.addAxis(a,"xAxis"===c,!1).touched=!0}),f&&p(b[c],function(a){a.touched?delete a.touched:q.push(a)}))});p(q,function(a){a.remove(!1)});k&&p(b.axes,function(a){a.update({},!1)});h&&p(b.series,function(a){a.update({},!1)});a.loading&&d(!0,b.options.loading,a.loading);k=n&&n.width;n=n&&n.height;e(k)&&k!==b.chartWidth||e(n)&&n!==b.chartHeight?b.setSize(k,n):C(c,!0)&&b.redraw()},setSubtitle:function(a){this.setTitle(void 0,
a)}});v(c.prototype,{update:function(a,c,d,e){function b(){n.applyOptions(a);null===n.y&&h&&(n.graphic=h.destroy());k(a,!0)&&(h&&h.element&&a&&a.marker&&void 0!==a.marker.symbol&&(n.graphic=h.destroy()),a&&a.dataLabels&&n.dataLabel&&(n.dataLabel=n.dataLabel.destroy()));m=n.index;f.updateParallelArrays(n,m);q.data[m]=k(q.data[m],!0)||k(a,!0)?n.options:a;f.isDirty=f.isDirtyData=!0;!f.fixedBox&&f.hasCartesianSeries&&(l.isDirtyBox=!0);"point"===q.legendType&&(l.isDirtyLegend=!0);c&&l.redraw(d)}var n=
this,f=n.series,h=n.graphic,m,l=f.chart,q=f.options;c=C(c,!0);!1===e?b():n.firePointEvent("update",{options:a},b)},remove:function(a,c){this.series.removePoint(l(this,this.series.data),a,c)}});v(q.prototype,{addPoint:function(a,c,d,e){var b=this.options,n=this.data,f=this.chart,h=this.xAxis,h=h&&h.hasNames&&h.names,m=b.data,k,l,q=this.xData,g,p;c=C(c,!0);k={series:this};this.pointClass.prototype.applyOptions.apply(k,[a]);p=k.x;g=q.length;if(this.requireSorting&&p<q[g-1])for(l=!0;g&&q[g-1]>p;)g--;
this.updateParallelArrays(k,"splice",g,0,0);this.updateParallelArrays(k,g);h&&k.name&&(h[p]=k.name);m.splice(g,0,a);l&&(this.data.splice(g,0,null),this.processData());"point"===b.legendType&&this.generatePoints();d&&(n[0]&&n[0].remove?n[0].remove(!1):(n.shift(),this.updateParallelArrays(k,"shift"),m.shift()));this.isDirtyData=this.isDirty=!0;c&&f.redraw(e)},removePoint:function(a,c,d){var b=this,e=b.data,n=e[a],f=b.points,h=b.chart,k=function(){f&&f.length===e.length&&f.splice(a,1);e.splice(a,1);
b.options.data.splice(a,1);b.updateParallelArrays(n||{series:b},"splice",a,1);n&&n.destroy();b.isDirty=!0;b.isDirtyData=!0;c&&h.redraw()};m(d,h);c=C(c,!0);n?n.firePointEvent("remove",null,k):k()},remove:function(a,c,d){function b(){e.destroy();f.isDirtyLegend=f.isDirtyBox=!0;f.linkSeries();C(a,!0)&&f.redraw(c)}var e=this,f=e.chart;!1!==d?u(e,"remove",null,b):b()},update:function(a,c){var b=this,e=b.chart,f=b.userOptions,n=b.oldType||b.type,m=a.type||f.type||e.options.chart.type,h=I[n].prototype,k,
l=["group","markerGroup","dataLabelsGroup","navigatorSeries","baseSeries"],q=b.finishedAnimating&&{animation:!1};if(Object.keys&&"data"===Object.keys(a).toString())return this.setData(a.data,c);if(m&&m!==n||void 0!==a.zIndex)l.length=0;p(l,function(a){l[a]=b[a];delete b[a]});a=d(f,q,{index:b.index,pointStart:b.xData[0]},{data:b.options.data},a);b.remove(!1,null,!1);for(k in h)b[k]=void 0;v(b,I[m||n].prototype);p(l,function(a){b[a]=l[a]});b.init(e,a);b.oldType=n;e.linkSeries();C(c,!0)&&e.redraw(!1)}});
v(G.prototype,{update:function(a,c){var b=this.chart;a=b.options[this.coll][this.options.index]=d(this.userOptions,a);this.destroy(!0);this.init(b,v(a,{events:void 0}));b.isDirtyBox=!0;C(c,!0)&&b.redraw()},remove:function(a){for(var b=this.chart,c=this.coll,d=this.series,e=d.length;e--;)d[e]&&d[e].remove(!1);t(b.axes,this);t(b[c],this);f(b.options[c])?b.options[c].splice(this.options.index,1):delete b.options[c];p(b[c],function(a,b){a.options.index=b});this.destroy();b.isDirtyBox=!0;C(a,!0)&&b.redraw()},
setTitle:function(a,c){this.update({title:a},c)},setCategories:function(a,c){this.update({categories:a},c)}})})(M);(function(a){var D=a.color,B=a.each,G=a.map,E=a.pick,r=a.Series,g=a.seriesType;g("area","line",{softThreshold:!1,threshold:0},{singleStacks:!1,getStackPoints:function(g){var p=[],r=[],u=this.xAxis,l=this.yAxis,e=l.stacks[this.stackKey],k={},f=this.index,d=l.series,x=d.length,C,c=E(l.options.reversedStacks,!0)?1:-1,q;g=g||this.points;if(this.options.stacking){for(q=0;q<g.length;q++)k[g[q].x]=
g[q];a.objectEach(e,function(a,c){null!==a.total&&r.push(c)});r.sort(function(a,c){return a-c});C=G(d,function(){return this.visible});B(r,function(a,d){var m=0,b,g;if(k[a]&&!k[a].isNull)p.push(k[a]),B([-1,1],function(m){var l=1===m?"rightNull":"leftNull",p=0,n=e[r[d+m]];if(n)for(q=f;0<=q&&q<x;)b=n.points[q],b||(q===f?k[a][l]=!0:C[q]&&(g=e[a].points[q])&&(p-=g[1]-g[0])),q+=c;k[a][1===m?"rightCliff":"leftCliff"]=p});else{for(q=f;0<=q&&q<x;){if(b=e[a].points[q]){m=b[1];break}q+=c}m=l.translate(m,0,
1,0,1);p.push({isNull:!0,plotX:u.translate(a,0,0,0,1),x:a,plotY:m,yBottom:m})}})}return p},getGraphPath:function(a){var g=r.prototype.getGraphPath,p=this.options,u=p.stacking,l=this.yAxis,e,k,f=[],d=[],x=this.index,C,c=l.stacks[this.stackKey],q=p.threshold,I=l.getThreshold(p.threshold),m,p=p.connectNulls||"percent"===u,J=function(b,e,m){var k=a[b];b=u&&c[k.x].points[x];var g=k[m+"Null"]||0;m=k[m+"Cliff"]||0;var n,p,k=!0;m||g?(n=(g?b[0]:b[1])+m,p=b[0]+m,k=!!g):!u&&a[e]&&a[e].isNull&&(n=p=q);void 0!==
n&&(d.push({plotX:C,plotY:null===n?I:l.getThreshold(n),isNull:k,isCliff:!0}),f.push({plotX:C,plotY:null===p?I:l.getThreshold(p),doCurve:!1}))};a=a||this.points;u&&(a=this.getStackPoints(a));for(e=0;e<a.length;e++)if(k=a[e].isNull,C=E(a[e].rectPlotX,a[e].plotX),m=E(a[e].yBottom,I),!k||p)p||J(e,e-1,"left"),k&&!u&&p||(d.push(a[e]),f.push({x:e,plotX:C,plotY:m})),p||J(e,e+1,"right");e=g.call(this,d,!0,!0);f.reversed=!0;k=g.call(this,f,!0,!0);k.length&&(k[0]="L");k=e.concat(k);g=g.call(this,d,!1,p);k.xMap=
e.xMap;this.areaPath=k;return g},drawGraph:function(){this.areaPath=[];r.prototype.drawGraph.apply(this);var a=this,g=this.areaPath,v=this.options,u=[["area","highcharts-area",this.color,v.fillColor]];B(this.zones,function(l,e){u.push(["zone-area-"+e,"highcharts-area highcharts-zone-area-"+e+" "+l.className,l.color||a.color,l.fillColor||v.fillColor])});B(u,function(l){var e=l[0],k=a[e];k?(k.endX=g.xMap,k.animate({d:g})):(k=a[e]=a.chart.renderer.path(g).addClass(l[1]).attr({fill:E(l[3],D(l[2]).setOpacity(E(v.fillOpacity,
.75)).get()),zIndex:0}).add(a.group),k.isArea=!0);k.startX=g.xMap;k.shiftUnit=v.step?2:1})},drawLegendSymbol:a.LegendSymbolMixin.drawRectangle})})(M);(function(a){var D=a.pick;a=a.seriesType;a("spline","line",{},{getPointSpline:function(a,G,E){var r=G.plotX,g=G.plotY,p=a[E-1];E=a[E+1];var t,v,u,l;if(p&&!p.isNull&&!1!==p.doCurve&&!G.isCliff&&E&&!E.isNull&&!1!==E.doCurve&&!G.isCliff){a=p.plotY;u=E.plotX;E=E.plotY;var e=0;t=(1.5*r+p.plotX)/2.5;v=(1.5*g+a)/2.5;u=(1.5*r+u)/2.5;l=(1.5*g+E)/2.5;u!==t&&(e=
(l-v)*(u-r)/(u-t)+g-l);v+=e;l+=e;v>a&&v>g?(v=Math.max(a,g),l=2*g-v):v<a&&v<g&&(v=Math.min(a,g),l=2*g-v);l>E&&l>g?(l=Math.max(E,g),v=2*g-l):l<E&&l<g&&(l=Math.min(E,g),v=2*g-l);G.rightContX=u;G.rightContY=l}G=["C",D(p.rightContX,p.plotX),D(p.rightContY,p.plotY),D(t,r),D(v,g),r,g];p.rightContX=p.rightContY=null;return G}})})(M);(function(a){var D=a.seriesTypes.area.prototype,B=a.seriesType;B("areaspline","spline",a.defaultPlotOptions.area,{getStackPoints:D.getStackPoints,getGraphPath:D.getGraphPath,
drawGraph:D.drawGraph,drawLegendSymbol:a.LegendSymbolMixin.drawRectangle})})(M);(function(a){var D=a.animObject,B=a.color,G=a.each,E=a.extend,r=a.isNumber,g=a.merge,p=a.pick,t=a.Series,v=a.seriesType,u=a.svg;v("column","line",{borderRadius:0,crisp:!0,groupPadding:.2,marker:null,pointPadding:.1,minPointLength:0,cropThreshold:50,pointRange:null,states:{hover:{halo:!1,brightness:.1,shadow:!1},select:{color:"#cccccc",borderColor:"#000000",shadow:!1}},dataLabels:{align:null,verticalAlign:null,y:null},
softThreshold:!1,startFromThreshold:!0,stickyTracking:!1,tooltip:{distance:6},threshold:0,borderColor:"#ffffff"},{cropShoulder:0,directTouch:!0,trackerGroups:["group","dataLabelsGroup"],negStacks:!0,init:function(){t.prototype.init.apply(this,arguments);var a=this,e=a.chart;e.hasRendered&&G(e.series,function(e){e.type===a.type&&(e.isDirty=!0)})},getColumnMetrics:function(){var a=this,e=a.options,k=a.xAxis,f=a.yAxis,d=k.reversed,g,C={},c=0;!1===e.grouping?c=1:G(a.chart.series,function(d){var b=d.options,
e=d.yAxis,m;d.type!==a.type||!d.visible&&a.chart.options.chart.ignoreHiddenSeries||f.len!==e.len||f.pos!==e.pos||(b.stacking?(g=d.stackKey,void 0===C[g]&&(C[g]=c++),m=C[g]):!1!==b.grouping&&(m=c++),d.columnIndex=m)});var q=Math.min(Math.abs(k.transA)*(k.ordinalSlope||e.pointRange||k.closestPointRange||k.tickInterval||1),k.len),r=q*e.groupPadding,m=(q-2*r)/(c||1),e=Math.min(e.maxPointWidth||k.len,p(e.pointWidth,m*(1-2*e.pointPadding)));a.columnMetrics={width:e,offset:(m-e)/2+(r+((a.columnIndex||0)+
(d?1:0))*m-q/2)*(d?-1:1)};return a.columnMetrics},crispCol:function(a,e,k,f){var d=this.chart,g=this.borderWidth,l=-(g%2?.5:0),g=g%2?.5:1;d.inverted&&d.renderer.isVML&&(g+=1);this.options.crisp&&(k=Math.round(a+k)+l,a=Math.round(a)+l,k-=a);f=Math.round(e+f)+g;l=.5>=Math.abs(e)&&.5<f;e=Math.round(e)+g;f-=e;l&&f&&(--e,f+=1);return{x:a,y:e,width:k,height:f}},translate:function(){var a=this,e=a.chart,k=a.options,f=a.dense=2>a.closestPointRange*a.xAxis.transA,f=a.borderWidth=p(k.borderWidth,f?0:1),d=a.yAxis,
g=a.translatedThreshold=d.getThreshold(k.threshold),C=p(k.minPointLength,5),c=a.getColumnMetrics(),q=c.width,r=a.barW=Math.max(q,1+2*f),m=a.pointXOffset=c.offset;e.inverted&&(g-=.5);k.pointPadding&&(r=Math.ceil(r));t.prototype.translate.apply(a);G(a.points,function(c){var b=p(c.yBottom,g),f=999+Math.abs(b),f=Math.min(Math.max(-f,c.plotY),d.len+f),k=c.plotX+m,l=r,x=Math.min(f,b),n,H=Math.max(f,b)-x;Math.abs(H)<C&&C&&(H=C,n=!d.reversed&&!c.negative||d.reversed&&c.negative,x=Math.abs(x-g)>C?b-C:g-(n?
C:0));c.barX=k;c.pointWidth=q;c.tooltipPos=e.inverted?[d.len+d.pos-e.plotLeft-f,a.xAxis.len-k-l/2,H]:[k+l/2,f+d.pos-e.plotTop,H];c.shapeType="rect";c.shapeArgs=a.crispCol.apply(a,c.isNull?[k,g,l,0]:[k,x,l,H])})},getSymbol:a.noop,drawLegendSymbol:a.LegendSymbolMixin.drawRectangle,drawGraph:function(){this.group[this.dense?"addClass":"removeClass"]("highcharts-dense-data")},pointAttribs:function(a,e){var k=this.options,f,d=this.pointAttrToOptions||{};f=d.stroke||"borderColor";var l=d["stroke-width"]||
"borderWidth",p=a&&a.color||this.color,c=a[f]||k[f]||this.color||p,q=a[l]||k[l]||this[l]||0,d=k.dashStyle;a&&this.zones.length&&(p=a.getZone(),p=a.options.color||p&&p.color||this.color);e&&(a=g(k.states[e],a.options.states&&a.options.states[e]||{}),e=a.brightness,p=a.color||void 0!==e&&B(p).brighten(a.brightness).get()||p,c=a[f]||c,q=a[l]||q,d=a.dashStyle||d);f={fill:p,stroke:c,"stroke-width":q};d&&(f.dashstyle=d);return f},drawPoints:function(){var a=this,e=this.chart,k=a.options,f=e.renderer,d=
k.animationLimit||250,p;G(a.points,function(l){var c=l.graphic;if(r(l.plotY)&&null!==l.y){p=l.shapeArgs;if(c)c[e.pointCount<d?"animate":"attr"](g(p));else l.graphic=c=f[l.shapeType](p).add(l.group||a.group);k.borderRadius&&c.attr({r:k.borderRadius});c.attr(a.pointAttribs(l,l.selected&&"select")).shadow(k.shadow,null,k.stacking&&!k.borderRadius);c.addClass(l.getClassName(),!0)}else c&&(l.graphic=c.destroy())})},animate:function(a){var e=this,k=this.yAxis,f=e.options,d=this.chart.inverted,g={};u&&(a?
(g.scaleY=.001,a=Math.min(k.pos+k.len,Math.max(k.pos,k.toPixels(f.threshold))),d?g.translateX=a-k.len:g.translateY=a,e.group.attr(g)):(g[d?"translateX":"translateY"]=k.pos,e.group.animate(g,E(D(e.options.animation),{step:function(a,c){e.group.attr({scaleY:Math.max(.001,c.pos)})}})),e.animate=null))},remove:function(){var a=this,e=a.chart;e.hasRendered&&G(e.series,function(e){e.type===a.type&&(e.isDirty=!0)});t.prototype.remove.apply(a,arguments)}})})(M);(function(a){a=a.seriesType;a("bar","column",
null,{inverted:!0})})(M);(function(a){var D=a.Series;a=a.seriesType;a("scatter","line",{lineWidth:0,findNearestPointBy:"xy",marker:{enabled:!0},tooltip:{headerFormat:'\x3cspan style\x3d"color:{point.color}"\x3e\u25cf\x3c/span\x3e \x3cspan style\x3d"font-size: 0.85em"\x3e {series.name}\x3c/span\x3e\x3cbr/\x3e',pointFormat:"x: \x3cb\x3e{point.x}\x3c/b\x3e\x3cbr/\x3ey: \x3cb\x3e{point.y}\x3c/b\x3e\x3cbr/\x3e"}},{sorted:!1,requireSorting:!1,noSharedTooltip:!0,trackerGroups:["group","markerGroup","dataLabelsGroup"],
takeOrdinalPosition:!1,drawGraph:function(){this.options.lineWidth&&D.prototype.drawGraph.call(this)}})})(M);(function(a){var D=a.pick,B=a.relativeLength;a.CenteredSeriesMixin={getCenter:function(){var a=this.options,E=this.chart,r=2*(a.slicedOffset||0),g=E.plotWidth-2*r,E=E.plotHeight-2*r,p=a.center,p=[D(p[0],"50%"),D(p[1],"50%"),a.size||"100%",a.innerSize||0],t=Math.min(g,E),v,u;for(v=0;4>v;++v)u=p[v],a=2>v||2===v&&/%$/.test(u),p[v]=B(u,[g,E,t,p[2]][v])+(a?r:0);p[3]>p[2]&&(p[3]=p[2]);return p}}})(M);
(function(a){var D=a.addEvent,B=a.defined,G=a.each,E=a.extend,r=a.inArray,g=a.noop,p=a.pick,t=a.Point,v=a.Series,u=a.seriesType,l=a.setAnimation;u("pie","line",{center:[null,null],clip:!1,colorByPoint:!0,dataLabels:{distance:30,enabled:!0,formatter:function(){return this.point.isNull?void 0:this.point.name},x:0},ignoreHiddenPoint:!0,legendType:"point",marker:null,size:null,showInLegend:!1,slicedOffset:10,stickyTracking:!1,tooltip:{followPointer:!0},borderColor:"#ffffff",borderWidth:1,states:{hover:{brightness:.1,
shadow:!1}}},{isCartesian:!1,requireSorting:!1,directTouch:!0,noSharedTooltip:!0,trackerGroups:["group","dataLabelsGroup"],axisTypes:[],pointAttribs:a.seriesTypes.column.prototype.pointAttribs,animate:function(a){var e=this,f=e.points,d=e.startAngleRad;a||(G(f,function(a){var f=a.graphic,c=a.shapeArgs;f&&(f.attr({r:a.startR||e.center[3]/2,start:d,end:d}),f.animate({r:c.r,start:c.start,end:c.end},e.options.animation))}),e.animate=null)},updateTotals:function(){var a,k=0,f=this.points,d=f.length,g,
l=this.options.ignoreHiddenPoint;for(a=0;a<d;a++)g=f[a],k+=l&&!g.visible?0:g.isNull?0:g.y;this.total=k;for(a=0;a<d;a++)g=f[a],g.percentage=0<k&&(g.visible||!l)?g.y/k*100:0,g.total=k},generatePoints:function(){v.prototype.generatePoints.call(this);this.updateTotals()},translate:function(a){this.generatePoints();var e=0,f=this.options,d=f.slicedOffset,g=d+(f.borderWidth||0),l,c,q,r=f.startAngle||0,m=this.startAngleRad=Math.PI/180*(r-90),r=(this.endAngleRad=Math.PI/180*(p(f.endAngle,r+360)-90))-m,t=
this.points,b,z=f.dataLabels.distance,f=f.ignoreHiddenPoint,u,y=t.length,A;a||(this.center=a=this.getCenter());this.getX=function(b,c,d){q=Math.asin(Math.min((b-a[1])/(a[2]/2+d.labelDistance),1));return a[0]+(c?-1:1)*Math.cos(q)*(a[2]/2+d.labelDistance)};for(u=0;u<y;u++){A=t[u];A.labelDistance=p(A.options.dataLabels&&A.options.dataLabels.distance,z);this.maxLabelDistance=Math.max(this.maxLabelDistance||0,A.labelDistance);l=m+e*r;if(!f||A.visible)e+=A.percentage/100;c=m+e*r;A.shapeType="arc";A.shapeArgs=
{x:a[0],y:a[1],r:a[2]/2,innerR:a[3]/2,start:Math.round(1E3*l)/1E3,end:Math.round(1E3*c)/1E3};q=(c+l)/2;q>1.5*Math.PI?q-=2*Math.PI:q<-Math.PI/2&&(q+=2*Math.PI);A.slicedTranslation={translateX:Math.round(Math.cos(q)*d),translateY:Math.round(Math.sin(q)*d)};c=Math.cos(q)*a[2]/2;b=Math.sin(q)*a[2]/2;A.tooltipPos=[a[0]+.7*c,a[1]+.7*b];A.half=q<-Math.PI/2||q>Math.PI/2?1:0;A.angle=q;l=Math.min(g,A.labelDistance/5);A.labelPos=[a[0]+c+Math.cos(q)*A.labelDistance,a[1]+b+Math.sin(q)*A.labelDistance,a[0]+c+Math.cos(q)*
l,a[1]+b+Math.sin(q)*l,a[0]+c,a[1]+b,0>A.labelDistance?"center":A.half?"right":"left",q]}},drawGraph:null,drawPoints:function(){var a=this,k=a.chart.renderer,f,d,g,l,c=a.options.shadow;c&&!a.shadowGroup&&(a.shadowGroup=k.g("shadow").add(a.group));G(a.points,function(e){if(!e.isNull){d=e.graphic;l=e.shapeArgs;f=e.getTranslate();var q=e.shadowGroup;c&&!q&&(q=e.shadowGroup=k.g("shadow").add(a.shadowGroup));q&&q.attr(f);g=a.pointAttribs(e,e.selected&&"select");d?d.setRadialReference(a.center).attr(g).animate(E(l,
f)):(e.graphic=d=k[e.shapeType](l).setRadialReference(a.center).attr(f).add(a.group),e.visible||d.attr({visibility:"hidden"}),d.attr(g).attr({"stroke-linejoin":"round"}).shadow(c,q));d.addClass(e.getClassName())}})},searchPoint:g,sortByAngle:function(a,k){a.sort(function(a,d){return void 0!==a.angle&&(d.angle-a.angle)*k})},drawLegendSymbol:a.LegendSymbolMixin.drawRectangle,getCenter:a.CenteredSeriesMixin.getCenter,getSymbol:g},{init:function(){t.prototype.init.apply(this,arguments);var a=this,k;a.name=
p(a.name,"Slice");k=function(e){a.slice("select"===e.type)};D(a,"select",k);D(a,"unselect",k);return a},isValid:function(){return a.isNumber(this.y,!0)&&0<=this.y},setVisible:function(a,k){var e=this,d=e.series,g=d.chart,l=d.options.ignoreHiddenPoint;k=p(k,l);a!==e.visible&&(e.visible=e.options.visible=a=void 0===a?!e.visible:a,d.options.data[r(e,d.data)]=e.options,G(["graphic","dataLabel","connector","shadowGroup"],function(c){if(e[c])e[c][a?"show":"hide"](!0)}),e.legendItem&&g.legend.colorizeItem(e,
a),a||"hover"!==e.state||e.setState(""),l&&(d.isDirty=!0),k&&g.redraw())},slice:function(a,k,f){var d=this.series;l(f,d.chart);p(k,!0);this.sliced=this.options.sliced=B(a)?a:!this.sliced;d.options.data[r(this,d.data)]=this.options;this.graphic.animate(this.getTranslate());this.shadowGroup&&this.shadowGroup.animate(this.getTranslate())},getTranslate:function(){return this.sliced?this.slicedTranslation:{translateX:0,translateY:0}},haloPath:function(a){var e=this.shapeArgs;return this.sliced||!this.visible?
[]:this.series.chart.renderer.symbols.arc(e.x,e.y,e.r+a,e.r+a,{innerR:this.shapeArgs.r,start:e.start,end:e.end})}})})(M);(function(a){var D=a.addEvent,B=a.arrayMax,G=a.defined,E=a.each,r=a.extend,g=a.format,p=a.map,t=a.merge,v=a.noop,u=a.pick,l=a.relativeLength,e=a.Series,k=a.seriesTypes,f=a.stableSort;a.distribute=function(a,e){function d(a,c){return a.target-c.target}var c,k=!0,g=a,m=[],l;l=0;for(c=a.length;c--;)l+=a[c].size;if(l>e){f(a,function(a,c){return(c.rank||0)-(a.rank||0)});for(l=c=0;l<=
e;)l+=a[c].size,c++;m=a.splice(c-1,a.length)}f(a,d);for(a=p(a,function(a){return{size:a.size,targets:[a.target]}});k;){for(c=a.length;c--;)k=a[c],l=(Math.min.apply(0,k.targets)+Math.max.apply(0,k.targets))/2,k.pos=Math.min(Math.max(0,l-k.size/2),e-k.size);c=a.length;for(k=!1;c--;)0<c&&a[c-1].pos+a[c-1].size>a[c].pos&&(a[c-1].size+=a[c].size,a[c-1].targets=a[c-1].targets.concat(a[c].targets),a[c-1].pos+a[c-1].size>e&&(a[c-1].pos=e-a[c-1].size),a.splice(c,1),k=!0)}c=0;E(a,function(a){var b=0;E(a.targets,
function(){g[c].pos=a.pos+b;b+=g[c].size;c++})});g.push.apply(g,m);f(g,d)};e.prototype.drawDataLabels=function(){var d=this,e=d.options,f=e.dataLabels,c=d.points,k,l,m=d.hasRendered||0,p,b,z=u(f.defer,!!e.animation),r=d.chart.renderer;if(f.enabled||d._hasPointLabels)d.dlProcessOptions&&d.dlProcessOptions(f),b=d.plotGroup("dataLabelsGroup","data-labels",z&&!m?"hidden":"visible",f.zIndex||6),z&&(b.attr({opacity:+m}),m||D(d,"afterAnimate",function(){d.visible&&b.show(!0);b[e.animation?"animate":"attr"]({opacity:1},
{duration:200})})),l=f,E(c,function(c){var m,n=c.dataLabel,q,h,w=c.connector,z=!n,y;k=c.dlOptions||c.options&&c.options.dataLabels;if(m=u(k&&k.enabled,l.enabled)&&null!==c.y)f=t(l,k),q=c.getLabelConfig(),p=f.format?g(f.format,q):f.formatter.call(q,f),y=f.style,q=f.rotation,y.color=u(f.color,y.color,d.color,"#000000"),"contrast"===y.color&&(c.contrastColor=r.getContrast(c.color||d.color),y.color=f.inside||0>u(c.labelDistance,f.distance)||e.stacking?c.contrastColor:"#000000"),e.cursor&&(y.cursor=e.cursor),
h={fill:f.backgroundColor,stroke:f.borderColor,"stroke-width":f.borderWidth,r:f.borderRadius||0,rotation:q,padding:f.padding,zIndex:1},a.objectEach(h,function(a,b){void 0===a&&delete h[b]});!n||m&&G(p)?m&&G(p)&&(n?h.text=p:(n=c.dataLabel=r[q?"text":"label"](p,0,-9999,f.shape,null,null,f.useHTML,null,"data-label"),n.addClass("highcharts-data-label-color-"+c.colorIndex+" "+(f.className||"")+(f.useHTML?"highcharts-tracker":""))),n.attr(h),n.css(y).shadow(f.shadow),n.added||n.add(b),d.alignDataLabel(c,
n,f,null,z)):(c.dataLabel=n=n.destroy(),w&&(c.connector=w.destroy()))})};e.prototype.alignDataLabel=function(a,e,f,c,k){var d=this.chart,m=d.inverted,g=u(a.plotX,-9999),b=u(a.plotY,-9999),l=e.getBBox(),q,p=f.rotation,x=f.align,n=this.visible&&(a.series.forceDL||d.isInsidePlot(g,Math.round(b),m)||c&&d.isInsidePlot(g,m?c.x+1:c.y+c.height-1,m)),H="justify"===u(f.overflow,"justify");if(n&&(q=f.style.fontSize,q=d.renderer.fontMetrics(q,e).b,c=r({x:m?this.yAxis.len-b:g,y:Math.round(m?this.xAxis.len-g:b),
width:0,height:0},c),r(f,{width:l.width,height:l.height}),p?(H=!1,g=d.renderer.rotCorr(q,p),g={x:c.x+f.x+c.width/2+g.x,y:c.y+f.y+{top:0,middle:.5,bottom:1}[f.verticalAlign]*c.height},e[k?"attr":"animate"](g).attr({align:x}),b=(p+720)%360,b=180<b&&360>b,"left"===x?g.y-=b?l.height:0:"center"===x?(g.x-=l.width/2,g.y-=l.height/2):"right"===x&&(g.x-=l.width,g.y-=b?0:l.height)):(e.align(f,null,c),g=e.alignAttr),H?a.isLabelJustified=this.justifyDataLabel(e,f,g,l,c,k):u(f.crop,!0)&&(n=d.isInsidePlot(g.x,
g.y)&&d.isInsidePlot(g.x+l.width,g.y+l.height)),f.shape&&!p))e[k?"attr":"animate"]({anchorX:m?d.plotWidth-a.plotY:a.plotX,anchorY:m?d.plotHeight-a.plotX:a.plotY});n||(e.attr({y:-9999}),e.placed=!1)};e.prototype.justifyDataLabel=function(a,e,f,c,k,g){var d=this.chart,l=e.align,b=e.verticalAlign,q,p,y=a.box?0:a.padding||0;q=f.x+y;0>q&&("right"===l?e.align="left":e.x=-q,p=!0);q=f.x+c.width-y;q>d.plotWidth&&("left"===l?e.align="right":e.x=d.plotWidth-q,p=!0);q=f.y+y;0>q&&("bottom"===b?e.verticalAlign=
"top":e.y=-q,p=!0);q=f.y+c.height-y;q>d.plotHeight&&("top"===b?e.verticalAlign="bottom":e.y=d.plotHeight-q,p=!0);p&&(a.placed=!g,a.align(e,null,k));return p};k.pie&&(k.pie.prototype.drawDataLabels=function(){var d=this,f=d.data,k,c=d.chart,g=d.options.dataLabels,l=u(g.connectorPadding,10),m=u(g.connectorWidth,1),p=c.plotWidth,b=c.plotHeight,z,r=d.center,y=r[2]/2,t=r[1],n,H,h,w,v=[[],[]],L,D,N,O,F=[0,0,0,0];d.visible&&(g.enabled||d._hasPointLabels)&&(E(f,function(a){a.dataLabel&&a.visible&&a.dataLabel.shortened&&
(a.dataLabel.attr({width:"auto"}).css({width:"auto",textOverflow:"clip"}),a.dataLabel.shortened=!1)}),e.prototype.drawDataLabels.apply(d),E(f,function(a){a.dataLabel&&a.visible&&(v[a.half].push(a),a.dataLabel._pos=null)}),E(v,function(e,f){var m,q,z=e.length,x=[],A;if(z)for(d.sortByAngle(e,f-.5),0<d.maxLabelDistance&&(m=Math.max(0,t-y-d.maxLabelDistance),q=Math.min(t+y+d.maxLabelDistance,c.plotHeight),E(e,function(a){0<a.labelDistance&&a.dataLabel&&(a.top=Math.max(0,t-y-a.labelDistance),a.bottom=
Math.min(t+y+a.labelDistance,c.plotHeight),A=a.dataLabel.getBBox().height||21,a.positionsIndex=x.push({target:a.labelPos[1]-a.top+A/2,size:A,rank:a.y})-1)}),a.distribute(x,q+A-m)),O=0;O<z;O++)k=e[O],q=k.positionsIndex,h=k.labelPos,n=k.dataLabel,N=!1===k.visible?"hidden":"inherit",m=h[1],x&&G(x[q])?void 0===x[q].pos?N="hidden":(w=x[q].size,D=k.top+x[q].pos):D=m,delete k.positionIndex,L=g.justify?r[0]+(f?-1:1)*(y+k.labelDistance):d.getX(D<k.top+2||D>k.bottom-2?m:D,f,k),n._attr={visibility:N,align:h[6]},
n._pos={x:L+g.x+({left:l,right:-l}[h[6]]||0),y:D+g.y-10},h.x=L,h.y=D,u(g.crop,!0)&&(H=n.getBBox().width,m=null,L-H<l?(m=Math.round(H-L+l),F[3]=Math.max(m,F[3])):L+H>p-l&&(m=Math.round(L+H-p+l),F[1]=Math.max(m,F[1])),0>D-w/2?F[0]=Math.max(Math.round(-D+w/2),F[0]):D+w/2>b&&(F[2]=Math.max(Math.round(D+w/2-b),F[2])),n.sideOverflow=m)}),0===B(F)||this.verifyDataLabelOverflow(F))&&(this.placeDataLabels(),m&&E(this.points,function(a){var b;z=a.connector;if((n=a.dataLabel)&&n._pos&&a.visible&&0<a.labelDistance){N=
n._attr.visibility;if(b=!z)a.connector=z=c.renderer.path().addClass("highcharts-data-label-connector highcharts-color-"+a.colorIndex).add(d.dataLabelsGroup),z.attr({"stroke-width":m,stroke:g.connectorColor||a.color||"#666666"});z[b?"attr":"animate"]({d:d.connectorPath(a.labelPos)});z.attr("visibility",N)}else z&&(a.connector=z.destroy())}))},k.pie.prototype.connectorPath=function(a){var d=a.x,e=a.y;return u(this.options.dataLabels.softConnector,!0)?["M",d+("left"===a[6]?5:-5),e,"C",d,e,2*a[2]-a[4],
2*a[3]-a[5],a[2],a[3],"L",a[4],a[5]]:["M",d+("left"===a[6]?5:-5),e,"L",a[2],a[3],"L",a[4],a[5]]},k.pie.prototype.placeDataLabels=function(){E(this.points,function(a){var d=a.dataLabel;d&&a.visible&&((a=d._pos)?(d.sideOverflow&&(d._attr.width=d.getBBox().width-d.sideOverflow,d.css({width:d._attr.width+"px",textOverflow:"ellipsis"}),d.shortened=!0),d.attr(d._attr),d[d.moved?"animate":"attr"](a),d.moved=!0):d&&d.attr({y:-9999}))},this)},k.pie.prototype.alignDataLabel=v,k.pie.prototype.verifyDataLabelOverflow=
function(a){var d=this.center,e=this.options,c=e.center,f=e.minSize||80,k,m=null!==e.size;m||(null!==c[0]?k=Math.max(d[2]-Math.max(a[1],a[3]),f):(k=Math.max(d[2]-a[1]-a[3],f),d[0]+=(a[3]-a[1])/2),null!==c[1]?k=Math.max(Math.min(k,d[2]-Math.max(a[0],a[2])),f):(k=Math.max(Math.min(k,d[2]-a[0]-a[2]),f),d[1]+=(a[0]-a[2])/2),k<d[2]?(d[2]=k,d[3]=Math.min(l(e.innerSize||0,k),k),this.translate(d),this.drawDataLabels&&this.drawDataLabels()):m=!0);return m});k.column&&(k.column.prototype.alignDataLabel=function(a,
f,k,c,g){var d=this.chart.inverted,m=a.series,l=a.dlBox||a.shapeArgs,b=u(a.below,a.plotY>u(this.translatedThreshold,m.yAxis.len)),q=u(k.inside,!!this.options.stacking);l&&(c=t(l),0>c.y&&(c.height+=c.y,c.y=0),l=c.y+c.height-m.yAxis.len,0<l&&(c.height-=l),d&&(c={x:m.yAxis.len-c.y-c.height,y:m.xAxis.len-c.x-c.width,width:c.height,height:c.width}),q||(d?(c.x+=b?0:c.width,c.width=0):(c.y+=b?c.height:0,c.height=0)));k.align=u(k.align,!d||q?"center":b?"right":"left");k.verticalAlign=u(k.verticalAlign,d||
q?"middle":b?"top":"bottom");e.prototype.alignDataLabel.call(this,a,f,k,c,g);a.isLabelJustified&&a.contrastColor&&a.dataLabel.css({color:a.contrastColor})})})(M);(function(a){var D=a.Chart,B=a.each,G=a.objectEach,E=a.pick,r=a.addEvent;D.prototype.callbacks.push(function(a){function g(){var g=[];B(a.yAxis||[],function(a){a.options.stackLabels&&!a.options.stackLabels.allowOverlap&&G(a.stacks,function(a){G(a,function(a){g.push(a.label)})})});B(a.series||[],function(a){var p=a.options.dataLabels,l=a.dataLabelCollections||
["dataLabel"];(p.enabled||a._hasPointLabels)&&!p.allowOverlap&&a.visible&&B(l,function(e){B(a.points,function(a){a[e]&&(a[e].labelrank=E(a.labelrank,a.shapeArgs&&a.shapeArgs.height),g.push(a[e]))})})});a.hideOverlappingLabels(g)}g();r(a,"redraw",g)});D.prototype.hideOverlappingLabels=function(a){var g=a.length,r,v,u,l,e,k,f,d,x,C=function(a,d,e,f,k,b,g,l){return!(k>a+e||k+g<a||b>d+f||b+l<d)};for(v=0;v<g;v++)if(r=a[v])r.oldOpacity=r.opacity,r.newOpacity=1,r.width||(u=r.getBBox(),r.width=u.width,r.height=
u.height);a.sort(function(a,d){return(d.labelrank||0)-(a.labelrank||0)});for(v=0;v<g;v++)for(u=a[v],r=v+1;r<g;++r)if(l=a[r],u&&l&&u!==l&&u.placed&&l.placed&&0!==u.newOpacity&&0!==l.newOpacity&&(e=u.alignAttr,k=l.alignAttr,f=u.parentGroup,d=l.parentGroup,x=2*(u.box?0:u.padding||0),e=C(e.x+f.translateX,e.y+f.translateY,u.width-x,u.height-x,k.x+d.translateX,k.y+d.translateY,l.width-x,l.height-x)))(u.labelrank<l.labelrank?u:l).newOpacity=0;B(a,function(a){var c,d;a&&(d=a.newOpacity,a.oldOpacity!==d&&
a.placed&&(d?a.show(!0):c=function(){a.hide()},a.alignAttr.opacity=d,a[a.isOld?"animate":"attr"](a.alignAttr,null,c)),a.isOld=!0)})}})(M);(function(a){var D=a.addEvent,B=a.Chart,G=a.createElement,E=a.css,r=a.defaultOptions,g=a.defaultPlotOptions,p=a.each,t=a.extend,v=a.fireEvent,u=a.hasTouch,l=a.inArray,e=a.isObject,k=a.Legend,f=a.merge,d=a.pick,x=a.Point,C=a.Series,c=a.seriesTypes,q=a.svg,I;I=a.TrackerMixin={drawTrackerPoint:function(){var a=this,c=a.chart.pointer,b=function(a){var b=c.getPointFromEvent(a);
void 0!==b&&(c.isDirectTouch=!0,b.onMouseOver(a))};p(a.points,function(a){a.graphic&&(a.graphic.element.point=a);a.dataLabel&&(a.dataLabel.div?a.dataLabel.div.point=a:a.dataLabel.element.point=a)});a._hasTracking||(p(a.trackerGroups,function(d){if(a[d]){a[d].addClass("highcharts-tracker").on("mouseover",b).on("mouseout",function(a){c.onTrackerMouseOut(a)});if(u)a[d].on("touchstart",b);a.options.cursor&&a[d].css(E).css({cursor:a.options.cursor})}}),a._hasTracking=!0)},drawTrackerGraph:function(){var a=
this,c=a.options,b=c.trackByArea,d=[].concat(b?a.areaPath:a.graphPath),e=d.length,f=a.chart,k=f.pointer,n=f.renderer,g=f.options.tooltip.snap,h=a.tracker,l,r=function(){if(f.hoverSeries!==a)a.onMouseOver()},t="rgba(192,192,192,"+(q?.0001:.002)+")";if(e&&!b)for(l=e+1;l--;)"M"===d[l]&&d.splice(l+1,0,d[l+1]-g,d[l+2],"L"),(l&&"M"===d[l]||l===e)&&d.splice(l,0,"L",d[l-2]+g,d[l-1]);h?h.attr({d:d}):a.graph&&(a.tracker=n.path(d).attr({"stroke-linejoin":"round",visibility:a.visible?"visible":"hidden",stroke:t,
fill:b?t:"none","stroke-width":a.graph.strokeWidth()+(b?0:2*g),zIndex:2}).add(a.group),p([a.tracker,a.markerGroup],function(a){a.addClass("highcharts-tracker").on("mouseover",r).on("mouseout",function(a){k.onTrackerMouseOut(a)});c.cursor&&a.css({cursor:c.cursor});if(u)a.on("touchstart",r)}))}};c.column&&(c.column.prototype.drawTracker=I.drawTrackerPoint);c.pie&&(c.pie.prototype.drawTracker=I.drawTrackerPoint);c.scatter&&(c.scatter.prototype.drawTracker=I.drawTrackerPoint);t(k.prototype,{setItemEvents:function(a,
c,b){var d=this,e=d.chart.renderer.boxWrapper,k="highcharts-legend-"+(a.series?"point":"series")+"-active";(b?c:a.legendGroup).on("mouseover",function(){a.setState("hover");e.addClass(k);c.css(d.options.itemHoverStyle)}).on("mouseout",function(){c.css(f(a.visible?d.itemStyle:d.itemHiddenStyle));e.removeClass(k);a.setState()}).on("click",function(b){var c=function(){a.setVisible&&a.setVisible()};b={browserEvent:b};a.firePointEvent?a.firePointEvent("legendItemClick",b,c):v(a,"legendItemClick",b,c)})},
createCheckboxForItem:function(a){a.checkbox=G("input",{type:"checkbox",checked:a.selected,defaultChecked:a.selected},this.options.itemCheckboxStyle,this.chart.container);D(a.checkbox,"click",function(c){v(a.series||a,"checkboxClick",{checked:c.target.checked,item:a},function(){a.select()})})}});r.legend.itemStyle.cursor="pointer";t(B.prototype,{showResetZoom:function(){var a=this,c=r.lang,b=a.options.chart.resetZoomButton,d=b.theme,e=d.states,f="chart"===b.relativeTo?null:"plotBox";this.resetZoomButton=
a.renderer.button(c.resetZoom,null,null,function(){a.zoomOut()},d,e&&e.hover).attr({align:b.position.align,title:c.resetZoomTitle}).addClass("highcharts-reset-zoom").add().align(b.position,!1,f)},zoomOut:function(){var a=this;v(a,"selection",{resetSelection:!0},function(){a.zoom()})},zoom:function(a){var c,b=this.pointer,f=!1,k;!a||a.resetSelection?(p(this.axes,function(a){c=a.zoom()}),b.initiated=!1):p(a.xAxis.concat(a.yAxis),function(a){var d=a.axis;b[d.isXAxis?"zoomX":"zoomY"]&&(c=d.zoom(a.min,
a.max),d.displayBtn&&(f=!0))});k=this.resetZoomButton;f&&!k?this.showResetZoom():!f&&e(k)&&(this.resetZoomButton=k.destroy());c&&this.redraw(d(this.options.chart.animation,a&&a.animation,100>this.pointCount))},pan:function(a,c){var b=this,d=b.hoverPoints,e;d&&p(d,function(a){a.setState()});p("xy"===c?[1,0]:[1],function(c){c=b[c?"xAxis":"yAxis"][0];var d=c.horiz,f=a[d?"chartX":"chartY"],d=d?"mouseDownX":"mouseDownY",k=b[d],h=(c.pointRange||0)/2,m=c.getExtremes(),g=c.toValue(k-f,!0)+h,h=c.toValue(k+
c.len-f,!0)-h,l=h<g,k=l?h:g,g=l?g:h,h=Math.min(m.dataMin,c.toValue(c.toPixels(m.min)-c.minPixelPadding)),l=Math.max(m.dataMax,c.toValue(c.toPixels(m.max)+c.minPixelPadding)),q;q=h-k;0<q&&(g+=q,k=h);q=g-l;0<q&&(g=l,k-=q);c.series.length&&k!==m.min&&g!==m.max&&(c.setExtremes(k,g,!1,!1,{trigger:"pan"}),e=!0);b[d]=f});e&&b.redraw(!1);E(b.container,{cursor:"move"})}});t(x.prototype,{select:function(a,c){var b=this,e=b.series,f=e.chart;a=d(a,!b.selected);b.firePointEvent(a?"select":"unselect",{accumulate:c},
function(){b.selected=b.options.selected=a;e.options.data[l(b,e.data)]=b.options;b.setState(a&&"select");c||p(f.getSelectedPoints(),function(a){a.selected&&a!==b&&(a.selected=a.options.selected=!1,e.options.data[l(a,e.data)]=a.options,a.setState(""),a.firePointEvent("unselect"))})})},onMouseOver:function(a){var c=this.series.chart,b=c.pointer;a=a?b.normalize(a):b.getChartCoordinatesFromPoint(this,c.inverted);b.runPointActions(a,this)},onMouseOut:function(){var a=this.series.chart;this.firePointEvent("mouseOut");
p(a.hoverPoints||[],function(a){a.setState()});a.hoverPoints=a.hoverPoint=null},importEvents:function(){if(!this.hasImportedEvents){var c=this,d=f(c.series.options.point,c.options).events;c.events=d;a.objectEach(d,function(a,d){D(c,d,a)});this.hasImportedEvents=!0}},setState:function(a,c){var b=Math.floor(this.plotX),e=this.plotY,f=this.series,k=f.options.states[a]||{},m=g[f.type].marker&&f.options.marker,n=m&&!1===m.enabled,l=m&&m.states&&m.states[a]||{},h=!1===l.enabled,q=f.stateMarkerGraphic,p=
this.marker||{},r=f.chart,u=f.halo,x,v=m&&f.markerAttribs;a=a||"";if(!(a===this.state&&!c||this.selected&&"select"!==a||!1===k.enabled||a&&(h||n&&!1===l.enabled)||a&&p.states&&p.states[a]&&!1===p.states[a].enabled)){v&&(x=f.markerAttribs(this,a));if(this.graphic)this.state&&this.graphic.removeClass("highcharts-point-"+this.state),a&&this.graphic.addClass("highcharts-point-"+a),this.graphic.animate(f.pointAttribs(this,a),d(r.options.chart.animation,k.animation)),x&&this.graphic.animate(x,d(r.options.chart.animation,
l.animation,m.animation)),q&&q.hide();else{if(a&&l){m=p.symbol||f.symbol;q&&q.currentSymbol!==m&&(q=q.destroy());if(q)q[c?"animate":"attr"]({x:x.x,y:x.y});else m&&(f.stateMarkerGraphic=q=r.renderer.symbol(m,x.x,x.y,x.width,x.height).add(f.markerGroup),q.currentSymbol=m);q&&q.attr(f.pointAttribs(this,a))}q&&(q[a&&r.isInsidePlot(b,e,r.inverted)?"show":"hide"](),q.element.point=this)}(b=k.halo)&&b.size?(u||(f.halo=u=r.renderer.path().add((this.graphic||q).parentGroup)),u[c?"animate":"attr"]({d:this.haloPath(b.size)}),
u.attr({"class":"highcharts-halo highcharts-color-"+d(this.colorIndex,f.colorIndex)}),u.point=this,u.attr(t({fill:this.color||f.color,"fill-opacity":b.opacity,zIndex:-1},b.attributes))):u&&u.point&&u.point.haloPath&&u.animate({d:u.point.haloPath(0)});this.state=a}},haloPath:function(a){return this.series.chart.renderer.symbols.circle(Math.floor(this.plotX)-a,this.plotY-a,2*a,2*a)}});t(C.prototype,{onMouseOver:function(){var a=this.chart,c=a.hoverSeries;if(c&&c!==this)c.onMouseOut();this.options.events.mouseOver&&
v(this,"mouseOver");this.setState("hover");a.hoverSeries=this},onMouseOut:function(){var a=this.options,c=this.chart,b=c.tooltip,d=c.hoverPoint;c.hoverSeries=null;if(d)d.onMouseOut();this&&a.events.mouseOut&&v(this,"mouseOut");!b||this.stickyTracking||b.shared&&!this.noSharedTooltip||b.hide();this.setState()},setState:function(a){var c=this,b=c.options,e=c.graph,f=b.states,k=b.lineWidth,b=0;a=a||"";if(c.state!==a&&(p([c.group,c.markerGroup,c.dataLabelsGroup],function(b){b&&(c.state&&b.removeClass("highcharts-series-"+
c.state),a&&b.addClass("highcharts-series-"+a))}),c.state=a,!f[a]||!1!==f[a].enabled)&&(a&&(k=f[a].lineWidth||k+(f[a].lineWidthPlus||0)),e&&!e.dashstyle))for(k={"stroke-width":k},e.animate(k,d(c.chart.options.chart.animation,f[a]&&f[a].animation));c["zone-graph-"+b];)c["zone-graph-"+b].attr(k),b+=1},setVisible:function(a,c){var b=this,d=b.chart,e=b.legendItem,f,k=d.options.chart.ignoreHiddenSeries,n=b.visible;f=(b.visible=a=b.options.visible=b.userOptions.visible=void 0===a?!n:a)?"show":"hide";p(["group",
"dataLabelsGroup","markerGroup","tracker","tt"],function(a){if(b[a])b[a][f]()});if(d.hoverSeries===b||(d.hoverPoint&&d.hoverPoint.series)===b)b.onMouseOut();e&&d.legend.colorizeItem(b,a);b.isDirty=!0;b.options.stacking&&p(d.series,function(a){a.options.stacking&&a.visible&&(a.isDirty=!0)});p(b.linkedSeries,function(b){b.setVisible(a,!1)});k&&(d.isDirtyBox=!0);!1!==c&&d.redraw();v(b,f)},show:function(){this.setVisible(!0)},hide:function(){this.setVisible(!1)},select:function(a){this.selected=a=void 0===
a?!this.selected:a;this.checkbox&&(this.checkbox.checked=a);v(this,a?"select":"unselect")},drawTracker:I.drawTrackerGraph})})(M);(function(a){var D=a.Chart,B=a.each,G=a.inArray,E=a.isArray,r=a.isObject,g=a.pick,p=a.splat;D.prototype.setResponsive=function(g){var p=this.options.responsive,r=[],l=this.currentResponsive;p&&p.rules&&B(p.rules,function(e){void 0===e._id&&(e._id=a.uniqueKey());this.matchResponsiveRule(e,r,g)},this);var e=a.merge.apply(0,a.map(r,function(e){return a.find(p.rules,function(a){return a._id===
e}).chartOptions})),r=r.toString()||void 0;r!==(l&&l.ruleIds)&&(l&&this.update(l.undoOptions,g),r?(this.currentResponsive={ruleIds:r,mergedOptions:e,undoOptions:this.currentOptions(e)},this.update(e,g)):this.currentResponsive=void 0)};D.prototype.matchResponsiveRule=function(a,p){var r=a.condition;(r.callback||function(){return this.chartWidth<=g(r.maxWidth,Number.MAX_VALUE)&&this.chartHeight<=g(r.maxHeight,Number.MAX_VALUE)&&this.chartWidth>=g(r.minWidth,0)&&this.chartHeight>=g(r.minHeight,0)}).call(this)&&
p.push(a._id)};D.prototype.currentOptions=function(g){function t(g,e,k,f){var d;a.objectEach(g,function(a,l){if(!f&&-1<G(l,["series","xAxis","yAxis"]))for(g[l]=p(g[l]),k[l]=[],d=0;d<g[l].length;d++)e[l][d]&&(k[l][d]={},t(a[d],e[l][d],k[l][d],f+1));else r(a)?(k[l]=E(a)?[]:{},t(a,e[l]||{},k[l],f+1)):k[l]=e[l]||null})}var u={};t(g,this.options,u,0);return u}})(M);(function(a){var D=a.addEvent,B=a.Axis,G=a.Chart,E=a.css,r=a.dateFormat,g=a.defined,p=a.each,t=a.extend,v=a.noop,u=a.timeUnits,l=a.wrap;l(a.Series.prototype,
"init",function(a){var e;a.apply(this,Array.prototype.slice.call(arguments,1));(e=this.xAxis)&&e.options.ordinal&&D(this,"updatedData",function(){delete e.ordinalIndex})});l(B.prototype,"getTimeTicks",function(a,k,f,d,l,p,c,q){var e=0,m,t,b={},z,x,y,A=[],n=-Number.MAX_VALUE,H=this.options.tickPixelInterval;if(!this.options.ordinal&&!this.options.breaks||!p||3>p.length||void 0===f)return a.call(this,k,f,d,l);x=p.length;for(m=0;m<x;m++){y=m&&p[m-1]>d;p[m]<f&&(e=m);if(m===x-1||p[m+1]-p[m]>5*c||y){if(p[m]>
n){for(t=a.call(this,k,p[e],p[m],l);t.length&&t[0]<=n;)t.shift();t.length&&(n=t[t.length-1]);A=A.concat(t)}e=m+1}if(y)break}a=t.info;if(q&&a.unitRange<=u.hour){m=A.length-1;for(e=1;e<m;e++)r("%d",A[e])!==r("%d",A[e-1])&&(b[A[e]]="day",z=!0);z&&(b[A[0]]="day");a.higherRanks=b}A.info=a;if(q&&g(H)){q=a=A.length;m=[];var h;for(z=[];q--;)e=this.translate(A[q]),h&&(z[q]=h-e),m[q]=h=e;z.sort();z=z[Math.floor(z.length/2)];z<.6*H&&(z=null);q=A[a-1]>d?a-1:a;for(h=void 0;q--;)e=m[q],d=Math.abs(h-e),h&&d<.8*
H&&(null===z||d<.8*z)?(b[A[q]]&&!b[A[q+1]]?(d=q+1,h=e):d=q,A.splice(d,1)):h=e}return A});t(B.prototype,{beforeSetTickPositions:function(){var a,k=[],f=!1,d,g=this.getExtremes(),l=g.min,c=g.max,q,r=this.isXAxis&&!!this.options.breaks,g=this.options.ordinal,m=this.chart.options.chart.ignoreHiddenSeries;if(g||r){p(this.series,function(c,b){if(!(m&&!1===c.visible||!1===c.takeOrdinalPosition&&!r)&&(k=k.concat(c.processedXData),a=k.length,k.sort(function(a,b){return a-b}),a))for(b=a-1;b--;)k[b]===k[b+1]&&
k.splice(b,1)});a=k.length;if(2<a){d=k[1]-k[0];for(q=a-1;q--&&!f;)k[q+1]-k[q]!==d&&(f=!0);!this.options.keepOrdinalPadding&&(k[0]-l>d||c-k[k.length-1]>d)&&(f=!0)}f?(this.ordinalPositions=k,d=this.ordinal2lin(Math.max(l,k[0]),!0),q=Math.max(this.ordinal2lin(Math.min(c,k[k.length-1]),!0),1),this.ordinalSlope=c=(c-l)/(q-d),this.ordinalOffset=l-d*c):this.ordinalPositions=this.ordinalSlope=this.ordinalOffset=void 0}this.isOrdinal=g&&f;this.groupIntervalFactor=null},val2lin:function(a,k){var e=this.ordinalPositions;
if(e){var d=e.length,g,l;for(g=d;g--;)if(e[g]===a){l=g;break}for(g=d-1;g--;)if(a>e[g]||0===g){a=(a-e[g])/(e[g+1]-e[g]);l=g+a;break}k=k?l:this.ordinalSlope*(l||0)+this.ordinalOffset}else k=a;return k},lin2val:function(a,k){var e=this.ordinalPositions;if(e){var d=this.ordinalSlope,g=this.ordinalOffset,l=e.length-1,c;if(k)0>a?a=e[0]:a>l?a=e[l]:(l=Math.floor(a),c=a-l);else for(;l--;)if(k=d*l+g,a>=k){d=d*(l+1)+g;c=(a-k)/(d-k);break}return void 0!==c&&void 0!==e[l]?e[l]+(c?c*(e[l+1]-e[l]):0):a}return a},
getExtendedPositions:function(){var a=this.chart,k=this.series[0].currentDataGrouping,f=this.ordinalIndex,d=k?k.count+k.unitName:"raw",g=this.getExtremes(),l,c;f||(f=this.ordinalIndex={});f[d]||(l={series:[],chart:a,getExtremes:function(){return{min:g.dataMin,max:g.dataMax}},options:{ordinal:!0},val2lin:B.prototype.val2lin,ordinal2lin:B.prototype.ordinal2lin},p(this.series,function(d){c={xAxis:l,xData:d.xData,chart:a,destroyGroupedData:v};c.options={dataGrouping:k?{enabled:!0,forced:!0,approximation:"open",
units:[[k.unitName,[k.count]]]}:{enabled:!1}};d.processData.apply(c);l.series.push(c)}),this.beforeSetTickPositions.apply(l),f[d]=l.ordinalPositions);return f[d]},getGroupIntervalFactor:function(a,k,f){var d;f=f.processedXData;var e=f.length,g=[];d=this.groupIntervalFactor;if(!d){for(d=0;d<e-1;d++)g[d]=f[d+1]-f[d];g.sort(function(a,d){return a-d});g=g[Math.floor(e/2)];a=Math.max(a,f[0]);k=Math.min(k,f[e-1]);this.groupIntervalFactor=d=e*g/(k-a)}return d},postProcessTickInterval:function(a){var e=this.ordinalSlope;
return e?this.options.breaks?this.closestPointRange:a/(e/this.closestPointRange):a}});B.prototype.ordinal2lin=B.prototype.val2lin;l(G.prototype,"pan",function(a,k){var e=this.xAxis[0],d=k.chartX,g=!1;if(e.options.ordinal&&e.series.length){var l=this.mouseDownX,c=e.getExtremes(),q=c.dataMax,r=c.min,m=c.max,t=this.hoverPoints,b=e.closestPointRange,l=(l-d)/(e.translationSlope*(e.ordinalSlope||b)),z={ordinalPositions:e.getExtendedPositions()},b=e.lin2val,u=e.val2lin,y;z.ordinalPositions?1<Math.abs(l)&&
(t&&p(t,function(a){a.setState()}),0>l?(t=z,y=e.ordinalPositions?e:z):(t=e.ordinalPositions?e:z,y=z),z=y.ordinalPositions,q>z[z.length-1]&&z.push(q),this.fixedRange=m-r,l=e.toFixedRange(null,null,b.apply(t,[u.apply(t,[r,!0])+l,!0]),b.apply(y,[u.apply(y,[m,!0])+l,!0])),l.min>=Math.min(c.dataMin,r)&&l.max<=Math.max(q,m)&&e.setExtremes(l.min,l.max,!0,!1,{trigger:"pan"}),this.mouseDownX=d,E(this.container,{cursor:"move"})):g=!0}else g=!0;g&&a.apply(this,Array.prototype.slice.call(arguments,1))})})(M);
(function(a){function D(){return Array.prototype.slice.call(arguments,1)}function B(a){a.apply(this);this.drawBreaks(this.xAxis,["x"]);this.drawBreaks(this.yAxis,G(this.pointArrayMap,["y"]))}var G=a.pick,E=a.wrap,r=a.each,g=a.extend,p=a.isArray,t=a.fireEvent,v=a.Axis,u=a.Series;g(v.prototype,{isInBreak:function(a,e){var k=a.repeat||Infinity,f=a.from,d=a.to-a.from;e=e>=f?(e-f)%k:k-(f-e)%k;return a.inclusive?e<=d:e<d&&0!==e},isInAnyBreak:function(a,e){var k=this.options.breaks,f=k&&k.length,d,g,l;if(f){for(;f--;)this.isInBreak(k[f],
a)&&(d=!0,g||(g=G(k[f].showPoints,this.isXAxis?!1:!0)));l=d&&e?d&&!g:d}return l}});E(v.prototype,"setTickPositions",function(a){a.apply(this,Array.prototype.slice.call(arguments,1));if(this.options.breaks){var e=this.tickPositions,k=this.tickPositions.info,f=[],d;for(d=0;d<e.length;d++)this.isInAnyBreak(e[d])||f.push(e[d]);this.tickPositions=f;this.tickPositions.info=k}});E(v.prototype,"init",function(a,e,k){var f=this;k.breaks&&k.breaks.length&&(k.ordinal=!1);a.call(this,e,k);a=this.options.breaks;
f.isBroken=p(a)&&!!a.length;f.isBroken&&(f.val2lin=function(a){var d=a,e,c;for(c=0;c<f.breakArray.length;c++)if(e=f.breakArray[c],e.to<=a)d-=e.len;else if(e.from>=a)break;else if(f.isInBreak(e,a)){d-=a-e.from;break}return d},f.lin2val=function(a){var d,e;for(e=0;e<f.breakArray.length&&!(d=f.breakArray[e],d.from>=a);e++)d.to<a?a+=d.len:f.isInBreak(d,a)&&(a+=d.len);return a},f.setExtremes=function(a,e,f,c,k){for(;this.isInAnyBreak(a);)a-=this.closestPointRange;for(;this.isInAnyBreak(e);)e-=this.closestPointRange;
v.prototype.setExtremes.call(this,a,e,f,c,k)},f.setAxisTranslation=function(a){v.prototype.setAxisTranslation.call(this,a);a=f.options.breaks;var d=[],e=[],c=0,k,g,l=f.userMin||f.min,p=f.userMax||f.max,b=G(f.pointRangePadding,0),z,u;r(a,function(a){g=a.repeat||Infinity;f.isInBreak(a,l)&&(l+=a.to%g-l%g);f.isInBreak(a,p)&&(p-=p%g-a.from%g)});r(a,function(a){z=a.from;for(g=a.repeat||Infinity;z-g>l;)z-=g;for(;z<l;)z+=g;for(u=z;u<p;u+=g)d.push({value:u,move:"in"}),d.push({value:u+(a.to-a.from),move:"out",
size:a.breakSize})});d.sort(function(a,b){return a.value===b.value?("in"===a.move?0:1)-("in"===b.move?0:1):a.value-b.value});k=0;z=l;r(d,function(a){k+="in"===a.move?1:-1;1===k&&"in"===a.move&&(z=a.value);0===k&&(e.push({from:z,to:a.value,len:a.value-z-(a.size||0)}),c+=a.value-z-(a.size||0))});f.breakArray=e;f.unitLength=p-l-c+b;t(f,"afterBreaks");f.options.staticScale?f.transA=f.options.staticScale:f.unitLength&&(f.transA*=(p-f.min+b)/f.unitLength);b&&(f.minPixelPadding=f.transA*f.minPointOffset);
f.min=l;f.max=p})});E(u.prototype,"generatePoints",function(a){a.apply(this,D(arguments));var e=this.xAxis,k=this.yAxis,f=this.points,d,g=f.length,l=this.options.connectNulls,c;if(e&&k&&(e.options.breaks||k.options.breaks))for(;g--;)d=f[g],c=null===d.y&&!1===l,c||!e.isInAnyBreak(d.x,!0)&&!k.isInAnyBreak(d.y,!0)||(f.splice(g,1),this.data[g]&&this.data[g].destroyElements())});a.Series.prototype.drawBreaks=function(a,e){var k=this,f=k.points,d,g,l,c;a&&r(e,function(e){d=a.breakArray||[];g=a.isXAxis?
a.min:G(k.options.threshold,a.min);r(f,function(f){c=G(f["stack"+e.toUpperCase()],f[e]);r(d,function(d){l=!1;if(g<d.from&&c>d.to||g>d.from&&c<d.from)l="pointBreak";else if(g<d.from&&c>d.from&&c<d.to||g>d.from&&c>d.to&&c<d.from)l="pointInBreak";l&&t(a,l,{point:f,brk:d})})})})};a.Series.prototype.gappedPath=function(){var g=this.options.gapSize,e=this.points.slice(),k=e.length-1,f=this.yAxis,d;if(g&&0<k)for("value"!==this.options.gapUnit&&(g*=this.closestPointRange);k--;)e[k+1].x-e[k].x>g&&(d=(e[k].x+
e[k+1].x)/2,e.splice(k+1,0,{isNull:!0,x:d}),this.options.stacking&&(d=f.stacks[this.stackKey][d]=new a.StackItem(f,f.options.stackLabels,!1,d,this.stack),d.total=0));return this.getGraphPath(e)};E(a.seriesTypes.column.prototype,"drawPoints",B);E(a.Series.prototype,"drawPoints",B)})(M);(function(a){var D=a.arrayMax,B=a.arrayMin,G=a.Axis,E=a.defaultPlotOptions,r=a.defined,g=a.each,p=a.extend,t=a.format,v=a.isNumber,u=a.merge,l=a.pick,e=a.Point,k=a.Tooltip,f=a.wrap,d=a.Series.prototype,x=d.processData,
C=d.generatePoints,c=d.destroy,q={approximation:"average",groupPixelWidth:2,dateTimeLabelFormats:{millisecond:["%A, %b %e, %H:%M:%S.%L","%A, %b %e, %H:%M:%S.%L","-%H:%M:%S.%L"],second:["%A, %b %e, %H:%M:%S","%A, %b %e, %H:%M:%S","-%H:%M:%S"],minute:["%A, %b %e, %H:%M","%A, %b %e, %H:%M","-%H:%M"],hour:["%A, %b %e, %H:%M","%A, %b %e, %H:%M","-%H:%M"],day:["%A, %b %e, %Y","%A, %b %e","-%A, %b %e, %Y"],week:["Week from %A, %b %e, %Y","%A, %b %e","-%A, %b %e, %Y"],month:["%B %Y","%B","-%B %Y"],year:["%Y",
"%Y","-%Y"]}},I={line:{},spline:{},area:{},areaspline:{},column:{approximation:"sum",groupPixelWidth:10},arearange:{approximation:"range"},areasplinerange:{approximation:"range"},columnrange:{approximation:"range",groupPixelWidth:10},candlestick:{approximation:"ohlc",groupPixelWidth:10},ohlc:{approximation:"ohlc",groupPixelWidth:5}},m=a.defaultDataGroupingUnits=[["millisecond",[1,2,5,10,20,25,50,100,200,500]],["second",[1,2,5,10,15,30]],["minute",[1,2,5,10,15,30]],["hour",[1,2,3,4,6,8,12]],["day",
[1]],["week",[1]],["month",[1,3,6]],["year",null]],J={sum:function(a){var b=a.length,c;if(!b&&a.hasNulls)c=null;else if(b)for(c=0;b--;)c+=a[b];return c},average:function(a){var b=a.length;a=J.sum(a);v(a)&&b&&(a/=b);return a},averages:function(){var a=[];g(arguments,function(b){a.push(J.average(b))});return a},open:function(a){return a.length?a[0]:a.hasNulls?null:void 0},high:function(a){return a.length?D(a):a.hasNulls?null:void 0},low:function(a){return a.length?B(a):a.hasNulls?null:void 0},close:function(a){return a.length?
a[a.length-1]:a.hasNulls?null:void 0},ohlc:function(a,c,d,e){a=J.open(a);c=J.high(c);d=J.low(d);e=J.close(e);if(v(a)||v(c)||v(d)||v(e))return[a,c,d,e]},range:function(a,c){a=J.low(a);c=J.high(c);if(v(a)||v(c))return[a,c];if(null===a&&null===c)return null}};d.groupData=function(a,c,d,e){var b=this.data,f=this.options.data,k=[],h=[],l=[],m=a.length,p,r,t=!!c,u=[];e="function"===typeof e?e:J[e]||I[this.type]&&J[I[this.type].approximation]||J[q.approximation];var z=this.pointArrayMap,y=z&&z.length,x=
0;r=0;var C,K;y?g(z,function(){u.push([])}):u.push([]);C=y||1;for(K=0;K<=m&&!(a[K]>=d[0]);K++);for(K;K<=m;K++){for(;void 0!==d[x+1]&&a[K]>=d[x+1]||K===m;){p=d[x];this.dataGroupInfo={start:r,length:u[0].length};r=e.apply(this,u);void 0!==r&&(k.push(p),h.push(r),l.push(this.dataGroupInfo));r=K;for(p=0;p<C;p++)u[p].length=0,u[p].hasNulls=!1;x+=1;if(K===m)break}if(K===m)break;if(z){p=this.cropStart+K;var B=b&&b[p]||this.pointClass.prototype.applyOptions.apply({series:this},[f[p]]),D;for(p=0;p<y;p++)D=
B[z[p]],v(D)?u[p].push(D):null===D&&(u[p].hasNulls=!0)}else p=t?c[K]:null,v(p)?u[0].push(p):null===p&&(u[0].hasNulls=!0)}return[k,h,l]};d.processData=function(){var a=this.chart,c=this.options.dataGrouping,e=!1!==this.allowDG&&c&&l(c.enabled,a.options.isStock),f=this.visible||!a.options.chart.ignoreHiddenSeries,k;this.forceCrop=e;this.groupPixelWidth=null;this.hasProcessed=!0;if(!1!==x.apply(this,arguments)&&e){this.destroyGroupedData();var n=this.processedXData,g=this.processedYData,h=a.plotSizeX,
a=this.xAxis,q=a.options.ordinal,p=this.groupPixelWidth=a.getGroupPixelWidth&&a.getGroupPixelWidth();if(p){this.isDirty=k=!0;this.points=null;var t=a.getExtremes(),e=t.min,t=t.max,q=q&&a.getGroupIntervalFactor(e,t,this)||1,h=p*(t-e)/h*q,p=a.getTimeTicks(a.normalizeTimeTickInterval(h,c.units||m),Math.min(e,n[0]),Math.max(t,n[n.length-1]),a.options.startOfWeek,n,this.closestPointRange),n=d.groupData.apply(this,[n,g,p,c.approximation]),g=n[0],q=n[1];if(c.smoothed){c=g.length-1;for(g[c]=Math.min(g[c],
t);c--&&0<c;)g[c]+=h/2;g[0]=Math.max(g[0],e)}this.currentDataGrouping=p.info;this.closestPointRange=p.info.totalRange;this.groupMap=n[2];r(g[0])&&g[0]<a.dataMin&&f&&(a.min===a.dataMin&&(a.min=g[0]),a.dataMin=g[0]);this.processedXData=g;this.processedYData=q}else this.currentDataGrouping=this.groupMap=null;this.hasGroupedData=k}};d.destroyGroupedData=function(){var a=this.groupedData;g(a||[],function(b,c){b&&(a[c]=b.destroy?b.destroy():null)});this.groupedData=null};d.generatePoints=function(){C.apply(this);
this.destroyGroupedData();this.groupedData=this.hasGroupedData?this.points:null};f(e.prototype,"update",function(b){this.dataGroup?a.error(24):b.apply(this,[].slice.call(arguments,1))});f(k.prototype,"tooltipFooterHeaderFormatter",function(b,c,d){var e=c.series,f=e.tooltipOptions,n=e.options.dataGrouping,k=f.xDateFormat,h,g=e.xAxis,l=a.dateFormat;return g&&"datetime"===g.options.type&&n&&v(c.key)?(b=e.currentDataGrouping,n=n.dateTimeLabelFormats,b?(g=n[b.unitName],1===b.count?k=g[0]:(k=g[1],h=g[2])):
!k&&n&&(k=this.getXDateFormat(c,f,g)),k=l(k,c.key),h&&(k+=l(h,c.key+b.totalRange-1)),t(f[(d?"footer":"header")+"Format"],{point:p(c.point,{key:k}),series:e})):b.call(this,c,d)});d.destroy=function(){for(var a=this.groupedData||[],d=a.length;d--;)a[d]&&a[d].destroy();c.apply(this)};f(d,"setOptions",function(a,c){a=a.call(this,c);var b=this.type,d=this.chart.options.plotOptions,e=E[b].dataGrouping;I[b]&&(e||(e=u(q,I[b])),a.dataGrouping=u(e,d.series&&d.series.dataGrouping,d[b].dataGrouping,c.dataGrouping));
this.chart.options.isStock&&(this.requireSorting=!0);return a});f(G.prototype,"setScale",function(a){a.call(this);g(this.series,function(a){a.hasProcessed=!1})});G.prototype.getGroupPixelWidth=function(){var a=this.series,c=a.length,d,e=0,f=!1,n;for(d=c;d--;)(n=a[d].options.dataGrouping)&&(e=Math.max(e,n.groupPixelWidth));for(d=c;d--;)(n=a[d].options.dataGrouping)&&a[d].hasProcessed&&(c=(a[d].processedXData||a[d].data).length,a[d].groupPixelWidth||c>this.chart.plotSizeX/e||c&&n.forced)&&(f=!0);return f?
e:0};G.prototype.setDataGrouping=function(a,c){var b;c=l(c,!0);a||(a={forced:!1,units:null});if(this instanceof G)for(b=this.series.length;b--;)this.series[b].update({dataGrouping:a},!1);else g(this.chart.options.series,function(b){b.dataGrouping=a},!1);c&&this.chart.redraw()}})(M);(function(a){var D=a.each,B=a.Point,G=a.seriesType,E=a.seriesTypes;G("ohlc","column",{lineWidth:1,tooltip:{pointFormat:'\x3cspan style\x3d"color:{point.color}"\x3e\u25cf\x3c/span\x3e \x3cb\x3e {series.name}\x3c/b\x3e\x3cbr/\x3eOpen: {point.open}\x3cbr/\x3eHigh: {point.high}\x3cbr/\x3eLow: {point.low}\x3cbr/\x3eClose: {point.close}\x3cbr/\x3e'},
threshold:null,states:{hover:{lineWidth:3}},stickyTracking:!0},{directTouch:!1,pointArrayMap:["open","high","low","close"],toYData:function(a){return[a.open,a.high,a.low,a.close]},pointValKey:"close",pointAttrToOptions:{stroke:"color","stroke-width":"lineWidth"},pointAttribs:function(a,g){g=E.column.prototype.pointAttribs.call(this,a,g);var p=this.options;delete g.fill;!a.options.color&&p.upColor&&a.open<a.close&&(g.stroke=p.upColor);return g},translate:function(){var a=this,g=a.yAxis,p=!!a.modifyValue,
t=["plotOpen","plotHigh","plotLow","plotClose","yBottom"];E.column.prototype.translate.apply(a);D(a.points,function(r){D([r.open,r.high,r.low,r.close,r.low],function(u,l){null!==u&&(p&&(u=a.modifyValue(u)),r[t[l]]=g.toPixels(u,!0))});r.tooltipPos[1]=r.plotHigh+g.pos-a.chart.plotTop})},drawPoints:function(){var a=this,g=a.chart;D(a.points,function(p){var r,v,u,l,e=p.graphic,k,f=!e;void 0!==p.plotY&&(e||(p.graphic=e=g.renderer.path().add(a.group)),e.attr(a.pointAttribs(p,p.selected&&"select")),v=e.strokeWidth()%
2/2,k=Math.round(p.plotX)-v,u=Math.round(p.shapeArgs.width/2),l=["M",k,Math.round(p.yBottom),"L",k,Math.round(p.plotHigh)],null!==p.open&&(r=Math.round(p.plotOpen)+v,l.push("M",k,r,"L",k-u,r)),null!==p.close&&(r=Math.round(p.plotClose)+v,l.push("M",k,r,"L",k+u,r)),e[f?"attr":"animate"]({d:l}).addClass(p.getClassName(),!0))})},animate:null},{getClassName:function(){return B.prototype.getClassName.call(this)+(this.open<this.close?" highcharts-point-up":" highcharts-point-down")}})})(M);(function(a){var D=
a.defaultPlotOptions,B=a.each,G=a.merge,E=a.seriesType,r=a.seriesTypes;E("candlestick","ohlc",G(D.column,{states:{hover:{lineWidth:2}},tooltip:D.ohlc.tooltip,threshold:null,lineColor:"#000000",lineWidth:1,upColor:"#ffffff",stickyTracking:!0}),{pointAttribs:function(a,p){var g=r.column.prototype.pointAttribs.call(this,a,p),v=this.options,u=a.open<a.close,l=v.lineColor||this.color;g["stroke-width"]=v.lineWidth;g.fill=a.options.color||(u?v.upColor||this.color:this.color);g.stroke=a.lineColor||(u?v.upLineColor||
l:l);p&&(a=v.states[p],g.fill=a.color||g.fill,g.stroke=a.lineColor||g.stroke,g["stroke-width"]=a.lineWidth||g["stroke-width"]);return g},drawPoints:function(){var a=this,p=a.chart;B(a.points,function(g){var r=g.graphic,u,l,e,k,f,d,t,C=!r;void 0!==g.plotY&&(r||(g.graphic=r=p.renderer.path().add(a.group)),r.attr(a.pointAttribs(g,g.selected&&"select")).shadow(a.options.shadow),f=r.strokeWidth()%2/2,d=Math.round(g.plotX)-f,u=g.plotOpen,l=g.plotClose,e=Math.min(u,l),u=Math.max(u,l),t=Math.round(g.shapeArgs.width/
2),l=Math.round(e)!==Math.round(g.plotHigh),k=u!==g.yBottom,e=Math.round(e)+f,u=Math.round(u)+f,f=[],f.push("M",d-t,u,"L",d-t,e,"L",d+t,e,"L",d+t,u,"Z","M",d,e,"L",d,l?Math.round(g.plotHigh):e,"M",d,u,"L",d,k?Math.round(g.yBottom):u),r[C?"attr":"animate"]({d:f}).addClass(g.getClassName(),!0))})}})})(M);(function(a){var D=a.addEvent,B=a.each,G=a.merge,E=a.noop,r=a.Renderer,g=a.seriesType,p=a.seriesTypes,t=a.TrackerMixin,v=a.VMLRenderer,u=a.SVGRenderer.prototype.symbols,l=a.stableSort;g("flags","column",
{pointRange:0,shape:"flag",stackDistance:12,textAlign:"center",tooltip:{pointFormat:"{point.text}\x3cbr/\x3e"},threshold:null,y:-30,fillColor:"#ffffff",lineWidth:1,states:{hover:{lineColor:"#000000",fillColor:"#ccd6eb"}},style:{fontSize:"11px",fontWeight:"bold"}},{sorted:!1,noSharedTooltip:!0,allowDG:!1,takeOrdinalPosition:!1,trackerGroups:["markerGroup"],forceCrop:!0,init:a.Series.prototype.init,pointAttribs:function(a,k){var e=this.options,d=a&&a.color||this.color,g=e.lineColor,l=a&&a.lineWidth;
a=a&&a.fillColor||e.fillColor;k&&(a=e.states[k].fillColor,g=e.states[k].lineColor,l=e.states[k].lineWidth);return{fill:a||d,stroke:g||d,"stroke-width":l||e.lineWidth||0}},translate:function(){p.column.prototype.translate.apply(this);var a=this.options,k=this.chart,f=this.points,d=f.length-1,g,r,c=a.onSeries;g=c&&k.get(c);var a=a.onKey||"y",c=g&&g.options.step,q=g&&g.points,t=q&&q.length,m=this.xAxis,u=this.yAxis,b=m.getExtremes(),z=0,v,y,A;if(g&&g.visible&&t)for(z=(g.pointXOffset||0)+(g.barW||0)/
2,g=g.currentDataGrouping,y=q[t-1].x+(g?g.totalRange:0),l(f,function(a,b){return a.x-b.x}),a="plot"+a[0].toUpperCase()+a.substr(1);t--&&f[d]&&!(g=f[d],v=q[t],v.x<=g.x&&void 0!==v[a]&&(g.x<=y&&(g.plotY=v[a],v.x<g.x&&!c&&(A=q[t+1])&&void 0!==A[a]&&(g.plotY+=(g.x-v.x)/(A.x-v.x)*(A[a]-v[a]))),d--,t++,0>d)););B(f,function(a,c){var d;void 0===a.plotY&&(a.x>=b.min&&a.x<=b.max?a.plotY=k.chartHeight-m.bottom-(m.opposite?m.height:0)+m.offset-u.top:a.shapeArgs={});a.plotX+=z;(r=f[c-1])&&r.plotX===a.plotX&&(void 0===
r.stackIndex&&(r.stackIndex=0),d=r.stackIndex+1);a.stackIndex=d})},drawPoints:function(){var e=this.points,g=this.chart,f=g.renderer,d,l,p=this.options,c=p.y,q,r,m,t,b,u,v,y=this.yAxis;for(r=e.length;r--;)m=e[r],v=m.plotX>this.xAxis.len,d=m.plotX,t=m.stackIndex,q=m.options.shape||p.shape,l=m.plotY,void 0!==l&&(l=m.plotY+c-(void 0!==t&&t*p.stackDistance)),b=t?void 0:m.plotX,u=t?void 0:m.plotY,t=m.graphic,void 0!==l&&0<=d&&!v?(t||(t=m.graphic=f.label("",null,null,q,null,null,p.useHTML).attr(this.pointAttribs(m)).css(G(p.style,
m.style)).attr({align:"flag"===q?"left":"center",width:p.width,height:p.height,"text-align":p.textAlign}).addClass("highcharts-point").add(this.markerGroup),m.graphic.div&&(m.graphic.div.point=m),t.shadow(p.shadow)),0<d&&(d-=t.strokeWidth()%2),t.attr({text:m.options.title||p.title||"A",x:d,y:l,anchorX:b,anchorY:u}),m.tooltipPos=g.inverted?[y.len+y.pos-g.plotLeft-l,this.xAxis.len-d]:[d,l+y.pos-g.plotTop]):t&&(m.graphic=t.destroy());p.useHTML&&a.wrap(this.markerGroup,"on",function(b){return a.SVGElement.prototype.on.apply(b.apply(this,
[].slice.call(arguments,1)),[].slice.call(arguments,1))})},drawTracker:function(){var a=this.points;t.drawTrackerPoint.apply(this);B(a,function(e){var f=e.graphic;f&&D(f.element,"mouseover",function(){0<e.stackIndex&&!e.raised&&(e._y=f.y,f.attr({y:e._y-8}),e.raised=!0);B(a,function(a){a!==e&&a.raised&&a.graphic&&(a.graphic.attr({y:a._y}),a.raised=!1)})})})},animate:E,buildKDTree:E,setClip:E});u.flag=function(a,g,f,d,l){return["M",l&&l.anchorX||a,l&&l.anchorY||g,"L",a,g+d,a,g,a+f,g,a+f,g+d,a,g+d,"Z"]};
B(["circle","square"],function(a){u[a+"pin"]=function(e,f,d,g,l){var c=l&&l.anchorX;l=l&&l.anchorY;"circle"===a&&g>d&&(e-=Math.round((g-d)/2),d=g);e=u[a](e,f,d,g);c&&l&&e.push("M",c,f>l?f:f+g,"L",c,l);return e}});r===v&&B(["flag","circlepin","squarepin"],function(a){v.prototype.symbols[a]=u[a]})})(M);(function(a){function D(a,d,e){this.init(a,d,e)}var B=a.addEvent,G=a.Axis,E=a.correctFloat,r=a.defaultOptions,g=a.defined,p=a.destroyObjectProperties,t=a.each,v=a.fireEvent,u=a.hasTouch,l=a.isTouchDevice,
e=a.merge,k=a.pick,f=a.removeEvent,d=a.wrap,x,C={height:l?20:14,barBorderRadius:0,buttonBorderRadius:0,liveRedraw:a.svg&&!l,margin:10,minWidth:6,step:.2,zIndex:3,barBackgroundColor:"#cccccc",barBorderWidth:1,barBorderColor:"#cccccc",buttonArrowColor:"#333333",buttonBackgroundColor:"#e6e6e6",buttonBorderColor:"#cccccc",buttonBorderWidth:1,rifleColor:"#333333",trackBackgroundColor:"#f2f2f2",trackBorderColor:"#f2f2f2",trackBorderWidth:1};r.scrollbar=e(!0,C,r.scrollbar);a.swapXY=x=function(a,d){var c=
a.length,e;if(d)for(d=0;d<c;d+=3)e=a[d+1],a[d+1]=a[d+2],a[d+2]=e;return a};D.prototype={init:function(a,d,f){this.scrollbarButtons=[];this.renderer=a;this.userOptions=d;this.options=e(C,d);this.chart=f;this.size=k(this.options.size,this.options.height);d.enabled&&(this.render(),this.initEvents(),this.addEvents())},render:function(){var a=this.renderer,d=this.options,e=this.size,f;this.group=f=a.g("scrollbar").attr({zIndex:d.zIndex,translateY:-99999}).add();this.track=a.rect().addClass("highcharts-scrollbar-track").attr({x:0,
r:d.trackBorderRadius||0,height:e,width:e}).add(f);this.track.attr({fill:d.trackBackgroundColor,stroke:d.trackBorderColor,"stroke-width":d.trackBorderWidth});this.trackBorderWidth=this.track.strokeWidth();this.track.attr({y:-this.trackBorderWidth%2/2});this.scrollbarGroup=a.g().add(f);this.scrollbar=a.rect().addClass("highcharts-scrollbar-thumb").attr({height:e,width:e,r:d.barBorderRadius||0}).add(this.scrollbarGroup);this.scrollbarRifles=a.path(x(["M",-3,e/4,"L",-3,2*e/3,"M",0,e/4,"L",0,2*e/3,"M",
3,e/4,"L",3,2*e/3],d.vertical)).addClass("highcharts-scrollbar-rifles").add(this.scrollbarGroup);this.scrollbar.attr({fill:d.barBackgroundColor,stroke:d.barBorderColor,"stroke-width":d.barBorderWidth});this.scrollbarRifles.attr({stroke:d.rifleColor,"stroke-width":1});this.scrollbarStrokeWidth=this.scrollbar.strokeWidth();this.scrollbarGroup.translate(-this.scrollbarStrokeWidth%2/2,-this.scrollbarStrokeWidth%2/2);this.drawScrollbarButton(0);this.drawScrollbarButton(1)},position:function(a,d,e,f){var c=
this.options.vertical,b=0,g=this.rendered?"animate":"attr";this.x=a;this.y=d+this.trackBorderWidth;this.width=e;this.xOffset=this.height=f;this.yOffset=b;c?(this.width=this.yOffset=e=b=this.size,this.xOffset=d=0,this.barWidth=f-2*e,this.x=a+=this.options.margin):(this.height=this.xOffset=f=d=this.size,this.barWidth=e-2*f,this.y+=this.options.margin);this.group[g]({translateX:a,translateY:this.y});this.track[g]({width:e,height:f});this.scrollbarButtons[1][g]({translateX:c?0:e-d,translateY:c?f-b:0})},
drawScrollbarButton:function(a){var c=this.renderer,d=this.scrollbarButtons,e=this.options,f=this.size,b;b=c.g().add(this.group);d.push(b);b=c.rect().addClass("highcharts-scrollbar-button").add(b);b.attr({stroke:e.buttonBorderColor,"stroke-width":e.buttonBorderWidth,fill:e.buttonBackgroundColor});b.attr(b.crisp({x:-.5,y:-.5,width:f+1,height:f+1,r:e.buttonBorderRadius},b.strokeWidth()));b=c.path(x(["M",f/2+(a?-1:1),f/2-3,"L",f/2+(a?-1:1),f/2+3,"L",f/2+(a?2:-2),f/2],e.vertical)).addClass("highcharts-scrollbar-arrow").add(d[a]);
b.attr({fill:e.buttonArrowColor})},setRange:function(a,d){var c=this.options,e=c.vertical,f=c.minWidth,b=this.barWidth,k,l,p=this.rendered&&!this.hasDragged?"animate":"attr";g(b)&&(a=Math.max(a,0),k=Math.ceil(b*a),this.calculatedWidth=l=E(b*Math.min(d,1)-k),l<f&&(k=(b-f+l)*a,l=f),f=Math.floor(k+this.xOffset+this.yOffset),b=l/2-.5,this.from=a,this.to=d,e?(this.scrollbarGroup[p]({translateY:f}),this.scrollbar[p]({height:l}),this.scrollbarRifles[p]({translateY:b}),this.scrollbarTop=f,this.scrollbarLeft=
0):(this.scrollbarGroup[p]({translateX:f}),this.scrollbar[p]({width:l}),this.scrollbarRifles[p]({translateX:b}),this.scrollbarLeft=f,this.scrollbarTop=0),12>=l?this.scrollbarRifles.hide():this.scrollbarRifles.show(!0),!1===c.showFull&&(0>=a&&1<=d?this.group.hide():this.group.show()),this.rendered=!0)},initEvents:function(){var a=this;a.mouseMoveHandler=function(c){var d=a.chart.pointer.normalize(c),e=a.options.vertical?"chartY":"chartX",f=a.initPositions;!a.grabbedCenter||c.touches&&0===c.touches[0][e]||
(d=a.cursorToScrollbarPosition(d)[e],e=a[e],e=d-e,a.hasDragged=!0,a.updatePosition(f[0]+e,f[1]+e),a.hasDragged&&v(a,"changed",{from:a.from,to:a.to,trigger:"scrollbar",DOMType:c.type,DOMEvent:c}))};a.mouseUpHandler=function(c){a.hasDragged&&v(a,"changed",{from:a.from,to:a.to,trigger:"scrollbar",DOMType:c.type,DOMEvent:c});a.grabbedCenter=a.hasDragged=a.chartX=a.chartY=null};a.mouseDownHandler=function(c){c=a.chart.pointer.normalize(c);c=a.cursorToScrollbarPosition(c);a.chartX=c.chartX;a.chartY=c.chartY;
a.initPositions=[a.from,a.to];a.grabbedCenter=!0};a.buttonToMinClick=function(c){var d=E(a.to-a.from)*a.options.step;a.updatePosition(E(a.from-d),E(a.to-d));v(a,"changed",{from:a.from,to:a.to,trigger:"scrollbar",DOMEvent:c})};a.buttonToMaxClick=function(c){var d=(a.to-a.from)*a.options.step;a.updatePosition(a.from+d,a.to+d);v(a,"changed",{from:a.from,to:a.to,trigger:"scrollbar",DOMEvent:c})};a.trackClick=function(c){var d=a.chart.pointer.normalize(c),e=a.to-a.from,f=a.y+a.scrollbarTop,b=a.x+a.scrollbarLeft;
a.options.vertical&&d.chartY>f||!a.options.vertical&&d.chartX>b?a.updatePosition(a.from+e,a.to+e):a.updatePosition(a.from-e,a.to-e);v(a,"changed",{from:a.from,to:a.to,trigger:"scrollbar",DOMEvent:c})}},cursorToScrollbarPosition:function(a){var c=this.options,c=c.minWidth>this.calculatedWidth?c.minWidth:0;return{chartX:(a.chartX-this.x-this.xOffset)/(this.barWidth-c),chartY:(a.chartY-this.y-this.yOffset)/(this.barWidth-c)}},updatePosition:function(a,d){1<d&&(a=E(1-E(d-a)),d=1);0>a&&(d=E(d-a),a=0);
this.from=a;this.to=d},update:function(a){this.destroy();this.init(this.chart.renderer,e(!0,this.options,a),this.chart)},addEvents:function(){var a=this.options.inverted?[1,0]:[0,1],d=this.scrollbarButtons,e=this.scrollbarGroup.element,f=this.mouseDownHandler,g=this.mouseMoveHandler,b=this.mouseUpHandler,a=[[d[a[0]].element,"click",this.buttonToMinClick],[d[a[1]].element,"click",this.buttonToMaxClick],[this.track.element,"click",this.trackClick],[e,"mousedown",f],[e.ownerDocument,"mousemove",g],[e.ownerDocument,
"mouseup",b]];u&&a.push([e,"touchstart",f],[e.ownerDocument,"touchmove",g],[e.ownerDocument,"touchend",b]);t(a,function(a){B.apply(null,a)});this._events=a},removeEvents:function(){t(this._events,function(a){f.apply(null,a)});this._events.length=0},destroy:function(){var a=this.chart.scroller;this.removeEvents();t(["track","scrollbarRifles","scrollbar","scrollbarGroup","group"],function(a){this[a]&&this[a].destroy&&(this[a]=this[a].destroy())},this);a&&this===a.scrollbar&&(a.scrollbar=null,p(a.scrollbarButtons))}};
d(G.prototype,"init",function(a){var c=this;a.apply(c,Array.prototype.slice.call(arguments,1));c.options.scrollbar&&c.options.scrollbar.enabled&&(c.options.scrollbar.vertical=!c.horiz,c.options.startOnTick=c.options.endOnTick=!1,c.scrollbar=new D(c.chart.renderer,c.options.scrollbar,c.chart),B(c.scrollbar,"changed",function(a){var d=Math.min(k(c.options.min,c.min),c.min,c.dataMin),e=Math.max(k(c.options.max,c.max),c.max,c.dataMax)-d,b;c.horiz&&!c.reversed||!c.horiz&&c.reversed?(b=d+e*this.to,d+=e*
this.from):(b=d+e*(1-this.from),d+=e*(1-this.to));c.setExtremes(d,b,!0,!1,a)}))});d(G.prototype,"render",function(a){var c=Math.min(k(this.options.min,this.min),this.min,k(this.dataMin,this.min)),d=Math.max(k(this.options.max,this.max),this.max,k(this.dataMax,this.max)),e=this.scrollbar,f=this.titleOffset||0;a.apply(this,Array.prototype.slice.call(arguments,1));if(e){this.horiz?(e.position(this.left,this.top+this.height+2+this.chart.scrollbarsOffsets[1]+(this.opposite?0:f+this.axisTitleMargin+this.offset),
this.width,this.height),f=1):(e.position(this.left+this.width+2+this.chart.scrollbarsOffsets[0]+(this.opposite?f+this.axisTitleMargin+this.offset:0),this.top,this.width,this.height),f=0);if(!this.opposite&&!this.horiz||this.opposite&&this.horiz)this.chart.scrollbarsOffsets[f]+=this.scrollbar.size+this.scrollbar.options.margin;isNaN(c)||isNaN(d)||!g(this.min)||!g(this.max)?e.setRange(0,0):(f=(this.min-c)/(d-c),c=(this.max-c)/(d-c),this.horiz&&!this.reversed||!this.horiz&&this.reversed?e.setRange(f,
c):e.setRange(1-c,1-f))}});d(G.prototype,"getOffset",function(a){var c=this.horiz?2:1,d=this.scrollbar;a.apply(this,Array.prototype.slice.call(arguments,1));d&&(this.chart.scrollbarsOffsets=[0,0],this.chart.axisOffset[c]+=d.size+d.options.margin)});d(G.prototype,"destroy",function(a){this.scrollbar&&(this.scrollbar=this.scrollbar.destroy());a.apply(this,Array.prototype.slice.call(arguments,1))});a.Scrollbar=D})(M);(function(a){function D(a){this.init(a)}var B=a.addEvent,G=a.Axis,E=a.Chart,r=a.color,
g=a.defaultOptions,p=a.defined,t=a.destroyObjectProperties,v=a.each,u=a.erase,l=a.error,e=a.extend,k=a.grep,f=a.hasTouch,d=a.isArray,x=a.isNumber,C=a.isObject,c=a.merge,q=a.pick,I=a.removeEvent,m=a.Scrollbar,J=a.Series,b=a.seriesTypes,z=a.wrap,K=a.swapXY,y=[].concat(a.defaultDataGroupingUnits),A=function(a){var b=k(arguments,x);if(b.length)return Math[a].apply(0,b)};y[4]=["day",[1,2,3,4]];y[5]=["week",[1,2,3]];b=void 0===b.areaspline?"line":"areaspline";e(g,{navigator:{height:40,margin:25,maskInside:!0,
handles:{backgroundColor:"#f2f2f2",borderColor:"#999999"},maskFill:r("#6685c2").setOpacity(.3).get(),outlineColor:"#cccccc",outlineWidth:1,series:{type:b,color:"#335cad",fillOpacity:.05,lineWidth:1,compare:null,dataGrouping:{approximation:"average",enabled:!0,groupPixelWidth:2,smoothed:!0,units:y},dataLabels:{enabled:!1,zIndex:2},id:"highcharts-navigator-series",className:"highcharts-navigator-series",lineColor:null,marker:{enabled:!1},pointRange:0,shadow:!1,threshold:null},xAxis:{className:"highcharts-navigator-xaxis",
tickLength:0,lineWidth:0,gridLineColor:"#e6e6e6",gridLineWidth:1,tickPixelInterval:200,labels:{align:"left",style:{color:"#999999"},x:3,y:-4},crosshair:!1},yAxis:{className:"highcharts-navigator-yaxis",gridLineWidth:0,startOnTick:!1,endOnTick:!1,minPadding:.1,maxPadding:.1,labels:{enabled:!1},crosshair:!1,title:{text:null},tickLength:0,tickWidth:0}}});D.prototype={drawHandle:function(a,b,c,d){this.handles[b][d](c?{translateX:Math.round(this.left+this.height/2-8),translateY:Math.round(this.top+parseInt(a,
10)+.5)}:{translateX:Math.round(this.left+parseInt(a,10)),translateY:Math.round(this.top+this.height/2-8)})},getHandlePath:function(a){return K(["M",-4.5,.5,"L",3.5,.5,"L",3.5,15.5,"L",-4.5,15.5,"L",-4.5,.5,"M",-1.5,4,"L",-1.5,12,"M",.5,4,"L",.5,12],a)},drawOutline:function(a,b,c,d){var e=this.navigatorOptions.maskInside,h=this.outline.strokeWidth(),f=h/2,h=h%2/2,g=this.outlineHeight,n=this.scrollbarHeight,k=this.size,l=this.left-n,m=this.top;c?(l-=f,c=m+b+h,b=m+a+h,a=["M",l+g,m-n-h,"L",l+g,c,"L",
l,c,"L",l,b,"L",l+g,b,"L",l+g,m+k+n].concat(e?["M",l+g,c-f,"L",l+g,b+f]:[])):(a+=l+n-h,b+=l+n-h,m+=f,a=["M",l,m,"L",a,m,"L",a,m+g,"L",b,m+g,"L",b,m,"L",l+k+2*n,m].concat(e?["M",a-f,m,"L",b+f,m]:[]));this.outline[d]({d:a})},drawMasks:function(a,b,c,d){var e=this.left,h=this.top,f=this.height,g,n,k,l;c?(k=[e,e,e],l=[h,h+a,h+b],n=[f,f,f],g=[a,b-a,this.size-b]):(k=[e,e+a,e+b],l=[h,h,h],n=[a,b-a,this.size-b],g=[f,f,f]);v(this.shades,function(a,b){a[d]({x:k[b],y:l[b],width:n[b],height:g[b]})})},renderElements:function(){var a=
this,b=a.navigatorOptions,c=b.maskInside,d=a.chart,e=d.inverted,f=d.renderer,g;a.navigatorGroup=g=f.g("navigator").attr({zIndex:8,visibility:"hidden"}).add();var k={cursor:e?"ns-resize":"ew-resize"};v([!c,c,!c],function(c,d){a.shades[d]=f.rect().addClass("highcharts-navigator-mask"+(1===d?"-inside":"-outside")).attr({fill:c?b.maskFill:"rgba(0,0,0,0)"}).css(1===d&&k).add(g)});a.outline=f.path().addClass("highcharts-navigator-outline").attr({"stroke-width":b.outlineWidth,stroke:b.outlineColor}).add(g);
v([0,1],function(c){a.handles[c]=f.path(a.getHandlePath(e)).attr({zIndex:7-c}).addClass("highcharts-navigator-handle highcharts-navigator-handle-"+["left","right"][c]).add(g);var d=b.handles;a.handles[c].attr({fill:d.backgroundColor,stroke:d.borderColor,"stroke-width":1}).css(k)})},update:function(a){v(this.series||[],function(a){a.baseSeries&&delete a.baseSeries.navigatorSeries});this.destroy();c(!0,this.chart.options.navigator,this.options,a);this.init(this.chart)},render:function(a,b,c,d){var e=
this.chart,h,f,g=this.scrollbarHeight,n,k=this.xAxis;h=k.fake?e.xAxis[0]:k;var l=this.navigatorEnabled,m,r=this.rendered;f=e.inverted;var w=e.xAxis[0].minRange;if(!this.hasDragged||p(c)){if(!x(a)||!x(b))if(r)c=0,d=k.width;else return;this.left=q(k.left,e.plotLeft+g+(f?e.plotWidth:0));this.size=m=n=q(k.len,(f?e.plotHeight:e.plotWidth)-2*g);e=f?g:n+2*g;c=q(c,k.toPixels(a,!0));d=q(d,k.toPixels(b,!0));x(c)&&Infinity!==Math.abs(c)||(c=0,d=e);a=k.toValue(c,!0);b=k.toValue(d,!0);if(Math.abs(b-a)<w)if(this.grabbedLeft)c=
k.toPixels(b-w,!0);else if(this.grabbedRight)d=k.toPixels(a+w,!0);else return;this.zoomedMax=Math.min(Math.max(c,d,0),m);this.zoomedMin=Math.min(Math.max(this.fixedWidth?this.zoomedMax-this.fixedWidth:Math.min(c,d),0),m);this.range=this.zoomedMax-this.zoomedMin;m=Math.round(this.zoomedMax);c=Math.round(this.zoomedMin);l&&(this.navigatorGroup.attr({visibility:"visible"}),r=r&&!this.hasDragged?"animate":"attr",this.drawMasks(c,m,f,r),this.drawOutline(c,m,f,r),this.drawHandle(c,0,f,r),this.drawHandle(m,
1,f,r));this.scrollbar&&(f?(f=this.top-g,h=this.left-g+(l||!h.opposite?0:(h.titleOffset||0)+h.axisTitleMargin),g=n+2*g):(f=this.top+(l?this.height:-g),h=this.left-g),this.scrollbar.position(h,f,e,g),this.scrollbar.setRange(this.zoomedMin/n,this.zoomedMax/n));this.rendered=!0}},addMouseEvents:function(){var a=this,b=a.chart,c=b.container,d=[],e,g;a.mouseMoveHandler=e=function(b){a.onMouseMove(b)};a.mouseUpHandler=g=function(b){a.onMouseUp(b)};d=a.getPartsEvents("mousedown");d.push(B(c,"mousemove",
e),B(c.ownerDocument,"mouseup",g));f&&(d.push(B(c,"touchmove",e),B(c.ownerDocument,"touchend",g)),d.concat(a.getPartsEvents("touchstart")));a.eventsToUnbind=d;a.series&&a.series[0]&&d.push(B(a.series[0].xAxis,"foundExtremes",function(){b.navigator.modifyNavigatorAxisExtremes()}))},getPartsEvents:function(a){var b=this,c=[];v(["shades","handles"],function(d){v(b[d],function(e,f){c.push(B(e.element,a,function(a){b[d+"Mousedown"](a,f)}))})});return c},shadesMousedown:function(a,b){a=this.chart.pointer.normalize(a);
var c=this.chart,d=this.xAxis,e=this.zoomedMin,f=this.left,g=this.size,k=this.range,n=a.chartX,l;c.inverted&&(n=a.chartY,f=this.top);1===b?(this.grabbedCenter=n,this.fixedWidth=k,this.dragOffset=n-e):(a=n-f-k/2,0===b?a=Math.max(0,a):2===b&&a+k>=g&&(a=g-k,l=this.getUnionExtremes().dataMax),a!==e&&(this.fixedWidth=k,b=d.toFixedRange(a,a+k,null,l),c.xAxis[0].setExtremes(Math.min(b.min,b.max),Math.max(b.min,b.max),!0,null,{trigger:"navigator"})))},handlesMousedown:function(a,b){this.chart.pointer.normalize(a);
a=this.chart;var c=a.xAxis[0],d=a.inverted&&!c.reversed||!a.inverted&&c.reversed;0===b?(this.grabbedLeft=!0,this.otherHandlePos=this.zoomedMax,this.fixedExtreme=d?c.min:c.max):(this.grabbedRight=!0,this.otherHandlePos=this.zoomedMin,this.fixedExtreme=d?c.max:c.min);a.fixedRange=null},onMouseMove:function(a){var b=this,c=b.chart,d=b.left,e=b.navigatorSize,f=b.range,g=b.dragOffset,k=c.inverted;a.touches&&0===a.touches[0].pageX||(a=c.pointer.normalize(a),c=a.chartX,k&&(d=b.top,c=a.chartY),b.grabbedLeft?
(b.hasDragged=!0,b.render(0,0,c-d,b.otherHandlePos)):b.grabbedRight?(b.hasDragged=!0,b.render(0,0,b.otherHandlePos,c-d)):b.grabbedCenter&&(b.hasDragged=!0,c<g?c=g:c>e+g-f&&(c=e+g-f),b.render(0,0,c-g,c-g+f)),b.hasDragged&&b.scrollbar&&b.scrollbar.options.liveRedraw&&(a.DOMType=a.type,setTimeout(function(){b.onMouseUp(a)},0)))},onMouseUp:function(a){var b=this.chart,c=this.xAxis,d=this.scrollbar,e,f,g=a.DOMEvent||a;(!this.hasDragged||d&&d.hasDragged)&&"scrollbar"!==a.trigger||(this.zoomedMin===this.otherHandlePos?
e=this.fixedExtreme:this.zoomedMax===this.otherHandlePos&&(f=this.fixedExtreme),this.zoomedMax===this.size&&(f=this.getUnionExtremes().dataMax),c=c.toFixedRange(this.zoomedMin,this.zoomedMax,e,f),p(c.min)&&b.xAxis[0].setExtremes(Math.min(c.min,c.max),Math.max(c.min,c.max),!0,this.hasDragged?!1:null,{trigger:"navigator",triggerOp:"navigator-drag",DOMEvent:g}));"mousemove"!==a.DOMType&&(this.grabbedLeft=this.grabbedRight=this.grabbedCenter=this.fixedWidth=this.fixedExtreme=this.otherHandlePos=this.hasDragged=
this.dragOffset=null)},removeEvents:function(){this.eventsToUnbind&&(v(this.eventsToUnbind,function(a){a()}),this.eventsToUnbind=void 0);this.removeBaseSeriesEvents()},removeBaseSeriesEvents:function(){var a=this.baseSeries||[];this.navigatorEnabled&&a[0]&&(!1!==this.navigatorOptions.adaptToUpdatedData&&v(a,function(a){I(a,"updatedData",this.updatedDataHandler)},this),a[0].xAxis&&I(a[0].xAxis,"foundExtremes",this.modifyBaseAxisExtremes))},init:function(a){var b=a.options,d=b.navigator,e=d.enabled,
f=b.scrollbar,g=f.enabled,b=e?d.height:0,k=g?f.height:0;this.handles=[];this.shades=[];this.chart=a;this.setBaseSeries();this.height=b;this.scrollbarHeight=k;this.scrollbarEnabled=g;this.navigatorEnabled=e;this.navigatorOptions=d;this.scrollbarOptions=f;this.outlineHeight=b+k;this.opposite=q(d.opposite,!e&&a.inverted);var n=this,f=n.baseSeries,g=a.xAxis.length,l=a.yAxis.length,p=f&&f[0]&&f[0].xAxis||a.xAxis[0];a.extraMargin={type:n.opposite?"plotTop":"marginBottom",value:(e||!a.inverted?n.outlineHeight:
0)+d.margin};a.inverted&&(a.extraMargin.type=n.opposite?"marginRight":"plotLeft");a.isDirtyBox=!0;n.navigatorEnabled?(n.xAxis=new G(a,c({breaks:p.options.breaks,ordinal:p.options.ordinal},d.xAxis,{id:"navigator-x-axis",yAxis:"navigator-y-axis",isX:!0,type:"datetime",index:g,offset:0,keepOrdinalPadding:!0,startOnTick:!1,endOnTick:!1,minPadding:0,maxPadding:0,zoomEnabled:!1},a.inverted?{offsets:[k,0,-k,0],width:b}:{offsets:[0,-k,0,k],height:b})),n.yAxis=new G(a,c(d.yAxis,{id:"navigator-y-axis",alignTicks:!1,
offset:0,index:l,zoomEnabled:!1},a.inverted?{width:b}:{height:b})),f||d.series.data?n.updateNavigatorSeries():0===a.series.length&&z(a,"redraw",function(b,c){0<a.series.length&&!n.series&&(n.setBaseSeries(),a.redraw=b);b.call(a,c)}),n.renderElements(),n.addMouseEvents()):n.xAxis={translate:function(b,c){var d=a.xAxis[0],e=d.getExtremes(),f=d.len-2*k,h=A("min",d.options.min,e.dataMin),d=A("max",d.options.max,e.dataMax)-h;return c?b*d/f+h:f*(b-h)/d},toPixels:function(a){return this.translate(a)},toValue:function(a){return this.translate(a,
!0)},toFixedRange:G.prototype.toFixedRange,fake:!0};a.options.scrollbar.enabled&&(a.scrollbar=n.scrollbar=new m(a.renderer,c(a.options.scrollbar,{margin:n.navigatorEnabled?0:10,vertical:a.inverted}),a),B(n.scrollbar,"changed",function(b){var c=n.size,d=c*this.to,c=c*this.from;n.hasDragged=n.scrollbar.hasDragged;n.render(0,0,c,d);(a.options.scrollbar.liveRedraw||"mousemove"!==b.DOMType)&&setTimeout(function(){n.onMouseUp(b)})}));n.addBaseSeriesEvents();n.addChartEvents()},getUnionExtremes:function(a){var b=
this.chart.xAxis[0],c=this.xAxis,d=c.options,e=b.options,f;a&&null===b.dataMin||(f={dataMin:q(d&&d.min,A("min",e.min,b.dataMin,c.dataMin,c.min)),dataMax:q(d&&d.max,A("max",e.max,b.dataMax,c.dataMax,c.max))});return f},setBaseSeries:function(a){var b=this.chart,c=this.baseSeries=[];a=a||b.options&&b.options.navigator.baseSeries||0;v(b.series||[],function(b,d){b.options.isInternal||!b.options.showInNavigator&&(d!==a&&b.options.id!==a||!1===b.options.showInNavigator)||c.push(b)});this.xAxis&&!this.xAxis.fake&&
this.updateNavigatorSeries()},updateNavigatorSeries:function(){var b=this,e=b.chart,f=b.baseSeries,g,k,l=b.navigatorOptions.series,m,p={enableMouseTracking:!1,index:null,linkedTo:null,group:"nav",padXAxis:!1,xAxis:"navigator-x-axis",yAxis:"navigator-y-axis",showInLegend:!1,stacking:!1,isInternal:!0,visible:!0},q=b.series=a.grep(b.series||[],function(c){var d=c.baseSeries;return 0>a.inArray(d,f)?(d&&(I(d,"updatedData",b.updatedDataHandler),delete d.navigatorSeries),c.destroy(),!1):!0});f&&f.length&&
v(f,function(a,f){var h=a.navigatorSeries,n=d(l)?{}:l;h&&!1===b.navigatorOptions.adaptToUpdatedData||(p.name="Navigator "+(f+1),g=a.options||{},m=g.navigatorOptions||{},k=c(g,p,n,m),f=m.data||n.data,b.hasNavigatorData=b.hasNavigatorData||!!f,k.data=f||g.data&&g.data.slice(0),h?h.update(k):(a.navigatorSeries=e.initSeries(k),a.navigatorSeries.baseSeries=a,q.push(a.navigatorSeries)))});if(l.data&&(!f||!f.length)||d(l))b.hasNavigatorData=!1,l=a.splat(l),v(l,function(a,d){k=c({color:e.series[d]&&!e.series[d].options.isInternal&&
e.series[d].color||e.options.colors[d]||e.options.colors[0]},a,p);k.data=a.data;k.data&&(b.hasNavigatorData=!0,q.push(e.initSeries(k)))});this.addBaseSeriesEvents()},addBaseSeriesEvents:function(){var a=this,b=a.baseSeries||[];b[0]&&b[0].xAxis&&B(b[0].xAxis,"foundExtremes",this.modifyBaseAxisExtremes);v(b,function(b){B(b,"show",function(){this.navigatorSeries&&this.navigatorSeries.show()});B(b,"hide",function(){this.navigatorSeries&&this.navigatorSeries.hide()});!1!==this.navigatorOptions.adaptToUpdatedData&&
b.xAxis&&B(b,"updatedData",this.updatedDataHandler);B(b,"remove",function(){this.navigatorSeries&&(u(a.series,this.navigatorSeries),this.navigatorSeries.remove(!1),delete this.navigatorSeries)})},this)},modifyNavigatorAxisExtremes:function(){var a=this.xAxis,b;a.getExtremes&&(!(b=this.getUnionExtremes(!0))||b.dataMin===a.min&&b.dataMax===a.max||(a.min=b.dataMin,a.max=b.dataMax))},modifyBaseAxisExtremes:function(){var a=this.chart.navigator,b=this.getExtremes(),c=b.dataMin,d=b.dataMax,b=b.max-b.min,
e=a.stickToMin,f=a.stickToMax,g,k,l=a.series&&a.series[0],m=!!this.setExtremes;this.eventArgs&&"rangeSelectorButton"===this.eventArgs.trigger||(e&&(k=c,g=k+b),f&&(g=d,e||(k=Math.max(g-b,l&&l.xData?l.xData[0]:-Number.MAX_VALUE))),m&&(e||f)&&x(k)&&(this.min=this.userMin=k,this.max=this.userMax=g));a.stickToMin=a.stickToMax=null},updatedDataHandler:function(){var a=this.chart.navigator,b=this.navigatorSeries;a.stickToMax=Math.round(a.zoomedMax)>=Math.round(a.size);a.stickToMin=x(this.xAxis.min)&&this.xAxis.min<=
this.xData[0]&&(!this.chart.fixedRange||!a.stickToMax);b&&!a.hasNavigatorData&&(b.options.pointStart=this.xData[0],b.setData(this.options.data,!1,null,!1))},addChartEvents:function(){B(this.chart,"redraw",function(){var a=this.navigator,b=a&&(a.baseSeries&&a.baseSeries[0]&&a.baseSeries[0].xAxis||a.scrollbar&&this.xAxis[0]);b&&a.render(b.min,b.max)})},destroy:function(){this.removeEvents();this.xAxis&&(u(this.chart.xAxis,this.xAxis),u(this.chart.axes,this.xAxis));this.yAxis&&(u(this.chart.yAxis,this.yAxis),
u(this.chart.axes,this.yAxis));v(this.series||[],function(a){a.destroy&&a.destroy()});v("series xAxis yAxis shades outline scrollbarTrack scrollbarRifles scrollbarGroup scrollbar navigatorGroup rendered".split(" "),function(a){this[a]&&this[a].destroy&&this[a].destroy();this[a]=null},this);v([this.handles],function(a){t(a)},this)}};a.Navigator=D;z(G.prototype,"zoom",function(a,b,c){var d=this.chart,e=d.options,f=e.chart.zoomType,h=e.navigator,e=e.rangeSelector,g;this.isXAxis&&(h&&h.enabled||e&&e.enabled)&&
("x"===f?d.resetZoomButton="blocked":"y"===f?g=!1:"xy"===f&&(d=this.previousZoom,p(b)?this.previousZoom=[this.min,this.max]:d&&(b=d[0],c=d[1],delete this.previousZoom)));return void 0!==g?g:a.call(this,b,c)});z(E.prototype,"init",function(a,b,c){B(this,"beforeRender",function(){var a=this.options;if(a.navigator.enabled||a.scrollbar.enabled)this.scroller=this.navigator=new D(this)});a.call(this,b,c)});z(E.prototype,"setChartSize",function(a){var b=this.legend,c=this.navigator,d,e,f,g;a.apply(this,
[].slice.call(arguments,1));c&&(e=b.options,f=c.xAxis,g=c.yAxis,d=c.scrollbarHeight,this.inverted?(c.left=c.opposite?this.chartWidth-d-c.height:this.spacing[3]+d,c.top=this.plotTop+d):(c.left=this.plotLeft+d,c.top=c.navigatorOptions.top||this.chartHeight-c.height-d-this.spacing[2]-("bottom"===e.verticalAlign&&e.enabled&&!e.floating?b.legendHeight+q(e.margin,10):0)),f&&g&&(this.inverted?f.options.left=g.options.left=c.left:f.options.top=g.options.top=c.top,f.setAxisSize(),g.setAxisSize()))});z(J.prototype,
"addPoint",function(a,b,c,d,e){var f=this.options.turboThreshold;f&&this.xData.length>f&&C(b,!0)&&this.chart.navigator&&l(20,!0);a.call(this,b,c,d,e)});z(E.prototype,"addSeries",function(a,b,c,d){a=a.call(this,b,!1,d);this.navigator&&this.navigator.setBaseSeries();q(c,!0)&&this.redraw();return a});z(J.prototype,"update",function(a,b,c){a.call(this,b,!1);this.chart.navigator&&!this.options.isInternal&&this.chart.navigator.setBaseSeries();q(c,!0)&&this.chart.redraw()});E.prototype.callbacks.push(function(a){var b=
a.navigator;b&&(a=a.xAxis[0].getExtremes(),b.render(a.min,a.max))})})(M);(function(a){function D(a){this.init(a)}var B=a.addEvent,G=a.Axis,E=a.Chart,r=a.css,g=a.createElement,p=a.dateFormat,t=a.defaultOptions,v=t.global.useUTC,u=a.defined,l=a.destroyObjectProperties,e=a.discardElement,k=a.each,f=a.extend,d=a.fireEvent,x=a.Date,C=a.isNumber,c=a.merge,q=a.pick,I=a.pInt,m=a.splat,J=a.wrap;f(t,{rangeSelector:{buttonTheme:{"stroke-width":0,width:28,height:18,padding:2,zIndex:7},height:35,inputPosition:{align:"right"},
labelStyle:{color:"#666666"}}});t.lang=c(t.lang,{rangeSelectorZoom:"Zoom",rangeSelectorFrom:"From",rangeSelectorTo:"To"});D.prototype={clickButton:function(a,c){var b=this,d=b.chart,e=b.buttonOptions[a],f=d.xAxis[0],g=d.scroller&&d.scroller.getUnionExtremes()||f||{},h=g.dataMin,l=g.dataMax,p,r=f&&Math.round(Math.min(f.max,q(l,f.max))),t=e.type,u,g=e._range,z,x,D,E=e.dataGrouping;if(null!==h&&null!==l){d.fixedRange=g;E&&(this.forcedDataGrouping=!0,G.prototype.setDataGrouping.call(f||{chart:this.chart},
E,!1));if("month"===t||"year"===t)f?(t={range:e,max:r,dataMin:h,dataMax:l},p=f.minFromRange.call(t),C(t.newMax)&&(r=t.newMax)):g=e;else if(g)p=Math.max(r-g,h),r=Math.min(p+g,l);else if("ytd"===t)if(f)void 0===l&&(h=Number.MAX_VALUE,l=Number.MIN_VALUE,k(d.series,function(a){a=a.xData;h=Math.min(a[0],h);l=Math.max(a[a.length-1],l)}),c=!1),r=b.getYTDExtremes(l,h,v),p=z=r.min,r=r.max;else{B(d,"beforeRender",function(){b.clickButton(a)});return}else"all"===t&&f&&(p=h,r=l);b.setSelected(a);f?f.setExtremes(p,
r,q(c,1),null,{trigger:"rangeSelectorButton",rangeSelectorButton:e}):(u=m(d.options.xAxis)[0],D=u.range,u.range=g,x=u.min,u.min=z,B(d,"load",function(){u.range=D;u.min=x}))}},setSelected:function(a){this.selected=this.options.selected=a},defaultButtons:[{type:"month",count:1,text:"1m"},{type:"month",count:3,text:"3m"},{type:"month",count:6,text:"6m"},{type:"ytd",text:"YTD"},{type:"year",count:1,text:"1y"},{type:"all",text:"All"}],init:function(a){var b=this,c=a.options.rangeSelector,e=c.buttons||
[].concat(b.defaultButtons),f=c.selected,g=function(){var a=b.minInput,c=b.maxInput;a&&a.blur&&d(a,"blur");c&&c.blur&&d(c,"blur")};b.chart=a;b.options=c;b.buttons=[];a.extraTopMargin=c.height;b.buttonOptions=e;this.unMouseDown=B(a.container,"mousedown",g);this.unResize=B(a,"resize",g);k(e,b.computeButtonRange);void 0!==f&&e[f]&&this.clickButton(f,!1);B(a,"load",function(){B(a.xAxis[0],"setExtremes",function(c){this.max-this.min!==a.fixedRange&&"rangeSelectorButton"!==c.trigger&&"updatedData"!==c.trigger&&
b.forcedDataGrouping&&this.setDataGrouping(!1,!1)})})},updateButtonStates:function(){var a=this.chart,c=a.xAxis[0],d=Math.round(c.max-c.min),e=!c.hasVisibleSeries,a=a.scroller&&a.scroller.getUnionExtremes()||c,f=a.dataMin,g=a.dataMax,a=this.getYTDExtremes(g,f,v),l=a.min,h=a.max,m=this.selected,p=C(m),q=this.options.allButtonsEnabled,r=this.buttons;k(this.buttonOptions,function(a,b){var k=a._range,n=a.type,t=a.count||1;a=r[b];var u=0;b=b===m;var w=k>g-f,y=k<c.minRange,v=!1,A=!1,k=k===d;("month"===
n||"year"===n)&&d>=864E5*{month:28,year:365}[n]*t&&d<=864E5*{month:31,year:366}[n]*t?k=!0:"ytd"===n?(k=h-l===d,v=!b):"all"===n&&(k=c.max-c.min>=g-f,A=!b&&p&&k);n=!q&&(w||y||A||e);k=b&&k||k&&!p&&!v;n?u=3:k&&(p=!0,u=2);a.state!==u&&a.setState(u)})},computeButtonRange:function(a){var b=a.type,c=a.count||1,d={millisecond:1,second:1E3,minute:6E4,hour:36E5,day:864E5,week:6048E5};if(d[b])a._range=d[b]*c;else if("month"===b||"year"===b)a._range=864E5*{month:30,year:365}[b]*c},setInputValue:function(a,c){var b=
this.chart.options.rangeSelector,d=this[a+"Input"];u(c)&&(d.previousValue=d.HCTime,d.HCTime=c);d.value=p(b.inputEditDateFormat||"%Y-%m-%d",d.HCTime);this[a+"DateBox"].attr({text:p(b.inputDateFormat||"%b %e, %Y",d.HCTime)})},showInput:function(a){var b=this.inputGroup,c=this[a+"DateBox"];r(this[a+"Input"],{left:b.translateX+c.x+"px",top:b.translateY+"px",width:c.width-2+"px",height:c.height-2+"px",border:"2px solid silver"})},hideInput:function(a){r(this[a+"Input"],{border:0,width:"1px",height:"1px"});
this.setInputValue(a)},drawInput:function(a){function b(){var a=q.value,b=(m.inputDateParser||Date.parse)(a),c=e.xAxis[0],f=e.scroller&&e.scroller.xAxis?e.scroller.xAxis:c,h=f.dataMin,f=f.dataMax;b!==q.previousValue&&(q.previousValue=b,C(b)||(b=a.split("-"),b=Date.UTC(I(b[0]),I(b[1])-1,I(b[2]))),C(b)&&(v||(b+=6E4*(new Date).getTimezoneOffset()),p?b>d.maxInput.HCTime?b=void 0:b<h&&(b=h):b<d.minInput.HCTime?b=void 0:b>f&&(b=f),void 0!==b&&c.setExtremes(p?b:c.min,p?c.max:b,void 0,void 0,{trigger:"rangeSelectorInput"})))}
var d=this,e=d.chart,k=e.renderer.style||{},l=e.renderer,m=e.options.rangeSelector,h=d.div,p="min"===a,q,u,x=this.inputGroup;this[a+"Label"]=u=l.label(t.lang[p?"rangeSelectorFrom":"rangeSelectorTo"],this.inputGroup.offset).addClass("highcharts-range-label").attr({padding:2}).add(x);x.offset+=u.width+5;this[a+"DateBox"]=l=l.label("",x.offset).addClass("highcharts-range-input").attr({padding:2,width:m.inputBoxWidth||90,height:m.inputBoxHeight||17,stroke:m.inputBoxBorderColor||"#cccccc","stroke-width":1,
"text-align":"center"}).on("click",function(){d.showInput(a);d[a+"Input"].focus()}).add(x);x.offset+=l.width+(p?10:0);this[a+"Input"]=q=g("input",{name:a,className:"highcharts-range-selector",type:"text"},{top:e.plotTop+"px"},h);u.css(c(k,m.labelStyle));l.css(c({color:"#333333"},k,m.inputStyle));r(q,f({position:"absolute",border:0,width:"1px",height:"1px",padding:0,textAlign:"center",fontSize:k.fontSize,fontFamily:k.fontFamily,left:"-9em"},m.inputStyle));q.onfocus=function(){d.showInput(a)};q.onblur=
function(){d.hideInput(a)};q.onchange=b;q.onkeypress=function(a){13===a.keyCode&&b()}},getPosition:function(){var a=this.chart,c=a.options.rangeSelector,a=q((c.buttonPosition||{}).y,a.plotTop-a.axisOffset[0]-c.height);return{buttonTop:a,inputTop:a-10}},getYTDExtremes:function(a,c,d){var b=new x(a),e=b[x.hcGetFullYear]();d=d?x.UTC(e,0,1):+new x(e,0,1);c=Math.max(c||0,d);b=b.getTime();return{max:Math.min(a||b,b),min:c}},render:function(a,c){var b=this,d=b.chart,e=d.renderer,l=d.container,m=d.options,
h=m.exporting&&!1!==m.exporting.enabled&&m.navigation&&m.navigation.buttonOptions,p=m.rangeSelector,r=b.buttons,m=t.lang,v=b.div,v=b.inputGroup,z=p.buttonTheme,x=p.buttonPosition||{},B=p.inputEnabled,F=z&&z.states,C=d.plotLeft,D,E=this.getPosition(),G=b.group,I=b.rendered;!1!==p.enabled&&(I||(b.group=G=e.g("range-selector-buttons").add(),b.zoomText=e.text(m.rangeSelectorZoom,q(x.x,C),15).css(p.labelStyle).add(G),D=q(x.x,C)+b.zoomText.getBBox().width+5,k(b.buttonOptions,function(a,c){r[c]=e.button(a.text,
D,0,function(){b.clickButton(c);b.isActive=!0},z,F&&F.hover,F&&F.select,F&&F.disabled).attr({"text-align":"center"}).add(G);D+=r[c].width+q(p.buttonSpacing,5)}),!1!==B&&(b.div=v=g("div",null,{position:"relative",height:0,zIndex:1}),l.parentNode.insertBefore(v,l),b.inputGroup=v=e.g("input-group").add(),v.offset=0,b.drawInput("min"),b.drawInput("max"))),b.updateButtonStates(),G[I?"animate":"attr"]({translateY:E.buttonTop}),!1!==B&&(v.align(f({y:E.inputTop,width:v.offset,x:h&&E.inputTop<(h.y||0)+h.height-
d.spacing[0]?-40:0},p.inputPosition),!0,d.spacingBox),u(B)||(d=G.getBBox(),v[v.alignAttr.translateX<d.x+d.width+10?"hide":"show"]()),b.setInputValue("min",a),b.setInputValue("max",c)),b.rendered=!0)},update:function(a){var b=this.chart;c(!0,b.options.rangeSelector,a);this.destroy();this.init(b)},destroy:function(){var b=this,c=b.minInput,d=b.maxInput;b.unMouseDown();b.unResize();l(b.buttons);c&&(c.onfocus=c.onblur=c.onchange=null);d&&(d.onfocus=d.onblur=d.onchange=null);a.objectEach(b,function(a,
c){a&&"chart"!==c&&(a.destroy?a.destroy():a.nodeType&&e(this[c]));a!==D.prototype[c]&&(b[c]=null)},this)}};G.prototype.toFixedRange=function(a,c,d,e){var b=this.chart&&this.chart.fixedRange;a=q(d,this.translate(a,!0,!this.horiz));c=q(e,this.translate(c,!0,!this.horiz));d=b&&(c-a)/b;.7<d&&1.3>d&&(e?a=c-b:c=a+b);C(a)||(a=c=void 0);return{min:a,max:c}};G.prototype.minFromRange=function(){var a=this.range,c={month:"Month",year:"FullYear"}[a.type],d,e=this.max,f,g,k=function(a,b){var d=new Date(a),e=d["get"+
c]();d["set"+c](e+b);e===d["get"+c]()&&d.setDate(0);return d.getTime()-a};C(a)?(d=e-a,g=a):(d=e+k(e,-a.count),this.chart&&(this.chart.fixedRange=e-d));f=q(this.dataMin,Number.MIN_VALUE);C(d)||(d=f);d<=f&&(d=f,void 0===g&&(g=k(d,a.count)),this.newMax=Math.min(d+g,this.dataMax));C(e)||(d=void 0);return d};J(E.prototype,"init",function(a,c,d){B(this,"init",function(){this.options.rangeSelector.enabled&&(this.rangeSelector=new D(this))});a.call(this,c,d)});E.prototype.callbacks.push(function(a){function b(){c=
a.xAxis[0].getExtremes();C(c.min)&&d.render(c.min,c.max)}var c,d=a.rangeSelector,e,f;d&&(f=B(a.xAxis[0],"afterSetExtremes",function(a){d.render(a.min,a.max)}),e=B(a,"redraw",b),b());B(a,"destroy",function(){d&&(e(),f())})});a.RangeSelector=D})(M);(function(a){var D=a.arrayMax,B=a.arrayMin,G=a.Axis,E=a.Chart,r=a.defined,g=a.each,p=a.extend,t=a.format,v=a.grep,u=a.inArray,l=a.isNumber,e=a.isString,k=a.map,f=a.merge,d=a.pick,x=a.Point,C=a.Renderer,c=a.Series,q=a.splat,I=a.SVGRenderer,m=a.VMLRenderer,
J=a.wrap,b=c.prototype,z=b.init,K=b.processData,y=x.prototype.tooltipFormatter;a.StockChart=a.stockChart=function(b,c,g){var h=e(b)||b.nodeName,l=arguments[h?1:0],n=l.series,m=a.getOptions(),p,r=d(l.navigator&&l.navigator.enabled,m.navigator.enabled,!0),t=r?{startOnTick:!1,endOnTick:!1}:null,u={marker:{enabled:!1,radius:2}},v={shadow:!1,borderWidth:0};l.xAxis=k(q(l.xAxis||{}),function(a){return f({minPadding:0,maxPadding:0,ordinal:!0,title:{text:null},labels:{overflow:"justify"},showLastLabel:!0},
m.xAxis,a,{type:"datetime",categories:null},t)});l.yAxis=k(q(l.yAxis||{}),function(a){p=d(a.opposite,!0);return f({labels:{y:-2},opposite:p,showLastLabel:!1,title:{text:null}},m.yAxis,a)});l.series=null;l=f({chart:{panning:!0,pinchType:"x"},navigator:{enabled:r},scrollbar:{enabled:d(m.scrollbar.enabled,!0)},rangeSelector:{enabled:d(m.rangeSelector.enabled,!0)},title:{text:null},tooltip:{shared:!0,crosshairs:!0},legend:{enabled:!1},plotOptions:{line:u,spline:u,area:u,areaspline:u,arearange:u,areasplinerange:u,
column:v,columnrange:v,candlestick:v,ohlc:v}},l,{isStock:!0});l.series=n;return h?new E(b,l,g):new E(l,c)};J(G.prototype,"autoLabelAlign",function(a){var b=this.chart,c=this.options,b=b._labelPanes=b._labelPanes||{},d=this.options.labels;return this.chart.options.isStock&&"yAxis"===this.coll&&(c=c.top+","+c.height,!b[c]&&d.enabled)?(15===d.x&&(d.x=0),void 0===d.align&&(d.align="right"),b[c]=this,"right"):a.apply(this,[].slice.call(arguments,1))});J(G.prototype,"destroy",function(a){var b=this.chart,
c=this.options&&this.options.top+","+this.options.height;c&&b._labelPanes&&b._labelPanes[c]===this&&delete b._labelPanes[c];return a.apply(this,Array.prototype.slice.call(arguments,1))});J(G.prototype,"getPlotLinePath",function(b,c,f,h,m,p){var n=this,q=this.isLinked&&!this.series?this.linkedParent.series:this.series,t=n.chart,v=t.renderer,w=n.left,y=n.top,x,A,z,B,C=[],D=[],E,H;if("xAxis"!==n.coll&&"yAxis"!==n.coll)return b.apply(this,[].slice.call(arguments,1));D=function(a){var b="xAxis"===a?"yAxis":
"xAxis";a=n.options[b];return l(a)?[t[b][a]]:e(a)?[t.get(a)]:k(q,function(a){return a[b]})}(n.coll);g(n.isXAxis?t.yAxis:t.xAxis,function(a){if(r(a.options.id)?-1===a.options.id.indexOf("navigator"):1){var b=a.isXAxis?"yAxis":"xAxis",b=r(a.options[b])?t[b][a.options[b]]:t[b][0];n===b&&D.push(a)}});E=D.length?[]:[n.isXAxis?t.yAxis[0]:t.xAxis[0]];g(D,function(b){-1!==u(b,E)||a.find(E,function(a){return a.pos===b.pos&&a.len&&b.len})||E.push(b)});H=d(p,n.translate(c,null,null,h));l(H)&&(n.horiz?g(E,function(a){var b;
A=a.pos;B=A+a.len;x=z=Math.round(H+n.transB);if(x<w||x>w+n.width)m?x=z=Math.min(Math.max(w,x),w+n.width):b=!0;b||C.push("M",x,A,"L",z,B)}):g(E,function(a){var b;x=a.pos;z=x+a.len;A=B=Math.round(y+n.height-H);if(A<y||A>y+n.height)m?A=B=Math.min(Math.max(y,A),n.top+n.height):b=!0;b||C.push("M",x,A,"L",z,B)}));return 0<C.length?v.crispPolyLine(C,f||1):null});G.prototype.getPlotBandPath=function(a,b){b=this.getPlotLinePath(b,null,null,!0);a=this.getPlotLinePath(a,null,null,!0);var c=[],d;if(a&&b)if(a.toString()===
b.toString())c=a,c.flat=!0;else for(d=0;d<a.length;d+=6)c.push("M",a[d+1],a[d+2],"L",a[d+4],a[d+5],b[d+4],b[d+5],b[d+1],b[d+2],"z");else c=null;return c};I.prototype.crispPolyLine=function(a,b){var c;for(c=0;c<a.length;c+=6)a[c+1]===a[c+4]&&(a[c+1]=a[c+4]=Math.round(a[c+1])-b%2/2),a[c+2]===a[c+5]&&(a[c+2]=a[c+5]=Math.round(a[c+2])+b%2/2);return a};C===m&&(m.prototype.crispPolyLine=I.prototype.crispPolyLine);J(G.prototype,"hideCrosshair",function(a,b){a.call(this,b);this.crossLabel&&(this.crossLabel=
this.crossLabel.hide())});J(G.prototype,"drawCrosshair",function(a,b,c){var e,f;a.call(this,b,c);if(r(this.crosshair.label)&&this.crosshair.label.enabled&&this.cross){a=this.chart;var g=this.options.crosshair.label,k=this.horiz;e=this.opposite;f=this.left;var l=this.top,m=this.crossLabel,n,q=g.format,u="",v="inside"===this.options.tickPosition,x=!1!==this.crosshair.snap,y=0;b||(b=this.cross&&this.cross.e);n=k?"center":e?"right"===this.labelAlign?"right":"left":"left"===this.labelAlign?"left":"center";
m||(m=this.crossLabel=a.renderer.label(null,null,null,g.shape||"callout").addClass("highcharts-crosshair-label"+(this.series[0]&&" highcharts-color-"+this.series[0].colorIndex)).attr({align:g.align||n,padding:d(g.padding,8),r:d(g.borderRadius,3),zIndex:2}).add(this.labelGroup),m.attr({fill:g.backgroundColor||this.series[0]&&this.series[0].color||"#666666",stroke:g.borderColor||"","stroke-width":g.borderWidth||0}).css(p({color:"#ffffff",fontWeight:"normal",fontSize:"11px",textAlign:"center"},g.style)));
k?(n=x?c.plotX+f:b.chartX,l+=e?0:this.height):(n=e?this.width+f:0,l=x?c.plotY+l:b.chartY);q||g.formatter||(this.isDatetimeAxis&&(u="%b %d, %Y"),q="{value"+(u?":"+u:"")+"}");b=x?c[this.isXAxis?"x":"y"]:this.toValue(k?b.chartX:b.chartY);m.attr({text:q?t(q,{value:b}):g.formatter.call(this,b),x:n,y:l,visibility:"visible"});b=m.getBBox();if(k){if(v&&!e||!v&&e)l=m.y-b.height}else l=m.y-b.height/2;k?(e=f-b.x,f=f+this.width-b.x):(e="left"===this.labelAlign?f:0,f="right"===this.labelAlign?f+this.width:a.chartWidth);
m.translateX<e&&(y=e-m.translateX);m.translateX+b.width>=f&&(y=-(m.translateX+b.width-f));m.attr({x:n+y,y:l,anchorX:k?n:this.opposite?0:a.chartWidth,anchorY:k?this.opposite?a.chartHeight:0:l+b.height/2})}});b.init=function(){z.apply(this,arguments);this.setCompare(this.options.compare)};b.setCompare=function(a){this.modifyValue="value"===a||"percent"===a?function(b,c){var d=this.compareValue;if(void 0!==b&&void 0!==d)return b="value"===a?b-d:b/d*100-(100===this.options.compareBase?0:100),c&&(c.change=
b),b}:null;this.userOptions.compare=a;this.chart.hasRendered&&(this.isDirty=!0)};b.processData=function(){var a,b=-1,c,d,e,f;K.apply(this,arguments);if(this.xAxis&&this.processedYData)for(c=this.processedXData,d=this.processedYData,e=d.length,this.pointArrayMap&&(b=u("close",this.pointArrayMap),-1===b&&(b=u(this.pointValKey||"y",this.pointArrayMap))),a=0;a<e-1;a++)if(f=d[a]&&-1<b?d[a][b]:d[a],l(f)&&c[a+1]>=this.xAxis.min&&0!==f){this.compareValue=f;break}};J(b,"getExtremes",function(a){var b;a.apply(this,
[].slice.call(arguments,1));this.modifyValue&&(b=[this.modifyValue(this.dataMin),this.modifyValue(this.dataMax)],this.dataMin=B(b),this.dataMax=D(b))});G.prototype.setCompare=function(a,b){this.isXAxis||(g(this.series,function(b){b.setCompare(a)}),d(b,!0)&&this.chart.redraw())};x.prototype.tooltipFormatter=function(b){b=b.replace("{point.change}",(0<this.change?"+":"")+a.numberFormat(this.change,d(this.series.tooltipOptions.changeDecimals,2)));return y.apply(this,[b])};J(c.prototype,"render",function(a){this.chart.is3d&&
this.chart.is3d()||this.chart.polar||!this.xAxis||this.xAxis.isRadial||(!this.clipBox&&this.animate?(this.clipBox=f(this.chart.clipBox),this.clipBox.width=this.xAxis.len,this.clipBox.height=this.yAxis.len):this.chart[this.sharedClipKey]?this.chart[this.sharedClipKey].attr({width:this.xAxis.len,height:this.yAxis.len}):this.clipBox&&(this.clipBox.width=this.xAxis.len,this.clipBox.height=this.yAxis.len));a.call(this)});J(E.prototype,"getSelectedPoints",function(a){var b=a.call(this);g(this.series,function(a){a.hasGroupedData&&
(b=b.concat(v(a.points||[],function(a){return a.selected})))});return b});J(E.prototype,"update",function(a,b){"scrollbar"in b&&this.navigator&&(f(!0,this.options.scrollbar,b.scrollbar),this.navigator.update({},!1),delete b.scrollbar);return a.apply(this,Array.prototype.slice.call(arguments,1))})})(M);return M});
